---
title: "YEAR TIME_UNIT Review"
date: {{ .Date }}
subtitle: ""
tags: [
    "reviews",
    "reflections"
]
comments: true
draft: false
---

_I regularly reflect on my life - my goals and aspirations, my systems and habits, and my respective progress and diligence. I find these reflections help me to understand myself, the world around me, and how we intersect - providing a foundation from which to ponder, plan, and execute the next steps of my journey. I publish these reflections on [my blog](https://blog.hamy.xyz/tags/reflections/) as a way to hold myself accountable, stay in touch with reality, and  provide an autobiographical history of Ham. Enter: The Hamniverse._

HAMY: opening paragraph with big things I did and links to each

HAMY: work, adventure, self, projects sections with brief explanations of what happened in each

HAMY: fin