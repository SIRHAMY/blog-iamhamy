---
title: "Hamventures NYC: Week of {{ dateFormat "2006.01.02" (now.AddDate 0 0 1) }}"
date: {{ .Date }}
subtitle: "This week: weird shit to do in NYC"
tags: [
    "hamventures",
    "nyc",
    "things-to-do"
]
comments: true
---

![Hamventures NYC](https://storage.googleapis.com/iamhamy-static/blog/hamventures/hamventures-marquee.jpg)

_To get Hamventures right in your inbox, [subscribe here](/hamventures/subscribe)_. Prefer Facebook? [Like the page](https://www.facebook.com/hamventuresnyc/).

# Monday ({{ dateFormat "2006.01.02" (now.AddDate 0 0 1) }})

nothingtoseehere

# Tuesday ({{ dateFormat "2006.01.02" (now.AddDate 0 0 2) }})

nothingtoseehere

# Wednesday ({{ dateFormat "2006.01.02" (now.AddDate 0 0 3) }})

nothingtoseehere

# Thursday ({{ dateFormat "2006.01.02" (now.AddDate 0 0 4) }})

nothingtoseehere

# Friday ({{ dateFormat "2006.01.02" (now.AddDate 0 0 5) }})

nothingtoseehere

# Saturday ({{ dateFormat "2006.01.02" (now.AddDate 0 0 6) }})

nothingtoseehere

# Sunday ({{ dateFormat "2006.01.02" (now.AddDate 0 0 7) }})

nothingtoseehere