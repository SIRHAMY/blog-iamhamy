---
title: "Release notes: MONTH YEAR"
date: {{ .Date }}
subtitle: ""
tags: [
    "release-notes",
    "reflections"
]
comments: true
draft: false
---

_See all [reflections](/tags/reflections)._


HAMY: filename 2020-11-release-notes.md

HAMY: command - `hugo new posts/2021-08/2021-08-release-notes.md --kind release-notes`

# Outline

* Intro
    * In x, I....
    * My name is Hamilton and this is my x in review.
* Self
    * x/10
* Adventure
    * x/10
* Projects
    * x/10
* Work
    * x/10
* Ham
    * AVG(x/10)