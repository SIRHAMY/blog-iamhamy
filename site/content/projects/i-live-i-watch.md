---
title: "I live, I watch"
date: 2019-07-22T15:42:55-04:00
subtitle: "an exploration of the continuous information channel between humans and technology."
tags: []
comments: true
---

# what

_I live, I watch_ is an exploration of the continuous information channel between humans and technology.

# how

This particular exploration focuses on the information that our phones provide us through text recommendations as artifacts of their understanding of our past actions and current intentions. 

Each phone / ecosystem / network will behave differently based on its internal code and the information at its disposal. As such, this project aims to cast a wide net in order to encompass many of these perspectives (from the device) and in order to do so must be highly interactive with low obstacles to entry.

# participate

In order to participate, I just need a few things.

* the text - take a prompt (all prompts are below), type it out, then use autocorrect / complete to form a complete (or maybe incomplete, your choice) thought.
* your phone model
* the OS and version that your phone is running
* (optional) your name
* (optional) your instagram handle

Email to yo [at] hamy.xyz

I'll curate them into publishable chunks and release periodically.

# prompts (running list that will be updated as new prompts arrive)

Choose one. Or all. Up to you.

2. Make America {your autocorrect}

3. Can't wait to {your autocorrect}

4. Sorry, can't. I'm {your autocorrect}

5. This is why we can't {your autocorrect}

6. I love you like {your autocorrect}