---
title: "I live, I watch: responses"
date: 2019-07-22T16:42:44-04:00
subtitle: ""
tags: []
comments: true
---

Response format

* [TEXT]
    * [EDIT]
    * [PHONE MODEL]
    * [OS]
    * [NAME]
    * [INSTAHANDLE]

1. I want somebody that loves me like kanye {your autocorrect}

* I want somebody that loves me like kanye day and day one night and then I get back from the summer

2. Make America {your autocorrect}

* Make America good and you get it right away lol [smiley-moji] day tis this weekend and you have to go back and try to meet with us
    * Make America good and you get it right away lol [...]
    * Megna S.