---
title: "The Simple (, Long, Short, Boring, Probable) Path to Wealth"
date: 2021-08-26T17:20:23Z
subtitle: ""
tags: [
    'contumption',
    'finance'
]
comments: true
draft: false
---

[The Simple Path to Wealth](https://amzn.to/3sPuXdb) describes my philosophy towards personal finance and Financial Independence in a way that would've taken me many years of thought, experience, and reflection to achieve. It's the book that I'll be recommending to my connections to provides a baseline understanding of wealth in a capitalist society and some straightforward strategies to attaining it.

It's not the shortest path to wealth nor is it the most exciting book to read but this is the single best resource I've found to date for providing a comprehensive crash course in personal finance and wealth building.

Below I'll list some of the main takeaways I had from the book and provide a bit of context into how I incorporate these into my own life.

# 1. Spend less than you earn

This is the core rule of personal finance and further of any sustainable system. If you can spend less resources than you acquire then you're in surplus which means you 1) have achieved a sustainable equilibrium in your circumstances, 2) have headroom to weather disasters, and 3) have resources to invest in growth.

For personal finance, Collins offers a more specific derivation which I quite like:

> Spend less than you earn, invest the rest, avoid debt.

If you can do these three things, you've created a sustainable financial system for yourself. 

A boat metaphor:

Let's say that your financial situation can be modeled by a boat. You are in the boat and the boat is your financial system, so your financial situation is quite literally supporting you - preventing you from drowning in the sea of debt / bad outcomes due to lack of financial resources.

By spending less than you earn, you are essentially creating a boat that is not sinking. If you spend more than you earn then your boat is sinking and you need to take action proportional to that dire situation.

We'll be using the boat metaphor going forward to see if it holds water.

# 2. Aim for a 50% Savings Rate

Let's assume we now have a boat that isn't sinking because we've been able to spend less than we earn. How long can we keep that boat from sinking? 

On a very calm lake / body of water we should be able to do it indefinitely. Unfortunately life and the financial markets aren't that serene - instead it's like being in the ocean with waves and even sometimes storms and with differing cargo that may be lighter or heavier depending on your situation.

So we actually need to do better than just spending less than we earn if we really want to be able to weather these storms and keep our cargo dry to ensure we can keep our boat afloat (be these storms like market crashes or medical expenses or change in cargo like kids, partners, or elderly relatives).

A good rule of thumb is to aim for a 50% savings rate. This means that if you make $10k / year, you want to be putting $5k / year into savings and investments. _Once you've got that down, I'd suggest looking into moving from savings rate to [savings commitment rate](https://blog.hamy.xyz/posts/savings-commitment-rate-vs-savings-rate/) for more accurate measurements._

50% is a good number because it provides a lot of buffer in case something goes wrong (like a really bad storm), it ensures that you're living sustainably and within your means, and it provides a good amount of resources to invest in growth when the weather is calm. 

In my mind, if you aren't able to save 50% then you aren't living a life within your means. If you aren't living within your means, I'd recommend doing some critical accounting of where your money is going and try to get as close as you can to 50%. _I've written about some of my own [financial audits](/tags/finance) and findings here on the blog._

This may seem harsh but if you aren't doing this, you're at serious risk of:

* Sinking your boat in the face of extreme, perhaps unforeseen events
* Crippling your long-term wealth potential which is both your safety net and source of freedom (at least in a capitalist society)

# 3. Make data-backed investments

Okay we have a non-sinking ship and some buffer in our burn rate to weather unforeseen events. Now what?

Now we make that money work for us. By doing this, we can further increase our robustness towards unforeseen events and start constructing a money-making machine that will continuously work to improve our financial standing. 

In the boat metaphor, we can kind of think of this as installing a pump that moves water out of the boat - without us doing any ongoing work, it's working to keep us afloat.

The way we can do this in the real world is by making investments. Through investments, we can allocate some of our money to different causes and hopefully see that money grow. We allocate it, it does the work of growing in value.

Now no market or investment is a sure thing. But we do have some data that we can use to forecast potential futures and allow us to try and pick a better market / investment choice.

Here's some data for us:

* Over the past century, the stock market grew by about 10% / year (see: [What is the average stock market return](https://www.nerdwallet.com/article/investing/average-stock-market-return) (also this book))
* VTSAX - a low-cost index fund that tracks the US stock market - has beat out 82% of actively managed funds over that time (See: [5 Reason's Why Vanguard's VTSAX Index Fund is our top FIRE Investment](https://www.ourrichjourney.com/blog/5-reasons-why-vanguards-vtsax-index-fund-is-our-top-fire-investment) (also this book))

Considering this, it seems we have a pretty good chance of growing our wealth by a significant % each year (if we amortize over a sufficient time period to account for volatility) and we can do this better than a majority of professional financial players by simply investing in a low-cost index fund.

_For more data and arguments on why this is true, click the links above and / or search more on Financial Independence and Bogleheads - this is key to the entire movement. Even better, consider reading [The Simple Path to Wealth](https://amzn.to/3sPuXdb) for a crash course in this entire space._

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">VTSAX: 95%<br>VTIAX: 4%<br>VBTLX: 1%<br><br>My asset allocations towards <a href="https://twitter.com/hashtag/financialindependence?src=hash&amp;ref_src=twsrc%5Etfw">#financialindependence</a>, inspired by The Simple Path to Wealth (<a href="https://t.co/Ojzsk5dTgb">https://t.co/Ojzsk5dTgb</a> - affiliate)</p>&mdash; Hamilton Greene (@SIRHAMY) <a href="https://twitter.com/SIRHAMY/status/1430581745494147073?ref_src=twsrc%5Etfw">August 25, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

As of time of writing, I'm investing my assets in various index funds provided by Vanguard. I like Vanguard because of its trustworthy operating model, low cost funds, and stellar fund performance.

* VTSAX - 95% - This index tracks the total stock market and as we've seen it's outperformed the majority of professional finance players while having industry low costs. This is the workhorse of my financial strategy.
* VTIAX - 4% - This index tracks the total international stock market (i.e. non-US). I use this as a hedge for international growth and to slightly increase my geographical asset diversification (though VTSAX does give pretty good diversification by default).
* VTBLX - 1% - This index tracks the bond market. I have it as a minimal hedge against deflation. I don't really expect to need this or for it to grow much but data shows that small diversification in bonds slightly outperforms allocations holding none over the past 50 years and it makes me a bit more comfortable to have it so I hold onto it.


# 4. Financial Independence

So we have a ship that doesn't sink even in some pretty extreme situations and we're investing that surplus in some data-backed assets. The next step is to keep growing that wealth until we reach Financial Independence (and beyond).

Financial Independence is when your assets grow enough in value every year for you to support your lifestyle through that growth. Put another way, it's when you can live on just the money that your investments make.

We call this Financial Independence because it means that you no longer rely on an external source of income to support yourself - you are financially independent of an external party.

In the boat metaphor, it's as if your water pump (remember: the money coming from your investments) is so good that you don't have to worry about your boat getting water in it - it can pump it out as fast as it gets in.

This is a very interesting position to be in because it allows you to remove (or at least decrease the importance of) finances from many of your decisions - as long as those decisions don't incur additional expenses. So you could for instance stop working your high paying job and retire or maybe go work as a barista at your local coffee shop or at your favorite charity - decisions that may have been bad choices for you previously due to finances.

So how does this work?

Recall from the previous section that the stock market grows at around 10% each year. This means that if you can live on just that % each year then you've achieved financial independence.

The devil's in the details a bit here and the actual % you can withdraw from your assets each year is closer to 4% for a very good chance and 3% for an almost guaranteed chance of being able to live off your assets indefinitely (and typically grow them!). _This withdrawal rate data is based on the Trinity study which studied different withdrawal rates and asset allocations against real market conditions over 30 year periods to understand how they affected whether money was left over or not (Read more: [Wikipedia](https://en.wikipedia.org/wiki/Trinity_study), [Bogleheads](https://www.bogleheads.org/wiki/Trinity_study_update))._

So financial independence is when:

```
BurnRate / AllInvestments <= 4%
```

Because this should - theoretically - mean that you can withdraw 4% or less indefinitely.

Personally, I'm tracking my Financial Independence against a 3% withdrawal rate. In other words, I will consider myself Financially Independent when 3% of my assets is greater than or equal to my burn rate - I can live off of 3% of my assets each year. I like 3% because according to the Trinity study this has a 100% success rate of not running out of money _and_ in most cases actually grows your money. For me this is cool because it 1) provides me the independence I seek and 2) it means that my engine is so good that it actually improves itself each year.

In fact, I actually would like to hit double my FI number, allowing me to live off of 1.5% of my assets each year - giving me room for lifestyle inflation (read: hedonism) as well as more security in case of strong headwinds.

So for example, let's say that I need $30k to live each year which means a $1M financial independence number. Because we expect 4% to conservatively be able to be pulled out each year, this gives me a 1% head room between what I'm withdrawing and what I _could_ be withdrawing. This headroom then is just kept invested which means every year, my total assets are growing, which in turn means my conservative 3% of assets is growing as well!

Let's see over a few years how this looks:

* Year 0: $1,000,000, $30,000 withdraw, $40k expected value increase -> $1,010,000
* Year 1: $1,010,000, $30,300 withdraw, $40,400 expected value increase -> $1,020,100
* Year 2: $1,020,100, $30,603 withdraw, $40,804 expected value increase -> $1,030,301
* Year 3: $1,030,301, $30,909 withdraw, $41,212 expected value increase -> $1,040,604
* Year 4: $1,040,604, $31,218 withdraw, $41,624 -> $1,051,010

So we can see that with a 3% withdrawal rate and 4% expected returns, we can let our money grow while we live off it!

# 5. Live your life on your terms

This is my financial philosophy. For me, Financial Independence and the freedom it affords me is a worthwhile goal and thus it's a core factor in how I structure and live my life. _This is why I regularly track my path to Financial Independence in [my reflections](/tags/reflections)._

But this philosophy and strategy may not be for everyone. You will need to examine your own values and how this strategy aligns or maybe doesn't with your own path.

I would argue that Financial Independence should be a part of everyone's strategy. If not for the freedom it provides, for the security it implies. We will all get old and at some point you'll need to support yourself unless you plan on having very supportive offspring or for the gov't to suddenly become very altruistic and help you out. And that's if things go well! If they go poorly you could be saddled with $100ks of medical bills and having some money buffer would def come in handy then. I'm a bit more of a pessimist about this stuff so I choose to make my own future robust. I think you should consider that too.

The most common counter argument I get is that aiming for Financial Independence is optimizing for my future life by sacrificing my current one. I have definitely seen this happen - you can browse r/financialindependence to read one of these ~weekly where someone sacrifices their life satisfactions in favor of money and ultimately spirals into depression. This is a real concern, but the antidote is simple: focus on what you value and ensure that you're allocating your resources to support that life. If you do this, you'll almost always have the funds to do valuable things and still save a ton of money by not spending it on things that are not worth the value to you.

I've written many examples of these value mismatches here on the blog. Here are some easy ones I found that I see people frequently missing:

* Coffee - [Making my own cold brew is worth $16,000 over 15 years](https://blog.hamy.xyz/posts/the-value-of-making-my-own-cold-brew/)
* Food - [Cooking my own meals at home is worth $180,000 over 15 years](https://blog.hamy.xyz/posts/the-financials-of-cooking-from-home/)

In general, you don't have to sacrifice the things you value as long as you are allocating funds to the things you actually value. Moreover by even realizing this is a problem / useful exercise you often find ways to get even more bang for your buck - like making your own coffee. Very few of us do this but I think more should.

Because I don't know what the future holds, I think that aiming for Financial Independence is a great strategy to optimize my current resource allocation such that I can weather bad futures and thrive in good futures. We can never know what the future holds so the best we can do is play the game the best we can. I think this is my best play so it's the one I'm making.

Live long and prosper (the simple way).

-HAMY.OUT