---
title: "How to Disagree"
date: 2022-06-26T20:50:37Z
subtitle: ""
tags: [
    'systems'
]
comments: true
draft: false
---

"A good disagreement motivates people to reexamine their own beliefs with humility and curiosity." - Adam Grant, [How to Argue about Abortion](https://adamgrant.bulletin.com/how-to-argue-about-abortion?utm_source=pocket_mylist)

Humans contain multitudes. Many humans contain many multitudes. It follows that there will be many things that any two humans will not directly see eye to eye on. We call this a disagreement.

Here I'll try to summarize the best approach I've found to handle disagreements - large and small.

* **Recognize: A disagreement is an opportunity to learn.** We are not trying to figure out who is "right" or "wrong". In all likelihood everyone involved is wrong in some way. Instead, we should aim to truly understand the domain and one another and come out with a greater understanding than we started with - and burn as few bridges as possible in the process.
* **Foreach view: understand where the Person x View is coming from**
    * What is the View?
    * Why does this Person hold this View?
    * What personal experiences do they have that is relevant to this View?
    * Ask clarifying questions for anything you're curious / unsure about.
    * Restate your understanding of the Person x View and confirm with the Person that your understanding is correct.
* **Work together to compare and contrast each view**
    * Where do they agree?
    * Where do they disagree?
    * Verify any facts / assumptions that they reference.
    * What are their respective strengths / weaknesses?
    * Are there any open questions we still need to resolve?
    * Are there additional relevant Views out there we haven't considered?
* **Summarize the Findings**
    * Can we come to a common agreement on anything?
    * Knowing what we know now, how would we change our own Views?
    * Share out and get more perspectives on the issue.

With a bit of work put into empathizing and understanding each other and why we believe what we believe I'm hopeful we can move beyond polarizing jabs and towards productive discourse. 


# References

* [How to Argue about Abortion](https://adamgrant.bulletin.com/how-to-argue-about-abortion) - Adam Grant