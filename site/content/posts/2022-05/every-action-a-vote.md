---
title: "Every Action a Vote"
date: 2022-05-26T13:22:40Z
subtitle: ""
tags: [
    'self',
    'projects',
    'hamforgood'
]
comments: true
draft: false
---

I'm a fan of this quote from [Atomic Habits](https://blog.hamy.xyz/posts/atomic-habits-your-systems-or-your-life/):

> Every action you take is a vote for the type of person you wish to become. - [Atomic Habits](https://amzn.to/3z2tkOI)

I strongly believe [every action is a vote](https://blog.hamy.xyz/posts/what-im-doing-to-help-the-amazon/#vote) - not just for your own outcomes, but for the outcomes of the world we live in.

In this post, I'm going to lay out my argument for this phenomena and why actions as votes is likely the easiest and most effective way to change this reality.

# Life is a series of probabilities

First we need to align on an understanding of this reality before we can influence it. 

I like to think of life as a series of probabilities.

* You take an action based on the state of reality
* Reactions occur in reality

These reactions are directly tied to your actions but your action is not the only thing affecting it. There are infinite other factors with the potential to affect the reactions that occur.

Intuitively we understand this so let's walk through an example:

* Action: You decide to stand still on a street corner for 10 mins. 
* Potential Reactions:
    * You get yelled at for blocking the walkway
    * You get rained on
    * You get jostled by passerby
    * You see someone get pickpocketed

Each reaction has different probabilities depending on the action you take and the state of reality at the time you take it.

# We are all connected

So now that we understand the core probabilities part, we can talk about how this phenomena works at scale. 

In the above example, we focused in on an isolated example. The thing is, we are never really taking actions (and causing reactions) in isolation. In truth, we are all perceiving the current state of reality, taking actions on it, and experiencing the corresponding reactions in concert.

I like to model this kind of like a graph of connected nodes. Our closeness to other nodes (and thus the impact of the corresponding reactions) is directly correlated to our closeness to that entity in reality.

This closeness could be due to many things like:

* Proximity
    * Location - In the same building, street, town
    * Relevance - Happens to a connection we are close to - family, friend, organization
* Value / Meaningfulness - A reaction occurs in something we hold deep meaning in
* Dependency - You depend on x for some core need / want

If we start to think of it this way, it's easy to see how reactions in one part of the graph could cause cascading reactions throughout the rest of it. Note: This is kind of like the [Butterfly Effect](https://en.wikipedia.org/wiki/Butterfly_effect).

Let's consider a contrived example:

* State: A new vegetarian restaurant opens up in your town.
* Action: You decide to try it out.
    * Reaction: You eat the food, it's good but the service is poor.
        * Action A: Leave a 4+ star review, noting that service needs to be improved.
            * Reaction: The restaurant takes note as they're trying to improve and they make changes to fix service.
            * Action: Typical GMaps customers only look for 4+ star places and decide to give this a try, noting that the restaurant was gracious and said they'd try to fix stuff.
                * Reactions:
                    * These people like the food and the restaurant starts gaining local fame for good, vegetarian food
                    * New entrepreneurs take note of this success and invest money to make more vegetarian food places take root in the town
        * Action B: Leave a 3- star review, saying the restaurant is not worth trying.
            * Reaction: The restaurant takes note as they're trying to improve and they make changes to fix service.
            * Action: Typical GMaps customers only look for 4+ star places, so fewer people decide to give it a try - even though the restaurant is gracious and said they'd try to fix stuff.
                * Reactions:
                    * The people that go still like the food and tell their friends
                    * But it takes a lot longer for it to hit critical mass / for others to notice success
                    * Eventually new entrepreneurs may take note of the opportunity and capitalize - or maybe there's a new trend in town they jump on

# Each action an influence

So every action has the ability to influence our graph's direct neighbors and to ripple far out into our indirect neighbors. The truth is, it's hard to know for sure what the full influence of a given action will be.

In the above contrived example, there are infinite alternate outcomes in this reality based on an infinite array of contributing factors:

* The restaurant succeeds regardless of what you do
* A shooting occurs outside, marking the neighborhood as dangerous, and thus every business on the street falls under hard times
* An Earth-friendly tax on beef is put in place which increases the opportunity of non-meat restaurants

But we should still try to estimate the impact of our actions and take action accordingly. This is because it's often not our individual action that counts, but the impact that action may have at scale via its direct and indirect reactions. 

This mental model is very similar to the one we must take when we vote in democratic elections:

* The expected direct impact of the vote outcome is large - whoever gets the most votes wins the ability to take large impact actions
* The expected direct impact of our individual vote is small - our vote = 1
* The expected indirect impact of lobbying, canvasing, etc is large - you can influence the votes of 10 / 100 / 1000+
    * (and this is why almost [$8 billion was spent on US election campaigns in 2020](https://www.fec.gov/data/spending-bythenumbers/))

Our vote may only be worth 1. But this action (coupled with sharing our beliefs with others) could influence 5 of our closest friends. If they in turn do the same thing, we've now indirectly affected 25 nodes in the graph.

# Actions to take

I believe this interconnected graph of actions and reactions is the basis of our reality. When we start to think of it this way, an array of possible ways to influence reality reveals itself. Perhaps overwhelmingly so.

The problem is:

* Not every action is highly impactful
* Not every action will have the reactions you hope
* Every action has a corresponding cost while we have finite resources

Over the years, I've come up with a few guiding principles to help narrow this opportunity space and place more effective, cost efficient votes for the reality I want to live in.

The first is a directional principle to decide if this is a good action to take:

> Ask yourself: Are you helping or are you hurting?

Sometimes the actions we take - even those with good intentions - are actually antithetical to the world we want to see. Consider the direct and indirect reactions your actions may cause and ensure they're at least moving the world in a direction you want to see - not the opposite.

Next are a common set of ways to vote with your actions and some examples of each:

* **Time** - Each action you take will require some amount of time - some more, some less. Using our time, we can take actions ourselves to influence the world around us.
    * Vote -> Affect local and national seats of power and the actions they promise to take
    * Leave reviews -> Affect the future money outlook of businesses / organizations
    * Volunteer -> Improve the impact of your chosen cause / organization
* **Money** - In a capitalist society, money is a proxy for value and thus the lifeblood of any entity. Voting with your money fuels the entity you give to and all its dependencies. Moreover, it signals to other entities that there is opportunity in mimicing the supported entity, thus creating a higher probability additional entities will follow suit.
    * Support entities you like -> This will help them stay afloat and encourage mimicry
    * Donate to entities you believe in -> This will help them continue to impact the world

Over time, you'll likely find that you have unfair advantages in some areas and thus that you are uniquely positioned to be more effective with those kinds of votes. Leaning into that can further increase your impact without increasing your costs.

As an example, I currently believe my unfair advantages lie in:

* Building tech products

I came to this conclusion after a decade of experimentation to determine:

* What I enjoy doing
* What I'm good at
* What people value

As such, I've built my life to leverage these advantages to increase the impact of my votes on the world. 

* [Time] I build tech products with [HAMY.LABS](https://labs.hamy.xyz/)
* [Money] I vote for organizations doing good work [HamForGood](https://labs.hamy.xyz/hamforgood/)

Remember kids:

> The power is yours! - Captain Planet

Vote with your actions -> build a better world.