---
title: "Release notes: May 2022"
date: 2022-05-30T17:02:42Z
subtitle: ""
tags: [
    "release-notes",
    "reflections"
]
comments: true
draft: false
---

_See all [reflections](/tags/reflections)._

May was a month of deep Reflection for me. It's the first time this year I've really done that and follows a long period of unmindfulness.

Ultimately I learned a lot about myself and have refactored many of my systems to better align my goals, domains, and values.

# Reflect

In the first 5 months of the year, I've embarked on [8+ large adventures](https://blog.hamy.xyz/posts/2022-04-release-notes/#adventure). This proved to be unsustainable for me and has triggered something of a pendulum swing towards sustainability in the past month.

The first large change is a realignment of my North Star, Domains, and Values. My North Star has long been to "Lead a happy, healthy, and impactful life." but I've come to realize that I haven't done the best of supporting that mission with the Domains and efforts I've built.

I've decided to attempt a refactor of all my Domains from being production-focused (what do these things produce) to outcome focused (what is the impact of these things). This aligns my personal domains better with my North Star and Values and also aligns my efforts with my newly-formalized [Creation Cycle](https://labs.hamy.xyz/posts/the-creation-cycle/).

Ultimately we are all a work-in-progress so I hypothesize applying a system of continuous improvement is as good a fit in this domain as it is in any other.

Though these aren't totally accurate translations, for the most part the refactor is:

* Adventure -> Observe
* Projects -> Create
* Self -> Reflect
* Share (new)

Some concrete outcomes from this realignment are:

* Focusing on my health - you cannot pour from an empty cup
    * More sleep
    * Less coffee / alcohol
    * More journaling / chores
* Dedicated time to balance my Creations (Career + Business)
    * At the end of the day, I'm tired of not doing the work I want to do / living the life I want to live

# Observe

Perhaps the largest thing I learned this past year (and that was strongly reinforced this month) was the importance of experiencing and understanding the world around me. This is a core component of any effective [Creation Cycle](https://labs.hamy.xyz/posts/the-creation-cycle/) and it became clear that I wasn't doing this when I reflected on myself from this perspective.

When I attempted to change my previous Domains to support a higher priority for this one it broke a lot of my systems and mental models. I took that as a good signal that my previous models weren't effectively modeling the world and therefore that it was a good time to change them.

# Create

The higher priority of Observe and realignment of all my Domains has led to some interesting insights.

* Better understanding of the world + better understanding of my goals -> Better Creation Cycles
* Better Problems (Observe) -> Better Businesses (Create)
* Career != Work -> Work is a way to make a Career but a Career is independent and not tied to a "job", rather it tracks your life's impact / legacy

# Share

The more I thought about it, the more I realized that any system requires both inputs and outputs. Sharing is the ultimate output - no matter what you create, you must eventually share it for it to leave your closed system / bubble.

This reorganization led me to further realize what I'd always kinda felt which is that Shares are not a Create priority for me. They are a means to give my Creations to the world - not really the Creation itself. This is not to say they are not important - I think in some ways this makes their true value that much easier to see / understand. Of course this clarity leads to more efficient investments.

Some changes here;

* [HAMY.LABS](https://labs.hamy.xyz/) will now be focusing on "Building better tech products" - Business / Product / Technology
* Art is now a Share rather than a Create which makes it more clear what its value is to me and extends the [refactors I made in April](https://blog.hamy.xyz/posts/2022-04-release-notes/#shares)
