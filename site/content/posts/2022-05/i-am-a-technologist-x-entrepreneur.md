---
title: "Technologist / Entrepreneur"
date: 2022-05-21T13:30:24Z
subtitle: ""
tags: [
    'technology',
    'business',
    'projects'
]
comments: true
draft: false
---

Over the past few months - and regularly every few months in [Reflections](https://blog.hamy.xyz/tags/reflections/) - I've been considering what impact I want to have on the world. I've had a deep-seeded feeling of misalignment - where my actions didn't really feel like they were progressing me where I wanted to go.

After much self reflection, experimentation, and discussions with friends I ultimately decided that the impact I want to have on the world is focused on the Businesses I create and the Technology I use to create them.

I think Business and Technology is a good fit for me because:

* It's impactful
* I'm interested in it

I think it may be a > good fit for me due to some unfair advantages:

* I have years of experience [building projects](https://labs.hamy.xyz/) (albeit 0 successful businesses)
* I am skilled at technology creation (at least that's what [LinkedIn](https://www.linkedin.com/in/hamiltongreene/) says)
* I really enjoy the creation process

Realistically this means very few changes to my Life Systems. Business and Technology have been core creation domains for me for several years. But it does mean treating them more seriously - [more like a Business, less like a hobby](https://blog.hamy.xyz/posts/2022-04-release-notes/#shares) - via higher prioritizations for my time, effort, and resources and higher emphasis on impact, strategy, and scale.

Perhaps the most visible change will be my [downgrading of Art from a 'business' to a hobby](https://blog.hamy.xyz/posts/2022-04-release-notes/#art). I decided that Art was something I enjoyed but not something I really saw as my primary impact on the world. Moreover I discovered that treating it seriously removed some of the inherent joy I got from it. Thus deprioritization will hypothetically lead to greater satisfaction in this domain while freeing up cycles to invest elsewhere.

One area I'm excited to invest these recouped cycles in is community. NY is a hotspot for tech and entrepreneurship in the US and it's nigh time I took advantage of that.

# Moving Forward

* More Businesses launched (#founder #bootstrapped)
* More Shares around Business and Technology
* More community engagement

I post most of my Projects (Business + Technology) updates on [HAMY.LABS](https://labs.hamy.xyz/).