---
title: "Life's Balance"
date: 2022-05-16T04:35:58Z
subtitle: ""
tags: []
comments: true
draft: false
---

Life is too short to take it:

* Too seriously
* Too lightly

On the one hand, you might as well enjoy it. [There is no inherent meaning or purpose to life](https://en.wikipedia.org/wiki/Nihilism). So it's up to you to create your own and you might as well create one you'll enjoy.

On the other hand, humans enjoy accomplishing things. Like it's literally built into us in the form of [dopamine hits](https://www.mindbodygreen.com/0-23924/the-brain-chemicals-that-make-you-happy-and-how-to-trigger-them.html). So in most cases we are most satisfied when we feel we are making progress towards some greater cause.

Thus to lead a satisfying life, we must take it:

* Light enough to enjoy
* Serious enough to be satisfied

Finding the balance may be a life's works.