---
title: "Weekly Journal - 2021.07.18 - Too much of anything can be a bad thing"
date: 2021-07-18T15:13:02Z
subtitle: ""
tags: [
    "journals"
]
comments: true
draft: false
---

# What went well?

Generally was back on my systems (more so than I've been all year) and it felt really good. I made progress towards my goals, I felt better than I have in a long time, and things generally seemed manageable - I approached a calm, healthy, accepting mindset that I've missed for quite some time.

* Started working out again
* Hung out with people
* Started building new Monoliths
    * Refactored monolith systems and realized I was doing them wrong -> new [Monoliths mission](https://labs.hamy.xyz/projects/monoliths/)

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">After about 6 months of <a href="https://twitter.com/hashtag/monoliths?src=hash&amp;ref_src=twsrc%5Etfw">#monoliths</a>, I realized I&#39;ve been doing them wrong.<br><br>System of Autonomy (0000): <a href="https://t.co/5vZFX4ck8c">https://t.co/5vZFX4ck8c</a></p>&mdash; Hamilton Greene (@SIRHAMY) <a href="https://twitter.com/SIRHAMY/status/1416448617272029186?ref_src=twsrc%5Etfw">July 17, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

* Was back in the office this week which was good for many reasons -> got me out of the house, connected with lot of coworkers, food and coffee, forced me to do a good bit of walking (~50 mins a day!)
* Improved my YouTube systems and released 2 new videos this week (see [HAMY LABS YouTube](https://www.youtube.com/channel/UCPBY44jxP7gOMkUsP5rlGnw))

# What went poorly?

* Fell into suboptimal ham patterns for 2 days this week
    * Sad, irritated, depressing, etc.
    * Think this usually happens when I don't fulfill all of my core satisfactions, thus am unbalanced, and begin to get anxious / feel dissatisfied
        * This week this was in the form of 1) not working out and 2) not doing my chores / letting a lot of necessary tasks pile up
* This was probably my most productive week in projects this year (not saying much cause 2021 H1 was pretty unproductive)
    * However, this also got me in trouble a bit as I was focusing on so much projects that I neglected other necessary life operations (see above). Too much of anything can be a bad thing.

# What should I change?

* Focus on doing the most important things each day first, drop nonessential things
    * Forces progress without necessarily becoming busy
    * Reduces tasks in view to just the most important, lessens probability of becoming overwhelmed
* Mark core satisfactions as 'important' and do all of them each day
    * It's unsustainable to overinvest in one satisfaction (like work / projects) at the expense of other satisfactions (like health, fun, connection, etc).
* Set aside dedicated time to do chores / catch up on necessary tasks
    * I've been behind on my system execution for several months now. I've had dedicated time on my calendar but never really used it. I'm setting Sunday mornings as dedicated time to ensure I focus on it as the most important thing and increase likelihood of completing

# Releases

* [2021 H1 Review](https://blog.hamy.xyz/posts/2021-h1-review/) is out!
* Update [Monoliths Focus](https://labs.hamy.xyz/projects/monoliths/)
* Released 2 new videos on [my YouTube channel](https://www.youtube.com/channel/UCPBY44jxP7gOMkUsP5rlGnw), exploring scripting visuals in Python PIL and how I built [Labeled Intentions](https://labs.hamy.xyz/projects/labeled-intentions/)

# This week

* New Monoliths dropping on [@hamy.art](https://www.instagram.com/hamy.art/) this week
* Building a new Monolith System
* Exploring NYC (hoping for good weather!)
