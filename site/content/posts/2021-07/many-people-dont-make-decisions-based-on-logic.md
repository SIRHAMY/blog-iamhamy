---
title: "People often don't make decisions based on logic"
date: 2021-07-23T02:01:17Z
subtitle: ""
tags: [
    'misinformation'
]
comments: true
draft: false
---

# Prompt

An FB post against vaccinations:

> "If it worked and was safe, you wouldn't need torun huge ad campaigns, mandate it, or offer incentives."

> "If it worked and was safe, you wouldn't need to block, censor, and threaten those who question it."

I don't always respond to FB posts but this one was triggering because it seemed like a core philosophical fallacy.

# Response

Unfortunately I think there are a lot of things that are good preventive measures (work) and are safe but still require constant enforcement, regulations, and incentives for people to actually do them - and _still_ people don't do them.

* Wear a seat belt - enforced by law, signs all over highways and roadways
* Contraception - Taught about in many schools, given away for free by many organizations 
* Don't drink and drive - enforced by law, signs on highways and sporting events
* Basic driving laws (stopping at red lights, driving on the right side of the road, driving the speed limit, etc) - enforced by law, regulated via drivers licenses, etc
* Eating and exercising healthily - Taught in schools, multiple articles published each week on the subject, almost every municipality has organizations and programs supporting this

For whatever reason I think many people refuse to make / change their mind even when faced with overwhelming evidence one way or another. Getting them to change their behaviors is even harder. I tend to think it says more about the individual who disregards the measure than it does about the efficacy of the measure itself.

An extreme but rather poignant example of holding onto beliefs and behaviors despite overwhelming evidence to the contrary: https://physicsworld.com/a/fighting-flat-earth-theory/