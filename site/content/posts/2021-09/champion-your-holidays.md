---
title: "Champion Your Holidays"
date: 2021-09-10T14:14:49Z
subtitle: ""
tags: [
    'holidays',
    'hamforgood',
    'self'
]
comments: true
draft: false
---

Holidays exist to commemorate a concept. This might be an impactful person, a key event, and / or a concept that's moving to many people. It's a regular reminder of things that we collectively think are important.

# Why holidays fail

Over time, these holidays tend to lose their potency - most commonly by society coopting them to its modern day conveniences and by low-affinity actions taken in the name of observance.

Change is the natural progression of life but we start to see a problem when we become removed from a holiday's original purpose and meaning. When we no longer channel a holiday's core, we move from productivity to busyness and ultimately to impotency of the original idea.

* Christmas -> A shopping day, benefitting large retailers
* Channukah -> A small holiday risen to a big one to parallel Christmas, benefitting large retailers
* Cinco de Mayo -> Now a drinking day, benefitting alcohol retailers
* Thanksgiving -> Once about cooperation between peoples, now the largest shopping weekend in the world, benefitting large retailers

A common tactic to prevent loss of the original meaning and purpose of a holiday is through strict observance of rituals and customs. These might come in the form of parades, religious services, special foods and activites, etc.
This tactic serves to cement the idea of importance - we remember these special customs we take for this specific day - but doesn't do so much to cement its purpose or encourage real action to be taken.

In the end, holidays lose potency over time. Even with strict observance of holidays, we end up with:

* Best case: Busy work in performative observation acts, very little productive actions taken
* Worst case: No observance, disregard purpose of holiday, take completely irrelevant actions -> often benefitting large companies / powers who have coopted the holiday

# Champion your holidays

I don't think that this process is inevitable - sure things will change with time but we don't need to lose touch with the purpose of the holidays nor miss the opportunity to do some real good with them. Here I'll propose a simple action plan to take back control of your holidays and use them as fonts for good.

Action plan:

* Observe the holiday - We must first understand the holiday - its history, purpose, and context. This is the first step to effectively championing it. 
    * Research the holiday and its interpretations
    * Discuss it and your interpretations with others
    * Reflect on how this relates to your own life
* Take action in its spirit - Based on your understanding of the holiday, now do something in its spirit.
    * Find modern examples of the issue / theme in the world
    * Find people and organizations working to change the world for the better
    * Work to support these efforts - give money, give time, give effort, etc.
    
I'd estimate that this would take max of about 2 hours of time each holiday to do this. I'd also estimate that by doing this for 2 hours each holiday, you'll end up in the top 95% of all holiday observers wrt good impact on the world.