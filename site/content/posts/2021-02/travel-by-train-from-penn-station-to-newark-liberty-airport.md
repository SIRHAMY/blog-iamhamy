---
title: "How To: Travel by train from Penn Station to Newark Liberty Airport"
date: 2021-02-18T16:36:19Z
subtitle: ""
tags: [
    'train',
    'travel',
    'finance'
]
comments: true
---

I took the train from Penn Station to Newark International Airport this morning and was confused / stressed in the process. So I'm writing this guide to help my future self and fellow travelers navigate this experience a little easier.

# Why Train

First off, wanted to set down my thoughts on Train vs Bus vs Uber:

Train

* 90 mins Kips Bay to terminal (no variation due to traffic)
* $15.75 (as of 2021.02.18)

Bus

* 90 mins Kips Bay to terminal (variation due to traffic)
* $10 (I think?)

Uber

* 45 mins Kips Bay to terminal (variation due to traffic)
* $90+

I think training is the best option if you can plan a bit ahead as it's relatively low cost and minimizes time variation (I've once had a 2 hour Uber on my way to EWR for an international flight - not fun). If you're short on time and not going in rush hour then I think Uber can make sense sometimes, but I'd generally boil that down to laziness on the traveler's part rather than any kind of necessity.

# How to take the train

I found the process of getting tickets to and finding the correct train a bit confusing so I'm doing a little brain dump about it.

* First, get to Penn Station (use Google). You should probably be able to walk / ride MTA to get there.
* Once there, purchase a ticket to ride a train to the airport.
    * You can identify an airport-bound train by looking at the train arrival board and finding the trains with an airplane symbol next to them. Take note of the soonest arrival / one you want to ride and what train line is running it (i.e. NJ Transit vs Amtrak).
    * Now go to the kiosk for the train line running your chosen train and buy a ticket with destination 'Newark Airport' (or some variation of that). This ticket will have both your fare for the train to get there and the transfer fee to enter the airport ($15.75 when I went)
* Now wait for the train you chose to have a track assigned to it and make your way to the track to board your train.
* Ride the train to Newark and transfer on over to the Airtrain!
* You did it! Rub some hand sanitizer on your hands and pat yourself on the back.