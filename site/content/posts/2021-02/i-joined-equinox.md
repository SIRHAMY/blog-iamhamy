---
title: "I Joined Equinox"
date: 2021-02-18T16:55:34Z
subtitle: ""
tags: [
    'health',
    'fitness',
    'finance'
]
comments: true
---

Last week I joined Equinox. This may come as a surprise to some as [I've previously criticized the institution for its price](https://blog.hamy.xyz/posts/the-value-of-choosing-nysc-over-equinox/). Today I wanted to log my reasoning behind the decision.

For context, I've been a long-time member of New York Sports Club (going on 3 years now). It was relatively cheap - $50 / month all access, $80 / month for flagship access, had all of the equipment I needed for my workouts, and had many locations throughout the city. In short, it fulfilled my needs for a reasonable cost.

However, in COVID its utility for me started to decline. Cleaning was light, the spaces were cramped, and several locations began to close / be sold off. This had the effect of preventing me from going to the gym (for fear of safety and inability to get equipment I wanted) and reducing the usefulness of my membership now and even after COVID all while costing me money.

I value exercise highly - I goal on it each half. It makes me feel good, it gives me more energy, and it theoretically makes me healthier.

So I decided to look at Equinox to see if it would be a better fit. From my research, I found it to be more expensive ([costing an additional $50,893 / 15 years](https://blog.hamy.xyz/posts/the-value-of-choosing-nysc-over-equinox/)), much cleaner, and less cramped. I figured this would reduce my savings rate, make me feel more comfortable going to the gym, and encourage me to get back into lifting.

To me, my health and happiness is worth the extra expense. So I joined. I'll reevaluate my membership in a year once my contract is up. For now, I think this will lead to a happier, healthier Ham.

Interested in joining Equinox? Drop me a line with any questions you may have and [use my referral code](https://www.equinox.com/referrals/5138768?mrfcid=web_direct_150giftcard) so we can both get discounts on our membership!

[Click my Equinox referral link to get started](https://www.equinox.com/referrals/5138768?mrfcid=web_direct_150giftcard)