# why I put ice in my coffee

If my coffee doesn't come cold / with ice in it, then I add ice myself. Some people find this weird. I find it practical. This is why.

There's really no difference between the substance of hot and cold coffee. Hot is hot, cold is cold, and eventually (if left to their own devices) both will meet in the middle as lukewarm coffee.

There is a difference, however, in how you go about drinking coffees of different temperatures. With cold coffee, you just go for it. With hot, you wait til it cools then drink it.

Thus hot coffee requires an extra step for consumption and is therefore the less efficient variant of the substance. To fix, add ice.