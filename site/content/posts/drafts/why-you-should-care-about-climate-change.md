---
title: "Why You Should Care About Climate Change"
date: 2019-07-16T22:04:49-04:00
subtitle: ""
tags: []
comments: true
draft: true
---

This year, I've been implementing things that I think have the potential to make the world a better place. One of the primary issues I've been focused on is climate change. I've had a few questions about why climate change is such a big deal to me, so for the sake of not repeating myself, here's why you, too, should care about climate change.

Climate change is an important actor of change in our lifetime and a particularly important one to focus on in the next few years. It effects, either directly or indirectly, every single person on the planet, the magnitude of these effects are at least the destruction of habitats and up to the destruction of whole ecosystems, the ability to reverse such effects are nearly non-existent, and the point of no-return is just a decade away with the work required to be completed in that time insurmountable by any single nation-state.

This is why climate change is so important.

# It affects everyone

Everybody's got weather and thus everyone will be affected. All the medias say that we'll continue to get worse and worse weather like storms and stuff.

But I think the worst part is really the simple rise and temperature. This changes the map wrt where plants and animals can reasonably survive and it's happened so fast that the typical Darwinian behavior movements haven't been able to keep up. What this means is that we'll likely see drastic reductions in plant and animal life just cause they couldn't migrate in time and those that do will be faced with the further issues of settling into a strange new land, likely leading to even further reductions in their numbers before finally becoming stable.

# got updates?

Did you like hearing me write? Would you like to read more about what I'm thinking?

[Subscribe](https://hamy.xyz/subscribe) to get periodic email blasts about what I'm doing, when I'm doing them!