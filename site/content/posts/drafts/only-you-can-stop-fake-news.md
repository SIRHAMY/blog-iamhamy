---
title: "Only you can prevent fake news"
date: 2020-12-12T14:10:11Z
subtitle: ""
tags: []
comments: true
draft: true
---

# Outline

* Disclaimer - I work at IG, views are my own, I build media-serving infrastructure so don't have any particular insight into the issues laid out here
* What is fake news
    * Origins
    * Definition
* Preventing fake news
    * Disclaimer2 
        * No expert in fake news dept.
    * 2020 has been a tumultuous year
        * All sorts of beliefs, accusations, evidence being shared
            * social media
            * media networks
            * news outlets
            * governments
            * friends and family
        * Many of them deemed fake news
            * Many of them not
        * I'm not here to debate censorship / regulation / etc.
            * Pointing out that
                * Many sources of information
                * Each with own ways to 
                    * consume
                    * vet
                    * and share info
                * Many are unhappy with current information regulation
    * I was frustrated with how much I felt the need to fact check
        * Largely my fault and due to my pedanticism but that's for another time
        * Pondered the state of the world and systems that could prevent this
            * Q1: How can I prevent the spread of fake news?
            * Q2: How can I help others prevent the spread of fake news?
    * technological innovation
        * Where we can make most progress in future
            * Works faster, harder, and more widespread than humans can
        * independent fact checkers with ability to tag info around world within seconds seems great
            * AI to 
        * But we're not there yet
            * Part of that has to do with nature of the problem
            * We may never really 'be there'
    * The problem
        * Analogies can help us understand a problem
            * Help us try on existing strategies to fix
        * Analogy 1 - Forest Fires
            * Problem similarities
                * Spreads by itself
                * Often spreads by a few careless acts
        * Analogy 2 - Coronavirus
            * Problem similarities
                * Spreads easily
                * Spreads from person to person
                * Often unsure if you have it
                * Can take awhile to test for
    * System of prevention
        * Understand work
            * Take 2 minutes to think about 
                * why you're posting
                * what you hope to get out of it
                * what your point is
                * what supporting evidence you have
            * Seek out 2 news sources on the subject from different backgrounds
                * Select from sources rated highly factual
                * Pick one that you like
                * Pick one with a different perspective
                    * leans with a different bias
                    * based out of another country
            * Re-read and consider holes in your argument
                * Investigate those holes
        * Everything checks out?
            * Post
                * You've done the 20% of work to weed out 80% of fake news
                * Because you provided supporting links, people will be able to notify you if information changes
                * We can all come up with perspectives based on a shared understanding of what fact is.