---
title: "System: Choosing a News Source"
date: 2021-01-09T15:00:20Z
subtitle: ""
tags: [
    'systems',
    'news',
    'fake-news'
]
comments: true
---

Fake News - news that is portrayed as factual but is actually nonfactual - has been a trending phrase throughout the Trump presidency. In 2020, and now 2021, the prevalence and power of Fake News has grown - convincing millions of United States citizens that the 2020 presidential election was fraudulent, [despite no evidence of widespread fraud](https://apnews.com/article/ap-fact-check-donald-trump-a98d72c0ccde16fa900e6053a4599cab).

Over the past year, I've had many discussions, mostly on social media platforms, about fake news, how it spreads, and methodologies to prevent it. A recent discussion was centered around where people got their news and how they chose those sources of information. In this post I'll share my thoughts on the news ecosystem, why we need to move from Fake News to Biased News, and my strategy for finding good sources of news.

# Fake News -> Biased News

Fake News is a huge deal. The propagation of misinformation is a dangerous act and should be stopped. But I think there are a lot of people already writing and doing things about this so here I'll just leave a simple rule to prevent fake news and move on.

> How you can stop Fake News: Take 5 minutes to find and read 2 reputable news sources that corroborate your information before posting or sharing something. Add links to those sources in your post / share.

With so much talk about Fake News and things to avoid, I thought it'd make sense to focus this post more on what you should be striving for.

This is where Biased News comes in. I think all news is inherently biased in one way or another. This is because all humans are inherently biased. However, I also think some news sources do a better job of containing that bias than others. 

So instead of trying to find 'Unbiased News', I think we need to just find Biased News and understand the biases they're writing from. I capture this in my news values - the values by which I rate my news sources:

**My News Values:**

* Factual and reliable reporting
* Multiple perspectives presented
* High local and global coverage
* Easy to read - highly accessible and good technology

# The Perfect News Source

Based on my belief that all news sources are inherently biased, I actually believe that there is no perfect news source - in the same way that I believe there's no perfect soul mate or notetaking app or, really, anything. But I do think there are a lot of close approximations and we can get an even closer approximation by combining several together to get a symbiosis of their attributes.

## Creating the Perfect News Source

So I think everyone's different in what they're looking for in a news source but I also think that my values are pretty generalized. We want factual and reliable reporting because we don't want to be lied to or misled. We want multiple perspectives presented because we want to be able to separate what's fact from what's interpretation and ensure we're seeing all angles of an event or story. We want high local and global coverage because we want to see the news that's important to us and that may have an impact on our lives. We want it to be easy to read because it's 2021 and we don't have time to deal with bad technology or experiences, nor should we continue to support services that do that.

So I'll continue on with these values but feel free to substitute them for your own.

We need to understand the news landscape before we can choose our news sources. We'll use our values as a framework to rate and compare each.

**Step 1: Identify factual sources**

For 'Factual and reliable reporting' and 'Multiple perspectives presented' I use the latest [Media Bias Chart by Ad Fontes Media](https://www.adfontesmedia.com/). The chart itself is a bit biased but I think it's a pretty good approximation and it's extremely easy to parse.

From this chart, I'll only consider sources in the 'Most Reliable' bucket. Everything else is too iffy to rely on for consistent factual reporting and I don't want to be lied to.

**Step 2: Rate coverage**

Everyone's going to have different things they want covered - whether it be a specific geographic region like North America or society area like Economics. Whatever you're into / are looking for coverage on, you should make sure that the news sources you're picking cover it sufficiently.

For me, I'm interested in getting news on local and global issues. I don't necessarily need a specific area covered in-depth but I do want it mentioned because I can go do my own independent research on it - but only if I know that thing exists.

For this, I'll just go to each news site and check out what kind of reporting they're doing, how much of it, and how often in my areas of interest. 

**Step 3: Rate accessibility**

Accessibility is a huge one for me though it may not be for others. Basically I want to get the news in a timely manner, be able to consume it wherever I am regardless of what I'm doing, and I want the reading experience to be good. 

So for me, I want a clean website design with no distractions, a fast website with mobile apps and email notificatiosn for the things I'm interested in, as well as concise writing with lots of linked sources. To rate this, I'll again visit the websites and give their apps and email subscriptions a try.

This one's a bit of a pain to get through and pretty time consuming but I think it pays off over time.

**Step 4: Multiple perspectives**

In the last step, we ensure we're getting multiple perspectives. I'd say this is second most important after picking a factual news source. We've limited lies but now we need to control for perspectives and biased coverage.

I've already mentioned that I don't believe there exists a perfect news source. So we're not rating these sources against each other in this bucket - rather we're going to be using this rating to help us build a 'balanced' portfolio of sources.

For this step, we'll take a look back at the Media Bias Chart from Step 1 and take note of the biases associated with the top news sources we've selected, by ranking. Then you should choose a couple (2 - 3) that balance each other out.

So you might have a left-leaning outlet like NPR and balance it out with a right-leaning outlet like Bloomberg and / or Wall Street Journal. Once you have those chosen, make it a habit to make them your regular sources of news.

# My sources

I went through this system last year and settled on The New York Times and Reuters as my two primary sources. I still read news from other outlets but will almost always refer back to these sources to cross check information. If I feel like I need another opinion, I'll fall back to Associated Press reporting.

Together, they provide good double coverage from different perspectives - a left-leaning and neutral one - and are rated among the most reliable in the industry. 

The New York Times and my values:

* Factual and reliable reporting: Mostly reliable
* Multiple perspectives presented: skews left
* High local and global coverage: High coverage around the world with timely reporting and lots of analyses on different areas if I want to dive deeper.
* Easy to read - highly accessible and good technology: Great ['Morning' newsletter](https://www.nytimes.com/series/us-morning-briefing) and various domain-specific newsletters to follow, good apps and sites. Good technology - I follow their [Open Tech blog](https://open.nytimes.com/) as well - usually a good read on what they're building and how they built it.

I like the New York Times so much I subscribed, supporting them as part of my [HamForGood - Personal](https://blog.hamy.xyz/pages/systems/hamforgood/) 'Freedom of Information' cause.

Reuters and my values:

* Factual and reliable reporting: Highly reliable, up there with Associated Press
* Multiple perspectives presented: Neutral, drifts left and right year to year
* High local and global coverage: Good coverage across many different interest areas
* Easy to read - highly accessible and good technology: Site is kinda funky but good for diving into a single topic or area

# Fin

Let me know what news sources you use and how you came to that decision. I'm always looking for ways to improve my systems and get better, more reliable news.

In Fact and Truth.

-HAMY.OUT