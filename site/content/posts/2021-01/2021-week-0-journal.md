---
title: "2021 Week 0 Journal - 2021"
date: 2021-01-03T15:03:21Z
subtitle: ""
tags: [
    "journals"
]
comments: true
draft: false
---

# Top of mind

2021 is upon us. A new year with new hopes, dreams, and goals. I spent New Year's in a NYC apartment with Megna and her family. Since 2021 began, we've taken the long weekend to visit city museums, binge tv, and generally enjoy each other's company.

2020 was a long year and I spent most of December reflecting on my year and planning for 2021. These past few days have been a much wanted respite from my work. 

But 2021 is here. That means the clock is ticking on my goals and opportunities are out there to be leveraged. So it's time for me to get back into my systems and make 2021 the best year yet.

# Releases

* [2020 review](/posts/2020-review/) - I released my yearly review. Take a gander.

# Stresses

This week I've found myself stressed about my goals and systems. The usual existential questions have been plaguing me:

_Have I goaled enough? Did I goal too much? Can I do this much work? Am I pushing myself too hard?_

Over the years I've built up a tolerance for this existential dread. Logically, there's no way to know the answer to any of these questions until I try. I've put many, many hours of thought and reflection into these systems. They're the best systems I've been able to construct so far. So the best course of action is to put in the work and see what happens.

I have really high hopes for 2021. I feel like I've achieved the highest level of clarity over the current state of my life, the ideal state of my life, and the steps that lie between that I ever have. This is both inspiring and scary. I know what I need to do and I know I can do it but it's a lot of work. Deep down I think my biggest fear is that I will achieve everything I set out to achieve and by doing that will completely upend many parts of my life that I've grown comfortable with. 

I think it's true. If I were to accomplish my goals, many things would change because I would be in a vastly different state. But I don't think that's a bad thing. A bad thing would be to do nothing, to allow life to pass me by. Change is a natural part of life, for better and for worse. The key is whether you're leveraging that change for your purposes or if you're being swept up in the change.

I aim to do the former. I trust that I'll continue to make the best decisions I can based on the knowledge I have at the time of the decision. That's really all I can do. I think that's enough.

# Learnings

* Unity HDRP pipeline - I've been learning a lot about Unity's HDRP pipeline which is the development 'mode' by which you can achieve high quality visuals with limited work. It's a cool process and I've enjoyed getting back into 3D development. I've been applying these learnings as I build out the first of my [Monoliths](https://labs.hamy.xyz/projects/monoliths).

# Gratitude

* 2020 is over and it was a strong year for me. I [built a lot of 'firsts'](https://labs.hamy.xyz/posts/2020-review-hamy-labs/) and [increased my net worth by 81%](https://blog.hamy.xyz/posts/2020-finances-covid-edition/#net-worth). I'm grateful that I was able to make it through this time relatively unscathed. Much of this outcome was due to planning and decisions I've made in the past yet a lot of it has to do with luck - luck that I started down the paths I did and that my general areas of life were unaffected. I try to balance my dance with luck by making risk-aware decisions that attempt to solve for the worst case and by pitching in to those in need through [HamForGood](https://labs.hamy.xyz/hamforgood/) and [HamForGood - Personal](https://blog.hamy.xyz/pages/systems/hamforgood/). I really do believe that we can help everyone by helping those in the greatest need.
* Indie Hackers building openly on Twitter. In the past few weeks I've taken a step back from social media and online forums. I think that I have built up a bad relationship with my phone - engaging in endless-scroll-fed contumption far more than is healthy. It was my go-to whenever I had down time and / or was in an awkward situation and I think it worked against me - giving me short-term relief from the situation but ultimately stealing my attention in spurious directions and making it harder to tackle the task at hand. As such I've built rules for myself to limit my contumption and remove endless scrolling from my life. So far it's worked well but there is one network where I've been spending more time - [Twitter](https://twitter.com/SIRHAMY). I've found many solo founders building startups and sharing their experiences there and it's been a huge inspiration to me, even helping me to align on [my new identity](https://blog.hamy.xyz/posts/2020-review/#who-i-am). 
* People consistently fighting for what's right. I'm in awe of those fighting day in and day out for the good of all people. It's tiring, thankless work. I want to do more myself and am approaching that by [increasing my goaling for HamForGood](https://blog.hamy.xyz/posts/2020-review/#2021). In time, I want to be providing substantial resources for organizations doing good for the world.