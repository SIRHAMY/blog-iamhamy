---
title: "Atomic Habits: Your Systems or Your Life"
date: 2021-01-04T23:25:22Z
subtitle: ""
tags: [
    'contumption',
    'systems'
]
comments: true
---

I just finished [Atomic Habits by James Clear](https://amzn.to/3naub6l) today - an excellent book on the habits that make up our lives, the strategies you could employ to change them, and ways to build and maintain new systems of action. 

I believe we are what we do. If we run a lot, we're considered a runner. If we code a lot, we're considered a programmer. If we lie a lot, we're considered a liar.

Our actions define us. Not just other people's views of ourselves but our own as well. Whether we are aware of it or not, we have an internal system of operation that we execute our lives by and we're constantly voting with our thoughts and actions on the kind of person we are today and that we will become tomorrow.

I call refer to these systems of operation as one's [Systems of Being](https://blog.hamy.xyz/posts/2020-review/#systems-of-being), but here I'll just call them our systems. Our systems run us even as we construct the systems we operate in. If we start running a few miles a day, it becomes easier for us to run more miles. If we start telling little lies regularly, it becomes easier for us to tell bigger lies in the future. As we execute in our system, we shape our system.

James Clear refers to these recurring, voting actions as habits. I think this is a useful, common scope for this but I prefer thinking of this as a holistic system. Our identity feeds our values which feed our actions which feeds back into the system. There are pros and cons for both but I'll be talking about this in my terms cause HAMY.BLOG.

I'm a huge fan of systems. I build systems for everything - exercising, building businesses, reflecting on my life, etc, etc. My systems consist of a vision of who I want to be, a set of values and principles to guide me, and recurring actions (read: habits) to get me there. These systems are my way of controlling what kind of person I will become and ensuring that I'm continuously improving myself towards those goals.

I believe that our systems can compound. As we improve on our systems, we can maintain the progress we've achieved and continually get more efficient at future progression. If we can improve our system by 1% each unitOfTime, then over time we'll be much, much better. This is similar to the compound interest principle for money.

As we are what we do and systems control what we do, a focus on systems is a highly efficient way of becoming (and exceeding) the people we want to be.

James Clear is much more concise and provides much better examples about the power of habits and systems than I've done in this post. It makes sense - he's written about this for years.

* [How To Start New Habits That Actually Stick](https://jamesclear.com/three-steps-habit-change)
* [How to Build a New Habit: This is Your Strategy Guide](https://jamesclear.com/habit-guide)

One thing I want to embed here is his strategy for building (and breaking) habits.

How to create a good habit:

* Make it obvious
* Make it attractive
* Make it easy
* Make it satisfying

How to break a bad habit:

* Make it invisible
* Make it unattractive
* Make it difficult
* Make it unsatisfying