---
title: "2021 Week 2 Journal - MLK"
date: 2021-01-20T15:18:34Z
subtitle: ""
tags: [
    "journals"
]
comments: true
draft: false
---

# Top of mind

This weekend was MLK weekend. It's important to reflect on all the struggles and sacrifices that were made to get us where we are today.

I don't have anything poignant or novel to share. Instead I encourage you to use this time to read some of MLK's works and reflect on what we can do to make society better for everyone.

Read: [Letter from Birmingham jail](https://www.csuchico.edu/iege/_assets/documents/susi-letter-from-birmingham-jail.pdf)

# Releases

* [Gaming on Ubuntu with Proton](https://youtu.be/6KoqKEJREkw) - I released a new video about gaming on Ubuntu. It was the first video I've done in awhile and it shows. I realized that I need to focus on making videos that are complimentary to the work I'm trying to do (this video does not) and that I have a long way to go to get my videos to a quality level I'm satisfied with. I'll be sharing things more often (at least once a week) on the blog and YouTube to force myself to focus on complimentary things and get better at production. I think sharing is something I like doing, is an area where I have a lot of room to grow, and is useful in building a community of people to create with - all things I'd like to improve on. I'm creating videos on my [Fujifilm x-t4](https://amzn.to/39R0m5Y) - the same camera I got for photos - and that's proved another thing to try and learn. Will report back with findings.
* [Genuary 2021](https://instagram.com/hamy.labs) - I've released my first entries in Genuary 2021 - a daily generative art challenge
* [System - Choosing a news source](https://blog.hamy.xyz/posts/choosing-a-news-source/) - I released a quick post on my general system for auditing and choosing a news source. I was getting into some internet fights about bias and fact checking so wanted to solidify my thoughts and verify my claims.

# Stresses

My biggest stress is still my poor system execution. In 2021 I've performed far worse in my systems than I have previously. I think part of this is due to lack of motivation and another part is that I'm now tracking my systems more rigorously, so I've noticed when I'm slacking more consistently.

I don't think there's much I can do about motivation but there is stuff I can do about execution. To combat this malaise I've modified my systems a bit:

* Created a habit tracker to show when I've hit and missed my systems
* Streamlined my system for taxing myself when I break my habits. Tax includes losing content consumption privileges and giving $25 to charity.
* Streamlining backlog systems so I always know what's most important to do next and why

With these changes, I hope I can again shift my execution to run more off discipline than motivation. We'll see.

# Learnings

* DSLR - I've been learning a lot about my [Fujifilm X-T4](https://amzn.to/3sCVyKh). I'm trying to get into [street photography](https://www.instagram.com/hamy.see/) and use it for [my YouTube videos](https://www.youtube.com/channel/UCPBY44jxP7gOMkUsP5rlGnw).
* Blender / 3D - I've been learning a lot about Blender and 3D modeling. This year I wanted to shift my art to better fit [my vision](https://blog.hamy.xyz/posts/2020-review/#systems-of-being) stylistically and conceptually. My vision has shifted again in the past few weeks but one thing that's stayed the same is that I think 3D is the correct medium for me and that learnings here will pay large dividends in many of my future projects. 

# Gratitude

* I'm grateful that the world hasn't ended yet. Let's see what this inauguration has in store for us.