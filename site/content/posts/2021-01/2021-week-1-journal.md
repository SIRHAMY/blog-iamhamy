---
title: "2021 Week 1 Journal - US Unrest"
date: 2021-01-11T03:06:11Z
subtitle: ""
tags: [
    "journals"
]
comments: true
draft: false
---

# Top of mind

Here we are, 10 days into 2021 and already we're seeing record levels of unrest in the United States. This week, mobs broke into the Capitol (the seat of our government!) as part of a protest against a rightful election.

In [my 2020 review](https://blog.hamy.xyz/posts/2020-review/#fin) I said that 2021 would only be better than 2020 if we made it better than 2020. That meant showing up and putting in the work required to change it. But I confess that I still harbored hopes that 2021 would magically be better than 2020. Maybe people learned their lesson or something - idk.

Now we're faced with the reality that we're still in the same state with the same people, prejudices, and systems as we were last year. If we want that to change, we'll need to put in a lot more work.

# Releases

* [Monolith 0000: Monolith of Being](https://labs.hamy.xyz/projects/monolith-0000-monolith-of-being/) - I released my first Monolith! Many more to come.
* Releasing photos on [@hamy.see](https://www.instagram.com/hamy.see/) - I started taking photos again and really liked it. I think I got a little disenchanted with it when I, again, saw how many edits were going into the photos I was looking at. This time I went back to basics, focusing on a prime lens, observing my surroundings, and a minimal (read: crops and rotations only) editing workflow. I really loved it and am really happy about my camera purchases and research in 2020.

# Stresses

Sticking to habits

It's 10 days into the new year and I've already paid $51 in System Tax for not following my systems. I'm a little stressed about this because it doesn't bode well for the rest of the year.

I think this had a few causes but mostly because December was a very chill month for me and my first week back at work was very not. I'd forgotten how much work gets done in a normal work period so it took a bit to warm up.

Actions:

* Commit to my systems
* Modfy my calendar to better fit my life cycle
* Modify my systems to better fit my life cycle
* Organize my new Notion workspace to make my processes more efficient

# Learnings

* Street photography - Practiced my street photography and came across some really helpful resources. I really like this post [70 street photography tips for beginners](https://erickimphotography.com/blog/70-street-photography-tips-for-beginners/)
* Notion - I started using [Notion](https://www.notion.so/) for all my personal docs and notes and honestly it's great. I'd previously been using a hybrid of Google Docs and Dropbox Paper but Notion feels like a tool custom built for how people actually take notes. I'm not getting rid of my Drive or Paper subscriptions - those have their use cases as well - but Notion feels like a much better fit for my notes. So I'm using it.
* Intimacy - The pandemic has had a ton of effects outside of the obvious risk of COVID. Many other effects are less talked about - like loss of connections, depression, and fitness. One such effect is intimacy and how being in the same 350 square foot box with a partner 95% of the time may effect it. For this I've been reading the aptly-named [Mating in Captivity](https://amzn.to/3qbyMH0). It's been an eye-opening read and gotten me to wonder what other crucial areas of long-term romantic relationships I may be completely oblivious to.

# Gratitude

This week I'm grateful that people are taking this anti-election movement seriously and doing something about it. This past year I've felt we've been dangerously close to losing sight of the principles the United States was built on time after time after time. Through the hard work of millions of Americans we've made strides to prevent that. The work isn't over, as this week's events made clear, but I want to pause and appreciate all the people who are trying to do good in this world. It's thankless work. Thank you for persisting.