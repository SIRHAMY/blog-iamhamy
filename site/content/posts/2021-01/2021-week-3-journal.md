---
title: "2021 Week 3 Journal - Inauguration"
date: 2021-01-25T14:00:59Z
subtitle: ""
tags: [
    "journals"
]
comments: true
draft: false
---

# Top of mind

The inauguration came and went without much fanfare. Nothing catastrophic happened and it seems like the transition of power happened peacefully in the US. 

I know that the conditions that brought about so much chaos and hurt in 2020 are still largely intact. But this is reassuring that we can do better in 2021.

# Releases

* [YouTube videos](https://www.youtube.com/channel/UCPBY44jxP7gOMkUsP5rlGnw) - I've started making YouTube videos again. I decided it made sense to make a system of YouTube sharing to 1) boost my following and share my work, 2) get better at talking about and selling my work, and 3) to better understand the videography / camera domain. I have a weekly goal to create and share one thing. So by the end of the year I should have 50+ new shares under my belt.
* Changing art mission - I'm again changing the mission of my art focus. For the past few weeks I've been learning 3D visualization technologies (like Blender and Unity) and creating art as part of [Genuary 2021](https://genuary2021.github.io/). It's been an extremely satisfying process and along the way I started to realize that the core of that satisfaction was from the generative systems I was building for each. So I've changed my art mission to better reflect that: "Explore the nature of systems through technology and data". I'll still be building largely through the lens of [Monoliths](https://labs.hamy.xyz/projects/monoliths/), just with a different perspective on why that is.
* Archiving [LineTimes](https://labs.hamy.xyz/projects/linetimes/) - I released LineTimes just over a month ago. It was a great experience to build it from the ground up - learning new frameworks, architecture paradigms, and cloud platforms. But, a month in, the site's only receiving 36 users / month - well below the 100 mark I set for business validation. I think a key part of being a successful startup creator is the ability to validate your problem fast and cheap - the more problems you can do that with, the greater your chance of finding one that works. LineTimes currently costs me around $70 / month while making $0 revenue - this is in direct conflict with [my 2021 goal](https://blog.hamy.xyz/posts/2020-review/#2021-h1-goals) of making $100 / month in profit from HAMY LABS. There's more I could've done to make this a success - like build more features and do more marketing - but there are other projects on my backlog that may have greater opportunity and fit better with my current goals so it seems like that's where I should focus my efforts.

# Stresses

My systems are still my primary source of stress. Since the beginning of the year I've been failing to hit them consistently. This past week I've done better but they're still not up to where I want them to be.

Actions:

* Keep executing on systems
* Block out more time for their execution

# Learnings

* 3D - Learned a lot about Blender and Unity, especially procedural generation and animation. I even rolled some of these learnings into [a new YouTube video](https://www.youtube.com/watch?v=YO-F3pPzVxw&ab_channel=HAMYLABS)
* FujiFilm X-T4 - I've continued to learn about my camera and the settings I can tweak to make it act the way I want it to. I'm very much in the capture-the-moment-as-it-is philosophy bucket which means I do almost zero edits after the fact. I started playing around with my X-T4's film filter settings and it's allowed me to find the right balance between visually stunning colors and zero editing. Will be releasing some of these images on [@hamy.see](https://www.instagram.com/hamy.see/)

# Gratitude

* I am grateful the inauguration didn't turn into a ground zero of cataclysm.
* I am grateful that I am financially secure and have the freedom to make most decisions without worrying about my fiscal survivability and that I've built systems that should continue to build on this security over time.
* I am grateful to be living in a rich nation in the 2000s. We have some really cool and exciting technology to play with and technological advances to make our lives better. I don't have to worry about clean water, electricity, or even access to the internet. That really is a wonder of the modern age.