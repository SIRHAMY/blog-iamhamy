---
title: "Hawaii in Review 2018"
date: 2018-10-23T19:38:18-04:00
subtitle: ""
tags: []
---

_Last month I took a trip to Hawaii. I never got around to posting anything about it, but did write some notes on the plane ride back. Here they are for posterity._

<iframe width="560" height="315" src="https://www.youtube.com/embed/v6KNf4qrgvc" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

[If the embed's not working, you can view the Geolog directly on YouTube](https://www.youtube.com/watch?v=v6KNf4qrgvc&t=140s)

# Fave things

![Mauna Kea summit](/posts/2018-10/hawaii-in-review-2018/maunakeasummit.JPG)

## 1. Mauna Kea star viewing and summit

The ohana took a trip up Mauna Kea on two separate nights to star gaze and mount the summit. Both views were amazing and well worth the extra effort required to grab a car that could make the trek.

![Cliff diving](/posts/2018-10/hawaii-in-review-2018/cliff-diving.JPG)

## 2. Cliff jumping at the southern-most point

This stop wasn't even on our itenerary when we arrived, but some fellow hostel guests had gone a few days before and recommended it highly. It took a bit for the group (sic Diane) to get warmed up to the idea of plunging 30 feet into the ocean, but it was an amazing experience once we did.

![Black sand beach](/posts/2018-10/hawaii-in-review-2018/black-sands-beach.JPG)

## 3. Various snorkeling

I believe this trip was the second time I'd ever gone snorkeling (the first being way back in high school during our religious school trip to Israel). Notable sightings:

* Sea turtles at the black sand beach
* A manta ray through a guided night dive tour (worth the ~$45 we paid)
* Assorted fish at Two Step (though no dolphins) and Captain Cook. Louis swears he saw an octopus, but it just looked like a rock to everyone else /shrug

## Other places of note

![Magic sands](/posts/2018-10/hawaii-in-review-2018/magic-sands.JPG)

* Magic Sands beach - right down the street from our hostel, with a lot of beach to roll around in

![Royal Kona](/posts/2018-10/hawaii-in-review-2018/royal-kona-sunset.JPG)

* Royal Kona Resort Luau - Yes, it's as corny as it sounds, but you can skip paying for the $100 tickets to the event itself, grab some drinks at the bar, and still get a great view of the festivities.

![Captain Cook hike](/posts/2018-10/hawaii-in-review-2018/captain-cook-hike.JPG)

* Volcano hikes if available, hiking through nature regardless - the mountain was still too active for us to go see any actual lava this time around (v bummed) but we still got to explore the beautiful natural formations all around.
* Da Poke Shack served up bomb Poke (and even had its own poke stop) and was situated right across from our primary hostel (My Hawaii Hostel)

![Coffee plantation](/posts/2018-10/hawaii-in-review-2018/coffee-plantation.JPG)

* Coffee "tour" - really just a round-about way to get you to buy coffee, but you don't actually have to buy and exploring the grounds and drinking several free tasting glasses was well worth the 20 min drive out there.

