---
title: "Cooking my own meals at home is worth ~$619 / month and ~$180,000 over 15 years"
date: 2020-05-17T08:35:49-04:00
subtitle: ""
tags: [
    "thevalueofmoney",
    'finance'
]
comments: true
---

COVID-19 has brought about drastic changes in daily life. One of the biggest changes for me is in how I acquire food. Typically my work provides breakfast, lunch, and dinner meaning I only need to worry about food on weekends. [I calculated the value of making my own food on weekends to be ~$1,310 / year](https://blog.hamy.xyz/posts/the-value-of-making-own-food-on-weekends/).

Now that I don't get those benefits, I wanted to calculate the new value of making my own food. Given current information, my bet is that New York won't be fully operational again until sometime next year so my hope is to use this information to make more informed decisions in this time.

# the value of making my own breakfast

I'm pulling out breakfast into its own thing cause I pretty much eat the same breakfasts every day and they're pretty simple and therefore pretty cheap compared to lunch and dinner.

In my [previous calculation for the cost of breakfast](https://blog.hamy.xyz/posts/the-value-of-making-own-food-on-weekends/#the-value-of-making-my-own-breakfast), I found that the average cost of a bacon, egg, and cheese when bought from a shop was ~$4.50 and the average cost of making an egg wrap for myself (my usual breakfast) was ~$1.43. This leaves me with an average savings of $4.57 / meal.

Here are those calculations:

```
cost_of_breakfast_top_dollars: float = 7.50
cost_of_breakfast_bottom_dollars: float = 4.50
 
average_cost_of_breakfast_dollars: float = (cost_of_breakfast_top_dollars + cost_of_breakfast_bottom_dollars) / 2
average_cost_of_breakfast_dollars # 6

cost_of_an_egg_dollars: float = 2.49 / 12
cost_of_a_wrap_dollars: float = 6.49 / 8
 
cost_of_making_breakfast: float = cost_of_an_egg_dollars * 3 + cost_of_a_wrap_dollars
 
cost_of_making_breakfast # 1.43
```

# the values of making my own lunch / dinner

In my [previous calculation for the costs of lunch and dinner](https://blog.hamy.xyz/posts/the-value-of-making-own-food-on-weekends/#the-value-of-making-my-own-lunch) I found that the average lunch spot cost about ~$11.99 for a meal and that my average cost of lunch was around ~$3.96 leaving a total savings opportunity of ~$8.03 / meal.

I think the estimate for average lunch spot cost is a little low for New York, particularly if you're getting sides or drink which can bump it up to $15-16. I also think the average cost of making a meal is a bit high here as I make my meals in batch with my [Instant Pot](https://amzn.to/2yefCey) so often buy items in bulk at slightly cheaper rates. These meals typically come out to anywhere from $1.75 - $3.50 depending on the complexity and ingredients required.

All that being said, I like my estimates to be a little on the conservative side to account for other assumptions I may have missed, so we'll continue on with the previous estimates. Another thing to note is that my lunches and dinners are largely the same, so we'll be using this estimate for dinners as well.

Now onto the full calculations.

# the value of making my own meals

So first let's see what my savings opportunity is for making my own food each day.

```
savings_breakfast: float = 4.57
savings_regular_meal: float = 8.03

savings_per_day: float = savings_breakfast + 2 * savings_regular_meal
savings_per_day
```

This gives us a total savings opportunity of $20.63 / day! That's pretty good.

If we extend this to each month and year, we get:

```
savings_per_month: float = savings_per_day * 30
savings_per_year: float = savings_per_day * 365

```

* monthly savings: $618.90
* yearly savings: $7,529.95

So the total opportunity for making my own food at home is ~$619 / month and ~$7,530 / year. 

But what's the true value of this? I'm a big fan of saving and investing - if my money can work for me then I want to put it to work.

Let's calculate that.

# the long-term value of making my own meals

When I have extra cash from savings, I like to allocate that to my investments. Over time, investments have a tendency to grow which I think is better than just leaving money in an account to slowly lose value from inflation. So when I think about the savings I could have by making a move, it's more accurate for me to value that savings based on the expected investment returns over time rather than just one lump sum.

I built [thevalueofmoney](https://thevalueofmoney.xyz/calculators/moneyovertime?initialValueDollars=100&additionalValueDollars=619&timeGranularity=month) to help with calculations like this because I found myself thinking about them pretty often.

So with the inputs:
* initial investment: $0
* additional investment: $618.90 / month
* investment length: 15 years
* interest: 6%

We have an [expected value of $179,987.70 after 15 years, with a total investment of $111,402.](https://thevalueofmoney.xyz/calculators/moneyovertime?initialValueDollars=0&additionalValueDollars=618.9&timeGranularity=month&timeHorizonYears=15)

If we look at a time horizon of 5 years, that still gives us an [opportunity of $43,180.67](https://thevalueofmoney.xyz/calculators/moneyovertime?initialValueDollars=0&additionalValueDollars=618.9&timeGranularity=month&timeHorizonYears=5).

[![thevalueofmoney - making your own food for 15 years](https://storage.googleapis.com/iamhamy-static/blog/2020/one-off/value-of-making-meals-at-home-all-year.png)](https://thevalueofmoney.xyz/calculators/moneyovertime?initialValueDollars=0&additionalValueDollars=618.9&timeGranularity=month&timeHorizonYears=15)

# fin

Hopefully we won't be in quarantine for the next 15 years, but I think it's still a good idea to use this time as a catalyst to examine your habits and change those that don't make sense long-term. For me, that's tweaking some of my entertainment and living budgets and figuring out how to make this time at home as sustainable and enjoyable as possible.

Stay safe, stay sane, stay healthy.

-HAMY.OUT