---
title: "Why I'm decreasing my savings budget"
date: 2018-12-01T19:35:52-05:00
subtitle: ""
tags: [
    'minimalism',
    'finance'
]
comments: true
---

Over the past year, I've become increasingly interested in the lifestyle ideas of frugality and minimalism. One of the primary reasons these lifestyles appealed to me was because their primary tenet is to [focus on that which is important _to you_](/posts/minimalism-more-of-what-you-love-less-of-what-you-dont/) in the face of disparate constraints, from the burdens of excess (minimalism) to the lack thereof (frugality). Together, they can be a powerful tool towards reshaping your life - introspection to understand what you value most, getting rid of the things you don't love (minimalism), and pairing down the things you do love to optimize for monetary expenditure (frugality).

Through this hybrid lens and subordinate experiments, I've personally discovered (note I didn't say realized cause even I am not a robot that doesn't buy Dunks every now and then) ~$2400 in savings each year by simply changing [how I consume coffee](/posts/case-for-self-brewing-cold-brew/) and [how I travel around New York](/posts/in-favor-of-citi-bike/). Of course, saving for saving's sake doesn't mean much, so this is a good time to introduce one of my long term vision goals (or HamAt40 as I like to call them): be financially independent.

What is financial independence?

In my investigations into frugality and minimalism, I stumbled upon the idea of FIRE (Financial Independence / Retire Early). Now it's important to note that having the first doesn't necessarily predicate the second, but it is certainly required for the second to be enjoyed whether early or not.

The whole idea sounds pretty good - put away enough money now such that you can enjoy your "golden years" to their fullest. Surprisingly, it takes just minor tweaks to your daily life to make very large improvements to your future life - brewing my own cold brew rather than buying shaved ~1 yr off my expected retirement date over a 15 year time horizon.

So I accepted the quest because why not? Best case, I'm ready to retire at 40, worst case I've got a lot more cash to deal with whatever this case is.

Further reading:

* [mr money moustache](https://www.mrmoneymustache.com/) - simple, strong arguments for frugal living
* [r/financialindependence](https://www.reddit.com/r/financialindependence) - good community, lots of discussions
* [networthify calculator](https://networthify.com/calculator/earlyretirement?income=50000&initialBalance=0&expenses=20000&annualPct=5&withdrawalRate=4) - best calculator I've found

Equipped with this knowledge, we now have both an end and a means - a complete setting for the rest of this story.

Since accepting my quest for FIRE, I've made numerous changes to my lifestyle to better align myself with it (some of which were shared above, some of which weren't):

* Budgeted strictly and aggressively with You Need a Budget
* Cut down on common expenses
    * Made my own cold brew
    * Cooked my own meals (bye bye $10 meals!)
    * Signed up for Citi bike
    * Took office leftovers home
    * Longer pregames, shorter bar games
    * Less drinking (drinking is expensive y'all), more exploring
* Cut down on seasonal (probably not the right word, but not sure what's better) expenses
    * Planned trips further out to get better prices on fares / accommodations
    * Planned trips around deals (Scott's Cheap Flights ftw)
* Allocated funds to aggressively pay down debts accumulated in my first (very fun) 5 months of being a New Yorker

Overall the changes have been incredibly positive. I've got more gold in my coffers and I've actually been traveling/exploring _more_ than I had been before I started this experiment. So experiment success.

Well, kinda.

The problem, and real point behind this post, is that I have gotten so caught up in optimizing for my long-term vision that I started weighing the monetary impact of a given opportunity over that of its implicit impact with respect to my core values. Now that's not to say that the monetary impact of a given opportunity shouldn't be taken into account, but that it shouldn't overshadow that which is really important to you.

```
weightedValue = implicitImpact // bad

weightedValue = implicitImpact * 0.3 + monetaryImpact * -0.7 // what I was doing

weightedValue = implicitImpact * 0.6 + monetaryImpact * -0.4 // better
```

This situation presented itself in two distinct ways, both in the form of missed opportunities:

1. Passing up opportunities that came along because they were "too expensive"
2. Not executing on daydreams (dreams was too strong a word so I prefixed day-) that I've had for months because they were "too expensive"

Now I believe that if you have a budget you should be strictly adherent to it. Budgeting and then allowing yourself to step outside of it without a good reason is a slippery slope. That being said, if you find you're unsatisfied with how your budget has been supporting your lifestyle, it's probably a good time to re-examine it.

So that's what I did.

What I found was that my budget was fine for my long-term vision, but my long-term vision was arbitrary while my current fulfillment was not. There was no real reason that I picked 40 vs 41* rather than it being a "round" number and it being my approximate cutoff at which point one becomes "old", yet that decision was affecting my current self's ability to find optimal fulfillment which seemed dumb. For 1), I don't even expect to want to retire at 40 - I like building software and will likely do it or something like it til I can't - and 2) the benefits of being able to retire 1-5 years earlier look a lot worse if you have to sacrifice significant fulfillment from the 15 years preceding it. When you factor in that retiring the 1-5 years earlier doesn't mean much to me, it's like I'm voluntarily sacrificing that fulfillment for nothing which doesn't just seem dumb, it seems v dumb.

If you know me, then you know that I don't like dumb things if there's a ready alternative and, in this case, that alternative was a modified budget to better suit my values. Here's what I did:

* I increased my budget for "Projects" which holds a pool of money to dip into for all of my creations
* I created a new budget category for "Self Development" which is to be allocated towards pursuits I believe are beneficial to myself in some form of learning

Together, they cover ~90% of the daydreams/stumbled upon opps I was missing out on with only a marginal decrease in the amount I was saving every month. Big implicitImpact, small monetaryImpact.

I'll be experimenting with this new budget in the coming months and expect to be making tweaks as they're hit with production events. One of the biggest tweaks I expect I'll be making is moving more money out of my savings budget and into my spending budget. This will further allow me to capitalize on upcoming opportunites though I'll need to be careful to remain conscientious of an increased desensitization towards spending lest it start a snowball.

To help combat this snowball, I have rudimentary algorithms in place that cap the amount of money I keep in certain budgets, transfering them over to my spending budgets periodically so that I am encouraged to spend frugally as those savings have a purpose while discouraging the rampant spending often spurred by the "use it or lose it" mentality (looking at you government) by allowing budget pools to increment. The hope is that these algs in cooperation with my own mindfulness and intent will be enough to keep my spending balanced between miserly and excessive.

But as I said, tweaks to come.

*_That one year difference in target retirement age (40 -> 41) may not seem like a lot, but it manifests itself as an ~$2K difference in yearly budget given my own finances._