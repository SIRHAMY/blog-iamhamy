---
title: "Release notes: February 2019"
date: 2019-03-06T00:41:31-05:00
subtitle: ""
tags: [ 
    "release-notes",
    "hamy",
    "the-hamniverse",
    "reflections" ]
comments: true
---

_February highlights from The Hamniverse_

Alongside the typical foci, I'm trying to be a little bit better at sharing my progress towards my goals. My hope is that this keeps my internal process a bit more transparent (people usually flip their shit when I show them how I organize things which, to me, means that I didn't do a good enough job publicizing it) and that it'll log this progress for posterity/provide motivation to actually finish shit. Recall that [I'm a pragmatist, not a masochist](/posts/2018-q2-in-review/) so I won't necessarily be sharing all the goals I have, rather I will be hand-selecting those that make the most sense to show and go from there. _People don't get mad at me for editing my [instaphotos](https://www.instagram.com/sir.hamy/) so I hope you don't get mad at me for editing my instablog._

# Self

Goals:

[_] Run 50 miles - _not even close, ankle still healing_

[x] Make NYC home - _moved into the new place, bought some art, and our living room is ~complete_

[~] Keep yogaing - _I did do a lot of yoga, but fell out of practice in the last few weeks so this isn't a complete win_

[x] Eat healthy and cheaply - _I had a bad streak of $50+ meals when we first moved in so I was hoping to steer away from that. Mission accomplished_

[_] Figure out how to do taxes - _lol no, we have another month? I think?_

* I moved to a new place (and it has room for a desk!!!!)
* My ankle _still_ isn't healed from my sprain back in January and now I'm worried it may be more serious than I'd thought =(
* The Hamventures are still going strong. Stay tuned for events I find interesting, each week (except those I forget/am lazy). See [this week's Hamventures.](/posts/hamventures-nyc-week-of-2019-03-04)
* This year I've been trying out meditation (among other pursuits people tend to recommend as good life influences). [I logged my findings one month in](/posts/one-month-of-meditation) (and will likely continue to log these at regular intervals)

# Projects

Goals:

[_] Release 1 side project - _nah it's not ready yet. But I have been working on it which is good and I expect it to be ready for eyeballs next month_

[x] Add all old blog posts to blog - _done!_

* I made slight changes to my [HAMY](https://hamy.xyz/) brandmark.
* Added a [project page for IAmHamy](https://labs.hamy.xyz/projects/iamhamy/) properties to make it more shareable
* Got a new rig and mechanical keyboard cause #treatyoself
* finally migrated all my old blog posts from a Ghost blog archive file resulting in 237 "new" posts on [Labs](https://labs.hamy.xyz/posts) dating back to early 2013

# Adventures

Goals:

[x] see/do 6 new things - _I don't have high bars for these things (though I do like high bars, like the 12+ stories kind) so hit this readily_

[x] go on 2 dates - _dating is fun, I wish I'd done this more, earlier /shrug_

* Adventures have been chugging along habitually
* I started taking hip-hop dance classes with some friends up here and then had to stop to allow my ankle more time to heal. Was fun, will def try to pick it back up again.
* One of my goals this year was to get out there and date more, so naturally I wrote about my thoughts: [Why I split the bill on dates](/posts/why-i-split-the-bill-on-dates)

# Fin

That's it. Come back in a month for a full reflection.

-HAMY.OUT