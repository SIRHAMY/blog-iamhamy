---
title: "Hamventures NYC: Week of 2019.02.18"
date: 2019-02-18T11:26:56-05:00
subtitle: ""
tags: []
comments: true
---

# Monday (2019.02.18)

Nothing =(

# Tuesday (2019.02.19)

## Intelligent Design: Fashion + AI (Free, 1930-2230)

A discussion on the intersection of fashion and AI, with a focus on current trends into the near future. Run by NYC Salon 'a speaker series that aims to be "TED with friends"'.

**Location:** Williamsburg Hotel

**Link:** https://www.eventbrite.com/e/intelligent-design-fashion-ai-nyc-salon-90-tickets-56287688993

_Reposted from https://joylist.nyc/_

## No lights, no lycra ($5, 2030-2130)

"A weekly dance jam in the dark, for the pure joy of dancing!". 1 hour of dark dancing for $5. Never been but seems like an interesting time.

**Location:** Greenpoint

**Link:** https://www.facebook.com/groups/110012725696878/

_Reposted from https://joylist.nyc/_

# Wednesday (2019.02.20)

Nothing =(

# Thursday (2019.02.21)

## The Get Down @ House of Yes ($20+, 1800-2200)

My fave club evr is hosting The Get Down this Thursday, a no-phone, no-drink dance event.

**Location:** Bushwick

**Link:** https://www.facebook.com/events/1194373597393011/

# Friday (2019.02.22)

## Asian Creative Network: NYC Showcase ($15, 1800-2200)

A showcase of NYC creatives. Music, dance, art, etc.

**Location:** Greenpoint

**Link:** https://www.facebook.com/events/232549650958071/ 

## Decade of Fire: Bushwick film screening + Community dinner (Free, 1830-2200)

A screening of _Decade of Fire_ documenting displacement in NYC boroughs and how it affects community. Looks like food and drinks will be available for purchase. Perhaps not the most exciting thing to do on a Friday but seems like a good time to start learning about this stuff if you haven't already.

**Location:** Bushwick

**Link:** https://www.facebook.com/events/516438685512048/

## Candlelight yoga with live piano, chocolate, and wine ($30, 1930-2100)

As if there weren't enough events to choose from on Friday, now you've got to compete with the #balance of yoga and chocolate. Tbh I'm not sure the wine and chocolate will go well with any hard yoga poses so I'd view this as more of a relaxation exercise than all-out Bikram session, but do whatever you want with it.

**Location:** Queens

**Link:** https://www.facebook.com/events/1030858690457224/

# Saturday (2019.02.23)

Nothing =(

# Sunday (2019.02.24)

## Artist talk with Wendy Red Star ($16, 1400-1500)

If you're into artist talks / Brooklyn Museum, this one might be for you. Pretty cheap and includes entrance to the museum (though I believe entrance is suggested anyway so theoretically you could get in for a lot less).

**Location:** Brooklyn Museum, Dumbo

**Link:** https://www.facebook.com/events/604289670030179/ 