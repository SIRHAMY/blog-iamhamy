---
title: "Hamventures NYC: Week of 2019.02.04"
date: 2019-02-03T14:12:39-05:00
subtitle: ""
tags: [ "hamventures", "nyc", "things-to-do" ]
comments: true
---

# Monday (2019.02.04)

## Feast3: February

Seems to be a ~monthly series of assorted performance-art pieces. $12 for entry and on St. Marks so pretty accessible. Got no expectations here so if you're free from 1900-2100 and have a few bucks laying around, could make for an interesting night.

**Price:** $12

**Location:** St. Marks (East Village)

**Link:** https://www.facebook.com/events/233520657579358/ 

## Backroom speakeasy mondays

No cover, live music, in a speakeasy. Pretty good way to spend a Monday.

**Price:** Free

**Location:** The Backroom (LES)

**Link:** https://www.facebook.com/events/391389231406553/ 

# Tuesday (2019.02.05)

# Wednesday (2019.02.06)

## Quiet mornings at MoMA

Every first Wednesday of the month, MoMA opens its doors early (0730-0900) for visitors to have an intimate viewing with their exhibitions without the crowds. Phones and talking are discouraged, there's a guided meditation practice ~0830.

**Price:** Normal prices ($15 adults, Free with membership)

**Location:** MoMA (midtown)

**Link:** https://www.facebook.com/events/1993774220716784/?event_time_id=1993774230716783

## Liminal Space opening reception

Graffiti/pop influences, 1800-2100

**Price:** Free

**Location:** GR Gallery (LES)

**Link:** https://www.facebook.com/events/2003834796371756/

## Variety, Guaranteed!

An open mic, welcoming many different kinds of performances. Being an open mic, you must expect that the quality will vary highly but that's part of the fun. If you plan on participating, entry is free otherwise $5.

**Price:** Free if participating, $5 if observing

**Location:** The Tank (Hell's Kitchen)

**Link:** https://www.facebook.com/events/2063329460421309/ 

# Thursday (2019.02.07)

## The unauthorized plans for New York

A free get-together to listen to practitioners of different kinds chat about alternative structures / plans to optimize NYC, particularly wrt to human displacement. 1830 - 2000

**Price:** Free

**Location:** CUNY (Murray Hill)

**Link:** https://www.facebook.com/events/1154954301320996/ 

# Friday (2019.02.08)

## New York Fashion Week with Art Hearts Fashion

Get a taste of NY fashion week from 1600-2200 Friday - Sunday. Tickets start at $50 (for standing room!) but obvs get better with more money. I still haven't been to a fashion show in NY and this might be a good option if you are trying to get your feet wet.

**Location:** LES

**Price:** $50+

**Link:** https://www.facebook.com/events/302414903739389/?event_time_id=302415547072658

# Saturday (2019.02.09)

## Deepbeats yoga at HOY

Deepbeats vinyasa flow at House of Yes. Likely less wild than their actual parties, but HoY is my fave place so still gotta rep their events. 1200-1300

**Price:** $25

**Location:** House of Yes (Bushwick)

**Link:** https://www.facebook.com/events/228205978058015/ 

## Brooklyn Wine Festival

Wine, food, music. Not sure how much you're getting for your money, but I'd assume you're not gonna be able to get totally blasted at this (if that's what you're looking for). Comes with a 5 oz tasting glass you can take home. 1200-1800

**Price:** $39

**Location:** Brooklyn Expo Center

**Link:** https://www.facebook.com/events/1014673232053848/

## San Junipero

A retrowave party (for free!) at the Knitting Factory. Haven't been to this event or this venue, but have heard both are bomb.

**Price:** Free

**Location:** The Knitting Factory (Willysburg)

**Link:** https://www.facebook.com/events/2004877749589935/

## The Lot Radio 3 year anniversary

The Lot Radio is taking over all 5 rooms of Brooklyn Bazaar with a rotating lineup on each (seriously there are a ton of names in the lineup). I don't know any of them, but I have to imagine most of them don't suck so probably going to be a good time. 1900-0500

**Price:** $30

**Location:** Brooklyn Bazaar (Green Point)

**Link:** https://www.facebook.com/events/356710888444738/

# Sunday (2019.02.10)

## A group art show at The Living Gallery BK

I don't know any of the artists off the top of my head, but have been following The Living Gallery BK for awhile so think this will probably be pretty good. Show runs from 1900-2200 and I believe entry is free as they'll be making money off art sales.

**Price:** Free entry

**Location:** The Living Gallery BK (Bushwick)

**Link:** https://www.facebook.com/events/2345550585476651/

# Notable Ongoings