---
title: "Why I split the bill on dates"
date: 2019-02-06T10:56:28-05:00
subtitle: ""
tags: [ "dating", "life", "value", 'finance' ]
comments: true
---

The processes/rules of dating have come up as a topic of discussion quite frequently in the past month or so, so today I wanted to sit down and explore an implicit policy I've held for a while now but never took the time to flesh out and share. That policy is always* splitting the bill on dates.

_*Obviously there are exceptions to every rule, but I believe the norm is more important than the exception and that one should always lead with pragmatism so I won't explore these edge cases further. Use your own discretion._

# Why I split the bill on dates

## A relationship is a partnership

I believe that a relationship (of any kind really, but obvs talking about the more traditional, intimate kind here) is a partnership. In more common terms, it's a two-way street. For me, this means that both parties should be doing the best they can, within reason and as is practical, for the good of the whole (all constituent parts).

_This in turn means if the partnership is not fulfilling these obligations, perhaps a change is in order._

Because this relationship is a partnership, it should be valued ~equally by both. If there are inconsistencies here, they can be overcome with mindfulness and intent but generally aren't healthy if they stick around (similar to how it's generally better if self/partner core values are not at odds with each other).

Money is a form of value, so if one party is not willing to share the load equally then, barring other external factors, it's generally a sign that their internal value isn't matching up with your own. Do with that information what you will.

## Dating is an evaluation

_Too analytical? Perhaps. But that's how I view the world._

Of course you went on this date to have fun. I believe happiness should be a (if not the) central goal in life. But there's also an implicit test here - who are you? do I like you? do I want to see you again?

If you're not willing to make a sacrifice of value equivalent to my own to see each other, then internally we probably value each other at different levels. If you're not going to value my time/presence as much as I value yours, then perhaps this isn't the right partnership for us and I should re-allocate my value to a partnership that will.

This may seem unduly paranoid/harsh, but I (and you as well) have a finite amount of time in this life and I want to put my value where I'll get the most returns. Sure I might still be using it to buy the same tacos and margs, but at least I have a sign that you aren't _just_ hanging out with me for the food/drinks but also the side of Ham.

## I save money

This one's not the most important to me, but I find it highly pragmatic. I'm not a billionaire (yet) so I have to cut costs where I can to make more resources available for allocation to the things I value most. Obviously I value you which is why I allocated time/money/effort to be with you in the first place, but this doesn't mean I should do so wastefully.

I expect this point to be idiomatic to most, but [I've written about this in the past](/posts/minimalism-more-of-what-you-love-less-of-what-you-dont) if you want to read more.

## It's 2019, fuck traditional societal norms

Where did this idea that men should always foot the bill come from? I haven't looked this up because I honestly don't care to waste my time on its origins because I think it's stupid. But I'd expect to find that it spawned from a time where men were the typical breadwinners and thus in control of the money and thus, by necessity, the ones paying for dinner.

Okay, I get that, but we now live in a world where, I hope, to all rational humans, your humanness is more valuable than your human attributes. Obviously there are still inequalities out there and humans actively and aggressively enforcing said inequalities. I can't (more, I have not as of now dedicated my life to fighting this) stop this alone, but I will also not be a blind host for these suboptimal ideas to live on.

So, in much the same way as I've decided to heal the world via [Tikkun Olam Tuesdays](introducing-tikkun-olam-tuesdays) - doing what I can as is pragmatic wrt my own lifestyle and iterating from there - I will make small changes to fight societal tendencies I believe are wrong. Through this lens, I will not blindly be the one to pay on dates simply because I'm a man and that's "just what people do". I will instead work to make splitting the bill - a symbol of equal value/power/footing for all involved parties - the norm.

# Is this right?

Whenever I hold an idea that so obviously bucks tradition, I like to sanity check whether what I'm doing is actually helping or if it's actually hurting. Unfortunately the answers to these mind explorations are rarely conclusive, forcing me to fall back to my mantra that we can only do our best with respect to the cards we are given. In this case, I can only make the best decision based on the knowledge I have.

So the answer here is "I don't know". But the best I can do is hold this opinion until new knowledge/insight sways me in another direction, so hold it (loosely) I shall.

-HAMY.OUT