---
title: "One month of daily meditation: more balance, more accomplishments, and more arguments"
date: 2019-02-13T08:44:49-05:00
subtitle: ""
tags: [ "meditation", "goals" ]
comments: true
---

Today(ish) (as of time of writing, I #schedule posts) marks one month since the beginning of 2019 and, for me, one month of daily meditation. At the beginning of every unitOfTime, I sit down to set goals for myself (I follow a loose [OKR methodology](https://www.atlassian.com/team-playbook/plays/okrs)). This year, one of my goals was to "be your best ham", which to me meant an increased focus on what was important to me and following through on those values. While meditation wasn't really something I valued highly, I had always been interested in it for its clear-mind, know/accept thyself properties and figured it was worth a shot to help empower the focus that would serve as a foundation for the rest of my endeavors.

# My findings

One month in and I've been able to experience life with a clarity and calm I didn't know was possible. I don't think I can solely attribute this feeling to meditation as I think living with authenticity wrt your values is far more important and something I've been doing simultaneously, but I do think it's been a non-negligible factor.

To be specific, I've found myself with a lot less anxiety which I think stems from the increased focus practice which has led me to be more action-oriented which, in turn, has led to me doing a lot more things I wanted to do which prevents a bunch of anxiety from ever manifesting because I've already taken action in pursuit/against x.

Again, super anecdotal and I really have zero data to back this up, but that's my best guess right now. It does seem to match general mental models of the boons of meditation though so no strong signals to the contrary.

One thing that I have noticed that came as a bit of a surprise is that I think I've actually become _more_ argumentative after I started daily meditation practice than I had been before. This may come as a surprise to some of you because I was extremely argumentative before and now I guess I'm just off the charts.

My current hypothesis is that this is due to my increased "headspace" (I kind of like to think of it as free cpu) for which to allocate to sticking to my values. One of my core ideals is the search and protection of truth which I enact daily by trying to be as radically honest as possible/practical and challenging those ideas of which I'm skeptical. The goal here is not to block all ideas that are not my own, rather to vet all ideas thoroughly to decide whether they will make it into my "collection" (and in turn come under my protection) or not. I like to think of it as a loose [socratic method](https://en.wikipedia.org/wiki/Socratic_method) but unsure if that analogy would pass formal examination /shrug.

So if you take my interest in truth, habit of applying a loose socratic method, and increased headspace with which to allocate towards the pursuit of my values, I think it's reasonable that one outcome might be an increase in arguments (I prefer the term discussions, but am trying to be authentic wrt what others likely call them).

# The methodology

I've just started so am certainly not an expert in the world of meditation but thought I'd go ahead and share what I do for posterity. I think it's worth noting that meditation seems like a practice that has infinite ways to practice successfully - it really is just a focus and acceptance exercise - so I'd encourage you to experiment with several methods if you're thinking of making the plunge.

For me, I've just used [Headspace](https://headspace.com/). I started with the free trial which got me like 10 sessions to try it out. I liked it so I bought the year subscription for ~$60 (so something like $5 / month for #headspace) which is a pretty great deal.

I typically do 10 mins of meditation in the morning right before I leave the house though if I miss that, I'll do it whever is convenient/practical (a few weeks ago I meditated in a Laguardia terminal!). The cool part is it doesn't really take anything special to practice, you just gotta do it. minimalBaggage++

# Moving forward

So moving forward, I'm going to continue meditating daily and see where it leads. I'll probably try to up the session duration to see if that has any noticeable effects on my experience and continue to explore the different "tracks" Headspace provides.

One thing I am going to try to change is how I go about my arguing. One of the big benefits of meditation is that by balancing yourself, you can prevent internal strife/upheaval from leaking out in the form of poor behavior towards those around you. My fear is that my increased argument without a softer touch may nullify this benefit. I'm not willing to backdown with my arguments - if I see a problem, I feel obligated to do something about it - so I'll try to adjust the way I approach it such that I reap the benefits of both.

Will obvs keep you updated on my progress.

# FIN

Want to get updates on my latest Hamventures? [Subscribe to my email list or follow along via RSS!](https://hamy.xyz/subscribe)