---
title: "2019.01 Release Notes"
date: 2019-02-01T22:31:48-05:00
subtitle: ""
tags: [ "hamy", "release-notes", "reflections" ]
comments: true
---

January has been a good, productive month. I've been relatively faithful to my resolutions and made some real progress in directions that matter to me. As such, most of my updates this month come from #self but expect more in other foci in the future.

# Self

The most notable happening this month has been my exploration of meditation and yoga. On the recommendation of several friends, I decided to try my hand at both via a free trial from Headspace and [ClassPass](http://class.ps/lNv0W) respectively (yes, that's an affiliate link and yes I'd like you to click it so I get free stuff). I won't go into much detail here as I'll likely write something more substantial later that can do it justice, but after a month of practicing regularly, I have found a sense of balance and calm that I didn't know I was missing.

So I've decided that these are practices I want to keep around for the foreseeable future with a plan to continue meditating every day and attempt to yoga 2-3x each week. Yoga is expensive af in the city so I'll be on the lookout for cheap options / creative budgeting. Stay tuned.

The acquisition of headspace came at a good time as I ramp up to move into my new place in FiDi. Nothing to share yet (as I still haven't moved in), but I'll provide pictures when I'm ready.

# Projects

My project output has been non-existent since I just started this month, but I did share that I'll be participating in [Tikkun Olam Tuesdays](/posts/introducing-tikkun-olam-tuesdays) this year. The gist is that I take one day a week and dedicate it to making the world a better place. I intentionally made the vision here vague so that it could be flexible wrt my evolving views of the world but my hope is that it helps keep me cognizant of what's going on around me and the impacts that my choices can have.

Thus far, I've started working on a website to support awareness for/knowledge of a common ailment (cause I like code and helping in a way you like is still helping) and attempting semi-vegetarian (i.e. vegetarian when it's not inconvenient/out of the way) Tuesdays.

Baby steps, but steps in the right direction.

# Adventures

Much [like I did last year](/posts/2018-in-review), in 2019 I'll be tracking my adventures for later display. Unlike 2018 though, I am trying to only keep track of those events/experiences in which I did something novel/new/adventurous. It is my hope that doing so will 1) highlight the good, memorable times of life and 2) reduce viewer fatigue caused by bad signal:noise a la my 3+ minute recaps of random geo-related things:

<iframe width="560" height="315" src="https://www.youtube.com/embed/AnYlr8gjOLo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

These memorable events will obvs have relevant memories to be attached so I'm considering ways in which to surface that to viewers to make it inviting, engaging, and authentic wrt my experience without being too much work (read: I like things that scale).

If you want to come along and live some of these memorable experiences (I can't promise they will all be memorable but the more you do the higher the odds), I've recently experimented with expanding my [Hamventures NYC](https://www.facebook.com/groups/155991128450556/) outreach by posting weekly round ups of events I'm interested in here in NYC.

For example, here's [last week's hamventures](/posts/hamventures-nyc-week-2019-01-27).

I don't actually go to all of these because no one will sell me a real time turner (if you have one pls dm me!), but I only list things I'd have high odds of going to in either this or a very similar universe.

If you want to stay up-to-date with these events, either join the Hamventures NYC group linked above or subscribe to the blog via RSS (instructions to do so can be found in the [Subscribe section](https://hamy.xyz/subscribe)). I do send out a monthly email but those usually only contain release notes and adventures happen weekly.

# Other announcements

Aside from that, not much to share. [My new build/deploy paradigm](https://labs.hamy.xyz/posts/labs-hosted-on-gke/) has been working awesome so I've been able to spend more time creating content and less supporting it. The highlights:

Blog

* [2018 in review](https://blog.hamy.xyz/posts/2018-in-review/) - my flagship reflection for 2018
* [Introducing Tikkun Olam Tuesdays](https://blog.hamy.xyz/posts/introducing-tikkun-olam-tuesdays/) - I mentioned this above, but provides a deeper explanation of my intentions / plans
* [How I get shit done](https://blog.hamy.xyz/posts/how-i-get-shit-done/) - the first of a series of slightly snarky but ultimately authentic responses to common questions I get

Labs

* [How I publish project demos alongside my Hugo blog](https://labs.hamy.xyz/posts/publish-static-sites-alongside-hugo-blog/) - exactly what it says

# fin

That's it. Hope your resolutions are still alive and kicking through the first 1/12th of their expected life span. I've got tons of OKRs to share in future months so don't let me outshine you.

Live long and prosper,

-HAMY.OUT