---
title: "By starting a Roth IRA and investing $6,000 / year I can make ~$8,300 more over 15 years (~$550 / year) vs a taxable index fund"
date: 2020-02-08T14:02:32-05:00
subtitle: ""
tags: [
    "money",
    "thevalueofmoney",
    "investments",
    'finance'
]
comments: true
---

I often think about my habits and their long-term effects. Lately, I've been giving particular focus to my money-saving habits. It's one of those things that's immensely powerful and easy to do, but also something that's easy to miss / forget about until it's too late.

At 25, I think it's high time that I get all my assets in order so that I'm charting the best course into the future I can and making the most of "the most powerful force in the universe".

# the Roth IRA

One move that's been on my radar for awhile is starting to invest in Roth IRA / traditional IRA. Over the last few months I've slowly been learning more about them and today I decided to sit down and really understand the pros / cons which eventually led to me re-allocating funds to them.

Here I'm going to list what I know (or at least think I know) about them as well as my calculations for how they benefit me for posterity. I'll note that I am by no means a tax / investment expert so take this as a snapshot of my knowledge but not as a recommendation / guarantee to you. In other words, do your own research and don't sue me for anything I say here cause I don't know if any of this is actually right.

Moving on.

The IRA / Roth IRA is another tax-efficient vehicle for your money, similar to the 401k. Similar to how a 401k has a Roth and Traditional form (the former being taxed now and not taxed later and the latter not being taxed now, but being taxed later) the IRA has those as well.

The main differences (that I saw) were:

* IRA has a low contribution limit ($6,000 / year) and a low cap on income before you're ineligible
* Roth IRA has a similar contribution limit, but has the ability to backdoor to get around that limit (but still capped at ~$57,000 as per 401k policy). Using this backdoor, you can get around the income cap.

I hit the income cap so IRA is out of the question for me, so diving into Roth IRA.

The Roth IRA is a tax-efficient vehicle in two ways (remember it doesn't get up-front tax deferrence, which is why it's not listed here):

* You aren't taxed on earnings when you withdraw your money (which happens when you sell stocks / positions / whatever to actually get useful money out). This is because you already paid your taxes up front. This results in ~15% in savings depending on your long-term holdnigs tax bracket.
* You aren't taxed yearly on in-fund turnover which varies in importance depending on the type of fund you're in. This was news to me but apparently you have a tax liability when your fund sells stuff and makes profit even if you don't sell anything yourself.

I mostly use Vanguard index funds which they say is [a tax-efficient holding by nature](https://investor.vanguard.com/investing/taxes/tax-saving-investments) so, for me, the real benefit lies in the tax break on long-term holdings.

Real quick:

* capital gains on long-term holdings - the tax rate that you are charged when selling a holding that you've had for more than a year
* ranges from 0% - 20% but 0% is < $40,000 / year and if I'm in NY probably need more than that and 20% is for > $400k which seems unreasonable unless I get super rich somehow, so 15% is my number
* selling within a year has v high tax rate so probably don't do that unless you really need to

So basically the difference for me between Roth IRA and my normal investments ins the 15% break on earnings.

Let's see how it plays out over 15 years (I like this time frame cause I'll be 40 then and that seems like an "adult" age so it's the money I could have by #adult time, plus it's long enough to actually see systemic differences).

Let's say that I'll invest $500 / month that was going to a taxable Vanguard to instead go to my Roth IRA. We'll say I'll invest that money in the same fund to keep things simple.

The value of my savings is then calculated as the capital gains I would realize over the period of investment, multiplied by the tax rate of 0.15. I don't want to do all the calculations by hand so I built [a new calculator](https://thevalueofmoney.xyz/calculators/rothiravstaxableindex?initialValueDollars=0&additionalValueDollars=500&timeGranularity=month) into thevalueofmoney that calculates this for me.

[![My savings w Roth IRA](https://storage.googleapis.com/iamhamy-static/blog/2020/one-off/thevalueofmoney-rothiravtaxable-mysavings.png)](https://thevalueofmoney.xyz/calculators/rothiravstaxableindex?initialValueDollars=0&additionalValueDollars=500&timeGranularity=month)

With inputs set as:

* initial investment - 0
* additional investment - 500 / month
* time horizon - 15 years
* interest - 6%

We get a total savings opportunity of ~$8,300 by using a tax efficient vehicle instead of a non-tax efficient vehicle on an investment of $6,000 / year which is equivalent to making an extra $500 each year.

[![Max savings with Roth IRA](https://storage.googleapis.com/iamhamy-static/blog/2020/one-off/thevalueofmoney-rothiravtaxable-maxsavings2.png)](https://thevalueofmoney.xyz/calculators/rothiravstaxableindex?initialValueDollars=0&additionalValueDollars=2083&timeGranularity=month)

If we went above the $6k / year, up to let's say $25k a year, the savings increase proportionally to about $34k. I'm no where near doing this, but if you could it'd be like making an extra $2,200 each year just by using a tax-efficient account instead of a regular one.

# my plan

So Roth IRA definitely seems the way to go. It's a bit more efficient and while there's a cap, I'm way off from hitting it so might as well squeeze a little extra goodness out where I can.

Over time, I hope to increase that investment and approach the second figure in savings. #fingerscrossed