---
title: "System: Build a month"
date: 2020-12-19T14:55:11Z
subtitle: ""
tags: [
    'projects',
    'systems'
]
comments: true
---

I've been talking about my plans to build 1 business / month [for awhile](https://blog.hamy.xyz/posts/2020-h1-review/#looking-forward) now but I figured I'd sit down to really formalize it and hold myself accountable. 

I believe that the best way to be successful is to put yourself in the right place at the right time consistently. This won't necessarily lead to success by itself but it will make sure you're in the right position to seize that opportunity should it present itself.

This is how I think about startups and businesses as well. You can't know for sure what business is going to blast off and which will fail miserably. But you can try these businesses out and see if they have potential or not - are people interested as they likely would be in a 'blast off' scenario or are they disinterested as they likely would be in a 'fail miserably' scenario.

Thus I think you can optimize for putting yourself in the right place at the right time by experimenting with many different businesses and doubling down on those that show promise. By adopting this strategy you can minimize the cost of trying each business and multiply your speed as you get better at businesses in general. This leads to a snowballing effect of business validations that puts you lightyears ahead of other strategies when it comes to opportunities tried and right place / right time been.

All that to say that I think habitual creation is a great strategy for finding things that work. Personally I find it fun as well.

I'll note that this strategy isn't just for business but for all sorts of creation types and problem domains. Business just happens to be a good concrete example of this.

So my build strategy for 2021 is as follows:

Each month:

* Validate 2 businesses
* Build (and launch) 1 business
* Build 0.5 art

To a productive 2021.

-HAMY.OUT