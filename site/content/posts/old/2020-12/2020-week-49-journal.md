---
title: "2020 Week 49 Journal - !phone"
date: 2020-12-09T03:19:44Z
subtitle: ""
tags: [
    "journals"
]
comments: true
draft: false
---

I'm late posting last week's journal so I'm going to keep it short. 

Top of mind for me is my new system of checking my phone less. I started this last week as an experiment to increase my productivity, inspired by a chapter of [Atomic Habits](https://amzn.to/3qIN57o). In the chapter the author mentions productivity gains from logging out of social media and placing your phone in another room while you work.

I'm not ready to quit social media yet - after all, [I work at Instagram](https://www.linkedin.com/in/hamiltongreene/) - but I did want to thoroughly vet this exercise. My system:

* While I'm doing something productive, place the phone out of reach (even if it's like 2 inches out of reach)
* Remove social media icons from phone home page
* No endless scrolling - just check notifications and a scroll or two

By doing these simple things, I've found myself more present, focused, and calm than I have in a long time. It's a similar step-change in presence as I felt when I started meditating (as I describe in my [2019 review](https://blog.hamy.xyz/posts/2019-in-review/#meditation-and-friends)).

My hypothesis is that this is caused by more mental 'downtime' or empty space for my mind to just be. This takes the place of the habitual scrolling I used to do whenever I had some free time / wanted a break. 

I think this works in two ways:

1. Provides time to recover from the previous task
2. Doesn't load spurious information from the endless scroll

Now I'll note that this change isn't all sunshine and roses. This extra focus also has a downside. I've found myself with the ability to game anxiety-free like I used to in my pre-college years. In the past week I've clocked ~20 hours which I think is rather impressive.

I've got some work to do to quell my rediscovered gaming affinity but I think this is a system I'll continue going forward. It hasn't made me more productive yet as measured by hours spent but it has certainly made my time and effort allocations more intentional. I think that's a win.