---
title: "System: Retro Dinners"
date: 2020-12-27T17:37:16Z
subtitle: ""
tags: [
    'systems',
    'relationship'
]
comments: true
---

[I live with my partner](https://blog.hamy.xyz/posts/2020-q3-review/#moving-in-with-megna) in a one bedroom in Manhattan. We've stayed in NY throughout almost all of the 2020 stay at home orders. What this means is we've spent a lot of time together in close quarters which has (in conjunction with other factors) led to high tensions.

One system that I've found helpful throughout our time together - particularly so during these times of high stress and low personal space - is our 'Retro Dinners'. It's a system constructed from many different systems of retrospection and communication I've come across (one of which is the agile retrospective system).

We employ it to regularly catch up on any and all pressing issues, to really listen and communicate with each other, and to ensure we are on the same page. Here's how it works:

# System: Retro Dinner

Once a month, near the end of the month, we do a retro dinner. We choose a restaurant we want to go to and we'll have dinner there. 

I think it's important to try and do these outside the home as it kind of liberates you from any cycles / feelings that may be caught up in your habitual surroundings like your kitchen / living room / bedroom. Also I like exploring new places so that's an added bonus. We've also done these successfully at home so if eating out isn't available to you due to covid restrictions, finances, or otherwise don't sweat this part too much - maybe do a picnic outside or in a weird part of your house.

Now before I go further it's important to note that I don't consider this a date night, more a business meeting. We are really here to assess our relationship, how it's going, any issues we've noticed, and our plans to solve for that in the coming cycle. I think date nights are a great way to regularly celebrate the connection you've built and maintain but think those should be in addition to this night, not in place of or merged into it.

The split and ordering of how we go through each topic varies depending on how we feel and what's top of mind but we always make sure to give each person a chance to talk through each of these buckets and to allow for all further discussion.

What's key here is that this whole thing is really about listening and understanding your partner. So whatever is required to do that you should do. Usually this is listening though sometimes it's followup questions and discussion topics. When in doubt, default to listening. You'll usually learn more that way.

Oh yeah last thing. Be honest. None of this works if you're not being completely honest.

Retro Dinner topics (each person):

* Last month:
    * Grows - Opportunities / areas for improvement 
    * Glows - Highlights / things you really liked
* Future:
    * Plans - Goals and how you're going to get there
    * Vision / Dreams - Long term feelings and direction
* Discussion - Any specific points to discuss
    * Things that came up as part of retro
    * Things we wanted to talk about (Often populated during month / before retro)

I've found it useful to keep a shared doc (like a Google Doc) that you can both refer back to and edit. This serves as both a log of all the things you've talked about and a way to schedule new things to discuss during future dinners.

# Fin

I'm always looking to make [my systems](/tags/systems) better so if you have any feedback, suggestions, and / or your own system of planning and communication please send them my way!

In communication and understanding.

-HAMY.OUT