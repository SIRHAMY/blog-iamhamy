---
title: "2020 Week 50 Journal - Do it right"
date: 2020-12-13T14:10:23Z
subtitle: ""
tags: [
    "journals"
]
comments: true
draft: false
---

# Top of mind

December is a month of reflection for me. I reflect on the year, write my [yearly review](/tags/reviews), and set my goals for the future. One phrase I've been thinking about this week is 'doing it right'.

I build a lot of systems to optimize my life. I have goaling systems, project systems, budgeting systems, etc all with the goal of making me do x 'right' each time. These systems take time to create and effort to upkeep. This system of systems I've created isn't the fastest method of moving through life as I must continually expend time and energy on the systems vs doing the thing the system was built for.

But this has never been a problem for me. I think it's possible to over invest in systems and never get anything done but I don't think I'm there. I've been thinking about how to put my pro-system determination into words and this is the best I've come up with.

There's a quote that goes something like this:

"If you don't have time to do it right, you must have time to do it over."

We can see this with an example. If task x takes y time to do then to fail and do it again could be considered to take 2y time to complete. Whereas if we take extra time to do it right so we don't have to do it over - even if that time is 50% more - it comes out to 1.5y, considerably less than the time it took to do it over. 

Now this is an extremely contrived example but I think it gets the point across. While doing it right may take some extra time and effort in the short term, it may also reduce the effort ceiling in the worst case. Not to mention that 'doing it right' often builds better skills and habits for the future, meaning you're guarding against the worst case and making all cases better / faster / more efficient.

All this to say that I think it's okay to take things slow, so long as the slower process has attractive trade offs compared to the faster one. In many cases it's okay to move slow in the short term to move fast in the long term.

# Releases

* [2020 Review - Finances](https://blog.hamy.xyz/posts/2020-finances-covid-edition/) - This week I released my 2020 financial review. I'm trying a new thing this reflection cycle where I spin off large reflection sections into their own posts as a way of making the creation and sharing more efficient.
* [LineTimes](https://linetimes.xyz) - I'm soft launching my latest creation LineTimes. It's a web app that gives you real-time updates on line / wait times at stores around NYC. Starting off with Trader Joe's in Manhattan and looking to expand from there.

# Stresses

Moving slow - As I mentioned in my top of mind, I've been stressing a bit about how slow I'm moving. 

For a concrete example, I had originally planned to release LineTimes the week of Thanksgiving. I think that timeline was pretty ambitious but I still expected to be done last weekend. The main issue has been how many different technologies and interfaces I'm having to learn to stand up my service. This will benefit me long-term as I get acquainted with tools I'll continue to leverage in future projects but there needs to be a balance between doing it right and getting things done.  

I'm still learning to find this balance - all I know is that my current pace won't allow me to hit my goals of validating 12 businesses, launching 6 businesses, and building 3 monoliths each quarter.

To resolve, I'm going to be more diligent in executing my systems and more conservative in the project scopes I take on each month.

# Learnings

Been working with a lot of technologies this week:

* Google App Engine
* C#
* dotnet core
* Docker
* NextJS
* React
* GitLab CI

# Gratitude

Money - I'm grateful that money hasn't been a challenge in my life. 

Particularly now as we enter the 10th month of the pandemic, I can't imagine what it must be like to have lost your job and to have been hemmorhaging money to survive all year. For many this time will have a lasting effect on their financial outlook - not to mention the many other aspects of life that may be effected.

So today I'm grateful for [my financial security](https://blog.hamy.xyz/posts/2020-finances-covid-edition/), to have [a job](https://www.linkedin.com/in/hamiltongreene/) that allows me flexibility in how I work and what I work on, and to have the support of friends and family to discuss and strategize for better decisions down the road. 

In this giving season, I'm considering ways I can continue to give back. As you think of gifts for loved ones, I encourage you to add those in need to your list as well.

Happy Hannukah,

-HAMY.OUT