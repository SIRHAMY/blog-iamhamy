---
title: "2020 Week 52 Journal - 2020 != 2021"
date: 2020-12-28T02:41:17Z
subtitle: ""
tags: [
    "journals"
]
comments: true
draft: false
---

HAMY: file name - `2020-week-x-journal.md --kind journal`

HAMY: Reference - https://paper.dropbox.com/doc/Review-Release-Notes-Journal-System--BAWUNOiF_LfhL91H0GVUiJ8xAg-sQdWAlVM1Ocy6UKZxFmeW

# Top of mind

This is the last full week of 2020. I've been working a lot on my [reflections](/tags/reflections) and my plans for 2021. 

One thing I've been thinking a lot about is my hope for 2021 to be better than 2020. I think it has the potential to be better but there's no guarantee. The only way we can really expect it to be better is if we put in the work to make it so. 

I'm making plans. I think you should, too.

# Releases

* Released [2020 Review - HAMY.LABS](https://labs.hamy.xyz/posts/2020-review-hamy-labs/)
* Committed to [Systems of Accountability for Productivity](https://blog.hamy.xyz/posts/systems-of-accountability-for-productivity/)
* Detailed [System: Retro Dinners](https://blog.hamy.xyz/posts/retro-dinners/)

# Stresses

I'm most stressed about wrapping up 2020 and giving 2021 room to grow. I always try to leave ample time at the end of each half to wrap up my work and transition into the next but it always seems like it comes down to the last few days.

This honestly isn't a very big stress. My new systems of being and subsequent systems of accountability have really done wonders to calm my mind and get stuff done. But it's important to me so I'll continue to keep it in my awareness.

Actions:

* Just keep executing on my systems

# Learnings

I think most of my learnings were in the form of 2020 EOY stats. I'll be publishing those with my 2020 Review later this week. Stay locked.

# Gratitude

* Megna's family - I'm grateful that Megna's family has been in New York for the past few weeks. It's been a nice respite from the routines we've built over the last 10 months.
* Friends - I've been digging through my camera roll to find good photos to feature in my 2020 review and messaging a lot of people along the way. I'm ever grateful for all the connections I've made and adventures I've shared along the way. 2020 wasn't the most productive in this arena but there were still many good times and it serves as a reminder of how much these mean to me.

Let's put a nail in this coffin.

-HAMY.OUT