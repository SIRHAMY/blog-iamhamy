---
title: "2020 Week 51 Journal - Launching LineTimes"
date: 2020-12-20T14:39:43Z
subtitle: ""
tags: [
    "journals"
]
comments: true
draft: false
---

# Top of mind

For the past few weeks I've been working on [LineTimes](https://linetimes.xyz) - a site that calculates the line times at local stores so you can make informed decisions about when to go. I came up with this idea almost a year ago but finally started building it last month and launched the MVP yesterday. 

It was a great experience and I learned a lot. Those learnings will serve me well for the many projects I have slated for 2021.

But I still have the feeling that there's so much more to learn and do. I honestly thought I could crank this thing out in my week off for Thanksgiving. 

But it took me another 3 weeks to complete - 300% more time than I'd originally budgeted with a grand total of 70 hours spent at MVP launch time.

70 hours isn't _that_ much. It's about 2 weeks of work at a normal job and I think it's reasonable to say "I launched x app in 2 weeks". But I think I should be better than that as I spend so much time building things and thinking about building things. 

This matters because 4 weeks of real-time to push out an app is just too costly for [my #buildamonth strategy](https://blog.hamy.xyz/posts/build-a-month/) of validating 2 businesses, building 1 business, and building 0.5 art each month. This strategy relies on the ability to stand up quality software in very short amounts of time in order to unlock quick iterations to maximize opportunities handled.

On the brightside I think I'm heading in that direction. A lot of the time I spent wasn't straight development but learning new technologies and how to get them to do what I needed to do. I picked up / brushed off four technologies that I've had limited or no exposure to in the last year for [the LineTimes build](https://labs.hamy.xyz/projects/linetimes/). I think I'm pretty warmed up on these and the next build on this stack will be 10-50% faster depending on similarity and how soon I do it. That'll put me squarely in the 2-3 week bucket, likely giving me enough time to allocate to the rest of #buildamonth.

I'll be switching gears for the last 1.5 weeks of 2020 to focus on my [reflections](/tags/reflections) and maybe make some new generative art in preparation for [Genuary 2021](https://genuary2021.github.io/). I'm stoked I was able to release my first real business this year and while I've got a lot more to do to reach my ideal self, think I'm in a good place to make that happen in 2021.

L'Chaim!

# Releases

* [LineTimes](https://linetimes.xyz) - Released my first 'real' business - a site that calculates the line times at local stores. Now live for Trader Joe's in Manhattan, NY.
* [2020 Review - Finances](https://blog.hamy.xyz/posts/2020-finances-covid-edition/) - I actually released this last week but this week I made some adjustments based on feedback from people. In the process I found out that I'd been overspending by ~37% every month wrt my budget but that it didn't effect many of my calculations because my 'self-healing' budget strategy was absorbing that over spending without it effecting my net worth. That's a validation vote for that strategy.
* [Build a month](https://blog.hamy.xyz/posts/build-a-month/) - I already talked about this here but I'm formalizing my plan to #movefastandbuildthings in 2021. I think this strategy has a good balance of making me build things that I like to build and ensuring I'm not over investing in an area that's not bringing me value. More to come as I work through this process.

# Stresses

My biggest stress was getting LineTimes out the door. I've already shared a bit about how I was frustrated with my slowness in the development process. I think this was largely due to my newness to the technology stack and that there wasn't much I could do to avoid it save choosing a different stack I was more familiar with.

After paying this initial cost, I just need to make sure that I'm getting benefits from it - that this stack and my dev processes are speeding me up and not slowing me down. Logically one of the big reasons I chose this stack was because it was so flexible and had the ability to handle pretty much any project I could throw at it. So I have pretty high confidence that this stack won't unreasonably hamstring me in the future.

Actions to reduce this cost:

* Continue to learn this stack
* Leverage these technologies and my learnings in future projects
* Look for chokepoints and systematize to make more efficient / remove entirely

# Learnings

* I learned [Genuary 2021](https://genuary2021.github.io/) existed and am excited to use it as an impetus to dive back into some art
* I learned that [I've been overspending my budget by ~37%](https://blog.hamy.xyz/posts/2020-finances-covid-edition/) on average this year (brought about mostly due to large one-off purchases in the past 2 months)
* I learned how to use [colors in Tailwind](https://tailwindcss.com/docs/customizing-colors) as part of building LineTimes. The more I use Tailwind the more I love it. It's simple and powerful.

# Gratitude

* I'm grateful for all my friends and family who have continued to support me in my project endeavors over the last several years. I got numerous messages after launching LineTimes offering kudos and feedback and it was a great reminder that the internet can be a very good way to connect in these isolated times.
* I'm grateful for [Megna](https://www.instagram.com/megna.saha/) who continues to question my ideas and arguments no matter how sure I am of their validity. It keeps me on my toes and ensures that I'm validating my own arguments before presenting them to the masses.
* I'm grateful to work for a solid company, to be able to work from home, and to be getting multiple days off in the next few weeks. Money and employment hasn't been a problem for me in 2020 and I want to continue to recognize that as a major factor in the stability of my life these tumultuous times.

Have a happy holidays.

-HAMY.OUT