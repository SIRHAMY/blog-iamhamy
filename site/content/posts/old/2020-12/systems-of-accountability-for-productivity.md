---
title: "Systems of Accountability for Productivity"
date: 2020-12-23T13:43:35Z
subtitle: ""
tags: [
    'systems',
    'productivity'
]
comments: true
---
        
Historically, I have a track record of missing my goals. In [2020 H1](https://blog.hamy.xyz/posts/2020-h1-review/#projects) I goaled on making $1000 on side projects and ended up making < $100. In [2019](https://blog.hamy.xyz/posts/2019-in-review/#projects) I made $0.

Now I think it's okay to miss goals. I like goals to be 50/50 as in I should expect to hit around 50% of the goals I set. I think this allows goals to be hard enough that you need to push yourself and reachable enough that you could still attain them. 

However I also believe that when you see a problem you should fix it. In my case, I've been habitually missing my project goals. If I don't change anything then I can only assume that I'll miss once again. That seems like a mindset of complacency, something I do my best to avoid.

I think the problem is that my systems don't enforce good results. I think flexibility is good - which is one reason why I use 50/50 goals - but when it becomes a pattern of failure, something needs to change.

# Systems of Accountability

To combat this, I'm instituting systems of accountability - systems whose purpose is to keep me focused and productive in my other systems and, by proxy, to push me towards my goals. I think about my core systems as pulling me towards my ideal self and my systems of accountability as guard rails against lapsing into my suboptimal self.

I got the idea to implement this system from reading [Atomic Habits](https://amzn.to/3nOtNvs). It defines a fourth law of habit creation as "Make it satisfying". You can help the creation of a habit by making that habit satisfying. Inversely you can obstruct the creation of a habit by making it unsatisfying (the inversion of that fourth law).

So here I wanted to come up with ways to make it unsatisfying for me to miss on my project goals. Further, I wanted to make it unsatisfying to miss on my project habits and systems. After all, these habits and systems are my best strategy to hit those goals. 

I've done this before with the creation of my [Earth Tax](https://blog.hamy.xyz/pages/principles/earth-tax/). Every time I do something bad for the Earth like toss non-recyclable object or order lamb / steak, I tax myself. I send those funds to [HamForGood](https://labs.hamy.xyz/hamforgood/) organizations supporting the environmnent. The most important part however is just that I tax myself. By taxing myself money (which I value) I'm making the act of doing bad things for the Earth unsatisfying. This helps curb this behavior while simultaneously helping to right that wrong.

Here's the system of accountability I've created to help me attain my goals in 2021:

* Take a money goal for a HamForGood cause each half
    * If I don't hit this goal then I must personally fund the difference
    * Example: I take a half goal on raising $100 for a cause but only end up raising $75. That means I need to pay $25 to that cause.
* Habit tax
    * Each time I miss a habit:
        * Take an amount of money equivalent to the miss streak from my entertainment budget and put it in my HamForGood (personal) budget
            * Example: 1 miss = $1, 2 misses = $2 dollars. So if I missed 2 days in a row, I would need to pay $1 + $2 = $3 to HamForGood (personal) budgets
        * Block an amount of contumption (content consumption like video games and tv) days equivalent to the miss streak
            * Example: 1 miss = +1 day !contumption, 2 misses = +2 days !contumption. So if I missed 2 days in a row, that would lead to 3 days of !contumption

A few notes:

* The Habit Tax goes towards [HamForGood (personal)](https://blog.hamy.xyz/pages/systems/hamforgood/) which is different than the HAMY.LABS HamForGood (though they share the same causes). This one is personal and funded from my personal accounts so money that goes here does _not_ go towards funding any goal difference.

I think this system will do 2 things:

1. Push me to set realistic project goals and fight to attain them
2. Keep me from missing my habits most days and from missing twice almost always 

In productivity.

-HAMY.OUT