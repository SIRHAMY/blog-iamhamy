---
title: "2020 Review - Finances"
date: 2020-12-09T04:00:15Z
subtitle: ""
tags: [
    'finance',
    'money',
    '2020'
]
comments: true
draft: false
---

I utilize many [systems of reflection](https://blog.hamy.xyz/tags/reflections/) to better understand, measure, and optimize my life. Finances are a major facet of these analyses as money underlies most everything. This post will be my first standalone financial review to provide room for deeper analyses and to share more of what I've learned.

# 2020 Finances

2020 brought with it many changes - good and bad. Here are a few that had significant impacts on my financial outlook:

* COVID - I live in NYC which is usually characterized by expensive drinks and full social calendars. With the onset of COVID, less places have been open and people have - thankfully - been much less social. This means I've been staying in more and spending less on the drinks, Ubers, dinners, tickets, and covers that usually line my credit card statements. Not to mention the several trips around / out of the country I've postponed until everything settles down.
* Moved in with Megna - At the [end of July](https://blog.hamy.xyz/posts/2020-q3-review/#moving-in-with-megna) I moved in with my partner. This had two major financial impacts: 1) I was now paying 1/2 rent and 2) we started cooking more at home and eating less meat.
* Paid off all debt - At the end of February I paid off the last of my student loans. That left me debt free with a few hundred dollars each month primed for investment.

## Savings Rate

While I have an entire library of different systems I use to govern my life, I like each to be simple and measurable. My finances are no different. I've chosen savings rate as my primary metric for tracking the health of my finances.

Savings rate is literally the rate at which you save. It can be described by this function: `savings_rate = money_saved / money_earned` where `money_earned = money_saved + money_spent`. I like it because it speaks to the sustainability of your financial practices as dictated by your own circumstances.

I'm not the only one who sees it this way. Savings rate is a staple of Financial Independence Retire Early philosophies as it directly measures how you live with respect to your means and indirectly how long it would take to build a sufficient amount of wealth to support that lifestyle. But this post isn't about that so just trust me (or debate me in the comments) that this is a reasonable measure to goal on. 

_For more on FIRE: [The shockingly simple math behind early retirement](https://www.mrmoneymustache.com/2012/01/13/the-shockingly-simple-math-behind-early-retirement/)_

In [2020 H1](https://blog.hamy.xyz/posts/2020-h1-review/#my-spending-and-covid) I achieved a 50% savings rate. I was proud of myself as that was the highest rate I'd been able to achieve thus far in my life. But I knew I could save more.

In my analyses I found strange fluctuations in my 'wants' categories, was already thinking about moving in with my partner, and was finding and [calculating new ways to save](/tags/thevalueofmoney) each month. All this led me to belive that there was room for optimization. So I set a new savings rate goal of 60% to see if I could hit it.

![2020 Savings Rate](https://storage.googleapis.com/iamhamy-static/blog/2020/2020-review/2020-review-finance-savings-rate-by-month.png)

_2020 Savings Rate_

Almost six months later, I was able to hit that with a savings rate of 61.8% for 2020 H2. I started off slow as I enjoyed the end of summer, then picked it up through the fall, and nearly wrecked myself in November with (absolutely necessary) black friday purchases.

## Expenses

I spend enough time at work and through [HAMY.LABS](https://labs.hamy.xyz) trying to make money so what I like to do in these financial reviews instead is take a look at my expenses and see where there may be room for improvement. After all a penny saved is a penny earned and when you're goaling on a high savings rate a penny saved is worth more like `1 / (1 - target_savings_rate_decimal)` pennies.

Overall, I actually overspent from my budget by about 37%. So while I'm pretty proud of the numbers I was able to hit this year I know there's a lot of room for improvement for me to get where I want to be.

![2020 Spend x Save breakdown](https://storage.googleapis.com/iamhamy-static/blog/2020/2020-review/2020-finances-spend-and-save-breakdown.png)

_2020 Spend x Save breakdown_

Looking at the spend and save breakdown gives the savings rate a bit more context. In some months I spent more and in others I made more. It looks like I settled into lockdown pretty well keeping relatively flat spending rates from March through August. You can even see a slight upward trend that likely coincides with the reopening of venues.

Then it looks like I have a little spending splurge in August when I moved, I settle down, then cap the year with my highest spending of 2020 in November.

![2020 Spending breakdown by category](https://storage.googleapis.com/iamhamy-static/blog/2020/2020-review/2020-finances-spending-breakdown-by-category.png)

_2020 Spending breakdown by category_

We can now dive into my spending by category and see where some of those expenses are coming from. We can see that the two most prominent buckets are needs (food, housing, health, etc) and wants (restaurants, entertainment, purchases, etc). At the end of July we can see a sharp decline in needs expenditures in large part due to moving in with Megna. We also see a bit of a dip in wants through the early half of the year with a few spikes towards the end.

Looking at this I think the spikes warrant the most investigation. It seems that these last few months would've consistently led in savings rate this year if it wasn't for them.

Diving into these spikes it seems like the majority come from various capital improvement purchases I made.

* In August, I bought an office chair and a new computer to outfit my WFH office. 
* In September I bought a camera to accompany [me on my adventures](https://www.instagram.com/sir.hamy/) and to up [my YouTube game](https://www.youtube.com/channel/UCPBY44jxP7gOMkUsP5rlGnw?). 
* Then in November I caved to the black friday sales by purchasing various clothes, electronics, and video games.

_Update 2020.12.19: I've updated this section to only include spending categories and to include a target budget breakdown._

![2020 Overall spending by category](https://storage.googleapis.com/iamhamy-static/blog/2020/2020-review/2020-total-spend-vs-category-v2.png)

_2020 Overall spending by category_

![2020 Budget by Category - Spending](https://storage.googleapis.com/iamhamy-static/blog/2020/2020-review/2020-budget-by-category-spending-2.png)

_2020 Budget by Category - Spending_


This is supported by comparing my actual spending on capital improvements of 28.2% of my total spend with my budget for spending on capital improvements of 4.7% of total spend. Major overspending here. 

I'll note that because I overspent by 37% these values are likely lower than they would be otherwise since each category is out of 100 when it should be out of 137 to be representative with respect to magnitude. But I'm too lazy to do that today so we'll continue with the donuts.

Some other areas of note:

* Rent is over budget (28.3% / 25.7%) because I just used my current budget for this whereas I was paying my old rent for half the year which was substantially higher
* hamy.labs is actually under budget at (4.9% / 6.7%) despite some likely unnecessary purchase throughout the year
* Vacation (1.7% / 8.8%) and entertainment (9.1% / 13.5%) are way down likely due to me staying indoors and not traveling for most of the year. In fact, I'm a little surprised entertainment isn't down _more_ considering the tiny amount I actually went out. 

All in all I'm not that upset about this. While I value frugality, I value allocating resources in a way that matches my values more. I've spent a lot of time at home at my desk building new things in 2020 and many of these purchases augment that experience.

I think I'd do well to limit my big purchases in 2021 to prevent spending spikes and to ensure I'm getting my money's worth out of these big purchases. Now that I'm stocked up on tech, I don't foresee this being much of a problem. 

_Tune in next year to find out._

## Net Worth

The last thing I want to touch on is my net worth. Whereas my savings rate is a good measure of how I'm doing at a given moment in time, net worth is a good measure of how I'm doing over a long period of time. 

My long term goal is to achieve a net worth that allows me to FIRE - even though I don't intend to ever 'retire' in the traditional sense. Ultimately what I'm really after is the FI - the Financial Independence - in FIRE as I believe this will afford me greater freedom and capability to optimize my life for my values.

Now for this to work, I need my money to grow in some way. I've already talked extensively about my savings rate above which is my primary way of growing my money. The other is another foundational pillar of FIRE - the index fund.

Pretty much all of my investments are in index funds. It's passive, has reasonable returns, and historically is an extremely safe bet. I've come to accept that I'm an extremely risk-averse person and I see no problem with taking a risk-averse stance with index funds.

*Read more about index funds: [Index Funds on bogleheads.org](https://www.bogleheads.org/wiki/Index_fund)*

![2020 Networth](https://storage.googleapis.com/iamhamy-static/blog/2020/2020-review/2020-networth-over-time.png)

_2020 Networth_

Despite all the ups and downs of the year I was extremely fortunate to be able to grow my net worth by 81% year over year. This number is a bit inflated due to my relative new-ness to the financial focus / investing world but it's still a big accomplishment in my overall financial outlook.

The most surprising thing about this graph to me is how stable the slope seems to be month over month despite my increases in savings rate. It's clear that there is a slope change somewhere around April but it's hard to know what part of that is attributable to my savings rate, what part to changes in the economy, and what part to other factors. 

This is one of the reasons why I don't directly goal on net worth - there are often too many factors at play to consistently impact and measure. I'll take this as a reminder of the slow-burn nature of my financial strategy and as a morsel of validation for its long-term efficacy.

## Room for improvement

Overall I'm pretty happy with my finances in 2020. I was lucky to continue to be employed in a high demand field despite all the issues effecting the world and to be able to leverage that position to grow my net worth. 

If I had to list three things to do better financially in 2021, I think these would be them:

* **Reign in spending on big purchases.** I've already talked about this in my expenses section but I made several large purchases in the last half of the year that caused my savings rate to drop precipitously. This kind of spending is not amenable to the kinds of savings rate goals I'm shooting for and is thus something I should work to curb and manage in 2021 so as not to fall into the trap of hedonic adaptation.
* **Get [HAMY.LABS](https://labs.hamy.xyz) cashflow positive.** HAMY.LABS has been my moonshot investment for the past several years. Whereas I regularly pump the majority of my money directly into my investment accounts, I also regularly allocate a small portion of that money to fuel my projects. My reasoning for this is two-fold 1) I value creating projects and 2) my projects have the potential to have outsized returns on this investment. To date, these projects have only lost money. However I've spent much of H2 strategizing, experimenting, and positioning myself to change this for 2021 and beyond. As I mentioned, it's a moonshot but I know I can position it for more chances to succeed in 2021.
* **Gather more granular financial data to fuel deeper financial analyses.** In this review I noticed that my spending categorizations weren't granular enough to easily dig into what areas had the most opportunity for improvement. For example, it would've been useful to be able to split my needs by things like housing, food, and healthcare. This could've provided insight into whether I was living optimally or if some months I became extraordinarily gluttonous and exceeded my food budget. Having the ability to compare my actual spending to my budgeted spending would also prove useful to more easily point out areas of improvement for each time bucket. 

If you have suggestions for things to improve - let me know! I'm always looking to make my systems better.

# Fin

I've still got a long way to go before I hit my FI goals but I think this year has been a good test case and validation for my financial systems. These systems sustained solid growth through major changes in my life and the world all while affording me a reasonable amount of opportunities to stop and buy the video games.

I'll be putting out my full 2020 [review](/tags/reviews) in the coming weeks so stay tuned for that if you're interested in learning about more facets of my life. I'm also planning on detailing a few of my financial systems before EOY. Let me know if there's a particular part that you want to know about and I'll prioritize it.

Live long and prosper,

-HAMY.OUT