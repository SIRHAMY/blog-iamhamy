---
title: "How I'm voting in the 2020 presidential election"
date: 2020-08-16T16:38:21Z
subtitle: ""
tags: [
    'elections',
    '2020'
]
comments: true
---

Elections are a critical part of a functioning democracy. They are the method by which individuals can voice their beliefs and, in turn, create tangible change in the societal structures in which we reside. In my [2020 H1 Review](https://blog.hamy.xyz/posts/2020-h1-review/#looking-forward) I committed to actively participating in this year's presidential elections. There are 78 days left, so it's time to start participating - to voice my beliefs and push for change.

In this post I'll explore my beliefs, the issues I care most about wrt government, and the stances that the leading parties have taken on each issue. By the end of it, I hope to have explained why I'm voting the way I'm voting and provided an example of how you might go through a similar comparison process.

# My American dream

Before diving into how I'm voting, I want to reflect on the core values and beliefs I hold with respect to the government. By starting here, I think it will be clearer why I hold different positions on issues.

## All humans are created equal

I believe that all humans are created equal. This doesn't mean that all people are the same, but it does mean that we're all just human at the end of the day. We come with all the pros and cons of our species. As such we should each be afforded the same treatment and opportunities as the next person. 

Doing anything else would mean that we don't believe that all humans are created equal.

## Government should be by the people, for the people

I believe the best governments are those that are run and supported by the people it governs and that work for the betterment of the people. Ultimately this creates a positive feedback loop, which is critical for complex systems to flourish. 

If a government is not by the people, then it's unlikely the incentives and priorities of the ruling class will align with that of the common person. If a government is not for the people, it means it's siphoning off the products of the people's labor for its own means - essentially a parasitic relationship.

## We should do more good than evil

It's not practical to be 100% good 100% of the time. Life is about tradeoffs. But it's not hard to be more good than evil. We should always make sure that the effect we're having on the world is more positive than negative. If it's not, we need to stop and change some things.

# Ranking the candidates

For this ranking process, I'm going to list the issues that are most important to me right now, how each candidate plans to address them, and how they compare. 

I'm only going to be researching and ranking the views of the Democratic and Republican parties. I'm doing this because I think one of them is going to be the party that wins with a very high probability - like > 99% probability. I don't think this is how our government should work as I'm rarely 100% aligned with either of these parties. I believe this is a factor of the 2 party system we've created through winner-take-all elections (you can learn more about this in Hasan Minhaj's [We're doing elections wrong](https://www.youtube.com/watch?v=MykMQfmLIro)). I think we could solve this by moving to ranked voting, but I also don't think such a large move will happen before November 3rd. Thus the most impactful thing for me to do is focusing on winning the game by its current rules, then pushing for a change of rules once the round is over. One thing at a time.

To compare the candidate platforms I'm going to implement a simple system:

* Each issue and relevant policies will get scored on a [-2, 2] scale
* I'll assign Dems the negative side and Reps the positive side.
* Higher absolute value means the ranking favors one side more. So if I strongly favor a Rep policy for a given issue, I'll assign a 2. If I slightly favor a Dem policy, I'll assign a -1.
* At the end I'll tally up my scores. More negative means more Dem. More positive means more Rep.

Let's get to ranking.

## The environment and climate change

For me, the biggest issue facing our world today is the destruction of the environment and climate change. If the world dies, we all die. I don't want to die. I'd guess most people don't want to die. So I want a government that pushes hard to prevent this inevitable doom in a way that leaves as few scars as possible.

While this environmental destruction affects everyone, I also want to point out that it'll likely start affecting poorer people sooner and with greater magnitude. Because the world already has entrenched wealth gaps along racial and geographic (among others) lines, this means that environmental destruction is also an equality issue.

I'm voting based on environmental issues because it's an equality issue, will adversely effect the people if not stopped (gov't for the people), and research seems to suggest that we're causing it which means we're being evil.

I tried finding the official Trump campaign's platform on the environment but was only able to find a single [Energy & Environment](https://www.whitehouse.gov/issues/energy-environment/) page on the Whitehouse website. I don't know if this is because my search history skews progressive or if they don't actually have much published content but it doesn't bode well for my process if I can't find official platforms. In its place I'll try to pull from official sources as much as I can.

* Trump has reversed (or is in the process of reversing) a least 100 environmental rules that limit carbon emissions and govern clean air, water, and toxic chemicals. [Here's a full list.](https://www.nytimes.com/interactive/2020/climate/trump-environment-rollbacks.html)
* A few pro environment things were passed by the admiinistration like a bill to clean up ocean plastics and increase the amount of public land under federal protection but these are eclipsed by subsequent rollbacks to public land protections, nonrenewable energy pushes, and various rollbacks of environmental protections despite public health risks. [National Geographic](https://www.nationalgeographic.com/news/2017/03/how-trump-is-changing-science-environment/)
* [350org](https://350action.org/2020-tracker/) (the policy-focused pro environment org I currently support through [HamForGood](labs.hamy.xyz/hamforgood) and personal donations) marks Trump as missing all four categories required for political endorsement

Biden's environmental plan has a [whole page](https://joebiden.com/climate-plan/) dedicated to it.

* [$1.7 trillion plan to reach net zero emissions by 2050](https://joebiden.com/climate-plan/)
    * limits to methane pollution from oil and gas operations, greater restrictions on vehicle fuel economy and a push for electric vehicles
    * federal government to modernize its facilities and vehicles to be more eco friendly 
    * requirements for companies to disclose and monitor the climate risks and climate impact of their operations
    * increase in land under federal protection and changes in rules around usage to support reforestation, protect biodiversity, and account for the climate impact of operations
    * The creation of ARPA-C - a cross-agency Advanced Research Projects Agency focused on climate change with various potential projects like increasing the efficiency of electric production and storage, researching carbon-free alternative fuels, and reducing the cost of environmentally friendly fabrication
    * Rejoin the Paris Agreement
* [350org](https://350action.org/2020-tracker/) marks Biden as passing 1/4 requirements to be endorsed. The remaining 3 are marked as uncertain.

The Trump campaign seems to have no interest in making moves to protect the environment or combat climate change and in fact seems to be pushing legislation that hurts the environment and exacerbates climate change. The Biden campaign has made one of the most aggressive environmental protection plans in recent history (though it falls short of the full Green New Deal). For this issue, the choice is between one side that is damaging the world and another with a grand plan to save it. I'm siding with the latter.

Score: -2 - strong lean Democrat

## Government for the people

I wasn't really sure what to call this category of policies I support so I went with the belief that I thought they were most related to. My belief is that the government we create as the people should be working for the people.

What this means to me is that the government should make moves to protect those that need protecting, help those in need of help, and raise the standard of living for everyone. Ultimately, it means it takes on problems for which it is best positioned, prioritizing those issues of highest urgency, highest import, and lowest availability of alternative quality, pragmatic solutions.

I think this is important because, if done correctly, it can create a positive feedback loop by pulling those up who have fallen and setting them on a path to live their best lives, creating net positives to be reinvested elsewhere in the system. I think this is important because, at its core, it's about helping people and I think helping people is good. When we factor in the health and [wealth gaps](https://www.nytimes.com/2020/06/09/your-money/race-income-equality.html) that exist between races in the United States, we can also see that helping those most in need is intimately tied with equality.

### Healthcare

I think healthcare is one of the standout examples of America doing something wrong. I think it's ridiculous that the US pays almost 2x per capita on healthcare vs other wealthy countries without achieving better health outcomes (source: [Peter G. Peterson Foundation](https://www.pgpf.org/blog/2020/07/how-does-the-us-healthcare-system-compare-to-other-countries#:~:text=The%20United%20States%20Spends%20More%20on%20Healthcare%20per%20Person%20than%20Other%20Wealthy%20Countries&text=In%202019%2C%20the%20United%20States,per%20capita%20across%20the%20OECD.)). I think it's crazy that health issues could seriously compromise or even wipe out someone's entire net worth - hospitalizations cause about 4% of all bankruptcies among nonelderly adults (6% for uninsured) ([National Center for Biotechnology Information](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5865642/)), a 20% annual decline in earning power, and 15% decline in employment probability ([National Center for Biotechnology Information](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5809140/)).

Providing healthcare to those in need is good. Its availability to you shouldn't be predicated on how much money you make or how you make it as wealth is already an issue of equality which would make healthcare an issue of equality, too. Quality healthcare is critical to living a life to its fullest which means policies here could make or break the 'for the people' feedback loop.

I'm again having trouble finding any official plans from the Trump campaign on their Healthcare plan. It appears they may not have released it yet ([WSJ](https://www.wsj.com/articles/trump-health-care-agenda-still-a-work-in-progress-11597158262)). So I'm going to keep using whatever sources I can find.

Trump

* Disregarded the recommendations of numerous health experts from around the world during COVID-19 and distributed demonstrably false information ([New York Times](https://www.nytimes.com/2020/07/17/us/politics/fauci-trump-coronavirus.html))
* Removed the United States from the WHO ([New York Times](https://www.nytimes.com/2020/07/07/us/politics/coronavirus-trump-who.html))
* Lowering drug prices by allowing for the safe importation of drugs from other countries where they're cheaper and the direct Medicare negotiation of drug prices ([Politico](https://www.politico.com/news/2020/07/26/trumps-health-care-again-with-2020-election-381473))
* Expanding telehealth services and ensuring healthcare for rural Americans ([Whitehouse](https://www.whitehouse.gov/briefings-statements/president-donald-j-trump-expanding-access-telehealth-services-ensuring-continued-access-healthcare-rural-americans/))
* Prevent billing for COVID-19 patients ([Whitehouse](https://www.whitehouse.gov/briefings-statements/president-donald-j-trump-expanding-access-telehealth-services-ensuring-continued-access-healthcare-rural-americans/))
* Transparency around price and quality of services before receiving them ([Whitehouse](https://www.whitehouse.gov/briefings-statements/president-donald-j-trump-expanding-access-telehealth-services-ensuring-continued-access-healthcare-rural-americans/))
* Effectively defunded Planned Parenthood ([NPR](https://www.npr.org/2019/08/19/752438119/planned-parenthood-out-of-title-x-over-trump-rule))
* Striking down the Affordable Care Act ([New York Times](https://www.nytimes.com/2020/06/26/us/politics/obamacare-trump-administration-supreme-court.html))

Biden

* A public health insurance option
* Lower premiums for low income people
* Greater access to Medicaid for those barred by the state
* Stop surprise billing caused by out-of-network providers working at in-network facilities, when the patient has no control over provider
* Allow Medicare to negotiate directly with drug corporations
* Limit launch prices for drugs without competition in the marketplace
* Limiting price increases for all brand, biotech, and abusively priced generic drugs to inflation
* Allow purchase of prescription drugs from other countries
* Expand access to contraception and protect the constitutional right to abortion, including restoring federal funding to Planned Parenthood
* Defending health care protections for all, regardless of gender, gender identity, or sexual orientation

source: [Joe Biden](https://joebiden.com/healthcare/)

Trump doesn't have a plan out yet but his record does share several things with Biden's plan like lowering drug prices, protecting healthcare patients from surprise billing, and expanding service availability for more Americans. I think where the Biden plan outshines Trump's is in its efforts to fight for better healthcare for low income people, equality in healthcare for all, larger efforts to curb the ridiculous health prices in the US, and support for women's health - specifically the power to choose. 

Trump's plan is fine on paper, but it doesn't go far enough to fight for the people in the most need and there are a lot of implicit effects of doing things like rolling back the Affordable Care Act, removing us from the WHO, and defunding Planned Parenthood that adversely affect these groups which I think makes it a step in the wrong direction. For that, I'm strongly siding with Biden on this one.

score: -2 - strongly lean Democrat

### Justice system

The unequal treatment of people based on their race, gender, sexual orientation, socioeconomic status, and other categorizations by the justice system is rampant in the United States. This summer's protests after a string of murders of Black people by state police forces is just the latest example of this inequality. 

I'll be voting based on justice policies because getting this right is directly related to equality in America and ensuring the government is for the people, not against them. I'm voting based on justice policies because getting this wrong is evil.

Some data on the current justice system:

* The United States incarcerates more people per capita than any other nation, at the rate of 698 / 100,000 people (see [Prison Policy Initiative](https://www.prisonpolicy.org/profiles/US.html)). To put it in perspective, [this chart](https://www.prisonpolicy.org/global/2018.html) shows each state compared to the rest of the world. 
* Black and Hispanic people are far more likely to be imprisoned in the US than white people (see [Pew Research Center](https://www.pewresearch.org/fact-tank/2020/05/06/share-of-black-white-hispanic-americans-in-prison-2018-vs-2006/))
* Black and Hispanic people are far more likely to be killed by police than white people (see [Mapping Police Violence](https://mappingpoliceviolence.org/))
* It's estimated that the United States pays $182 billion / year on prisons / incarceration (see [Prison Policy Initiative](https://www.prisonpolicy.org/reports/money.html)) at a cost of ~$35k / year for a Federal inmate (see [Federal Register](https://www.federalregister.gov/documents/2019/11/19/2019-24942/annual-determination-of-average-cost-of-incarceration-fee-coif))

It's again tough to find Trump's official plan of action for Justice system changes, so I'm going to be relying on whatever sources I can find.

* Trump signed the First Step Act which provides rehabilitative programs, fairer sentencing (by allowing judges to ignore mandatory minimums in some cases), more practical ways to house and administrate elderly and ill prisoners, and programs to support societal reentry (see [Whitehouse](https://www.whitehouse.gov/briefings-statements/president-donald-j-trump-committed-building-successes-first-step-act/))
* Increased compassionate release sentence reductions and home confinement
* Restart use of the death penalty
* Narrowed the disparity in sentencing between crack and powder cocaine
* $1.8 billion in funding to states for treatment initiatives and data collecting around opioids as well as restrictions on opioid prescriptions and greater access to overdose drugs
* Keep cash bail
* Will likely keep private prisons, as the administration sued California over its state ban on private prisons ([Los Angeles Times](https://www.latimes.com/california/story/2020-01-25/trump-administration-sues-california-over-private-prison-ban))
* Expand Pell Grants to provide education and training to inmates prior to release to help them secure work ([Whitehouse](https://www.whitehouse.gov/briefings-statements/president-donald-j-trump-championed-reforms-providing-hope-forgotten-americans/))


sources: [Politico](https://www.politico.com/interactives/2020/justice-reform-biden-trump-candidate-policy-positions/), [Reuters](https://www.reuters.com/article/us-usa-election-criminal-justice-factbox/trump-and-2020-democrats-brand-themselves-criminal-justice-reformers-idUSKBN1ZQ14K)

As for Biden

* Will work to repeal mandatory minimums
* Incentivize states to create programs that prevent and reduce incarceration through a $20 billion grant program ([Joe Biden](https://joebiden.com/justice/))
* Commute 'unduly long' sentences for those convicted of non-violent federal crimes
* Incentivize states and municipalities to stop jailing people for failing to pay municipal fees / fines
* Eliminate the death penalty at the federal level and incentivize states to do so as well
* Reduce use of solitary confinement
* Provide opportunities for all incarcerated people to earn a GED
* Has a record of tough-on-crime legislation in the 80s and 90s which seems at odds with these more lenient plans
* Supports decriminalization of marijuana at a federal level (and retroactive expungement of prior convictions), will leave legalization up to states
* $75 billion in flexible grants to states and localities for prevention, treatment, and recovery efforts related to the opioid crisis
* End the sentencing disparity between crack and powder cocaine, ensure it is applied retroactively
* No incarceration for drug use alone, instead sent to drug courts and treatment, and increasing funding for programs that identify, research, and support mental health and substance abuse disorders
* Encourage the elimination of criminal history questions in employment applications
* Allow previously incarcerated people to receive public assistance, incentivize states to restore voting rights for those who were convicted of felonies and served their time
* End cash bail
* End private prisons
* Eliminate all methods of profiteering off of incarceration
* Support the COPS program which invests in personnel, training, and resources for local police on the condition that departments have diversity that mirrors the communities they serve
* Triple funding for Title 1, the federal program funding schools with a high percentage of students from low income families
* Expand budgets for public defender's offices to help close the wage disparity wrt quality representation
* Various programs to reduce juvenile imprisonment and its negative impacts - investing in alternative rehabilitation programs, expanding after school programs, ending the use of detention as punishment for offenses that would otherwise be legal if they were older (like alcohol use), and increasing the protections on juvenile criminal history records

sources: [Politico](https://www.politico.com/interactives/2020/justice-reform-biden-trump-candidate-policy-positions/), [Reuters](https://www.reuters.com/article/us-usa-election-criminal-justice-factbox/trump-and-2020-democrats-brand-themselves-criminal-justice-reformers-idUSKBN1ZQ14K), [Joe Biden](https://joebiden.com/justice/)

Trump's policies did reduce sentences for non-violent drug offenders and help people get a second chance at life. But some policies like the death penalty and keeping private prisons seem like moves in the wrong direction and overall there doesn't seem to be that much promised that will actually have significant impact on issues of incarceration rates, particularly in their racial and socioeconomic disparities. Biden's plan includes line items that specifically try to reduce the number of people entering prisons by solving for common factors, attempts to make the judicial process itself fairer and less negative, and to help people reenter society successfully. I think this array of small, highly targeted steps focusing on making the justice system more about rehabilitiation than punishment has a higher chance of success than Trump's.

score: -2 - strong lean Democrat

# Foreign policy

Foreign policy isn't the most important issue to me but I think it's one of those things, like money, that is important to keep in mind. This is how we interact with the world and my stance here is that we should be a force for good - building cooperative relationships with the people of the world and helping out where we can. 

I think national security is important and understand that having a large military force at your disposal gives you more leverage on the world stage than not having it. But I also believe that doing so has its own repercussions and the military isn't always, and maybe mostly isn't, the right answer. I think we should be focusing on lending a helping hand, only using force as the last resort. We're at a point where we can literally destroy the entire world - the answers to our problems isn't to keep getting better at destroying it, it should be to get better at keeping it alive.

I'm voting based on foreign policy because doing it wrong is evil. Doing it right can support governments for the people, by the people and equality beyond our borders.

I, again, can't find an official Trump platform for foreign policy. Falling back to whatever sources I can find.

Trump

* Increased NATO support for Poland ([Whitehouse](https://www.whitehouse.gov/briefings-statements/statement-president-regarding-enhanced-defense-cooperation-agreement-republic-poland/))
* Withdraws from the Trans-Pacific Partnership ([Whitehouse](https://www.whitehouse.gov/presidential-actions/presidential-memorandum-regarding-withdrawal-united-states-trans-pacific-partnership-negotiations-agreement/))
* Travel ban on nationals of six muslim-majority countries and refugee intake from Syria
* Leave the Paris climate accord ([Whitehouse](https://www.whitehouse.gov/briefings-statements/statement-president-trump-paris-climate-accord/))
* Getting rid of DACA
* Recognizes Jerusalem ([Council on Foreign Relations](https://www.cfr.org/backgrounder/whats-stake-us-recognition-jerusalem))
* 'hardening' the southern border
* Ukraine dealings and subsequent impeachment ([BBC](https://www.bbc.com/news/world-us-canada-49800181))
* Drone strike on Soleimani increasing tensions with Iran ([The Hill](https://thehill.com/homenews/administration/476700-trump-says-iranian-commander-was-killed-to-stop-a-war))

sources: [Council on Foreign Relations](https://www.cfr.org/timeline/trumps-foreign-policy-moments)

Biden

* Belief that domestic and foreign policy are deeply connected
* Stop separation of families at border and holding children in for-profit prisons
* Will continue to invest in border technologies and security
* Protect undocumented members of armed services from deportation
* Terminate the travel ban on people from Muslim-majority countries
* Reverse Trump's asylum policies and raise target for refugee admissions
* End Global Gag Rule which prevents money from going to international NGOs that talk about abortion
* Rejoin the Paris Climate Accord and push the world to do better wrt climate change
* A summit thing to discuss democracies?
* Pursue a new Iran nuclear deal
* Pursue a denuclearization treaty with North Korea
* Further arms control with Russia
* Reduce the role of nuclear weapons to be a deterrent and retaliatory option

sources: [Joe Biden](https://joebiden.com/americanleadership/)

Both Trump and Biden seem to value the ideas that national security is important and sometimes force is necessary. I think where Biden's platform outshines Trump's is in the recognition that force can only go so far and that we must first build trust and cooperation around the world. Big issues like climate change, denuclearization, and social stability can't be ignored and they can't be solved alone. We must be willing to work with others to solve our problems, and that oftentimes means making concessions, otherwise we perish.

score: -2 - strongly lean Democrat

# The outcome

* issues: 4
* score: -8
* outcome: Biden / Harris

To be honest, I thought the split was going to be much closer. But the score is -8 over 4 issues which means that I'm leaning as far towards Biden as possible.

I think some of this has to do with not having access to Trump's official platform. I don't know if it's just not available yet or if Google isn't letting me search for it, but I think the lack of specific policies to help solve each issue was a major negative for his platform. I'll keep an eye out for any new releases and update my findings here.

If you've got thoughts on my process or findings (or know where I can find Trump's official platform), please let me know! Part of my writing this was so that I could refine and test my ideas on how I want to vote before I actually vote.

Before I let you go, I want to again encourage you to vote. Registering is super fast (< 2 minutes according to [vote.org](https://www.vote.org/)) and it really does make a difference.

If you're on the fence because neither Trump or Biden was your favorite candidate, I think you should still vote. I'm in a similar boat - I was a much bigger fan of Bernie and Warren in the primaries. Although neither of them are options, it doesn't mean that I can't work towards the things I value. 

I found this quote to be a great description of this:

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Time to remember the best voting advice I ever heard: voting isn&#39;t marriage, it&#39;s public transport. You&#39;re not waiting for &quot;the one&quot; who&#39;s absolutely perfect: you&#39;re getting the bus, and if there isn&#39;t one to your destination, you don&#39;t not travel- you take the one going closest.</p>&mdash; Debbie Moon (@DebbieBMoon) <a href="https://twitter.com/DebbieBMoon/status/1189288265901326336?ref_src=twsrc%5Etfw">October 29, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

> ...voting isn't marriage, it's public transport. You're not waiting for "the one" who's absolutely perfect: you're getting the bus, and if there isn't one to your destination, you don't not travel- you take the one going closest.

I've got two buses. Neither are going exactly where I want. But I'm still gonna take the one that gets me closest. I think you should too.

We've got 78 days. Learn as much as you can, encourage all your friends to register, go vote.

-HAMY.VOTE