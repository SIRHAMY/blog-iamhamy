---
title: "This simple trick can give $1 / year to a cause you care about FOR FREE"
date: 2019-06-20T15:25:34-04:00
subtitle: "A click-baity exploration of the costs and benefits of Mine for Good and how to get started TODAY"
tags: [
    "mineforgood",
    "tikkun-olam"
]
comments: true
---

_This quarter, I released a new project called [Mine for Good](https://labs.hamy.xyz/projects/mineforgood/) and I wanted to take some time to talk about what it is, what it does, and how (with your help) I think it could make a decent impact on the world we live in._

**TL;DR:** [Mine for Good](https://mineforgood.xyz) is a service that generates money just by opening (and clicking the "mine" button) and keeping open the web page in your browser. It's free, takes no additional load on your computer, and is super easy to set up (or at least a marginally insignificant amount) and can net up to $15 / year for a cause you care about. That's free money to make the world a better place!

Ready to help out? Jump to the [Quickstart Guide](#quickstart-guide).

Not convinced? Keep reading so I can convince you.

# Mine for Good

[Mine for Good](https://labs.hamy.xyz/projects/) is a project I built that allows you to mine crypto in your browser with the proceeds going to organizations that support a cause of your choice. The causes supported and the organizations we give to in support of those causes are hand-selected, but I'm always open to suggestions and criticism so if you've got ideas, send them my way! The way that crypto mining works is out of the scope of this post, but I think it's sufficient to say that you're basically trading the power of your computer for payment in small amounts of crypto. Mining is not the most efficient way to create revenue on the internet, as I argue in this [analysis](https://blog.hamy.xyz/posts/ad-revenue-vs-browser-crypto-mining-revenue/) but over time and with enough users it can really add up. As I'll argue below, it's also essentially free so the big question becomes _why not?_

# The cost of browser-based crypto mining

So as I've posited, mining is not a very efficient way to monetize the internet. In [my analysis](https://blog.hamy.xyz/posts/ad-revenue-vs-browser-crypto-mining-revenue/), I calculate that **mining is ~58x worse than ads at revenue-creation from the same site traffic**. However, this is calculated only for normal site traffic and not for those making an effort to produce in this way.

As I mentioned earlier, the basis of crypto mining is that you're trading your computer's power for rewards. The more power you give, the more rewards you receive. Power can typically be given in a set amount over time, so, unlike ad-based revenue which gets rewards per view, mining revenue continually trickles in the longer someone's there mining.

This means that although mining is far less efficient at making money in the short-term, it can actually make up for that if users are willing to keep mining for a long enough period of time. Another benefit over ads is that mining can be set up once and then it can continue running passively in the background, which means less work for more gain.

But what about the financial cost?

Well mining certainly isn't free in the same way that running your computer isn't free. It takes the power of your computer which in turn takes electricity. But when's the last time you limited your computer usage because it took too much electricity? I'd guess probably never.

**So while mining does use more electricity than the funds it produces, if you do it passively while on your computer anyway, the difference is negligible. Read: free money.** 

But it's gotta have an impact on my computer's performance, right?

Yeah it totally has an impact on your computer's performance, but again just in the same way that running any additional program on your computer has an impact on your computer's performance. Because it's running in the browser, any additional load should automagically be balanced across your computer's resources. Imagine pulling up a YouTube video or maybe a browser-based game. Some of the heavy ones may cause your computer to run slower but most don't and if it's a problem you can always just close the tab.

Anecdotally, I've played AAA games while using Mine for Good in the background with no noticeable performance degradation.

**So while mining may impact your computer's performance, it's likely to be negligible and not any worse than say opening up a new tab with heavy media. If it seems like it is causing slowness, just close the tab.**

# How much money can browser-based crypto mining _really_ make for charities

_All the calculations held herein can be viewed [here](https://gitlab.com/SIRHAMY/junkyard-script-calculations/blob/master/2019/mine-for-good-sell/mine-for-good-sell.go)._

Okay so I've talked about some of the costs associated with browser-based crypto mining, but now let's talk about why we should consider doing this in the first place.

Let's crunch some numbers.

* My desktop which is a bit over-powered can hit a hashrate of ~45 hashes / s
* My laptop which is pretty average can hit a hashrate of ~20 hashes / s
* My phone (a Pixel 3) can hit a hashrate of ~ 7 hashes / s

The mining pool I use is currently awarding 0.000058678216 XMR / 1M hashes. At the current XMR prices of 105.78 USD, that means we're getting $0.00000000620698168848 / hash. So each second, we're getting approximately this much on each device:

* desktop - $0.00000029172813935856
* laptop -  $0.0000001241396337696
* phone -   $0.00000004344887181936

So if we did this all day every day for a year, each device would be making, approximately this much for their cause each year:

* desktop - $9.20
* laptop - $3.91
* phone - $1.37

But all day every day kinda seems like a lot, what if we only mine in the background while we're on our computers. I'll assume that average computer usage is ~6 hours each day, with some days being lighter / heavier than others. So if we did that, we'd be at:

* desktop - $2.30
* laptop - $0.98
* phone - $0.34

That's cool, but what if I don't want to use my own power to mine. Well most people go to work, right? I don't know if this is kosher at all work-places, but for the purposes of this thought experiment let's say it's fine and that you just want to mine in the background while at work. I'll still assume that you've got your computer up for ~6 hours / day but now we'll go with just 5 days a week at around 48 weeks / year. This means that just mining in the background on workdays, you could generate around:

* desktop - $1.51
* laptop - $0.64
* phone - $0.22

# conclusion

So yeah, **if you use this project persistently and reasonably, you could probably make like $1 per year for charity**. Which, I'll admit, isn't really a lot. Now, in aggregate, if we were able to get this on tons of computers then it could add up to something. But I'm optimistic, not (totally) delusional so that probably won't happen.

Instead, if I just get each of you readers to give $5 directly to one of the organizations we're trying to support, I could likely make an impact many magnitudes larger than this project will ever have given current ecosystem variables.

I'm a fan of fighting climate change, so if you're for a list of orgs to donate to consider using [my list](https://mineforgood.xyz/climatechange/).

If you're still interested in helping me out with this, then by all means jump to the Quickstart Guide!

# quickstart guide

If you're jumping here directly from the intro, you should probably take a look at my post's [conclusion](#conclusion) before continuing.

If you've accepted the expected results and are ready to make an informed decision, read on adventurer!

Here's what you've got to do to start mining:

* go to a cause page, take the [climate change](https://mineforgood.xyz/climatechange/) one for instance
* make sure that the adblockers are off (they don't seem to like the mining pool I'm using)
* click start mining!

That's it! Simple as 1, 2, 3, wait... wait... keep waiting... but seriously this is going to take forever...

For best results, you should just leave this open in a tab somewhere. I like to pin it to Chrome so that it stays in the corner and doesn't clutter things up but you do you.

Anyway, if you've decided to help mine thanks a bunch! It's not much but it is something.

If you haven't but decided to donate instead, thanks a bunch as well! You've likely made a larger impact through that one action than you would've through your lifetime of Mine for Good usership.

If you did neither, well thanks for reading I guess. Maybe subscribe?

# got updates?

Did you like hearing me write? Would you like to read more about what I'm thinking?

[Subscribe](https://hamy.xyz/subscribe) to get periodic email blasts about what I'm doing, when I'm doing them!