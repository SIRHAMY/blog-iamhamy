---
title: "Release notes: May 2019"
date: 2019-05-30T23:21:09-04:00
subtitle: ""
tags: [
    "release-notes",
    "reflections"
]
comments: true
---

HoWdY y'AlL, it's time for another monthly update from the depths of the Hamniverse. 

# work

The biggest update this month isn't really an update at all. I'm still going through the interview process but I can finally see the light at the end of the tunnel. My hope is to make a decision on what my next step will be at the end of next week and start in that direction sometime in June.

Many people have been asking me if I'll take time inbetween my accept date and start date, but I feel I've already taken a good amount of time between the month of May and previously as I used my vacation days. So it's a tentative no to that. I've got more travel planned for later in the year so might as well get paid for it.

# projects

## mineforgood

This month I continued on my exploration of creations that do good for the world (as part of my [Tikkun Olam tuesdays](https://blog.hamy.xyz/posts/introducing-tikkun-olam-tuesdays/)). The result of that work this month was [Mine for Good](https://labs.hamy.xyz/projects/mineforgood/), a site that allows you to use the power of your browser to mine crypto for causes you care about.

It's live, so [check it out](https://mineforgood.xyz)

Afterwards [I crunched the numbers](https://blog.hamy.xyz/posts/ad-revenue-vs-browser-crypto-mining-revenue/) to see the likelihood of this project ever making money (the whole point of it) and confirmed that the odds were slim to none. Oh well, it's built and perhaps the advent of RandomX in October will give it a boost /shrug

## future stuff

I have nothing else to show atm but there are a few projects in the pipeline that I'm trying to finish before I start working again. Stay tuned, they'll probably roll out in the next few weeks.

# adventure

There existed adventure in the month of May. I went to Atlanta to see friends and fam and used my non-interview days to visit new areas / spots like Met Breur, Cooper Hewitt, Death & Co, et al. Mostly excited about upcoming adventures cause #summer.

# self

Despite the chaotic scheduling of interviews, I was able to stick to many of my #self habits. I...

* went back to yoga
* bought and ate from the grocery store (I even started a new series called HAMEATS that explores these creations, check out [the first one](https://blog.hamy.xyz/posts/ham-eats-hamboard/))
* made my own coffee
* continued meditating
* stayed within budget

I think keeping up with these rituals, despite the added overhead (and thus less time for studying), likely helped me do as well as I did as it served as a solid foundation from which to work. My hope is that a more regular schedule will help me continue these good habits into the future.

# fin

That's all I've got this month. It's late on a school night, and I'm tired.

Happy summer, go play by a pool.

-HAMY.OUT

# got updates?

Did you like hearing me write? Would you like to read more about what I'm thinking?

[Subscribe](https://hamy.xyz/subscribe) to get periodic email blasts about what I'm doing, when I'm doing them!