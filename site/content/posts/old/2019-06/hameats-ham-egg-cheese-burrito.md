---
title: "HAMEATS: Ham, egg, and cheeses burrito"
date: 2019-06-03T10:15:38-04:00
subtitle: ""
tags: [
    "hameats",
    "burrito",
    'finance'
]
comments: true
---

On this installment of [HAMEATS](/pages/hameats), we discuss a home-made ham, egg, and cheese burrito.

# tl;dr

This thing takes ~10 mins to make and costs next to nothing ($2.76). Compare this to a similar meal at Dunks that'll cost $10+ and that's savings of ~70%.

Do it.

# ingredients

* 1 tortilla -> $7.78 / 8 => $0.97
* 3 eggs -> ($1.99 / 12) * 3 => $0.51
* 1 serving ham -> $3.49 / 4.5 => $0.77

**Total:** $2.25

Want to add some coffee in the mix? Last year, [I started brewing my own coldbrew](/posts/case-for-self-brewing-cold-brew/) and found that each serving cost me ~$0.51.

**Total with coffee:** $2.76

# steps

* make scrambled eggs - I use this [microwave egg cooker](https://amzn.to/2HVXecx) for mine. Pop it in microwave for 90 secs and you're gucci
* tortilla on plate
* ham on tortilla
* eggs on ham
* optional cheese
* required ketchup
* roll burrito
* pour coffee
* enjoy

# got updates?

Did you like hearing me write? Would you like to read more about what I'm thinking?

[Subscribe](https://hamy.xyz/subscribe) to get periodic email blasts about what I'm doing, when I'm doing them!