---
title: "Why I have multiple Instagrams"
date: 2019-06-10T09:41:32-04:00
subtitle: ""
tags: [
    "instagram"
]
comments: true
---

For the past 3ish years (if memory serves, I started this in the [summer of 2016](https://medium.com/@SIRHAMY/in-review-my-summer-2016-88c9c3c74ca7)), I've run multiple Instagram accounts. Each of these accounts has a different purpose. I'll list these below, in rough order of activeness:

* [@sir.hamy](https://www.instagram.com/sir.hamy/) - pictures of me and what I'm up to
* [@hamy.streetart](https://www.instagram.com/hamy.streetart/) - pictures of street art I've found and liked
* [@hamy.labs](https://www.instagram.com/hamy.labs/) - pictures and announcements around my code experiments (this closely follows the content of [labs.hamy.xyz](https://labs.hamy.xyz))
* [@hamy.art](https://www.instagram.com/hamy.art/) - This follows my art creations, heavily focused on my sticker game when that was front and center
* [@hamy.see](https://www.instagram.com/hamy.see/) - pictures I've taken and liked that aren't of me (cause that would go in @sir.hamy ofc)
* [@sirhamy.me](https://www.instagram.com/sirhamy.me/) - a product of my angsty self seeing models on clothing sites and being like "I could do that better" (a common trope in my project origin stories)
* There exist a few others but they aren't fun / active so won't list. Extra points if you find them though.

When I tell people I have so many Instagram accounts, many people guffaw at me then proceed to ask why. The general answer to this is that I did it because it served each of my projects better than trying to lump all that content into one profile.

The result is that you build smaller audiences of more engaged users which I hold as better than larger audiences of unengaged users.

To this many go "what do you mean?" and so I'll explain:

# the benefits of multiple Instagrams

## branding

The most important benefit of separating your content out by purpose (what I usually refer to as projects) is the branding boost you get. To illustrate this point, let's do a thought experiment.

Imagine I'm just a regular millenial that loves to have pictures taken of me but I also like to make stickers and deface public property with them. _I know, v hard to imagine._

You could imagine then that I probably have 3 type of followers:

* people that know me in real life / love my face - they came for pics of me
* people that love my stickers / subsequent sticking them to the man - they came for pics of my stickers and vandalism
* people that like both cause we in sync like dat

Let's pretend that each of these groups makes up ~1/3 of your follower base.

Now say we post something with my face in it, no sticker. This means that 2/3 of my following will like it, but now 1/3 of the following won't.

Now let's say we post something a sticker, but not my face. Thiss still means that 2/3 of my following will like it, but 1/3 of the following won't.

The only way we can get 3/3 of the following to like it is if I do both the sticker and my face. But that's restricting and I'm an _artist_, so I've lost some of my artistic license and must now choose between what's good for my art / good for my following.

Note that these are also very optimistic ratios. At any one given time, that group is just the max potential like size and each post actually has to win them to get a like to unlock Instagram rating boosts from engagement.

So that's what happens if we post two different things in the same account and we can imagine that it gets even worse for our audience if I'm really into 3/4/5+ things.

Now if we were to break our content out into many different Instagrams, we basically give our following the choice to follow just the kind of content that they're interested in. The sticker people can follow my vandalism, the hamnatics can see pics of my face. Now we've created an environment where each post has the potential to be liked by 100% of the followers.

Now that that's in our brain, we can talk a little bit about why it's important that each of our posts has a strong like potential.

## Instagram rankings and keeping followers

The first reason why we want to maximize the potential size of followers that like our content is the way that social media ranking algorithms work. In general, they decide what content is good and what content isn't by the amount of "engagement" a given piece of content gets. In its simplest form we can think about this as the number of likes and comments something gets.

But that's not all, you'll notice that most social networks will also try to feature posts that are "new and trending" (or some other similar phrase). The way they calculate this is often a ratio of engagement / potentialEngagement. Thus by maximizing the potential ratio of followers of our account who might like something, we're able to put our content in a position to do well on both the standard engagement rankings and to be picked up by "new and trending" crawlers. 

The second big reason to do this is just to keep followers. Let's say you're following a street artist but 9/10 posts are of his dog. Even if you like dogs, at some point you're going to be annoyed cause you wanted to see the street art and not his dog. Eventually you might get so annoyed that you unfollow.

By giving your audience the ability to choose what content they want to see, you're also able to increase follower retention.

## creative freedom

In the end, what all of these changes means is that we've been able to uncouple our content from negative follower / engagement side effects. This gives us the freedom to post what we think is best for our projects without the risk of pacifying a large portion of our followers. _Okay obviously this isn't totally true, you could post something super nasty and still lose all your followers, but you get the idea._

For me this has been super rewarding. If I have street art or code content, I can just post without worrying about anything else in my content queue. It goes when I want in the shape that I want it.

## organizing feeds

We've pretty much covered the out -> in perspective of why many Instas is a good thing but there's one other, oft overlooked, benefit of this approach from the in -> out perspective.

The benefit here is the ability to basically folder the accounts you're following. The way I do this is that I try to match up each of my Instagrams with accounts like it. So my street art account is in charge of following the street artists I like and my code experiment account follows creative coders. What's cool about this is now I have collections of feeds geared towards each one of my interests so it's easy to switch over when bored to a curated feed of just X.

Not ground-breaking but def helps with content consumption.

# drawbacks of multiple Instagrams

## keeping up with multiple accounts

Now obviously having multiple Instagrams isn't all sunshine and roses. The most obvious drawback of this is the extra overhead. It takes a little bit of time to set up and to log in to your account the first time. Also it's usually good to keep each feed active which means you might need to post some extra content / always be thinking about what content needs to be posted where and when.

That being said, this actually isn't a huge deal. If you've decided to make the switch to multiple accounts, it's likely that you already have plenty of content for each project which is what drove you to split in the first place. Moreover, Instagram has done a great job in the past few years to make it easy to admin for multiple accounts at once. Currently it's as easy to switch accounts as it is to see your profile. #10pointsforinstador

## starting over from scratch

Probably the most daunting drawback is needing to start from scratch. On your personal Insta, it's likely that you've built up a following of 100s of users over the years. Everytime you post something from your projects, these people have the potential of engaging on it.

When you start a new account, you're back at 0. It'll take some time to get that number up. 

But what you lack in followers at the beginning should soon balance itself out with the increased engagement you have from those followers. It just takes time and good content.

# fin

Having multiple Instas is good as it tends to create a smaller, more engaged following and, in turn, lend itself to greater creative freedoms for you, the creator.

If you want to chat more about this, dm me on one of my Instas (higher likelihood of engagement on the more active ones) and consider giving me a follow.

Happy gramming!

-HAMY.OUT