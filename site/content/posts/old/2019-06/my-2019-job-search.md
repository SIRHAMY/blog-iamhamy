---
title: "Post-mortem: My 2019 Job Search"
date: 2019-06-17T11:46:55-04:00
subtitle: "My journey through a full-time SWE job search"
tags: [
    "swe",
    "interview",
    "software-engineer"
]
comments: true
---

Last week, I finished my 2+ month full-time job search. It wasn't easy. But it also wasn't particularly hard. It did however take lots of time, lots of effort, and lots of thought. Here I want to walk through my process for both posterity and the optimistic thought that it might help you, reader, in a future search of your own.

My name is Hamilton and I make money as a [software engineer](https://www.linkedin.com/in/hamiltongreene/). This is my story.

![2019 job search timeline](/posts/2019-06/2019-job-search-timeline.png)

_Right-clicking on the chart and opening in a new tab might make it a bit easier to read_

# listing your goals

My search consisted of ~2 months of active search and perhaps a month or so of passive / irregular thought / actions before that. This is a lot of time to spend on one thing and is roughly consistent with data I got from my peers about their own searches.

Knowing this (and looking back now I can confirm) I thought it was a good idea to list out the things I cared about / wanted to focus on in the next step of my career. These can then be used to compare opportunities against what you actually want / value as well as compare against the opportunities at your current company to vet whether incurring the moving cost is even worthwhile. I found that having these goals readily available helped to focus my search as well as remind myself why I was going through this effort (very useful during the frequent weeknight study sessions). 

Your own goals will likely be different, but here are mine for posterity.

* **Experience with larger scale:** Coming out of college, I was interested in how enterprise systems worked together to create a cohesive whole. Up til then, I'd only worked on relatively small codebases for classes and side projects and understanding how hundreds (and in many cases thousands!) of services much larger than anything I'd written could work together to produce something useful was beyond me. Having been in the industry for a little while I can now say that I'm starting to get it and along the way have only increased my interest in large scale systems / architectures. But one question that has continued to elude me is how these systems remain resilient under extremely high load - like millions of users simultaneously performing a sleuth of actions against your platform. This is something that's hard to experience in my own side projects (unfortunately [Mine for Good](https://labs.hamy.xyz/projects/mineforgood/) is still mostly mined by me /tear) and thus something I felt I needed to find out in the wild.
* **Jumping into a new platform / organization, seeing how it ticks:** Any time you start something new, there's always a learning curve. It's during this time that you might not be having the largest impact / output but also where you're undergoing an extreme amount of learning. I'd noticed that my learning had started to plateau and so I wanted to make a significant change in my day-to-day to re-initiate that process and continue on that path.
* **Lead bigger projects w more responsibilities:** Over time I've come to realize that I like to build things and that I learn things best when I'm experiencing them hands-on. I've gotten pretty comfortable working on small parts of apps and services and want to continue growing by building bigger and bigger things wrt scope, impact, and, in some cases, pure size. Each of these aspects brings with it different challenges and I see these as fundamental hurdles to advance both in my career as a software engineer and personally as someone who likes to build.

# finding opportunities

So I had some goals that I wanted to pursue in the next step of my career but you'll notice that most of them are really around problems areas and don't really prescribe any qualities about an org that would be able to furnish them. This was good in that there were a ton of places that could give me these opportunities. This was bad in that there were a ton of places that could give me these opportunities.

So I kinda just started looking everywhere using some basic filtering to weed out deal-breakers. Those deal breakers for me were 1) if I couldn't stay in NY and 2) if they were known for having a bad culture / bad tech. Tbh this didn't get rid of that many cos but it was a start.

One benefit of casting such a wide net was that I could hedge my bets against opportunities and leverage all the prep work I was doing across them to effectively increase the value of the average worst case scenario and simultaneously increase the average best case scenario. A win-win at the cost of marginal additional load for each additional opportunity I decided to pursue (as most everyone interviews in approximately the same way).

I went about this opportunity search in a few ways:  

* **my brain** - keeping a list of companies I thought were cool and that I might want to work for. This was good cause these companies had some sort of implicit attraction for me but there were also a lot of misses as a lot of the cos I liked didn't have large engineering organizations in NY. Which still baffles me but that's the way it is. That being said, I do think it's getting better so probably won't always be this way.
* **my peeps** - It's always good to ask your friends what companies they like and who they've heard are good to work for.
* **the internet** - I expanded my search through places like LinkedIn, [BuiltInNyc](https://www.builtinnyc.com/), and Reddit to see where other people were saying were cool places to work
* **being open** - many sites are recruiter-focused so I set myself to "looking" on LinkedIn and StackOverflow just in case people were looking for candidates with my skills. They were and I actually got a good amount of leads from there.

# prepping

Tbh, if we were doing this post chronologically, this section would've come before finding opportunities but we're not. I think prepping should start before finding opportunities (or at least before reaching out) because 1) prep work can take a lot of time, especially when you have to do it on top of other responsibilities / priorities and 2) many cos will have a pretty quick turnaround which means starting your search before your preparation can lead to being caught off guard.

I've already written a post on my preparation process so I won't rehash it here. It's got all the deets though so go read [my SWE interview guide](https://labs.hamy.xyz/posts/my-swe-interview-guide/) if you're curious.

My guesstimates are that I spent ~53 days and ~1.5 hours each of those days studying. There were definitely some days where I didn't study at all but others I was studying 6+. So, **in total, I'd say I spent about 79.5 hours just studying for interviews**.

# interviewing

The interview process itself takes a lot of time and energy. There are generally 3 kinds of interviews 1) online phone / code challenges, 2) on-sites, 3) take-home tests. Take-home tests are kinda just homework so we'll focus on 1) and 2) going forward. In addition to the actual interviews, there's a good amount of planning that goes into the scheduling and matching process, tons of emails and phone chats.

Here's the breakdown from my process by type:

![2019 job search event count by type](/posts/2019-06/2019-job-search-event-count-by-type.png)

And roughly the amount of hours spent on those, in aggregate:

![2019 job search event hours by type](/posts/2019-06/2019-job-search-event-hours-by-type.png)

So I spent around 15.5 hours on chats, 15 hours in interviews, 40 hours in onsite interviews, and about 3 on takehomes. **In total, this means I spent ~73.5 hours in interviews or in the process of scheduling interviews.**

Most processes will have you first go through 1) in order to screen candidates and then if you pass that section go to 2) which usually consists of 3-5 interviews on various subjects - data structures and algorithms, system architecture, and behavioral. Each interview runs ~1 hour and you're going to use that entire time either problem solving or talking. So it's very active and there's really not that much time to zone out. As such, I think it's important to know that these days aren't going to be easy and to do whatever prep works for you beforehand so that you put yourself in the best place to do well.

For me, good prep is just sticking to my normal routine.

* a good night's sleep
* a shower
* a meditation session
* a large coffee and substantial breakfast
* some sort of physical activity at night to work off the stress

But YMMV

That being said, even with good prep and staying healthy, it's still possible to burn out.

My rule of thumb was 

* only 1 onsite / day (w/ no other interviews)
* only 2-3 online phone / code challenges / day (w/ no other interviews)
* limiting these packed days to 3 times / week

There were definitely times where it wasn't possible to stick to these rules but I found it helped to keep my schedule manageable and to prevent burn out from one interview to the next.

# deciding

After doing this process for several weeks, I finally started getting some offers. This posed the dilemma of how I choose between them. A good problem to have to be sure, but not a particularly easy one.

Of course I'd made my goals at the beginning of this process and I was able to use that to distance a few from the pack but at some point there were still options that checked all the boxes. From there, I felt I had to go deeper to better differentiate. I did this in a few ways:

* prioritizing my original goals
* looking at other metrics I cared about - culture, people, tech, career prospects, TC
* reflecting on how I felt - while feeling shouldn't be how you make all of your decisions, I find it a fallacy to completely neglect it as well

Last Friday, I came to a decision and thus concluded my 2019 job search. 

_Woo, yeah! I'm done, finally! AHHHHHHH! I can read other books, do other things, stop dreaming in DS&A..._

# things I did well and things I didn't

## well

* **I gave myself plenty of time to study and take interviews:** Originally I had planned for the search to take only one month but it ended up taking two. I had a large vacation planned at the start of that second month so this actually put me in a position where taking the second month off completely was the optimal choice for me. As a side effect, this allowed me to really focus on my interviews in that second half which is also when most of my time-intensive on-sites were. This allowed me to be more prepared and to space out these interviews at a cadence that worked for me. I was in a good position to do this due to good life habits and some careful planning in the preceding months but I really do think this helped and would consider something similar in the future.
* **I put increased emphasis on preparing through mock interviews:** During [my last search](https://medium.com/@SIRHAMY/in-review-my-fall-2016-1b4c24d2c6c7) I mostly just studied the technical side. However looking back after my second job search, I've come to realize that the technical side is really only half the battle. Where a lot of people struggle (me included) is in the realm of clearly communicating and executing on a technical problem in a time-boxed environment under high (depending on your own personal social anxiety) pressure. Going through mocks helped me get used to this process in lower pressure environments which really helped during the real thing. I list some of the resources I used to help me with this in [my SWE interview guide](https://labs.hamy.xyz/posts/my-swe-interview-guide/).
* **I cast a wide net:** While this incurred additional overhead through scheduling and executing interviews, I think it helped in many ways. 1) it meant that I could leverage my prep work across many different opportunities and thus see a greater array of options, increasing the likelihood I got something that really fit. 2) it meant that even if I failed a few I knew I had other opportunities coming up which helped keep me out of a slump when things went bad - as they did and inevitably will.

## didn't

* **My timelines were overly optimistic:** I had this idea in my head that I'd be able to do the entire job search in 1 month. I didn't. Partly this is because of procrastinating on preparation and partly this is because of my vacation in the middle, however if I'd tried to go through this whole process while also working I think it would've been even longer so it kinda evens out. I think next time I'll be more conservative with my timelines and try not to schedule so many non-interview things during the process. 
* **My resume really sucked at the beginning:** When I first started applying, I had this idea that only content mattered and so made my own brutalist resume to send out. I got no good responses to this. I sent it out to some friends to review and they all said that the theme looked terrible and also that the content sucked. So I went back to the drawing board and rehashed it several times til I came out the other side with a more standard theme and some good content which finally started getting bites. The learning here is that you should really just get feedback as much as you can, particularly on your resume. This probably could've sped up my search by 1-2 weeks if I'd just done this at the beginning.

# fin

Well that's it, the end of my search and my funemployment (which I'm kind of glad to be done with). It was an interesting process and I learned a lot but I'm excited to (hopefully) not go through it for another few years.

In the end, you can't know if you've made the absolute best decision. But that's life. What you can do coming out of the process is know that you made the best decision for you with the knowledge at hand. That's enough. It must be, because no one can do better.

In total, **I spent about 153 hours in my interview process**. I did some things that certainly bloated it a bit like casting a wide net and taking that vacation right in the middle, but I think it's still pretty average in the industry. For reference, [I spent ~157 hours on my first job search](https://medium.com/@SIRHAMY/in-review-my-fall-2016-1b4c24d2c6c7) which younger me calculated as approximately equivalent to the number of hours you'd be in lecture taking a full 12 hour course-load over a semester (at 156 hours). I think I'm smarter than younger me, but he probably got those numbers ~right.

If you've got any questions / comments, you can add them here or [ping me on LinkedIn](https://www.linkedin.com/in/hamiltongreene/) or [use another way to get in touch](https://hamy.xyz/connect). If you just want to see what I'm up to, you can [read about it now](/tags/release-notes) or [subscribe](https://hamy.xyz/subscribe) to get periodic email blasts containing just that. If not, that's cool too and thanks for reading!

-HAMY.OUT