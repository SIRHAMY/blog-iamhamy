---
title: "What I Do at Instagram"
date: 2019-12-02T19:36:53-05:00
subtitle: ""
tags: [
    "work",
    "instagram"
]
comments: true
draft: true
---

Earlier this year I joined Instagram. So naturally, I've gotten a lot of questions about how my job is and what it is I actually do there. In this post, I'll try to explain just that so I don't have to keep doing it over and over again.

First off, I work at Instagram as a software engineer. This means I'm solving problems primarily with and / or through software.

I work on the Media Consumption team which is focused on building, maintaining, and enhancing the infrastructure involved in getting media to the end user.

What media you might ask? Well all the images, videos, and audio you know and love in your feed, profile, stories, explore, messages, etc. If you see some media in the app, it's likely it came through our services.

Some people have then asked "Oh, so you build the apps too?". My answer to that is not really, we build the parts that get the media from our datacenters to the phones and makes it usable to the app. So we do touch the app, but we don't really touch other parts of the app, so we don't really build the app. Moreover, I work almost exclusively on the server side of things, so I really don't build the app.

Many people like pretty pictures, so here's a pretty picture illustrating, in very simple terms, where we fit in the flow.

HAMY - image of where we fit in the flow here

That's what I do at Instagram.
