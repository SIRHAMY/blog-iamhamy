---
title: "Why you should save for retirement (on retirement part 1)"
date: 2019-12-08T14:25:48-05:00
subtitle: ""
tags: [
    "on-retirement",
    "retirement",
    "life",
    "money",
    'finance'
]
comments: true
---

I've recently talked with a few people about my thoughts on retirement and how it relates to my current spending habits. Each time my answers were a little different and I realized that it'd been a few years since I really sat down and thought about this subject and that it might be a good time to sit down and take another look at my strategies to know what I think and make sure I'm moving in the right direction.

In this post I'll tackle the issue of why you should be saving for retirement. I'll note that I do live in the U.S., that I'm generally a conservative spender, and that I'm not an expert just a human with strong opinions so you can use that to come to your own conclusions about my biases.

Here are my thoughts:

# Why you should save for retirement

There are a few reasons why I think everyone should save for retirement:

1. [Retirement costs a lot of money](#retirement-costs-a-lot-of-money)
2. [You probably don't want to work forever](#you-probably-don-t-want-to-work-forever)
3. [Even if you do want to work forever, you probably won't be able to](#even-if-you-do-want-to-work-forever-you-probably-won-t-be-able-to-do-so)
4. [More money is almost always better than less](#more-money-is-almost-always-better-than-less)

But before we dive into these, let's first come to a unified understanding of what retirement itself is.

# what is retirement

In the U.S. retirement is the state of being in which you exist without working. I believe it literally stands for retiring from working, though you don't necessarily need to have worked to reach this state.

Because you exist without working, you're in a special state where the money you need to live must come from some source other than a workplace.

Where does that money come from?

It can come from a lot of places but commonly it might be:

* social security
* friends and family
* passive investments / business interests

The important thing to note from this section is mostly that even when retired you need money and that money must come from somewhere. I'll note that I don't think social security is a good bet as it doesn't seem sustainable to me in the current system starting about 10 years from now and I don't think friends and family should be relied on because 1) it seems variable wrt their means and generosity and 2) doesn't seem very fair to burden them like that.

So in this post, and in my personal philosophy, you should only be relying on passive investments / business interests as they're much more stable and controllable - i.e. you have a direct effect on their size and makeup and thus how much they support you in your retirement.

Now on to why you should be saving for retirement:

# retirement costs a lot of money

So I've already explained a little bit about why retirement costs money (cause you're living and things don't magically become free) and where that money may come from. I think where many people go wrong here is just how much money you actually need to retire.

The average age of retirement in the US is ~65 (via [the Center for Retirement Research at Boston College](http://crr.bc.edu/wp-content/uploads/2015/10/Avg_ret_age_men.pdf))

According to [the CDC](https://www.cdc.gov/nchs/fastats/life-expectancy.htm), the average life expectancy in the US is ~78 years.

So let's say you're average and thus have ~13 years between retiring and dying. This means you need 13 years of runway to live without going broke (let's assume going broke is a losing scenario).

According to this [2015 report from the Bureau of Labor Statistics](https://www.bls.gov/opub/btn/volume-4/pdf/consumer-expenditures-vary-by-age.pdf), the average household with a head > 65 years of age spends $46,757 each year. This means, at this spending rate for the 13 years between retirement and death, **this relatively average household would need to have saved up $607,841 just to avoid going broke during retirement**.

# you probably don't want to work forever

Now the above calculations may not scare you much if you're currently working and making over $46,757 each year. At that income rate, you're net neutral each year and can live indefinitely.

But I'm assuming you probably don't want to work forever. If you do then theoretically you'll never have to save for retirement because you'll never actually achieve retirement because you'll always be working.

But do you really want to be working at 70? What about 60?

If you don't want to work forever, where does that money come from? You.

# Even if you do want to work forever, you probably won't be able to do so

Let's say you do want to be working at age 80, what kind of work would you be able to do? Barring some advanced medical breakthroughs, I think at 80 (if I'm still alive) I'll be pretty stationary and only have a fraction of the energy I now employ.

So let's say that you lose the ability to work at an age 1 year before you die. How are you going to pay for the living expenses for that 1 year of life if you haven't saved for retirement?

Now let's use a more common scenario:

_Note that for simplicity I'm just going to assume that in retirement you'll be spending every dollar that comes in and that your lifestyle is roughly equivalent cost-wise compared to pre-retirement which usually isn't true, but it makes the following calculations a lot easier._

The average age of retirement in the US is ~65 (via [the Center for Retirement Research at Boston College](http://crr.bc.edu/wp-content/uploads/2015/10/Avg_ret_age_men.pdf))

According to [the CDC](https://www.cdc.gov/nchs/fastats/life-expectancy.htm), the average life expectancy in the US is ~78 years.

This means, on average, you'll have ~13 years of retirement that you'll need to fund yourself with.

According to [this document from the Department of Health and Human Services](https://www.federalregister.gov/documents/2015/01/22/2015-01120/annual-update-of-the-hhs-poverty-guidelines), the poverty line for an individual's yearly income is $11,770.

So if you're living at the poverty line and have the average duration between retirement start and death, then you'll need ($11,770 * 13 years =) $153,010 dollars saved up just to live out the rest of your life without going broke.

Let's say you're not living at the poverty line though and are instead living with a lifestyle equivalent to the median individual income of $33,700 (according to [this 2018 survey from the US census](https://www.census.gov/data/tables/time-series/demo/income-poverty/cps-pinc/pinc-01.html)).

This means that you'd need closer to $438,100 to live the rest of your life without going broke.

These values aren't so small that you can just magically come up with them and while they are overcounting a bit in that most people won't be using _all_ of their income on life expenses (and thus will spend less each year than they make and thus need less saved up), these calculations are undercounting for the half of people that will live beyond the average life expectancy and for the half of people that make more than (and particularly spend more than) the average US income and definitely undercounting if you ever need some major medical care here in the US.

# more money is almost always better than less

So we have some big numbers and a lot of uncertainty, how can we do better / decrease our error? There's a lot of fancy things you can try, but in this case I think the simplest method is best. The simplest method is to save for the worst case, which usually means saving more than you would otherwise.

In this scenario, if you end up living way over the average life expectancy - retirement age average, then you have more money to pull from before going broke. In fact, you can even have enough to live off of that for all of eternity - you know, if we ever figure out how to make people live forever. Moreover, healthcare is expensive in the US and as you get older you're more and more likely to need to use it in greater and greater fashions - having a bigger money pile to pull from could literally be the difference between life and death (or at least life with and without crippling debt).

In the scenario where you die before ever hitting retirement or shortly thereafter and thus have accrued way more money than necessary, rejoice in that you likely had very little stress in where the retirement money would come from and I'm sure you can figure out how best to make use of that lump of cash in your will.

Let's say that you even start out wanting to save for retirement but then decide a decade in that you don't really care - well now you have a whole bunch of cash to use on whatever midlife crisis you so choose which is likely better than the opposite midlife crisis in which you don't have that money and choose to spend like you do anyway. _Note: I totally do not endorse this scenario._

In general, I think money gives you more decision-making power by keeping more doors open. It removes money from the equation as a limiting factor, thus letting you do what you want to do for a longer period of time. I think that's an oft overlooked superpower and one I like to take pains to protect.

# save for retirement

All this to say I think everyone should save for retirement. Retirement costs money. It costs way more money than I think people realize. I think most people shouldn't work til they die, even if you wanted to work til you die you probably won't be able to, and having more money is almost always a good thing.

Bottom line: Save for retirement.

Always saving,

-HAMY.OUT

# further reading

I found these resources to be very helpful / informative / interesting in my research and understanding around retirement:

* [The shockingly simple math behind early retirement](https://www.mrmoneymustache.com/2012/01/13/the-shockingly-simple-math-behind-early-retirement/)
* [The 4% Rule: The Easy Answer to “How Much Do I Need for Retirement?”](https://www.mrmoneymustache.com/2012/05/29/how-much-do-i-need-for-retirement/)