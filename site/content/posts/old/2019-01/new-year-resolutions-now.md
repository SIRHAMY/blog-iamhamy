---
title: "Don't wait for a new year to change your life"
date: 2019-01-14T12:11:17-05:00
subtitle: ""
tags: [ "new-year", "resolutions", "goals", "musings", "change" ]
comments: true
---

For many people, a new year means rebirth - a clean slate from all that happened in the year prior, a new foundation upon which to build the life they've always wanted to lead. Lofty I know, but you'd be hard-pressed to scroll through your favorite social platform without a bombardment of reflections, resolutions, and inspirational quotes from people doing just that.

Personally, I'm against resolutions.

This isn't to say that I think they're bad, I actually make a ton of resolutions each year. I think the new year's resolution train is a great vehicle with which to channel your inner growth mindset into real world change.

But I do think they're easily turned into an excuse to put off a change you desire. Procrastination is one of my biggest vices so whenever I see an opening for it to sneak into my life, I get anxious.

Change can happen at _any_ time. It does not depend on the increment of some arbitrary counter we use to model the passage of time in our reality. All it requires is some thought and concerted effort from an individual - you.

So continue to reflect on your shortcomings and create new year's resolutions if that's what works for you. [It does for me](/posts/2018-in-review/).

But never forget that the only thing preventing you from changing for the better is you and all you've got to do to change that is to start. 

Don't wait. Just start.