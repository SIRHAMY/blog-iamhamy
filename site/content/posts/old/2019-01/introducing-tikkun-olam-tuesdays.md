---
title: "Introducing Tikkun Olam Tuesdays"
date: 2019-01-07T17:50:45-05:00
subtitle: ""
tags: [ "tikkun-olam-tuesdays", "giving", "tikkun-olam", "repairing-the-world", "charity" ]
comments: true
---

Today I'm announcing a new endeavor I'll be starting in 2019 - "Tikkun Olam Tuesdays".

Tikkun Olam is a "jewish concept defined by acts of kindness performed to perfect or repair the world" (from [learningtogive.org](https://www.learningtogive.org/resources/tikkun-olam)). As such, the goal of Tikkun Olam Tuesdays (maybe I should refer to it as TOTs?) is to habitually take some action to perfect/repair/give back to the world every Tuesday.

I don't really have a framework in place for what this action will be, but my current belief is that it will take more of a doing form than a giving form. Many people give money to charities and I think that's fine, I do that as well. But, in the current state of Ham, I think I will find it more rewarding to do something more hands-on (when it makes sense) and that my particular set of skills may produce value far beyond that which I would reasonably give.

Whether or not that is actually true is left to be seen, but rest assured I will be logging data to try and answer that question.

**So what does this mean?** In the near-term, nothing. But in the longer term, I should have some new projects/posts out describing my actions and their impact.

**I want in, how do I contribute?** One great thing about giving is that it's free! Just start giving back and let me know what you're up to. I'll try to feature your efforts in a way that makes sense. We can all help each other make this place a little better.

# FAQ

## Why Tuesday?

Seemed like a good day. Plus alliteration. And not a Monday. Or a weekend day. Or a Friday. And it recurs so it has a built-in habit-creator.

## Why am I Giving?

I think giving is good. It makes you feel good, it helps the world, it forces you to act. I like those things.

It puts me on the good side of "Are you helping or are you hurting".

## Why Tikkun Olam?

If you know me, you know that I'm not particularly religious. But that doesn't bar me from agreeing with many core philosophies. I was brought up as a reform jew and I'm proud of that identity. So when my plans overlapped with a core philosophy of the faith, I was ecstatic to pick up the mantle.

## How do I stay up-to-date?

The best way to keep up with TOTs progress is probably to [subscribe to my blog](https://hamy.xyz/subscribe). I'll be posting updates here so you should get them by doing so.