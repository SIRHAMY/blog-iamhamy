---
title: "Hamventures NYC: Week of 2019.01.20"
date: 2019-01-21T14:00:34-05:00
subtitle: ""
tags: [ "hamventures" ]
comments: true
---

# Monday - 2019.01.21

## Virtual Virtual Reality

**Name:** Virtual Virtual Reality

**Price:** $10

**Location:** [Secret Project Robot](https://www.secretprojectrobot.org/) (a v trendy public art space)

**Link:** https://www.facebook.com/events/802776643392049/

**Brief:** Secret Project Robot is a public art space that's been on my list for several months now. On Monday, they're going to be featuring a new indie VR game. If VR isn't your thing, I hear they also have a pretty good bar and large array of free-to-play arcade games.

# Tuesday (2019.01.22)

## Art Apple NYC x Vol 1 x Soho NY

**What:** A visual art show with open bar and small foods. Presumably the first one, but idk.

**When:** 2019.01.22, 1800-2300

**Price:** $10 and up

**Location:** Some random location, look at the event

**Link:** https://www.facebook.com/events/2325927324302687/

**Brief:** I like it when there are events like these because you can see a bunch of art from a variety of people. Because the event is so "big", those artists will likely pull their best stuff for display which makes the bang for buck that much better. Also $10 for an art show that includes food and drink is a bomb price.

## Free yoga in Bryant Park

**What:** Free yoga by Core Power v early in the morning

**When:** 2019.01.22, 0800-0900

**Price:** FREEZY

**Location:** Bryant Park

**Link:** https://www.facebook.com/events/633120183770301/?event_time_id=633120187103634

**Brief:** Reposted from [BYT NYC](https://brightestyoungthings.com/articles/nyc-best-weekday-bets-24)

# Wednesday (2019.01.23)

## 100s under 100

**What:** A series of informal lightning talks from artists about their practice.

**When:** 2019.01.23, 1900-2200

**Price:** Free or donation

**Location:** Liberty Hall at the Ace Hotel

**Link:** https://www.facebook.com/events/2120266848286721/

**Brief:** I like lightning talks. I like art. I like free stuff.

## Babes and brews

**What:** Free beer tastings and food pairings in the spirit of celebrating women in/around beer.

**When:** 2019.01.23, 1830-2130

**Price:** FREEZY

**Location:** [The Beer Fridge](https://www.facebook.com/beerfridgenyc/?eid=ARA2GRxnXNWnT0zf3qdgl7dW4bEyoqgSNwIWsnsM8T-LDe8gU-Z5LbcXSxQGFooIM-8nLF8MAs0VaXEy)

**Link:** https://www.facebook.com/events/2272397776326098/

**Brief:** Free beer. Free food. Celebrate women. Reposted from [BYT NYC](https://brightestyoungthings.com/articles/nyc-best-weekday-bets-24)

# Thursday (2019.01.24)

## Poetry and Prose Open Mic

**What:** Poetry and prose, duh

**When:** 2019.01.24, 1930-???

**Price:** $5 and up depending on when you buy

**Location:** [The Secret Loft](https://www.facebook.com/SecretLoft/?eid=ARAsoWAjUtNXO_BVzLc7spU4u8CiWtnPJPUyZsX3Qb7AxZFCo6XHRB08IkO3fTcYGgu5whLdEpIYLINq)

**Link:** https://www.facebook.com/events/1444795458990827/

**Brief:** I've been to a few events at The Secret Loft and they're usually a weird time. This will probably be fun and if the poetry/prose runs out, they've got some resident comedians that can step in to fill the void.

# Friday (2019.01.25)

## Free Fridays at the Morgan

**Name:** Free Fridays at the Morgan

**What:** Come see the Morgan, fo freezy

**Price:** FREEE

**Location:** [The Morgan](https://www.themorgan.org/)

**Link:** https://www.facebook.com/events/610101236092613/?event_time_id=610101242759279

**Brief:** I've walked by the Morgan a few times and noted how cool it looks from the outside. I still haven't been and it's likely going to be a small, quick visit but I think it's worth checking out if you haven't been before.

## MoMA Scavenger Hunt and Cocktails

**What:** A scavenger hunt in the MoMA w/ other stuff to buy

**Price:** $26

**Location:** [MoMA](https://www.moma.org/) (note: I am a member)

**Link:** https://www.facebook.com/events/2202121970062783/?event_time_id=2238648386410141

**Brief:** Scavenger hunts aren't my favorite thing in the world but some people like them. If it brings more people to museums/art, I'm in support.

# Saturday (2019.01.26)

## Warhol Factory Party by ArtsClub

**What:** A party where you dress like it's the 1960s and make art with an open bar

**When:** 2019.01.26, 1900-2200

**Price:** $49 for open bar

**Location:** The Warhol Factory ? Check the event

**Link:** https://www.facebook.com/events/2424622677610002/

**Brief:** I don't love going to big events with large price tags but $49 isn't terrible for an open bar and if you really do get to go drink in Warhol's old studio, then I think it's well worth the price (I'd wager people would pay upwards of $25 just to see his studio so $24 for an open bar for 3 hours isn't half bad). 

# Sunday (2019.01.27)

## Lower East Side Gallery Tour

**What:** A gallery tour hosted by some random Meetup

**When:** 2019.01.27, 1400-1700

**Price:** FREE

**Location:** idk, check the event

**Link:** https://www.meetup.com/artforward/events/258141395/

**Brief:** I've never done anything with this Meetup but worst case you are forced to leave your home and can tour the galleries yourself.

# Notable ongoings

## The winter show

**Name:** The Winter Show

**When:** 2019.01.18-27, ~1200-1800

**What:** A big art show

**Price:** $25

**Location:** The Armory

**Link:** https://www.eventbrite.com/e/the-winter-show-2019-january-18-27-2019-tickets-51724137287

**Brief:** This show looks a lot like the Armory art show (no surprise since it looks to be in the same space). I don't typically like shows like this because they rarely have super weird art, but at a show of this scale you'll likely find a few interesting pieces. Also it's always entertaining to ask the galleries how much a given piece is. My current record is $30k.