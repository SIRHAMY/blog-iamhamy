---
title: "How many humans does it take to change the world?"
date: 2019-01-28T10:04:59-05:00
subtitle: ""
tags: [ "hamq", "motivation", "snark" ]
comments: true
---

Just one. Just you.