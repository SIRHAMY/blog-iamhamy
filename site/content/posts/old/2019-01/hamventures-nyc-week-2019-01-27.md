---
title: "Hamventures NYC: Week of 2019.01.28"
date: 2019-01-27T15:34:58-05:00
subtitle: ""
tags: [ "hamventures", "nyc" ]
comments: true
---

# Monday (2019.01.28)

## Kubernetes and Microservices powering modern consumer banking

**What:** A talk on Kubernetes and how modern enterprises are leveraging it.

**When:** 2019.01.28, 1900-2100

**Price:** FREE

**Location:** IBM CODE NYC

**Link:** https://www.meetup.com/ibmcodenyc/events/257997377/

**Brief:** Kubernetes is cool and the IBM Code NYC events have been relatively good at giving high level introductions to some of the newer technologies, complete with some internal advocates to answer questions you might have. If you're interested in k8s, might be worthwhile.

## Pigeon pages January reading

**What:** Writers reading stuff they wrote

**When:** 2019.01.28, 1900-2100

**Price:** Free?

**Location:** Pigeon Pages, Brooklyn

**Link:** https://www.facebook.com/events/2003470759956652/ 

**Brief:** I haven't been to a reading in awhile but this could be cool. I don't think that it's poetry, probably more prose given the "literature" tag so if you go, just be warned it could really be them reading anything /shrug

## Backroom speakeasy mondays

**What:** Every Monday, the backroom hosts musicians playing ~jazz~

**When:** Mondays 2130-0030 (doors at 1930)

**Price:** free

**Location:** The Backroom

**Link:** https://www.facebook.com/events/2183684015217482/ 

**Brief:** Ever since the Roost stopped doing their Thursday live music stuff I've been looking for an alternative. This might be it.

# Tuesday (2019.01.29)

## Carbon Five Hack Night

**What:** Open space (presumably with plenty of seats and outlets) to come and hack with others, complete with free beer and pizza

**When:** 2019.01.29, 1830-2230

**Price:** FREE

**Location:** Carbon Five NY

**Link:** https://www.meetup.com/Carbon-Five-NYC-Hack-Nights/events/hjmgpqyzcbmc/

**Brief:** I've been trying to put something similar together via [Make Sessions NYC](https://www.facebook.com/groups/493273147848768/) but it's been slow to kick off so this might be a good one to try on the last Tuesday of each month.

# Wednesday (2019.01.30)

## DayBreaker NYC // Havana Mornings

**What:** DayBreaker is a cool org that puts on early morning, sober parties. This one comes with the option to add a salsa class.

**When:** 2019.01.30, 0600-0900

**Price:** $25 just dance, $40 to include salsa class

**Location:** Highline Ballroom

**Link:** https://www.facebook.com/events/456644984872342/ 

**Brief:** DayBreaker is a super dope organization. If I could ever get up early enough to get to their events, I'd go infinitely more

## Yahoo Finance Breakout with OpenDoor CEO Sarah Friar (IHamInterested)

**What:** Discussions on running businesses with people who are good at it

**When:** 2019.01.30, 1700-1745

**Price:** FREE

**Location:** SubCulture, LES

**Link:** https://www.facebook.com/events/319900548622761/ 

**Brief:** #powerfulwomen #learnsomebusiness #getdatbread

## Passport to Bushwick launch party

**What:** A launch party for Passport to Bushwick, open bar from 2100-2200

**When:** 2019.01.30 2000-???

**Price:** FREE

**Location:** Hart Bar

**Link:** https://www.facebook.com/events/521539321700116/ 

**Brief:** I don't think this event itself will be that dope, but I kinda like the idea behind the launch. [Passport to Bushwick](https://www.passporttobushwick.com) is a coupon book (go ahead, make your Jew joke) that gets you a free drink at 19 bars in Bushwick for $25. I like Bushwick, drinks, and free stuff so they might actually get my money.

# Thursday (2019.01.31)

## Web Animation Series: CSS & SVGs

**What:** Some people teach you how to do cool web stuff

**When:** 2019.01.31, 1830-2130

**Price:** $45

**Location:** Brooklyn Research

**Link:** https://www.facebook.com/events/449687982233299/ 

**Brief:** I like coding, seems like a good way to get the basics and maybe even some advanced stuff on CSS / SVGs.

## What is Freemasonry?

**What:** A lecture on what freemasonry is

**When:** 2019.01.31, 1830-2000

**Price:** FREE

**Location:** Robert R Livingston Masonic Library

**Link:** https://www.facebook.com/events/209304986685259/

**Brief:** I have dance class on Thursday otherwise I'd probably be going to this because I do want to know what they do/how they make money/why they exist

## Rust: First meeting 2019

**What:** First meeting of 2019 for the RustLang programming language

**When:** 2019.01.31, 1830-2030

**Price:** FREE

**Location:** MongoDB

**Link:** https://www.meetup.com/Rust-NYC/events/258157271/ 

**Brief:** Rust is cool and I want to learn more about it.

## Brasstracks @ Brooklyn Steel

**What:** [Brasstracks](https://soundcloud.com/brasstracks) is playing

**When:** 2019.01.31 1900-2300

**Price:** $20+

**Location:** Brooklyn Steel

**Link:** https://www.facebook.com/events/191089105151850/ 

# Friday (2019.02.01)

## Smorgasburg x VICE Night Market

**What:** Assorted food and drinks (not included in tickets) from popular smorgasburg vendors + djs

**When:** 2019.02.01 1800-2359

**Price:** FREEMIUM, free to get in but more perks for paying

**Location:** Villain

**Link:** https://www.facebook.com/events/1133563416814716/ 

**Brief:** I've been wanting to go to this for the past few months. Seems like it could be a _very_ good date spot if you've got someone you want to go with. The $75 option for two seems expensive at face value, but if it gets you seated and drinks and, like me, you like to split the bill that's only $37.50 for a pretty balling tie.

## First fridays at the Frick Collection

**What:** Free gallery viewings and live music at the Frick Collection

**When:** 2019.02.01, 1800-2100

**Price:** FREE

**Location:** The Frick Collection

**Link:** https://www.facebook.com/events/333159927293494/

**Brief:** Still haven't been but the pictures look fun and #museums

## Harlem Jazz Singers Showcase

**What:** A long line up of a bunch of different jazz artists

**When:** Every friday, 1900-2200

**Price:** $30

**Location:** Gin Fizz Harlem

**Link:** https://www.facebook.com/events/2469308953318794/?event_time_id=2469308976652125

**Brief:** Never been to Harlem and never been that into jazz, but this could be a good opportunity to try both

## Night at the museum: Lunar new year party with BUBBLE_T (IHamInterested)

**What:** A lunar new year party thrown at MoMA PS1 with food, music, drinks, and access to the exhibits.

**When:** 2019.02.01 2000-2359

**Price:** $15

**Location:** MoMA PS1

**Link:** https://www.facebook.com/events/296961197627745/ 

**Brief:** I still haven't been to PS1 so this seems like a pretty awesome way to see it, see their current exhibit before it leaves on 2019.02.25, and have an odd night out. V interested in going to this one.

# Saturday (2019.02.02)

## Flickr photo walk

**What:** A photo walk to iconic places with a bunch of photographers, led by some famous photographer

**When:** 2019.02.02, 0900-1100

**Price:** FREE

**Location:** Manhattan

**Link:** https://www.facebook.com/events/2018102331559580/ 

**Brief:** Earlier this year I hung up my camera to focus more on my other, more core outlets but this seems like a great opportunity to meet and work with fellow, local photographers if you're still wielding your lens (and think you can get up before noon on a Saturday).

## Live Code Lab (IHamInterested)

**What:** Code workshops followed by showcases, all for free

**When:** 2019.02.02, 1130-2000

**Price:** FREE

**Location:** NYU

**Link:** https://www.facebook.com/events/271161637076761/

**Brief:** If you like to play with code, this is likely the event for you. See what it's capable of and build some dope shit yourself.

## Winter Play: NYC game expo (IHamInterested)

**What:** A big game expo of a bunch of indie developers + free pizza

**When:** 2019.02.02, 1200-1700

**Price:** FREE

**Location:** Microsoft Times Square

**Link:** https://www.facebook.com/events/1067411356752461/

**Brief:** Free video games and pizza, why not?

## Drunk Shakespeare

**What:** Actors take 5 shots then attempt to act out Shakespeare scenes

**When:** 2019.02.02, 1600 - 2359

**Price:** $29 (based on link)

**Location:** Drunk Shakespeare

**Link:** https://www.facebook.com/events/134520524091400/?event_time_id=134520557424730

**Brief:** I don't know if this will be any good but it reminds me of Drunk History and I thought that was funny so maybe this will be funny as well.

## Kings of New York

**What:** A big dance battle / competition!!!

**When:** 2019.02.02, 1900-LATE

**Price:** $20 CASH-ONLY

**Location:** some random place, look at the event

**Link:** https://www.facebook.com/events/189020755340050/permalink/227860908122701/ 

**Brief:** A bunch of people I know have started dancing recently so could be fun/educational to go and watch some established crews show their moves

# Notable Ongoings

## Wave, Particle, Duplex by STUDIO SWINE

**What:** An ongoing art show at A / D / O. They usually have cool stuff, so would expect this to be #quality.

**When:** Looks like it's daily from 1000-1900 through 02.10

**Price:** FREE

**Location:** see event

**Link:** https://www.facebook.com/events/2106731669638095/