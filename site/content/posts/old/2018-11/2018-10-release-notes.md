---
title: "October 2018 Release Notes"
date: 2018-11-07T18:48:56-05:00
subtitle: ""
tags: [ "reflections" ]
---

Spooktober is over and I didn't even write a quarterly report. This is because I didn't really have much to say for ~2 reasons 1) my last review was put out a bit late and 2) I haven't done too much I'm ready to share. My hope is that a few of my projects will reach shareability in Q4 which will trigger an avalanche of updates, but wouldn't recommend betting on it. Anywho, on to October's hamlease notes:

# Projects

Project-wise I've been a little slow on production, focusing more on learning and exploration. I've done a few improvements to some side projects and written a few blog posts, but have mostly been thinking about ways to improve, where I want to go next, and investigating some of these possibilities. 

To capture some of this work, I've decided to log some FutureIdeas in these blogs. Recently, I've shied away from doing this as I felt that it made me feel too much like I'd done something when I really hadn't which led to never actually accomplishing these goals. However, I kind of like being able to go back and see what I was thinking about at a given moment of Ham so I'm hoping that the framing of these as ideas will be able to both snapshot that state without giving me a false sense of accomplishment. Will report back with how that actually works out.

**Releases:**

* Started the HAMQ series to provide an outlet for my outwardly non-sensical investigations into things I wonder about
* [Added BitMoji support for Geolog](http://labs.hamy.xyz/posts/2018-10/geolog-supports-bitmoji/) which basically just means that I have a process to use arbitrary BitMojis (including the fetching of your avatar's comics) in the animations

**FutureIdeas:**

* Add CI/CD for these blogs - right now it's extremely manual /vom
* Look into service meshes for future projects

# Adventure

I don't think I did that much formal hamventuring this past month, but it was chock full of social engagements - spurred by the spooky season, the introduction of game nights, and resuming attendance of GT events.

**Releases:**

* Released [Hawaii trip recap](http://blog.hamy.xyz/posts/hawaii-in-review-2018/)

**FutureIdeas:**

* Get sofars back on the calendar
* Start weekly make sessoins
* Continue game nights
* Do monthly stay-at-home brunches

# Self

I haven't been sticking to my habits much this month, but I think it's kinda nice to live life freely for a time every now and again. It helps give some perspective and distance from your discipline which is useful to re-evaluate what is actually working and what you're just doing because you've been doing it. That being said, I think it's about time to re-engage some habits to pick up velocity in some of my life areas.

Right now, don't have much in the way of releases, but one of the habits I have been sticking with is reading and I'm currently making my way through _Zen and the art of motorcycle maintenance_ which is a dense but extremely satisfying book. Would rec for tumbles down the rabbit hole of thinking wrt thinking.

**FutureIdeas:**

* Eat healthier
* Start running again
* Actually do some dance/yoga classes

# Work

We spun up a new team and are making really great headway. It's been a nice blast of fresh air to start moving faster (and ofc breaking things) and I hope we can keep that velocity up - we'll see. Nothing more to share here but hoping to get at least a little more of this knowledge/experience out in the wild in some fashion. Might have to pass through some red tape first though.

# Fin.

That's it.

-HAMY.OUT