---
title: "HAMQ: How does Thieve.co make money?"
date: 2018-11-10T13:39:12-05:00
subtitle: ""
tags: []
comments: true
---

_This post contains affiliate links. You do not have to click them, but it may compensate me a bit so if you like me you might click them anyway._

[Thieve](https://thieve.co) is a site that recently showed up on [ProductHunt](https://www.producthunt.com/posts/the-heist). Essentially, they'll send you items that you've "stolen" from other users. It appears that this works by releasing a given item out into the wild and then people steal back and forth with the winner being the last one to hold it once the timer hits 00:00. That winner will, apparently, be sent the stolen item along with every other member of the "crew" they're in.

This model seemed intriguing as, on the surface, the user doesn't appear to be giving anything up (I had originally suspected this to be a penny auction scheme wherein no individual loses a lot of money at one time but the company gets far more than the purchase price in aggregate). Moreover, the mechanism wherein every member of the crew gets part of the reward seems non-scalable for crews sufficiently large. I still haven't figured this one out, but perhaps time will shed some light on it.

So far, what I've gleaned is that Thieve is actually an affiliate marketing engine. Basically they draw users in with their thieving mechanism (and implied promise of free goods) and present them with a hand-selected array of goods. You can think of these goods as "featured" as users are drawn to inspecting each one for the initial purpose of vetting it for stealability, but are also cleverly being driven to vet the item for ownership.

You can see the affiliate marketing Id in the example link below (aff_trace_key):

```
https://www.aliexpress.com/item/Xiaomi-WiFi-Amplifier-Pro-300Mbps-Amplificador-Wi-Fi-Repeater-Wifi-Signal-Cover-Extender-Repeater-2-4G/32836423616.html?cv=47843&af=516290&aff_platform=aaf&mall_affr=pr3&cpt=1541874975843&afref=https%253A%252F%252Fthieve.co%252Fproducts%252Fxiaomi-wifi-repeater&sk=VnYZvQVf&aff_trace_key=99aa5421700c4b7ea3207674532e9758-1541874975843-02701-VnYZvQVf&dp=626bd00f624b332ff6f199c1d3058f05&terminal_id=0630e0a4b8794cb5a486654dea2c4270
```

Once the user has put the time in to actually think about the item, they are much more likely to go through with a purchase down the line than an arbitrary user who was not part of the site. Thieve of course has really big buttons to lead you directly to the site to buy the chosen item and they encourage the likability of the item by hand-selecting and bubbling up info on highly-rated items with relatively low cost of entry.

I'm not exactly sure what the details of the [AliExpress affiliate marketing program](https://portals.aliexpress.com/) are but we can do some simple, conservative math to see how the business model breaks down.

You can [follow along with these calculations on GitLab](https://gitlab.com/SIRHAMY/junkyard-script-calculations/blob/master/2018/thieve-co/thieve-co-revenue.py)

Let's say that, conservatively, the program pays out 2% commision on an item (Amazon pays out some categories at less than 2%, but most are above and I'd bet they intelligently choose categories that pay out higher rates, see [Amazon affiliate schedule](https://affiliate-program.amazon.com/help/operating/schedule)). That means that for a single item that's won, they would need to have that item bought at least 50 times to break even.

That may seem like a lot of times, but even if we assume that only 2% of users will click through to the site ([WordStream claims that's the avg of AdWords click-throughs](https://www.wordstream.com/average-ctr)) and that only 1 in 8 (12.5%) of those (or 0.25% overall) actually buy the item, then they only need ~20k views overall (per item steal) to break even.

Of course, these are pretty conservative conversion rates as the users are already on the site to get an item, the items that are picked are likely high quality, and they're likely getting items that are also good deals. As such, if we bump the click-through rate up to even 4%, Thieve could start breaking even at around 10k views which isn't too bad at all.

Add onto this the fact that Thieve is promoting its [Chrome extension](https://chrome.google.com/webstore/detail/thieve-aliexpress-tools/apfkfccpcldeeaampkebgommjmdoghbf?utm_source=inline-install-disabled) as part of the steal process, they could be raking in addiitonal commisions without losing any overhead to purchasing items for the winners, making it a straight profit scheme.

Where this starts breaking down a little bit for me is the long-term feasibility of a company like this due to 1) the non-scalability of giving out prizes to each member of a crew and 2) the actual value proposition of using such a service.

For 1) it starts to break down because for every additional member of a crew (which I believe is just inviting a friend to join), they are increasing their requisite totalViews proportionally. So with a crew of just 3, they've now increased their requisite views to break even from ~10k to ~30k. You could imagine a few crews growing to 50+ and how this might break their ability to break even. Of course, they might be banking on the network effects of these invites such that every member invites someone so while you do have a larger pocket of people getting these prizes, the overall pool of people who are engaged with the platform may have increased even more, slowly allowing Thieve to not only keep pace but make room for profits.

This is all well and good, but the success of 1) is also predicated on the success of 2). It doesn't matter how many people you have on the platform if they aren't active and the only way to even approximate assurity that people will stay with the platform is to provide a useful service for them. I'm sure that Thieve has a great system in place for bubbling up featured items/great deals, but there are tons of sites that already do this well and that aren't relegated to only AliExpress (see: [Honey](http://joinhoney.com/ref/hztoln), [SlickDeals](https://slickdeals.net/)).

So in the present state of things, I see them getting a big bump from their PH submission but will start to see dwindling numbers until they are able to change some of their mechanisms to include more outlets, provide more promotions, and/or include ways to save money (a la Honey) so that the perceived returns for users amount to more than a sweepstakes every now and then. If they do this fast and well enough, they might be able to convert a good amount of this user surge to lasting users. It's always good to have the ball in your court.

So, that's my take on how Thieve.co makes money. My personal take on the purchase of any good is that if you didn't know that you wanted the good until you saw it, then you don't actually need that good. This isn't to say you shouldn't buy it, but it's a useful thought process for determining worth.