---
title: "What I spend in a week living in Manhattan under Coronavirus lockdown"
date: 2020-03-15T09:39:02-04:00
subtitle: ""
tags: [ 'finance' ]
comments: true
draft: true
---

I was watching YouTube with my girlfriend a few weeks ago and we noticed that a bunch of people were putting out videos going over what they spent in a week while living in New York. I thought about this practice and figured it could be an insightful exercise for being mindful about where your money is _actually_ going and decided I'd do one myself.

Now usually I get my meals during the week at work and one of the benefits of doing this in my mind was to compare how much I was spending and on what to the allocations of others. Food can be a large expense so I put off doing this while I figured out a way to compare expenses with food and without food to regain this benefit.

Then last week we got a strong recommendation to work from home due to #coronavirus and I was like "This is it! I can make my video!". So this week I tracked all my spending - where my money was going, how much of it was going there - and will be aggregating all of it for your viewing pleasure below.

If you prefer to consume this in video format, you can also do that:

JK I didn't make a video or add any images in here cuase I lost interest. I think the content's still interesting though, so publishing the blog.

Otherwise this post will have all the numbers and calculations and stuff for you to pour over. Enjoy!

# monday

Total spend: $29.88

I woke up at my girlfriend's Monday morning to a sleuth of emails saying that [New York was strongly encouraging work from home](https://www.nytimes.com/2020/03/12/nyregion/coronavirus-new-york-update.html) and a Workplace message echoing that sentiment. So I was like well what do I do now?

I figured I needed a few things:

* coffee
* breakfast
* food for the week

With those, I should be good to wfh for at least a few days which would give me runway to figure out whatever else I needed to do.

It was at this point that I decided I would make this video.

But first coffee. And I was at my gf's which doesn't have my cold brew and even if I was at my own place, I hadn't made any coldbrew (cause who knew #coronavirus was gonna be such a big deal?) so I went to the best thing -> Dunks.

Dunks coffee - $4.08

With Dunks in hand (big turtle-killing straw and all), I set off to TJ's to stockpile some food at deliciously reasonable prices. I tried to buy things that could last awhile if necessary, that were easy to make (cause cooking is a pain), and that were relatively nutritious:

* eggs
* cheese
* spinach
* tortillas
* pre-cooked chicken
* carrots
* hummus

I also wanted to keep the load light because I had a 30 min train ride and ~15 min walk ahead of me plus I still had my Dunks to carry and I only have two hands - you do the math.

TJ's deliciously reasonably priced food - $32.29

I then headed to 33rd street station to catch the 6. I have an unlimited pass that I get paid for by work so I kind of got the train ride for free. But it's not really free because I think I still pay taxes on the cost of the unlimited pass. I ran those calculations and that means I'm paying ~$x / day for this. See full calculations HERE.

When I got home I'd had my coffee for the morning and also my food for the week but you'll notice that I neglected breakfast so I went to work making that. I eat pretty simply most mornings and this is a perfect example - 3 eggs, cheese, ketchup, and a tortilla of some sort (today it was 3 little tortillas).

HAMY - image of breakfast

This breakfast came out to ~$1.99.

I also wanted to have coffee for the rest of the week and previous calculations have deemed homebrewed coffee to be a great money saver so I made that.

At this point the morning was almost over and I was about ready for lunch so I checked my messages and found that pretty much everyone was scrambling around trying to get ready for a week (or more =() of wfh and remembered I wanted to get a different @hamy.brand hoodie size to see if it fit me better than my previous one.

@hamy.brand hoodie sample - $18.20

Then I made lunch. Idk why I decided to make this given I'd just gotten so much other food but I did and it was quite good. For lunch, I made two PB&J tacos.

HAMY - image of PB&J tacos

In hindsight, it wasn't quite enough food for me but you gotta start somewhere. On the brightside, this was super cheap coming out to $1.30.

I like to workout in the early afternoon (starting at like 2 or 3) so I did that after I worked some and then made myself a snack of carrots and hummus which came out to ~$0.63.

This didn't really fill me up that much after my workout so I made a run to the CVS across the street which is not blessed by the same reasonable prices as TJ's. I got a few snacks, toiletries, and protein bars for $25.47 - almost as much as it cost for food for the week.

For dinner, I wanted more eggs (and also couldn't eat my chicken cause am doing meatless mondays!) so I made more breakfast with a side of carrots and hummus.

dinner - $1.99 + 0.65

The next thing I consumed was some popcorn while my roommate and I continued our binge of the new season of Altered Carbon, coming out to ~$1.06.

## monday maths

While I did a lot of buying things in bulk on Monday, I think it's more accurate to just log how much of that stuff I consumed as many of the things I bought will last multiple days / weeks. So although I did "spend" that money on Monday / this week, I didn't really "consume" the value of it until I actually utilize it. Because of this, the numbers here will only reflect the fully consumed value rather than the transferred value.

Three tacos breakfast: $1.99

```
eggs_price_dozen: float = 1.49
eggs_price_one: float = eggs_price_dozen / 12

eggs_price_one

little_tortilla_count: int = 10
little_tortilla_price: float = 5.38

little_tortilla_price_one: float = little_tortilla_price / little_tortilla_count
little_tortilla_price_one

breakfast_three_tacos_price: float = eggs_price_one * 3 + little_tortilla_price_one * 3
breakfast_three_tacos_price
```

PB&J tacos lunch: $1.30

```
peanut_butter_price: float = 2.22 #this is from amazon
peanut_butter_servings: int = 34
peanut_butter_serving_price_one: float = peanut_butter_price / peanut_butter_servings

jelly_price: float = 1.98
jelly_servings: int = 42
jelly_serving_price_one = jelly_price / jelly_servings

peanut_butter_jelly_taco_price_one: float = peanut_butter_serving_price_one + jelly_serving_price_one + little_tortilla_price_one
peanut_butter_jelly_taco_price_one

monday_lunch_price: float = peanut_butter_jelly_taco_price_one * 2
monday_lunch_price
```

carrots and hummus snack: $0.63

```
hummus_price: float = 3.49
hummus_servings: int = 15
hummus_serving_price_one: float = hummus_price / hummus_servings

carrots_price: float = 1.99
carrots_servings: int = 5
carrots_serving_price_one: float = carrots_price / carrots_servings

carrots_and_hummus_price: float = hummus_serving_price_one + carrots_serving_price_one
carrots_and_hummus_price
```

popcorn: $1.06

```
popcorn_price: float = 3.19
popcorn_servings: int = 3
popcorn_serving_price_one: float = popcorn_price / popcorn_servings

popcorn_serving_price_one
```

total spend: $29.88

```
hoodie_sample_order_price: float = 18.20
coffee_dunkin_price: float = 4.08

monday_total_spend = breakfast_three_tacos_price + coffee_dunkin_price + hoodie_sample_order_price + monday_lunch_price + carrots_and_hummus_price + popcorn_serving_price_one + breakfast_three_tacos_price + carrots_and_hummus_price
monday_total_spend
```

# tuesday

total spend: $88.80

On Tuesday I woke up to two emails notifying me of subscription charges.

The first was $9.79 for MailChimp on which I run both my hamventures and Updates from the Hamniverse email lists.

The second was from my mom asking me if a $59.99 charge for Norton Antivirus software was mine or not. Norton is typically for Windows devices and I've been running Linux for the past ~2 years (with the occasional Mac in the mix) so I thought the charge was odd but then remembered I had subscribed nearly 3 years ago to protect my comp and that maybe I'd just never canceled. So I checked and the auto renew option was on, so that's $60 I won't be getting back =(.

Anywho, I again made breakfast and poured myself some coffee (this time my own) which came out to $1.99 and $0.26 respectively.

For lunch, I made some simple tacos with that pre-cooked chicken I'd bought, spinach, and a dash of hummus - coming out to ~$4.08. I like to have a coffee at lunch time as well to stave off any possible carbo comas and keep me energized through the afternoon so I poured one here as well - another $0.26.

HAMY - photo of lunch

After my workout, I ate a bar - $3.19

For dinner, I made the exact same thing sans coffee - $4.08.

HAMY - photo of dinner

I like to build things and will typically spend 1-2 hours a night working on some projects. The previous weekend I'd been working on a visualization of the spread of the coronavirus around the globe. I was about ready to release it to friends and fam for feedback but before I could do that I needed a domain, so I went ahead and purchased [coronation.xyz](https://coronation.xyz) on NameCheap for $1.18 for the year.

coronation.xyz is now live so you should check it out! A quick demo:

<iframe width="560" height="315" src="https://www.youtube.com/embed/eXF-nbkJ2uQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

After #projects time my roommate and I again sat down for popcorn and Altered Carbon - $1.09.

## tuesday maths

coffee price per cup - $0.26

```
coffee_bag_price: float = 11.44
coffee_bag_ounces: int = 20
coffee_ounces_per_cup: int = 3 # from previous calcs: https://blog.hamy.xyz/posts/case-for-self-brewing-cold-brew/#my-actual-savings
coffee_ground_cups_per_brew: float = 1.5

brews_per_bag: float = coffee_bag_ounces / (coffee_ounces_per_cup * coffee_ground_cups_per_brew)
brew_price_one: float = coffee_bag_price / brews_per_bag

coffee_cups_per_brew: int = 10
coffee_cup_price_one: float = brew_price_one / coffee_cups_per_brew
coffee_cup_price_one
```

total spend - $88.80

```
tuesday_total_spend: float = mailchimp_monthly_price + norton_yearly_price + coronation_xyz_price + breakfast_three_tacos_price + 2* coffee_cup_price_one + 2 * tuesday_lunch_price + popcorn_price + protein_bar_price
tuesday_total_spend
```

# wednesday 

total spend: $59.81

I was running out of tortillas so I ended up making my eggs without them, which actually made my breakfast a good deal cheaper.

breakfast with homebrewed coffee - $0.63

We had a competition at work for best home-cooked meal to boost #morale and I decided to submit my PB&J tacos cause they worked out so well. Unfortunately I didn't get any novelty points because one of my coworkers made a PB&J quesadilla. QQ

lunch - $1.95

post workout Quest bar - $3.19 (these things are expensive!)

My roommate's parents called and expressed their worry over the situation which prompted us to go stockpile more food. This round I focused more on food that would last quite a long time - frozen, packaged meats, etc. - as I already had a stockpile for the next week or so and wanted this food to be useful even if #coronatine only lasted a week. At this point looks like I needn't worry as we'll be quarantined for the foreseeable future.

apocalypse shopping spree - $46.97

I then went back to my gf's where we cooked dinner, watched tv, and ate snacks.

dinner - $x

## wednesday maths

```
# wednesday
apocalypse_shopping_spree: float = 46.97

wednesday_breakfast: float = eggs_price_one * 3 + coffee_cup_price_one
wednesday_breakfast

wednesday_lunch: float = peanut_butter_jelly_taco_price_one * 3 + coffee_cup_price_one
wednesday_lunch

wednesday_dinner_and_snacks: float = 10 # need real numbers from megna

wednesday_total: float = wednesday_breakfast + wednesday_lunch + wednesday_dinner_and_snacks + apocalypse_shopping_spree
wednesday_total
```

# thursday

total spend: $78.48

Because I was at gf's and no homebrewed coffee / eggy to microwave my eggs, I again went to Dunks and one of my fave breakfast places in the city Bagel Express 3. I can't vouch for the other numbers, but 3 is v good and reasonably priced.

Dunks - $4.08

Bagel Express 3 - $5.17

I'd been a little hungry after my previous lunches, so I decided to add in an extra snack alongside it to boost my calories and hopefully reduce snacking / increase energy throughout the day. For lunch I had chicken with a bed of greens, carrots and hummus, Ritz cheese crackers, and coffee to keep me lively. Yum.

Lunch - $3.91

preworkout bar - $3.19

For dinner, we went to [Nai Tapas](https://goo.gl/maps/NfJqjLfZuFFA2Tqs6) to celebrate a friend's birthday. It was a good experience, but rather odd. Usually the place is packed but we walked in to find the ground floor completely empty and what looked like the entire wait staff eagerly awaiting patronage. We sat upstairs where a live flamenco show ensued, to a half-full dining room. Tapas were v good and good portions (for tapas) even if a little on the expensive side.

dinner - $62.13

## thursday maths

```
# thursday
bagel_express_bec_price: float = 5.17

thursday_breakfast: float = bagel_express_bec_price + coffee_dunkin_price
thursday_breakfast

ritz_serving_price_one: float = 3.69 / 8
thursday_lunch: float = carrots_and_hummus_price + spinach_serving_price_one * 2 + chicken_serving_price_one + coffee_cup_price_one + ritz_serving_price_one

thursday_lunch

thursday_dinner: float = 62.13

thursday_spend: float = thursday_breakfast + thursday_lunch + protein_bar_price + thursday_dinner
thursday_spend
```

# friday

total spend: $72.10

Again cause at gf's (I really should just cook there) I got my morning Dunks and Bagel Express 3 - $9.25

On the way home, I stopped by work to pick up some computer accessories so I could complete my wfh setup and squeeze as much productivity out of myself as I could. While there, I stopped by the cafe to get a take-home lunch, drank some coffee, and picked up a few snacks to take with me.

lunch - FREE

after workout protein bar - FREE

We were all feeling a little stir crazy so decided to head to a nearby bar. We figured a local bar would be empty enough to get the benefits of social distancing but full enough to make us feel a little less isolated. The food was good and reasonably priced and the drinks were priced for hh.

dinner and drinks - $38.20

Then we wanted more drinks, so we did that too - $24

We then came back and had #drunchies so shared some oven-heated chicken nuggets with ketchup and Chicken Fil A sauce left over from a previous visit.

drunchies - $0.65

## friday maths

```
# friday
friday_breakfast: float = coffee_dunkin_price + bagel_express_bec_price
friday_breakfast

friday_lunch: float = 0

friday_dinner_and_drinks: float = 30
friday_after_dinner_drinks: float = 24

chicken_nuggies_serving_price_one: float = 6.49 / 10

friday_total_spend: float = friday_breakfast + friday_lunch + friday_dinner_and_drinks + friday_after_dinner_drinks + chicken_nuggies_serving_price_one
friday_total_spend
```

# saturday

total spend: $98.58

I was finally back at my place so I made coffee and breakfast:

breakfast and coffee - $1.55

For lunch I wanted to mix things up a bit so broke into my sandwich meats, making a bologna wrap with cheese and spinach.

lunch - $3.21

After my workout I ate another one of those bars I'd taken from work.

after workout bar - $3.19

We'd planned to go meetup with some people before dinner but decided it'd be best to limit exposure by just meeting at dinner. Because of this, we had extra time to pregame so I went and got a case of claws.

whiteclaws - $25.63

We showed up at dinner but reservations weren't ready so we went next door to grab a drink. Yeah I know we wanted to limit exposure but we also didn't want to just stand there for 20 mins.

pre dinner drink - $16

Then we ate! Amazing burgers and pizza. 10/10 would go again.

dinner - $49

## saturday maths

# sunday

total spend: $36.84

Now we're all caught up and I'm sitting here writing this while sipping my coffee. Kinda weird to write in past tense about something that will happen in the future but here we are.

breakfast - $1.55
coffee - $0.25

lunch - $3.21 - bologna wrap with ritz crackers

pre workout bar - $3.19

Then I went and bought even more stuff to prepare for the apocalypse which came out to $28.89

## sunday maths

# week total

total spent: $464.49

So my total is $464.49 which is kinda ridiculous for a week's worth of spending. Much of that comes from my food preparedness ($75.86) and going out to hang with people due to coronavirus (I don't usually do this many dinners, $165.33) which brings my usual spend to ~$220 which does seem a little more reasonable.
