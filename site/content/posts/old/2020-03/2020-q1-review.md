---
title: "2020 Q1 Review"
date: 2020-04-01T20:12:28-04:00
subtitle: ""
tags: [
    "release-notes",
    "reflections",
    'reviews'
]
comments: true
---

In the first quarter of 2020, I [created a YouTube channel](#hamy-labs-youtube-channel), [had 10,734 visitors to my core sites (a 2,632% increase from last year)](#iamhamy), [redesigned those core sites](#iamhamy-redesign), [built a data monolith](#coronation), [hit a 50% savings rate](#finance), and did a lot of [wfh](#work).

My name's Hamilton and this is my Q1 2020 in review.

If you've only got 2 minutes, check out the highlights:

<iframe width="560" height="315" src="https://www.youtube.com/embed/fMYKw6-2Ksc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# projects

In #projects I had 3 main goals:

## goal: make $500 (on side projects)

This half I have a goal to make $1000 on my side projects, so this quarter I took a goal of making $500 to stay on track for that. I didn't hit that, grossing just $55.16 this quarter.

My side project income breakdown:

* [iamhamy](#iamhamy) ad revenue - $7.27
* [hamy.brand](https://shop.hamy.xyz) sales - $47.89

Why did I goal on money?

It's not the primary goal of my side projects - those values are to build something cool, enjoyable, that challenges myself, and is impactful - but it's not completely divorced from those goals either. When it comes to impact, it depends on what the project is aiming to accomplish. But I'd found that my projects were often not accomplishing anything at all. 

For example, I built [willigetcancer](https://labs.hamy.xyz/projects/willigetcancer/) to help spread awareness about individual cancer risk to try and make the issue more personal and incite action, but it got so little views that even if it was extremely good at that, its reach was too small to matter. [mineforgood](https://labs.hamy.xyz/projects/mineforgood/) - a site that allows you to mine cryptocoins in your browser for a cause of your choice - did much better but [later analyses](https://blog.hamy.xyz/posts/make-money-for-causes-with-mine-for-good/) showed that the strategy was so inefficient that not mining was likely more effective than actually mining.

I like to run my life kind of like a business because I think that a lot of the processes businesses use to organize and execute are pretty logical and tackle real-world issues by necessity. So I thought about how businesses prioritize their projects and came to the conclusion that it was usually (hopefully) based on metrics - the idea that x project would move y metric that you care about.

While money isn't the only metric that matters, most of us have an innate mapping of that to value - a coffee is $4+ in NY and that's totally worth it - and value is the thing I wanted to make sure I was providing - a valuable experience, a valuable use case, a valuable outcome. So while not all of my projects have a pathway to monetization I wanted to use this goal as a directional pointer to keep me thinking about the value my projects were providing and to habitually create projects that could prove this when possible.

It's not a perfect solution, but it's worked pretty well so far. Through this value lens, I've been thinking about reach as value, pushing me to create [a YouTube channel](#hamy-labs-youtube-channel), [refocus my online identity](#iamhamy-redesign), and [build more, better art](#hamy-art) as well as skills as value, pushing me to explore 3d technologies in [threejs](#coronation) and Unity - all things I'd wanted to do but that had never come to the front of the priority queue.

## goal: make a collab with someone

This half one of my goals was to make an official music video. I've been building visualizers every now and then for the past ~1.5 years (starting with [moon-eye](https://labs.hamy.xyz/projects/moon-eye/)) and wanted to take that to the next level. To do that, I decided to take a goal that would force me to focus on both the vision and execution of one. I figured making a music video would do just that - it'd have to be cool enough to satisfy me, the other artist, as well as all the viewers. In other words, it would have to be all-around valuable. So I took it.

For this quarter, I actually just goaled on collabing with someone as I wasn't quite sure I'd find an opportunity to make a music video with anyone - no music, no video. I've been trying to get into some collabs for awhile but haven't been as habitual as I'd like so I figured this was a first step to making a good music video anyway.

Luckily, [Steve](https://steveis.space) was hard at work on his new album this quarter so I hopped on the chance to try and build a visualizer fit for real music. It's not quite ready for release and that's okay - life rarely fits in the arbitrary buckets we make for it - but we do hope to release it soon so if you're interested [stay tuned](https://hamy.xyz/subscribe).

## goal: make 4 YouTube videos

My last #projects goal was to create 4 YouTube videos. This was a goal I picked up in February. I was watching some YouTube with Megna and was kinda like "I could do this". So then I asked myself "should I do this?" and realized that it would be beneficial in many ways - opportunity to reach a different audience, to create complimentary video content for my word-heavy content, and help get over my fear of public speaking. So I set out to do it.

So far, I've actually released [11 videos](https://www.youtube.com/channel/UCPBY44jxP7gOMkUsP5rlGnw/videos) but I only count 4 of those in this number as they actually involve me speaking into the camera. I'll talk a little bit more about my motivations and strategy [below](#hamy-labs-youtube-channel) but long story short is that it's been a great experience and I'll likely continue working on it in the foreseeable future.

Those were my goals in #projects, now I'll dive into some of the projects I worked on this quarter along with links to play with them.

![iamhamy 2020 q1 vs 2019 q1 stats](https://storage.googleapis.com/iamhamy-static/blog/2020/2020-q1-review/iamhamy-2020-q1-vs-2019-q1-stats.png)

_iamhamy 2020 q1 vs 2019 q1 stats_

## iamhamy

For those new here, iamhamy is my collective name for my core group of sites here on the internet. I've been a big website person since around 2010 when I created my first Blogger blog so my sites are always top of mind and I like to do little recaps of them each quarter.

The sites within the iamhamy sphere:

* hamy.xyz
* blog.hamy.xyz
* labs.hamy.xyz
* shop.hamy.xyz

This quarter my core sites received 10,727 visitors. This is a 2,632% increase from last year's 2019 Q1 visitor total of 408. Now this isn't exactly a fair comparison as my sites were just gaining link authority in their new forms and I now have a bunch more sites / properties pulling in visitors but those are the numbers.

Of course, I don't just have these 5 core sites. I also have a bunch of little sites for my web based projects. Here's a breakdown over time of how these properties are contributing to these numbers.

![2020 q1 iamhamy site stats over time](https://storage.googleapis.com/iamhamy-static/blog/2020/2020-q1-review/2020-q1-iamhamy-site-stats.png)

_2020 q1 iamhamy site stats over time_

We can see that my sites this year are doing much better than last year, though their growth has returned back to normal since the spike from my [two submissions](https://blog.hamy.xyz/posts/2019-in-review/#iamhamy) to Hacker News that hit the front page late last year.

Every now and then I like to look at my top-visited pages to see what's doing well and if there might be any opportunities for optimization there. This quarter my top visited pages were:

* [How to Copy and Paste in Autodesk Sketchbook Mobile](https://labs.hamy.xyz/posts/2016-04-06-how-to-copy-and-paste-in-autodesk-sketchbook-mobile/) - 3,116 views
* [coronation](https://coronation.xyz/) - 1,019 views
* [C#: List does not contain a definition for 'OrderBy'](https://labs.hamy.xyz/posts/csharp-list-does-not-contain-a-definition-for-orderby/) - 700 views
* [React Select: how to change 'No options' placeholder text](https://labs.hamy.xyz/posts/change-react-select-no-options-placeholder-text/) - 643 views
* https://hamy.xyz/ - 616 views
* [I moved my sites from Google Kubernetes Engine to Netlify and saved $1000 / year (plus numerous hours of maintenance)](https://labs.hamy.xyz/posts/i-moved-to-netlify-from-gke-and-saved-60-per-month/) - 439 views
* https://labs.hamy.xyz/ - 356 views
* [My 2019 in review](https://blog.hamy.xyz/posts/2019-in-review/) - 307 views
* [Python: ImportError: No module named asyncio](https://labs.hamy.xyz/posts/python-asyncio-not-found/) - 300 views
* [willigetcancer](https://willigetcancer.xyz/) - 260 views

I write a lot of small technical articles in the process of building. Whenever I run into something that takes me more than ~10 minutes to solve / find, I take a few more minutes to jot that down - what the problem was and what the solution I found is. I find this often saves me time in the future - if you're doing similar things over and over you'll likely run into similar problems - and from the data it seems that other people are finding it useful as well. These posts are the backbone of where most of my traffic comes from, though you'll notice that a large bulk of my traffic actually comes from a how-to post from 4 years ago.

This quarter, I'm glad that some of my projects ([coronation](https://coronation.xyz/) and [willigetcancer](https://willigetcancer.xyz/)) and a reflection ([My 2019 in review](https://blog.hamy.xyz/posts/2019-in-review/)) were able to make it into the top 10 (and even top 5!) after not making the cut in [2019](https://blog.hamy.xyz/posts/2019-in-review/). As part of my [brand refocus](#iamhamy-redesign) (which I'll talk about in the next session) I'm trying to make my projects more valuable wrt reach and impact so this is a good sign that my efforts are working.

### iamhamy redesign

This month I was thinking about what to build next as I'm oft want to do and got to thinking about what my values were and what I wanted to be building in the future. I've been doing a lot of creative coding lately and have really enjoyed it so was thinking about how I could display and sell that better - with the sell part here meaning getting people to [collab with me](https://hamy.xyz/connect).

What I came up with was a minimal redesign that better fit my current branding and a renewed focus on my projects and getting people to build with me. To that end, I did several things:

* killed art.hamy.xyz
* created a shared styling across all my projects
* built out a `Build with me` funnel

Much of my core site structure is the same but a few tweaks here and there have made my sites feel a lot more in-tune with my current vision. For more on this redesign, read my [project post](https://labs.hamy.xyz/projects/iamhamy-redesign-build-focus/).

## HAMY Labs YouTube channel

As I mentioned in the #projects opener, I made a [YouTube channel](https://www.youtube.com/channel/UCPBY44jxP7gOMkUsP5rlGnw/featured?view_as=subscriber)!

I had a lot of reasons to do this but the primary ones were:

* reach a different audience
* build out video content
* work on my public speaking fear

For more on this, in a rather cringey but ultimately relevant fashion, check out my first ever (this year and with me talking into the camera) YouTube video!

<iframe width="560" height="315" src="https://www.youtube.com/embed/Mdmw40UM5B8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## hamy.art

[![@hamy.art](https://storage.googleapis.com/iamhamy-static/blog/2020/2020-q1-review/hamy-art-top-of-profile-web.png)](https://www.instagram.com/hamy.art/)

[@hamy.art](https://www.instagram.com/hamy.art/) is my primary outlet for finished, polished pieces before they head over to [HAMY.SHOP](https://shop.hamy.xyz). This has been my intent for the account for awhile now but I hadn't built up the backlog and processes to actually do that which meant it was often displaying more rough, experimental stuff instead. 

I love experimenting so that was fine for me but after talking to some friends and followers and looking at user behavior I found that wasn't necessarily the best approach - for me or my audience. Publishing those experiments often felt tedious and misplaced as I tried to polish what wasn't supposed to be polished. 

So I took a breath, took a step back, and rethought my approach:

* post on [@hamy.art](https://www.instagram.com/hamy.art/) when something's ready to be published. If nothing's ready, so be it.
* use [@hamy.labs](https://www.instagram.com/hamy.labs/) for what it was made for - experiments.
* take the time to do what's best for the work - frame, exhibit, publish, contextualize, and maybe just wait

My hope is that this leads to a calmer, more productive e2e process.

## coronation

I, like many others, have been under coronavirus lockdown for the past month or so. This was an odd span that gave a lot of time to just do whatever. I was extremely restless for the first few days but then decided I would channel that into something productive. So I built things.

The first thing I built during this period was [coronation.xyz](https://coronation.xyz) - a visualization of the spread of coronavirus around the globe. I've been seeing a lot of data monoliths being put out by creative technologists around the world. These are virtual structures that react to data but that don't necessarily do anything useful with that data. I was intrigued and wanted to create my own, so I did.

Here's a quick demo:

<iframe width="560" height="315" src="https://www.youtube.com/embed/eXF-nbkJ2uQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## other projects

* I wrote a [post](https://labs.hamy.xyz/posts/how-i-scale-my-sites-to-500k-users-per-month-for-free/) and corresponding [video](https://youtu.be/1s8USlQrAsU) on how I scale my sites to 500k* visitors a month for free*
* [monoform](https://labs.hamy.xyz/projects/monoform/) - a visualizer using basic 3d objs
* I'm slowly working my way through the nature of code with p5py and [posting the videos / source code](https://www.youtube.com/watch?v=aVUATf93Ro8&list=PLI2enMwPbeHid9173itBriT30dZltYRdb)

# self

In #self, my goals were to:

* keep up with my habits - diet, meditation, working out
* move to only one beef / lamb meal a week

Those are pretty self explanatory so I won't dive much into that. What I do want to talk about is my new goaling system cause it's really working for me and I like documenting stuff.

## my goaling system

This half, I restructured my goaling processes to be light-weight, flexible, and, above all, usable. I've found that life is rarely linear in the short term and creating extremely rigid short term goals led to "missing" quite often. But I've also found that life is roughly linear in the long term and having no goals or weak direction can greatly sway that slope, often towards the negative.

To try to optimize for both time frames, I created a two tier system consisting of a theme - a lens through which to view the journey - and goals - objectives to hit along the way. The goals aim to keep the long term path on track. The theme provides a framework through which to allow the inevitable short term diversions while maintaining consistency with the rest of your journey. For me, this provides balance - the discipline to get what I want done, the flexibility to do life.

The specifics are as follows:

* I have a single document (I'm using Dropbox's Paper) that lists all my goals, prioritizations, and logged efforts - halfly, quarterly, monthly, weekly
* each smaller timeframe is confined within its larger timeframe, so quarterly goals within a half would go within (really below) that half, but would also go above other, previous halves.
* previous sections get shoved to the bottom - they've already happened so aren't as important.

So an example of this might be

2020 H1

* px - goal 1
* py - goal 2
* py - goal 3

Q1

* px - goal 1.1
* py - goal 2.1
* py - goal 3.1

March

* px - goal 2.1.1

week of 2020.03.x

* px - goal 2.1.1.1

I enjoy this approach because it's simple, structured, and I can have it open at all times (which I do, in a pinned Chrome tab).

_NOTE: this is a total 180 from my nuanced, prescriptive approach laid out in my [2019 reflection](https://blog.hamy.xyz/posts/2019-in-review/#ownership) and is also my 3rd goaling system in just as many years so maybe wait a few halves before believing this is actually what I use to goal_

## finance

I had some smaller personal finance goals throughout the quarter and created a good amount of outputs that reflect that focus. Of note, I was able to increase my savings rate up to 50% which is a great achievement in my path to FIRE. Ofc I'm not gonna stop there, but milestones should be celebrated.

* created and released [thevalueofmoney](https://labs.hamy.xyz/projects/thevalueofmoney/) to house my monetary calculators
* I ran the numbers on [opening a Roth IRA](https://blog.hamy.xyz/posts/starting-my-roth-ira-investments/). By moving some of my investment allotment around, I estimate I'm making an extra ~$550 / year.
* I calculated that [I could save ~$1000 / year by making my own coffee](https://blog.hamy.xyz/posts/the-value-of-making-my-own-cold-brew/)
* I calculated that [choosing New York Sports Club over Equinox could save me ~$50,000 over 15 years](https://blog.hamy.xyz/posts/the-value-of-choosing-nysc-over-equinox/)
* Calculated that [bringing my own mat and towel to yoga was worth ~$8,800 over 15 years](https://blog.hamy.xyz/posts/the-value-of-bringing-own-mat-and-towel-to-yoga/)
* [Making my own food on weekends is worth ~$1,300 / year](https://blog.hamy.xyz/posts/the-value-of-making-own-food-on-weekends/)


# adventure

In #adventure my goals were:

* get > 100 hamventures subscribers
* go on a roadtrip w meg
* meet / hangout with 2 new people

I totally failed getting to 100 hamventures subscribers but I did come up with a new paradigm (a focus on art + tech events) and [site](https://hamventures.xyz/) that seems more true to me and its purpose and sustainable so I'll keep doing this despite its dismal returns.

We did not roadtrip, but I also didn't leave my house all month so maybe they're correlated.

I don't know if I actually met / started hanging out with 2 new people but I liked this goal as it pushed me to keep going out of my way and making connections. This is a goal that I'd like to keep doing.

So not much progress on my adventure goals but still did some adventuring around, mostly in the forms of photoshoots with [@megna.photo](https://www.instagram.com/megna.photo/):

<blockquote class="instagram-media" data-instgrm-permalink="https://www.instagram.com/p/B8wUIdjFk4b/?utm_source=ig_embed&amp;utm_campaign=loading" data-instgrm-version="12" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:540px; min-width:326px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:16px;"> <a href="https://www.instagram.com/p/B8wUIdjFk4b/?utm_source=ig_embed&amp;utm_campaign=loading" style=" background:#FFFFFF; line-height:0; padding:0 0; text-align:center; text-decoration:none; width:100%;" target="_blank"> <div style=" display: flex; flex-direction: row; align-items: center;"> <div style="background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 40px; margin-right: 14px; width: 40px;"></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 100px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 60px;"></div></div></div><div style="padding: 19% 0;"></div> <div style="display:block; height:50px; margin:0 auto 12px; width:50px;"><svg width="50px" height="50px" viewBox="0 0 60 60" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-511.000000, -20.000000)" fill="#000000"><g><path d="M556.869,30.41 C554.814,30.41 553.148,32.076 553.148,34.131 C553.148,36.186 554.814,37.852 556.869,37.852 C558.924,37.852 560.59,36.186 560.59,34.131 C560.59,32.076 558.924,30.41 556.869,30.41 M541,60.657 C535.114,60.657 530.342,55.887 530.342,50 C530.342,44.114 535.114,39.342 541,39.342 C546.887,39.342 551.658,44.114 551.658,50 C551.658,55.887 546.887,60.657 541,60.657 M541,33.886 C532.1,33.886 524.886,41.1 524.886,50 C524.886,58.899 532.1,66.113 541,66.113 C549.9,66.113 557.115,58.899 557.115,50 C557.115,41.1 549.9,33.886 541,33.886 M565.378,62.101 C565.244,65.022 564.756,66.606 564.346,67.663 C563.803,69.06 563.154,70.057 562.106,71.106 C561.058,72.155 560.06,72.803 558.662,73.347 C557.607,73.757 556.021,74.244 553.102,74.378 C549.944,74.521 548.997,74.552 541,74.552 C533.003,74.552 532.056,74.521 528.898,74.378 C525.979,74.244 524.393,73.757 523.338,73.347 C521.94,72.803 520.942,72.155 519.894,71.106 C518.846,70.057 518.197,69.06 517.654,67.663 C517.244,66.606 516.755,65.022 516.623,62.101 C516.479,58.943 516.448,57.996 516.448,50 C516.448,42.003 516.479,41.056 516.623,37.899 C516.755,34.978 517.244,33.391 517.654,32.338 C518.197,30.938 518.846,29.942 519.894,28.894 C520.942,27.846 521.94,27.196 523.338,26.654 C524.393,26.244 525.979,25.756 528.898,25.623 C532.057,25.479 533.004,25.448 541,25.448 C548.997,25.448 549.943,25.479 553.102,25.623 C556.021,25.756 557.607,26.244 558.662,26.654 C560.06,27.196 561.058,27.846 562.106,28.894 C563.154,29.942 563.803,30.938 564.346,32.338 C564.756,33.391 565.244,34.978 565.378,37.899 C565.522,41.056 565.552,42.003 565.552,50 C565.552,57.996 565.522,58.943 565.378,62.101 M570.82,37.631 C570.674,34.438 570.167,32.258 569.425,30.349 C568.659,28.377 567.633,26.702 565.965,25.035 C564.297,23.368 562.623,22.342 560.652,21.575 C558.743,20.834 556.562,20.326 553.369,20.18 C550.169,20.033 549.148,20 541,20 C532.853,20 531.831,20.033 528.631,20.18 C525.438,20.326 523.257,20.834 521.349,21.575 C519.376,22.342 517.703,23.368 516.035,25.035 C514.368,26.702 513.342,28.377 512.574,30.349 C511.834,32.258 511.326,34.438 511.181,37.631 C511.035,40.831 511,41.851 511,50 C511,58.147 511.035,59.17 511.181,62.369 C511.326,65.562 511.834,67.743 512.574,69.651 C513.342,71.625 514.368,73.296 516.035,74.965 C517.703,76.634 519.376,77.658 521.349,78.425 C523.257,79.167 525.438,79.673 528.631,79.82 C531.831,79.965 532.853,80.001 541,80.001 C549.148,80.001 550.169,79.965 553.369,79.82 C556.562,79.673 558.743,79.167 560.652,78.425 C562.623,77.658 564.297,76.634 565.965,74.965 C567.633,73.296 568.659,71.625 569.425,69.651 C570.167,67.743 570.674,65.562 570.82,62.369 C570.966,59.17 571,58.147 571,50 C571,41.851 570.966,40.831 570.82,37.631"></path></g></g></g></svg></div><div style="padding-top: 8px;"> <div style=" color:#3897f0; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:550; line-height:18px;"> View this post on Instagram</div></div><div style="padding: 12.5% 0;"></div> <div style="display: flex; flex-direction: row; margin-bottom: 14px; align-items: center;"><div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(0px) translateY(7px);"></div> <div style="background-color: #F4F4F4; height: 12.5px; transform: rotate(-45deg) translateX(3px) translateY(1px); width: 12.5px; flex-grow: 0; margin-right: 14px; margin-left: 2px;"></div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(9px) translateY(-18px);"></div></div><div style="margin-left: 8px;"> <div style=" background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 20px; width: 20px;"></div> <div style=" width: 0; height: 0; border-top: 2px solid transparent; border-left: 6px solid #f4f4f4; border-bottom: 2px solid transparent; transform: translateX(16px) translateY(-4px) rotate(30deg)"></div></div><div style="margin-left: auto;"> <div style=" width: 0px; border-top: 8px solid #F4F4F4; border-right: 8px solid transparent; transform: translateY(16px);"></div> <div style=" background-color: #F4F4F4; flex-grow: 0; height: 12px; width: 16px; transform: translateY(-4px);"></div> <div style=" width: 0; height: 0; border-top: 8px solid #F4F4F4; border-left: 8px solid transparent; transform: translateY(-4px) translateX(8px);"></div></div></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center; margin-bottom: 24px;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 224px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 144px;"></div></div></a><p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="https://www.instagram.com/p/B8wUIdjFk4b/?utm_source=ig_embed&amp;utm_campaign=loading" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">A post shared by Hamilton Greene (@sir.hamy)</a> on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2020-02-19T16:19:21+00:00">Feb 19, 2020 at 8:19am PST</time></p></div></blockquote> <script async src="//www.instagram.com/embed.js"></script>

<blockquote class="instagram-media" data-instgrm-permalink="https://www.instagram.com/p/B8gz17MFcyJ/?utm_source=ig_embed&amp;utm_campaign=loading" data-instgrm-version="12" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:540px; min-width:326px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:16px;"> <a href="https://www.instagram.com/p/B8gz17MFcyJ/?utm_source=ig_embed&amp;utm_campaign=loading" style=" background:#FFFFFF; line-height:0; padding:0 0; text-align:center; text-decoration:none; width:100%;" target="_blank"> <div style=" display: flex; flex-direction: row; align-items: center;"> <div style="background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 40px; margin-right: 14px; width: 40px;"></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 100px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 60px;"></div></div></div><div style="padding: 19% 0;"></div> <div style="display:block; height:50px; margin:0 auto 12px; width:50px;"><svg width="50px" height="50px" viewBox="0 0 60 60" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-511.000000, -20.000000)" fill="#000000"><g><path d="M556.869,30.41 C554.814,30.41 553.148,32.076 553.148,34.131 C553.148,36.186 554.814,37.852 556.869,37.852 C558.924,37.852 560.59,36.186 560.59,34.131 C560.59,32.076 558.924,30.41 556.869,30.41 M541,60.657 C535.114,60.657 530.342,55.887 530.342,50 C530.342,44.114 535.114,39.342 541,39.342 C546.887,39.342 551.658,44.114 551.658,50 C551.658,55.887 546.887,60.657 541,60.657 M541,33.886 C532.1,33.886 524.886,41.1 524.886,50 C524.886,58.899 532.1,66.113 541,66.113 C549.9,66.113 557.115,58.899 557.115,50 C557.115,41.1 549.9,33.886 541,33.886 M565.378,62.101 C565.244,65.022 564.756,66.606 564.346,67.663 C563.803,69.06 563.154,70.057 562.106,71.106 C561.058,72.155 560.06,72.803 558.662,73.347 C557.607,73.757 556.021,74.244 553.102,74.378 C549.944,74.521 548.997,74.552 541,74.552 C533.003,74.552 532.056,74.521 528.898,74.378 C525.979,74.244 524.393,73.757 523.338,73.347 C521.94,72.803 520.942,72.155 519.894,71.106 C518.846,70.057 518.197,69.06 517.654,67.663 C517.244,66.606 516.755,65.022 516.623,62.101 C516.479,58.943 516.448,57.996 516.448,50 C516.448,42.003 516.479,41.056 516.623,37.899 C516.755,34.978 517.244,33.391 517.654,32.338 C518.197,30.938 518.846,29.942 519.894,28.894 C520.942,27.846 521.94,27.196 523.338,26.654 C524.393,26.244 525.979,25.756 528.898,25.623 C532.057,25.479 533.004,25.448 541,25.448 C548.997,25.448 549.943,25.479 553.102,25.623 C556.021,25.756 557.607,26.244 558.662,26.654 C560.06,27.196 561.058,27.846 562.106,28.894 C563.154,29.942 563.803,30.938 564.346,32.338 C564.756,33.391 565.244,34.978 565.378,37.899 C565.522,41.056 565.552,42.003 565.552,50 C565.552,57.996 565.522,58.943 565.378,62.101 M570.82,37.631 C570.674,34.438 570.167,32.258 569.425,30.349 C568.659,28.377 567.633,26.702 565.965,25.035 C564.297,23.368 562.623,22.342 560.652,21.575 C558.743,20.834 556.562,20.326 553.369,20.18 C550.169,20.033 549.148,20 541,20 C532.853,20 531.831,20.033 528.631,20.18 C525.438,20.326 523.257,20.834 521.349,21.575 C519.376,22.342 517.703,23.368 516.035,25.035 C514.368,26.702 513.342,28.377 512.574,30.349 C511.834,32.258 511.326,34.438 511.181,37.631 C511.035,40.831 511,41.851 511,50 C511,58.147 511.035,59.17 511.181,62.369 C511.326,65.562 511.834,67.743 512.574,69.651 C513.342,71.625 514.368,73.296 516.035,74.965 C517.703,76.634 519.376,77.658 521.349,78.425 C523.257,79.167 525.438,79.673 528.631,79.82 C531.831,79.965 532.853,80.001 541,80.001 C549.148,80.001 550.169,79.965 553.369,79.82 C556.562,79.673 558.743,79.167 560.652,78.425 C562.623,77.658 564.297,76.634 565.965,74.965 C567.633,73.296 568.659,71.625 569.425,69.651 C570.167,67.743 570.674,65.562 570.82,62.369 C570.966,59.17 571,58.147 571,50 C571,41.851 570.966,40.831 570.82,37.631"></path></g></g></g></svg></div><div style="padding-top: 8px;"> <div style=" color:#3897f0; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:550; line-height:18px;"> View this post on Instagram</div></div><div style="padding: 12.5% 0;"></div> <div style="display: flex; flex-direction: row; margin-bottom: 14px; align-items: center;"><div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(0px) translateY(7px);"></div> <div style="background-color: #F4F4F4; height: 12.5px; transform: rotate(-45deg) translateX(3px) translateY(1px); width: 12.5px; flex-grow: 0; margin-right: 14px; margin-left: 2px;"></div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(9px) translateY(-18px);"></div></div><div style="margin-left: 8px;"> <div style=" background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 20px; width: 20px;"></div> <div style=" width: 0; height: 0; border-top: 2px solid transparent; border-left: 6px solid #f4f4f4; border-bottom: 2px solid transparent; transform: translateX(16px) translateY(-4px) rotate(30deg)"></div></div><div style="margin-left: auto;"> <div style=" width: 0px; border-top: 8px solid #F4F4F4; border-right: 8px solid transparent; transform: translateY(16px);"></div> <div style=" background-color: #F4F4F4; flex-grow: 0; height: 12px; width: 16px; transform: translateY(-4px);"></div> <div style=" width: 0; height: 0; border-top: 8px solid #F4F4F4; border-left: 8px solid transparent; transform: translateY(-4px) translateX(8px);"></div></div></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center; margin-bottom: 24px;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 224px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 144px;"></div></div></a><p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="https://www.instagram.com/p/B8gz17MFcyJ/?utm_source=ig_embed&amp;utm_campaign=loading" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">A post shared by Hamilton Greene (@sir.hamy)</a> on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2020-02-13T15:48:36+00:00">Feb 13, 2020 at 7:48am PST</time></p></div></blockquote> <script async src="//www.instagram.com/embed.js"></script>

<blockquote class="instagram-media" data-instgrm-permalink="https://www.instagram.com/p/B7lZqJplc9B/?utm_source=ig_embed&amp;utm_campaign=loading" data-instgrm-version="12" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:540px; min-width:326px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:16px;"> <a href="https://www.instagram.com/p/B7lZqJplc9B/?utm_source=ig_embed&amp;utm_campaign=loading" style=" background:#FFFFFF; line-height:0; padding:0 0; text-align:center; text-decoration:none; width:100%;" target="_blank"> <div style=" display: flex; flex-direction: row; align-items: center;"> <div style="background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 40px; margin-right: 14px; width: 40px;"></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 100px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 60px;"></div></div></div><div style="padding: 19% 0;"></div> <div style="display:block; height:50px; margin:0 auto 12px; width:50px;"><svg width="50px" height="50px" viewBox="0 0 60 60" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-511.000000, -20.000000)" fill="#000000"><g><path d="M556.869,30.41 C554.814,30.41 553.148,32.076 553.148,34.131 C553.148,36.186 554.814,37.852 556.869,37.852 C558.924,37.852 560.59,36.186 560.59,34.131 C560.59,32.076 558.924,30.41 556.869,30.41 M541,60.657 C535.114,60.657 530.342,55.887 530.342,50 C530.342,44.114 535.114,39.342 541,39.342 C546.887,39.342 551.658,44.114 551.658,50 C551.658,55.887 546.887,60.657 541,60.657 M541,33.886 C532.1,33.886 524.886,41.1 524.886,50 C524.886,58.899 532.1,66.113 541,66.113 C549.9,66.113 557.115,58.899 557.115,50 C557.115,41.1 549.9,33.886 541,33.886 M565.378,62.101 C565.244,65.022 564.756,66.606 564.346,67.663 C563.803,69.06 563.154,70.057 562.106,71.106 C561.058,72.155 560.06,72.803 558.662,73.347 C557.607,73.757 556.021,74.244 553.102,74.378 C549.944,74.521 548.997,74.552 541,74.552 C533.003,74.552 532.056,74.521 528.898,74.378 C525.979,74.244 524.393,73.757 523.338,73.347 C521.94,72.803 520.942,72.155 519.894,71.106 C518.846,70.057 518.197,69.06 517.654,67.663 C517.244,66.606 516.755,65.022 516.623,62.101 C516.479,58.943 516.448,57.996 516.448,50 C516.448,42.003 516.479,41.056 516.623,37.899 C516.755,34.978 517.244,33.391 517.654,32.338 C518.197,30.938 518.846,29.942 519.894,28.894 C520.942,27.846 521.94,27.196 523.338,26.654 C524.393,26.244 525.979,25.756 528.898,25.623 C532.057,25.479 533.004,25.448 541,25.448 C548.997,25.448 549.943,25.479 553.102,25.623 C556.021,25.756 557.607,26.244 558.662,26.654 C560.06,27.196 561.058,27.846 562.106,28.894 C563.154,29.942 563.803,30.938 564.346,32.338 C564.756,33.391 565.244,34.978 565.378,37.899 C565.522,41.056 565.552,42.003 565.552,50 C565.552,57.996 565.522,58.943 565.378,62.101 M570.82,37.631 C570.674,34.438 570.167,32.258 569.425,30.349 C568.659,28.377 567.633,26.702 565.965,25.035 C564.297,23.368 562.623,22.342 560.652,21.575 C558.743,20.834 556.562,20.326 553.369,20.18 C550.169,20.033 549.148,20 541,20 C532.853,20 531.831,20.033 528.631,20.18 C525.438,20.326 523.257,20.834 521.349,21.575 C519.376,22.342 517.703,23.368 516.035,25.035 C514.368,26.702 513.342,28.377 512.574,30.349 C511.834,32.258 511.326,34.438 511.181,37.631 C511.035,40.831 511,41.851 511,50 C511,58.147 511.035,59.17 511.181,62.369 C511.326,65.562 511.834,67.743 512.574,69.651 C513.342,71.625 514.368,73.296 516.035,74.965 C517.703,76.634 519.376,77.658 521.349,78.425 C523.257,79.167 525.438,79.673 528.631,79.82 C531.831,79.965 532.853,80.001 541,80.001 C549.148,80.001 550.169,79.965 553.369,79.82 C556.562,79.673 558.743,79.167 560.652,78.425 C562.623,77.658 564.297,76.634 565.965,74.965 C567.633,73.296 568.659,71.625 569.425,69.651 C570.167,67.743 570.674,65.562 570.82,62.369 C570.966,59.17 571,58.147 571,50 C571,41.851 570.966,40.831 570.82,37.631"></path></g></g></g></svg></div><div style="padding-top: 8px;"> <div style=" color:#3897f0; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:550; line-height:18px;"> View this post on Instagram</div></div><div style="padding: 12.5% 0;"></div> <div style="display: flex; flex-direction: row; margin-bottom: 14px; align-items: center;"><div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(0px) translateY(7px);"></div> <div style="background-color: #F4F4F4; height: 12.5px; transform: rotate(-45deg) translateX(3px) translateY(1px); width: 12.5px; flex-grow: 0; margin-right: 14px; margin-left: 2px;"></div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(9px) translateY(-18px);"></div></div><div style="margin-left: 8px;"> <div style=" background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 20px; width: 20px;"></div> <div style=" width: 0; height: 0; border-top: 2px solid transparent; border-left: 6px solid #f4f4f4; border-bottom: 2px solid transparent; transform: translateX(16px) translateY(-4px) rotate(30deg)"></div></div><div style="margin-left: auto;"> <div style=" width: 0px; border-top: 8px solid #F4F4F4; border-right: 8px solid transparent; transform: translateY(16px);"></div> <div style=" background-color: #F4F4F4; flex-grow: 0; height: 12px; width: 16px; transform: translateY(-4px);"></div> <div style=" width: 0; height: 0; border-top: 8px solid #F4F4F4; border-left: 8px solid transparent; transform: translateY(-4px) translateX(8px);"></div></div></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center; margin-bottom: 24px;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 224px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 144px;"></div></div></a><p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="https://www.instagram.com/p/B7lZqJplc9B/?utm_source=ig_embed&amp;utm_campaign=loading" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">A post shared by Hamilton Greene (@sir.hamy)</a> on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2020-01-21T14:04:36+00:00">Jan 21, 2020 at 6:04am PST</time></p></div></blockquote> <script async src="//www.instagram.com/embed.js"></script>

# work

Work is [still](https://blog.hamy.xyz/posts/2019-in-review/#work) splendid. I'm constantly challenged and envigorated by the sheer scale of our work and ownership with which I'm empowered to get it done.

Of course, given the current situation, working from home is top of mind. It's been tough. It's hard not having regular changes of scenery and harder still to convince yourself that you're in a different mode (work, play, otherwise) when you're still wearing the same clothes, sitting in the same seat, and in front of the same desk. It's been a process getting back into a productive mindset, but I think, in the long run, this has been beneficial. At its core, this is a discipline and process problem - coming up with processes to make up for the new scenario and the discipline to execute it even when no one else is watching.

I'm still working on [the same team](https://blog.hamy.xyz/posts/2019-in-review/#what-i-do-at-ig) I was last year - building, maintaining, and optimizing the infrastructure through which you download your media from Instagram. 

This quarter I've been focusing on gaining deep knowledge of my team's video pipelines as well as executing projects to prove and leverage its acquisition. Specifically, I've been ramping up in the video delivery and video playback logging space, understanding e2e how we get you the best possible videos and how we log information to better understand how the changes we make affects the experience of your video playback sessions.

The sheer amount of code and systems it takes to power these processes is still mind-boggling to me but I'm slowly getting better at these ramp ups as I gain more knowledge to leverage in the acquisition of new knowledge. 

There's a ton more to share but I don't know what's really relevant or interesting to you so I'll leave it at that with a call to action. I'm trying to do more tech talks - both personal and professional - so if you have any questions or are interested in anything I'm working on, let me know and I'll see what I can do / share.

Until then, [HMU on IG](https://www.instagram.com/sir.hamy/).

# fin

That's it for this quarter. This reflection was kind of everywhere but my mind's been kind of everywhere as well so maybe that means it's working /shrug.

If you're interested in getting updates, [subscribe to my email list](https://hamy.xyz/subscribe), [follow me on IG](https://www.instagram.com/sir.hamy/), and / or [sub to my new YouTube channel](https://www.youtube.com/channel/UCPBY44jxP7gOMkUsP5rlGnw).

If there are other things you'd like me to share - either personal or what I'm working on at IG - let me know and I'll see what I can do!

Finally, thanks for reading and good health / happiness getting through this. 

-HAMY.OUT