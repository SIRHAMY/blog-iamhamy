---
title: "By making my own cold brew instead of buying, I can save $16,844 over 15 years - and I only buy coffee 4 times a week"
date: 2020-01-05T12:32:26-05:00
subtitle: ""
tags: [ "coldbrew", "money", 'finance' ]
comments: true
---

I'm often thinking about the long-term implications of making different choices. Sometimes I attribute this to internal pessimism, curiosity, anxiety, and draw to systems and their design. Other times to boredom.

One category of choice that I've found myself pondering a lot recently is habitual monetary choices. Things like "How much does it really matter if I make my own meals instead of eating out?" and "Is brewing your own cold brew really worth the effort?" (the answer to that last one, for me, is certainly yes).

Figuring out the answers to these questions individually isn't that hard, but when it's coming up over and over it is tedious to recalculate and even more tedious to explain the system, its validity, and shortcomings to curious humans.

So I built [thevalueofmoney](https://thevalueofmoney.xyz/) to hold these processes in code for #ergonomics.

My first calculator, the [MoneyOverTime Calculator](https://thevalueofmoney.xyz/calculators/moneyovertime) aims to give an accurate picture of the value of some amount of money over time. This aims to solve those questions like "Is brewing your own cold brew really worth the effort?" as it allows me to see how that systematic change in my lifestyle could affect me long-term monetarily and then decide if the extra effort was really worth it or not. #informeddecisionmaking

# the cost of coffee

Before I can use my calculator (because it only does $$$ not products - for now), I need to first figure out how much I pay for coffee.

I like coffee a lot so I usually get a large, iced coffee regardless of season or day right when I wake up and right after lunch to stave off the #carbocoma. I prefer Dunkin' Donuts over Starbucks and there's one right around the corner from where I live so I usually go there. I also live in Manhattan so my price of coffee may be higher than you're used to seeing, depending on where you live.

We can model this with:

```
bought_cold_brew_price_dollars: float = 4.13
bought_cold_brew_frequency_per_day: int = 2

bought_cold_brew_price_per_day_dollars: float = bought_cold_brew_price_dollars * bought_cold_brew_frequency_per_day
```

Which brings my total cost per day `bought_cold_brew_price_per_day_dollars` = $8.26

I work 5 days a week (Monday through Friday) and we have generous amounts of free coffee so I always get my fix there when I can. This leaves 2 days a week where I'm buying coffee.

```
days_buying_coffee_per_week: int = 2
total_cost_of_buying_coffee_per_week_dollars: float = bought_cold_brew_price_per_day_dollars * days_buying_coffee_per_week
```

**Which means I'm paying `total_cost_of_buying_coffee_per_week_dollars` = $16.52 each week for coffee.**

In [The case for brewing your own cold brew](https://blog.hamy.xyz/posts/case-for-self-brewing-cold-brew/), I calculated the cost of each serving of cold brew to be ~$0.51.

So if we assume that that's still true (I'm definitely smarter now than I was when I made those calculations but hopefully they aren't totally wrong), we can just run that cost through the same calculations to get the price of cold brew each week.

```
brewed_cold_brew_price_dollars: float = 0.51
brewed_cold_brew_frequency_per_day: int = bought_cold_brew_frequency_per_day

brewed_cold_brew_price_per_day_dollars: float = brewed_cold_brew_price_dollars * brewed_cold_brew_frequency_per_day

days_drinking_brewed_coffee_per_week: int = days_buying_coffee_per_week
total_cost_brewing_coffee_per_week_dollars: float = brewed_cold_brew_price_per_day_dollars * days_drinking_brewed_coffee_per_week
```

**Which means the cost of brewing and drinking my own cold brew each week is `total_cost_brewing_coffee_per_week_dollars` = $2.04.**

# the value of switching to cold brew

So now that we know the cost of buying and home brewing my cold brew each week, we can figure out how much I'd save by making the switch. I'm going to assume that there are 4 weeks in every month because it makes the math easier and also means that I'll underestimate my savings which I think is good cause even if I dedicate myself to cold brewing I'm still human and will likely opt for a coffee every now and then.

```
weekly_savings_by_brewing_coffee_dollars: float = total_cost_of_buying_coffee_per_week_dollars - total_cost_brewing_coffee_per_week_dollars

weeks_in_month: int = 4
monthly_savings_by_brewing_coffee_dollars: float = weekly_savings_by_brewing_coffee_dollars * weeks_in_month
```

**This means that by switching to brewing my own cold brew vs buying it, I can save `monthly_savings_by_brewing_coffee_dollars` = $57.92 each month.**

# the real value of switching to cold brew

So we can all agree that an extra $57.92 each month is pretty nice to have, but it's not _that_ much. Like one night at the bars could easily eclipse that and it only buys you [one of my art prints](https://shop.hamy.xyz/collections/prints/products/1567476286-1713612_alipzn_uj-ijy4ofdy-poster?variant=30148617797712).

So what's the real cost of continuing my habit of buying all my coffee? 

Well besides [not helping to save the world](https://blog.hamy.xyz/posts/save-money-and-the-world-by-making-own-cold-brew/), there's also an opportunity cost of not having that money. By investing, you can (theoretically) take advantage of compound interest to grow your money by simply waiting.

This is where my [MoneyOverTime Calculator](https://thevalueofmoney.xyz/calculators/moneyovertime) again comes into play - by inputting the values we've come up with thus far in the calculation, we can see the long-term opportunity cost of not switching to cold brew.

Here are the inputs I'll be using:

* initial investment - $0
* additional investment - $57.92 each month
* years - 15 (kinda arbitrary but I like to know where I'd be at by 40)
* interest - 6% (I think this is average for the stock market?)

![Screenshot of my cold brew calculation on thevalueofmoney](https://storage.googleapis.com/iamhamy-static/blog/2020/the-value-of-making-your-own-cold-brew/MoneyOverTime-coldbrew.png)_Screenshot of [my cold brew calculation on thevalueofmoney](https://thevalueofmoney.xyz/calculators/moneyovertime?initialValueDollars=0&additionalValueDollars=57.92&timeGranularity=month&yearlyInterestRatePercent=6&timeHorizonYears=15)_

**From these calculations, we can see that these savings over 15 years are equivalent to $10,425.60 and could be up to $16,844.22 depending on the markets.**

Not too shabby for a few extra intentional minutes each week.

You can play with this calculation yourself in the [MoneyOverTime Calculator](https://thevalueofmoney.xyz/calculators/moneyovertime?initialValueDollars=0&additionalValueDollars=57.92&timeGranularity=month&yearlyInterestRatePercent=6&timeHorizonYears=15).

# fin

So yeah, I'll be brewing cold brew more often.

If you've got any feedback on my calculations in this post or on [thevalueofmoney](https://thevalueofmoney.xyz/) or on anything in general, you can always reach me via [my contact page](https://hamy.xyz/connect/).

Happy saving!

-HAMY.OUT