---
title: "The value of making my own food on weekends is ~$1,310 per year and worth ~$32,635 over 15 years"
date: 2020-01-20T11:26:56-05:00
subtitle: ""
tags: [
    "money",
    "food",
    "thevalueofmoney",
    'finance'
]
comments: true
---

I've been thinking a lot about the long-term monetary impact of habitual choices I make today. One such choice I try to make each weekend is to make my own breakfast and lunch. I don't do dinner as I often will go meet people out at restaurants which I value and think is worth the money, but breakfast and lunch by myself is an opportunity for monetary optimization.

So I set out to figure how just how valuable this habit was over the long-term.

# the value of making my own breakfast

I'm a pretty plain breakfast eater and, in Manhattan, breakfast can be kind of expensive if ordered out. The breakfast place next to me costs $7.50 for a bacon, egg, and cheese bagel (my fave) and other breakfast places tend to go around $4.50 / $5, making the average cost of breakfast ~$6.

```
cost_of_breakfast_top_dollars: float = 7.50
cost_of_breakfast_bottom_dollars: float = 4.50

average_cost_of_breakfast_dollars: float = (cost_of_breakfast_top_dollars + cost_of_breakfast_bottom_dollars) / 2
average_cost_of_breakfast_dollars
```

The breakfasts I make for myself typically consist of 3 eggs in a wrap of some sort with cheese and ketchup on top. Based on the costs of my last trip to the grocery store, this meal cost me ~$1.43 to make.

```
cost_of_an_egg_dollars: float = 2.49 / 12
cost_of_a_wrap_dollars: float = 6.49 / 8

cost_of_making_breakfast: float = cost_of_an_egg_dollars * 3 + cost_of_a_wrap_dollars

cost_of_making_breakfast
```

So right there I save ~$4.57 on average and $3.07 if comparing to the cheaper breakfast option (which we'll use as the conservative opportunity size for this practice).

```
savings_by_making_breakfast_average: float = average_cost_of_breakfast_dollars - cost_of_making_breakfast
savings_by_making_breakfast_bottom: float = cost_of_breakfast_bottom_dollars - cost_of_making_breakfast
```

I only need to make breakfast twice a week (on the weekends) as my work provides breakfast for me while I'm there. So if we multiply the savings of making a breakfast by the opportunities I have to make breakfast, then my savings come out to $474.89 on average and $318.89 when compared to the cheap breakfast per year.

By using my project [thevalueofmoney](https://thevalueofmoney.xyz/calculators/moneyovertime?initialValueDollars=0&additionalValueDollars=474.89), we find that this habit is worth ~$11,827 over 15 years based on the average cost of breakfast and $7,942 based on the cheap cost of breakfast.

[![The value of making own breakfasts over time](https://storage.googleapis.com/iamhamy-static/blog/2020/one-off/thevalueofmoney-makingownfoodweekends.png)](https://thevalueofmoney.xyz/calculators/moneyovertime?initialValueDollars=0&additionalValueDollars=318.89) _The value of making own breakfasts over time_

# the value of making my own lunch

Now that we've found the value of breakfasts, we can find the value of lunch.

In my area, eating lunch out is pretty expensive. I'd guesstimate it comes out at around $11.99 for an entree - larger if you add sides / drink and stuff.

```
cost_of_buying_lunch: float = 11.99
```

My usual lunch meal consists of various meats and veggies in a wrap. Based on my last trip to the grocer store, I estimate that this costs me ~$3.96 per meal. Putting this together with the cost of buying lunch in my area, we get average expected savings of ~$8.03 per meal.

```
cost_of_lunch_filling: float = 6.29 / 2

cost_of_lunch_dollars: float = cost_of_lunch_filling + cost_of_a_wrap_dollars

savings_opportunity_making_lunch_per_meal: float = cost_of_buying_lunch - cost_of_lunch_dollars
```

Just like breakfasts, I'm lucky to be fed at work everyday during lunch, which means I just need to make my lunch every weekend. So if we multiply the average saving opportunity of making my lunch by the number of opportunities I have to do so, we can project my total savings opportunity of $835.51 each year.

```
opportunities_making_lunch_weekly: int = 2
opportunities_making_lunch_yearly: int = opportunities_making_lunch_weekly * 52

total_savings_opportunity_making_lunch_per_year: float = opportunities_making_lunch_yearly * savings_opportunity_making_lunch_per_meal
```

Over 15 years, the total value of this habit comes out to ~$20,808.

[![The value of making own lunch over 15 years](https://storage.googleapis.com/iamhamy-static/blog/2020/one-off/thevalueofmoney-makingownfood-lunch.png)](https://thevalueofmoney.xyz/calculators/moneyovertime?initialValueDollars=0&additionalValueDollars=835.51) _The value of making own lunch over 15 years_

# the value of making own food on weekends

When we put the savings of both lunch and breakfast together, we get a total savings opportunity of $1,310 each year.

```
total_savings_opportunity_making_food_on_weekends_dollars_per_year: float = total_savings_opportunity_making_lunch_per_year + total_savings_opportunity_making_breakfast_average
```

Over 15 years, this is a total savings opportunity of $32,635.

[![The value of making own food on weekends over 15 years](https://storage.googleapis.com/iamhamy-static/blog/2020/one-off/thevalueofmoney-makingownfood-weekends.png)](https://thevalueofmoney.xyz/calculators/moneyovertime?initialValueDollars=0&additionalValueDollars=1310.4) _The value of making own food on weekends over 15 years_