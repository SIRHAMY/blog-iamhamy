---
title: "By bringing my own mat and towel to yoga instead of renting, I can save ~$8,800 over 15 years"
date: 2020-01-18T19:40:59-05:00
subtitle: ""
tags: [
    "money",
    "yoga",
    "thevalueofmoney",
    'finance'
]
comments: true
---

I go to yoga pretty regularly, around 2 times a week. Most studios have the option to rent a mat and towel each time you go so I wondered what the value of bringing my own mat and towel was over the long term. Here I'll figure that out.

I'm currently going to Core Power and they charge $2 / mat and $2 / towel rental.

We can easily calculate that by bringing my mat and towel each week, I'm saving $8 - [which is like 2 coffees where I live](https://blog.hamy.xyz/posts/the-value-of-making-my-own-cold-brew/).

```
cost_of_mat_rental_dollars: float = 2
cost_of_towel_rental_dollars: float = 2

times_at_yoga_per_week: int = 2

cost_of_renting_at_yoga_each_week_dollars: float = (cost_of_mat_rental_dollars + cost_of_towel_rental_dollars) * times_at_yoga_per_week

cost_of_renting_at_yoga_each_week_dollars
```

We can then project those savings over the course of the year and find that by bringing my own mat and towel to yoga all year, I could save $416.

```
weeks_per_year: int = 52

cost_of_renting_at_yoga_each_year_dollars: float = cost_of_renting_at_yoga_each_week_dollars * weeks_per_year

cost_of_renting_at_yoga_each_year_dollars
```

Of course, bringing your own mat and towel isn't _really_ free - you have to buy your mat and wash your towels. 

If we assume that you replace your mat every 2 years and that the average mat you'd buy costs $100 then the mat itself costs you ~$50 / year.

```
cost_of_mat_dollars: float = 100
mat_lifetime_years: int = 2

cost_of_mat_per_year: float = cost_of_mat_dollars / mat_lifetime_years
cost_of_mat_per_year
```

Based on [this article from The Simple Dollar](https://www.thesimpledollar.com/save-money/a-cost-comparison-of-home-laundry-and-laundromats/), the average load of laundry costs $0.97. If we then assume that you wash your towels in the same load and that the two towels take up ~1/4 of the load space then the cost of laundering our towels is ~$0.25 each week and ~$12.61 per year.

```
# figure from https://www.thesimpledollar.com/save-money/a-cost-comparison-of-home-laundry-and-laundromats/
cost_of_laundry_load_dollars: float = 0.97
two_towel_proportion_of_load_volume: float = 1/4

cost_of_laundering_towels_per_week: float = cost_of_laundry_load_dollars * two_towel_proportion_of_load_volume
cost_of_laundering_towels_per_week

cost_of_laundering_towels_per_year: float = cost_of_laundering_towels_per_week * weeks_per_year
cost_of_laundering_towels_per_year
```

So if we take the costs of buying and maintaining our mat and towels each year from the cost of renting each year, we can find our adjusted savings each year by foregoing renting and bringing our own mats and towels. When we do this, we find that bringing our own stuff saves us ~$353.39 each year.

That's pretty sizeable (not as much as [making your own coldbrew](https://blog.hamy.xyz/posts/the-value-of-making-my-own-cold-brew/), but sizeable), but what's the real value of that money over the long-term?

I'll calculate this using my project [thevalueofmoney](https://thevalueofmoney.xyz/calculators/moneyovertime?initialValueDollars=0&additionalValueDollars=353.39).

If we set the inputs to be:

* initial investment - $0
* additional investment - $353.39 / year
* time horizon - 15 years
* interest rate - 6%

Then the value of bringing our own stuff to yoga over 15 years is ~$8,800.

[![Screenshot of thevalueofmoney calculating the value of bringing your mat and towel to yoga vs renting](https://storage.googleapis.com/iamhamy-static/blog/2020/one-off/thevalueofmoney-bringing-mat-and-towel-to-yoga.png)](https://thevalueofmoney.xyz/calculators/moneyovertime?initialValueDollars=0&additionalValueDollars=353.39) _Screenshot of thevalueofmoney calculating the value of bringing your mat and towel to yoga vs renting_