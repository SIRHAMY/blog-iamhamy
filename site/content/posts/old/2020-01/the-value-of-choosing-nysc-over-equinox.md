---
title: "Choosing New York Sports Club over Equinox is worth $50,893 to me over 15 years"
date: 2020-01-14T09:22:49-08:00
subtitle: ""
tags: [
    "thevalueofmoney",
    "money",
    'finance'
]
comments: true
---

I often think about the tradeoffs between making different decisions and one of the common tradeoffs between decisions is the cost of doing one thing vs another. I live in New York and there are a few really big gyms that most people in my circles go to. I won't list them all here, but I've been going to New York Sports Club for the last year or so and many of my connections have started going to Equinox.

In an effort to vet my NYSC decision and determine if I wanted to switch to Equinox, I decided to do some quick calculations to help determine the monetary portion.

# NYSC v Equinox

First some assumptions we'll use throughout the course of these calculations.

* I won't be comparing the value of NYSC and Equinox wrt the services they provide. I'm going to assume that I use a gym just for a gym (like weights and stuff) and not for the showers, classes, trainers etc. as that's typically how I use gyms. This is just for monetary purposes.
* I'm basing my numbers on what I've personally experienced and the limited data I've gathered from a few connections in my city, so this likely won't hold for everyone.

I pay ~$85 / month for the NYSC passport membership

I've heard that the Equinox membership is ~$260 / month

So to determine the difference we can just subtract the NYSC membership cost from Equinox, putting us at ~$175 / month as our savings opportunity.

That's pretty sizable but what's the _real_ value of that money? We can use [thevalueofmoney's MoneyOverTime calculator](https://thevalueofmoney.xyz/calculators/moneyovertime?initialValueDollars=0&additionalValueDollars=175&timeGranularity=month) to find out.

With an

* initial investment: $0
* additional investment: $175 / month
* time horizon: 15 years
* interest: 6%

we get a total savings of $50,893.

So by choosing NYSC over Equinox, I can save $50,893 over 15 years.

[![The value of choosing NYSC over equinox](https://storage.googleapis.com/iamhamy-static/blog/2020/the-value-of-choosing-nysc-over-equinox/the-value-of-choosing-nysc-over-equinox.png)](https://thevalueofmoney.xyz/calculators/moneyovertime?initialValueDollars=0&additionalValueDollars=175&timeGranularity=month)