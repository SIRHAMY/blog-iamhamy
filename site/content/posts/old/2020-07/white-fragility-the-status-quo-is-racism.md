---
title: "White Fragility: racism is the status quo"
date: 2020-07-06T22:04:57-04:00
subtitle: ""
tags: [
    'reading',
    'social-justice',
    'contumption'
]
comments: true
---

I picked up [White Fragility](https://amzn.to/3gCq3sD) last month to continue my learnings around white privilege and racism in America. It's truly alarming how prevalent racism is in our society, particularly when coupled with the willful ignorance of its existence by the dominant class (white people).

There were a lot of enlightening thoughts in the book but I think my biggest takeaways really were that racism, in particular white supremacy, is all around us and that the machine is built in such a way that if you aren't actively fighting against it, you are helping it.

To turn this learning into action, in H2 [I'm committing to building projects](https://blog.hamy.xyz/posts/2020-h1-review/#looking-forward) that raise $1000 and reach 10k people to further the pursuit of social justice.

Other learnings that I want to remember:

* denying that we're white, that the system is racist, or that we're racist just continues racism
* we are all racist because we grew up in a racist system. it takes constant struggle to fight it and the work will never be done.
* we must examine ourselves and others to be continually antiracist or else pervade the system (which is racist) and thus be racist
* examples of white fragility include thinking 'only bad people are racist', crying / making a scene when called racist - these paths of thinking play into the racist paradigms at work in our society to maintain the status quo and ultimately maintain dominance over oppressed groups
* the dominant class has the ability to discriminate at rates much higher than other classes and thus has the power of racism on their side. It is not possible to have reverse racism as the oppressed cannot muster enough power to do that - else they wouldn't be oppressed in the first place.
* The ending of oppression must be helped by the dominant body. If the dominant body does not help, the oppressed would have to work so much harder as to make it nigh impossible to accomplish. Read another way - it is up to white people to end the oppression of other races. It is not duty of any other race for they are not the problem - we are the problem, we are the ones keeping the status quo, we are the ones with our knees on their necks. We must hold ourselves accountable to right these wrongs.

If you've been looking for a book that explores the day to day racism in society, how hard it is to notice in ourselves and others, and ways we can deal with our own racism and try to fix it, I'd recommend White Fragility.