---
title: "Save the world with a personal carbon tax"
date: 2020-07-14T01:41:53Z
subtitle: ""
tags: [
    'hamforgood',
    'climatechange',
    'carbon-offset'
]
comments: true
---

This half, some of [my goals](/posts/2020-h1-review/#looking-forward) are to raise money and awareness for climate change and to reduce my carbon footprint by 10%.

In the past I've done this by [changing my diet](/posts/2019-q3-review/#eating-less-meat) and [paying a carbon tax](/posts/what-im-doing-to-help-the-amazon/). Recently I've been intrigued by the power of economic motivators as an agent of behavior change and particularly how I could utilize those motivators myself.

One thing I've been thinking about recently is to expand my personal carbon tax to help encourage myself to make better decisions and to make up for those decisions when I make a poor choice. Now I realize that such an endeavour has a limited blast radius - it only affects me and those behaviors I choose to monitor - but my hope is that this may serve as an example for others to follow and draw inspiration from. Further it may convince people that such a tax could be useful when levied on large corporations with the ability to create a much larger blast radius.

# a personal carbon tax

Currently I have a personal carbon tax of $10 / month. This happens regardless of what I do / think in any given month. This is good because it's a passive, sustainable way to offset my carbon but is bad because it's not enough to fully offset my carbon and doesn't really teach me to be a better steward of the Earth.

One can create behavior change by matching up an unwanted behavior with a bad stimulus. An example of this is taking away the ability to watch TV from a teenager for missing curfew. In this example the unwanted behavior is missing curfew and the bad stimulus is removing TV time.

So in my case, the unwanted behavior is anything that is bad for the environment and seems reasonable not to do. The bad stimulus will be a tax. So the idea is that when I perform an unwanted behavior, I must, literally, pay the price for it.

This paradigm works on two fronts.

1. I am taxed every time I do something bad for the environment, theoretically I'll do less bad things for the environment
2. Carbon taxes help offset the carbon 'badness' of these actions, righting the wrong while I learn from it

# what do I tax?

For this thought experiment I wanted to focus on two areas that had a high carbon footprint and that I could substantially change through my habits.

1. food
2. transportation

I've slowly been making my diet more environmentally-friendly over the past few halves by cutting out red meat and eating more full vegetarian meals. Currently I consider myself poulcetarian (poultry + pesce + tarian) which means I eat chicken, fish, and veggies. I allow myself the occasional pig, cow, etc. and it's here that I think I have the largest opportunity for behavioral change.

According to the [EPA](https://www.epa.gov/ghgemissions/sources-greenhouse-gas-emissions), transportation accounts for 28.2% of America's carbon emissions. I live in NYC and do a lot of walking, training, and bussing than I would in a city without as much walkability or public transportation, but I do do a fair amount of flying and Ubering in non-quarantine times and get a ton of deliveries in current quarantine times. It's here that I think I have the largest opportunity for behavioral change.

# how do I tax?

I think the important thing for taxing is to ensure:

1. the tax covers the carbon footprint of the behavior
2. it's large enough to deincentivize the behavior
3. it's easy to implement / remember

## food

I've done deep analyses in the past on food carbon emissions and there's a lot out there already so I'm just going to keep this shallow using some Googled internet data.

Pulling data from:

* [The Case for a Carbon Tax on Beef](https://www.nytimes.com/2018/03/17/opinion/sunday/carbon-tax-on-beef.html)
* [Meat Eater’s Guide to climate change and health](http://static.ewg.org/reports/2011/meateaters/pdf/methodology_ewg_meat_eaters_guide_to_health_and_climate_2011.pdf)

We get a food to CO2 hierarchy measured in emissions kg of CO2 / kg of product something like this (in descending order):

* lamb - 20.44
* beef - 15.5
* cheese - 9.82
* chicken - 5.00
* pork - 4.9
* salmon - 4.5
* milk 2.5
* eggs - 2.4

The US doesn't have a carbon tax and [country taxes vary widely](https://www.worldbank.org/content/dam/Worldbank/document/SDN/background-note_carbon-tax.pdf) so I had to be a little creative to find a good tax rate. I ended up going with Canada cause it was easy and they seem kinda like the better US. This also means I may need to revisit these calculations in the future.

According to [carbontax.org](https://www.carbontax.org/where-carbon-is-taxed/british-columbia/), Canada is working towards a cap of $50 Canadian per tonne kg of CO2 by 2022. According to Google, this is equivalent to about $36.71 USD today. 

When we do the math, this means that for every kg of lamb I eat, I'd need to pay around (20.44 / 1000) * 36.71 = $0.75 to offset it.

That was actually way cheaper than I was expecting and I'm not sure that's large enough to de-incentivize me and it's not that easy to remember, so I'll hand-wavily set my carbon tax for beef and lamb as:

* beef - $1
* lamb - $2 (cause it's worse)

I'm not gonna tax the other things because I'm not sure I really need to - they're relatively less bad for the planet and are something I think I should keep eating for #health.

## transportation

For travel, I want to limit the amount of flight, single / low occupancy vehicle, and long distance trips I have.

**flights**

[sustainable travel](https://sustainabletravel.org/our-work/carbon-offsets/offset-footprint/)
* Short flight (less than 3 hours) 1,100 pounds CO2 - $6.47
* medium flight (3-6 hours) - $8.91
* long flight (6+ hours) - $25.01

[atmosfair](https://www.atmosfair.de/en/offset/flight/)
* New York to Atlanta (762 miles) - $6.80

So it seems that a long flight will cost me about $25. That seems big enough to de-encentivize me, will pay off pretty much any flight, and is easy to remember. $25 it is.

**single / low-occupancy vehicle**

According to [carbonfootprint.com](https://www.carbonfootprint.com/car_offsetting.html) the cost to offset the carbon footprint of the average care each year is about $6.50. That's way too low to actually change my behavior and it'd be hard to calculate each time how much I owed. So I'll just set this to $1.

**shipping**

It looks like some companies may already be doing this (like [Etsy](https://www.etsy.com/impact)) but I think it's still a good thing to get in the habit of doing. According to [Carbon Credit Capital](https://carboncreditcapital.com/offsets/shipping-offset/) the cost to carbon offset the shipping of a large package (6-20 lbs) is about $1 which seems reasonable to me as a tax rate.

# the taxes

When we put it all together, here are the taxes I end up with:

Food (charged per food item bought with it in it)

* beef - $1
* lamb - $2

transportation

* flight - $25
* car ride - $1
* shipping - $1

It doesn't look like much now but I think this could add up pretty quick. I'll report back when I have more data to share.

Cheers

-HAMY.OUT