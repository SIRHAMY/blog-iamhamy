---
title: "HAMEATS: Dorm-style HamBoard"
date: 2019-05-29T22:04:29-04:00
subtitle: ""
tags: [
    "hameats",
    'finance'
]
comments: true
---

![dorm-style hamboard](/posts/2019-05/hamboard.jpg)

# goal

The goal of a HAMEATS is to be "a unit of creation that are a) consumable, b) cheap, and c) easy to create" - [HAMEATS](/pages/hameats)

# TL;DR 

The dorm-style HamBoard is a twist on the traditional charcuterie board that comes with crackers, 4 different types of cheeses, two types of meat, and a red wine all for less than $20 and with less than 5 mins prep time. Per serving, this comes down to somewhere between $5-7 depending on individual hunger / how many alcoholics you invited over.

# ingredients

## food stuff

I bought everything from the TJ's on 14th street cause they are usually cheap, have a lot of fancy looking things, and are right next door to TJ's wine which is a must for random drinking nights like this. If you don't have a TJ's near you, hopefully you live in a low enough COL area to be able to whip something similar together.

I was opting for a slight variety in flavors but also shooting for a good price to volume ratio. idk what's good on a cheese plate besides that salami is usually there so I got a salami and then just found another meat that seemed like it'd have an exotic flavor and was ready to eat without extra cooking - that happened to be the chicken sausage.

* cheese party tray (comes with like a yellow cheese, a white cheese, a white/yellow cheese, and pepperjack) - $5.49
* sausage chicken andouille (it's like chicken but there's extra flavor) - $3.99
* sliced hot calabrese (I think it just said salami) - 2.99
* TJ's multigrain crackers - $2.29

**total:** $14.76 after tax

## drink stuff

For wine, I just got a red pinot noir from TJ's wine shop for 4.99 cause I thought red was supposed to go with meat or something.

**grand total:** slightly less than $20

# how to make

**prep time:** 5 mins (depends how long it takes you to open wine and format your HamBoard)

* get plates out
* open all packages
* put things on plates in whatever format you want
* get cups
* open wine
* pour wine
* eat
* drink
* be merry

# got updates?

Did you like hearing me write? Would you like to read more about what I'm thinking?

[Subscribe](https://hamy.xyz/subscribe) to get periodic email blasts about what I'm doing, when I'm doing them!