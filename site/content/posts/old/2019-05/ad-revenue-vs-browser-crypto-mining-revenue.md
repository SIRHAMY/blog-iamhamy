---
title: "An analysis of Ad revenue vs browser-based crypto mining revenue"
date: 2019-05-27T18:51:39-04:00
subtitle: ""
tags: [
    "ads",
    "crypto",
    "mining",
    "monero"
]
comments: true
---

_Last week I released a project ([Mine for Good](https://labs.hamy.xyz/projects/mineforgood/)) that provides a landing page for users to turn their browser into a crypto miner with the proceeds going to organizations supporting their chosen cause. Because the primary goal of the site is to create revenue to be passed through to these orgs, it got me thinking about how effective it is compared to other common website revenue paradigms. What follows is that analysis._

_The calculations behind all figures held herein can be found on [my GitLab](https://gitlab.com/SIRHAMY/junkyard-script-calculations/blob/master/2019/ad-v-crypto/AdsVsCrypto.go)_

# how do sites generate revenue?

Before diving into the analysis of ad revenue vs mining revenue, let's talk a little bit about different ways sites can make revenue:

* **Ads** - This one's likely the most common. Basically companies pay distribution partners to circulate ads through their networks. These partners then pay individual site owners to display said ads on their site, typically giving commisions based on impressions and/or clicks. This is basically how Google's Adsense network works and you can see these types of ads on Google's search page and most sites with ads.
    * **Affiliate marketing** - Affiliate marketing is basically just a sub-category of ads but instead of getting commissions on impressions or clicks of ads, you only get commissions on sales produced through your clicks. This is typically how ecommerce retailers (think Amazon) pays out for advertising. You can see ads like this on sites like Wirecutter. 
* **Direct sales** - Most sites on the internet that are not information-based are really just store fronts for different products. As such, they make money by actually selling the products for which they were built.
* **Crypto mining** - Crypto hasn't been in the mainstream lens for long so naturally this form of revenue creation is pretty nascent. Essentially crypto works by taking from a pool of potential transactions then solving math problems to determine which of those transactions should be cemented in history via the blockchain next. Because doing those math problems takes work, computers that have pitched in their power are given rewards when they produce the next correct "block" of transactions. This reward is in the form of a crypto token and can thus be transformed back into the currency of your choice which is the revenue in this paradigm. The specifics of mining can get pretty complicated so if you want to know more, do a search on it. Currently no one really uses this method as it's unclear how efficient it is (hint: it's not). The only place I'd expect you to see this as a primary form of income is on sketchy sites that wouldn't be accepted into real ad programs. Currently I think the Pirate Bay runs some but that's about it.

_So this post is geared towards just ads and crypto mining but obviously there are other paradigms. For the other paradigms, it's pretty obvious that the revenue is tied directly to the sales so don't think it's useful to go any further there anyway._

# how much do ads make?

According to [Adstage](https://blog.adstage.io/google-display-ads-cpm-cpc-ctr-benchmarks-in-q1-2018), the average cost per thousand impressions (CPM) for an ad distributed via the Google Display Network (GDN) was $2.80. If we say that the GDN takes an ~10% cut of that then we can guesstimate that about $2.50 goes to the site that was actually displaying those ads.

```
adRevenueUsdPerThousandImpressions := 2.50
adRevenueUsdPerImpression := adRevenueUsdPerThousandImpressions / 1000 // $0.0025
```

So for each visitor, on average, we're making something close to **$0.0025**.

# how much does browser-based crypto mining make?

Crypto mining is a little tougher to calculate as it depends not only on the number of site visitors but also on the amount of time that a user stayed on a site, the difficulty of mining the given cryptocurrency, the number of other miners out there, the power of each machine mining, and the going price of the corresponding token at time of conversion to your destination currency. 

[According to a 2017 study by Brafton](https://www.brafton.com/blog/strategy/brafton-2017-content-marketing-benchmark-report/), **the average session duration is 2 minutes and 17 seconds**.

```
averageSessionDurationSeconds := 137
```


At time of writing, [CoinMarketCap](https://coinmarketcap.com/currencies/monero/) has Monero (the coin my project mines) at **$97.87 / coin**.

```
cryptoTokenPriceUsd := 97.87
```

Using the [climage change cause](https://mineforgood.xyz/climatechange/) on my site as our test harness, **my desktop gets ~50 hashes / second**. My desktop is pretty good though so we should consider this to be on the upper end of what any visitor's machine would be able to produce. For comparison, that same page on **my phone (Pixel 3) gets ~6 hashes / second**.

```
hashesPerSecondDesktop := 50
hashesPerSecondMobile := 6
```

I don't think anyone in their right mind would go out and build their own mining pool for each of their sites so we'll use a ready-made mining pool, [Minero](https://minero.cc), to help us calculate how much we can actually get paid for browser-based crypto mining. As of the time of writing, **Minero pays out 0.000064527455 XMR / million hashes**. _Note that they do take a cut but so do ad networks when they distribute ads and I feel like the cut (10% as of writing) is well worth not sinking the engineering resources into building and maintaining one's own pool._

```
xmrRewardPerMillionHashes := 0.000064527455
usdRewardPerMillionHashes := xmrRewardPerMillionHashes * cryptoTokenPriceUsd
usdRewardPerHash := usdRewardPerMillionHashes / 1000000 // 0.000000006315
```

Now to determine how much each visitor is worth to us, we have to combine our `usdRewardPerHash`, `hashesPerSecondX`, and the `averageSessionDurationSeconds` found earlier

```
valuePerVisitorCryptoMiningUsdOnDesktop := hashesPerSecondDesktop * usdRewardPerHash * averageSessionDurationSeconds // $0.0000433
valuePerVisitorCryptoMiningUsdOnMobile := hashesPerSecondMobile * usdRewardPerHash * averageSessionDurationSeconds // $0.00000519
```

So for each visit, on average, we're making something close to **$0.0000433 in the best case and $0.00000519 in the worst** (worst case being they stay just as long but are viewing on a weak computer and thus can't mine as well).

# ads v crypto

A cursory glance through the above calculations concludes that ads are obviously paying more per visit than crypto. How much more? **~58x more!**

```
adRevenueUsdPerImpression / valuePerVisitorCryptoMiningUsdOnDesktop // 57.790348338797784
```

That's even assuming the best case for crypto in which everyone who visits is running on a solid machine. In many cases, they'll be using mobile phones or old hardware. In the case where all visitors are on their phone, the ads pay closer to **480x more** than crypto alone!

```
adRevenueUsdPerImpression / valuePerVisitorCryptoMiningUsdOnMobile // 481.5862361566481
```

# the viability of crypto-based online revenue

So we can see that ads are far and away better for monetizing the average session. Is crypto thus an infeasible path to revenue on the modern web?

Not necessarily. If we tweak some parameters, we might be able to get crypto back in the fight.

We can't really change the market price of XMR or the kind of computer a user decides to visit us with but we can affect how long our user stays with us. This can be done in numerous ways - think a site that serves games or a site that mostly serves long reads. This helps us because the longer a user stays on our site, the more time we have to compute hashes resulting in a linear increase in our resultant revenue.

So let's say we have a site that produces long form content and because our content is longer, we have an average session duration of ~10 minutes.

```
longFormContentSessionDurationSeconds := 600.0
```

This means that, for visitors on desktop, the average value per visitor is **$0.00019**.

```
valuePerVisitorOfLongFormCryptoMiningUsdOnDesktop := hashesPerSecondDesktop * usdRewardPerHash * longFormContentSessionDurationSeconds // 0.00018945906062549998
```

While still not as good as the ad generation of ~$0.0025, we got a lot closer. Instead of ads being ~58x better, now they're at **~13x better**.

Is this a win for crypto then? Certainly not. Recall that the 13x figure is when crypto is at its best - a user running a powerful computer to visit your site. If all visitors use mobile, then ads are still **~100x better** than crypto mining.

# so what good is crypto?

In today's internet landscape, it seems that browser-based crypto mining can't really stand up to the revenue produced by ads. This makes sense as if it ever could, there would likely be a large influx of powerful machines to the mining ecosystem which would quickly lower the rewards of mining again.

For the average site, crypto mining doesn't seem feasible and is likely more trouble than it's worth. For sites boasting long-form content of some kind, it's possible that crypto starts to make a little more sense if taken as a hybrid approach. In that case, it's possible to increase prospective revenue by anywhere from 1-10% depending on the visitor's machine. Just be sure to weigh the potential benefits with the downsides like increased load on your visitors' machines and potential blowback if you mine without their consent.

# tl;dr

In conclusion, crypto is likely not a great revenue paradigm for your site. If you can do ads, start with ads. There are some exceptions where crypto becomes a bit more appealing, but it still seems its effectiveness is limited to a complimentary capacity.

What this means for me is that I'll be looking into how to incorporate ads on [Mine for Good](https://labs.hamy.xyz/projects/mineforgood/) in the hopes of making it actually useful.

# got updates?

Did you like hearing me write? Would you like to read more about what I'm thinking?

[Subscribe](https://hamy.xyz/subscribe) to get periodic email blasts about what I'm doing, when I'm doing them!