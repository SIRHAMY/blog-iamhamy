---
title: "Release notes: April 2019"
date: 2019-05-10T17:06:17-04:00
subtitle: ""
tags: [
    "release-notes",
    "reflections"
]
comments: true
---

It's been awhile since my last update but not because I've been lazy, moreso because I've been so busy getting shit done. As such, this update will be terse so I can go back to focusing on higher priority items. But I knew y'all were wondering what I was up to so didn't want to leave you _entirely_ in the dark.

# work

Currently my focus is on the next step in my career. For the past few months I've been asking myself:

* What do I want to accomplish?
* How will I accomplish it?
* Where will I accomplish it?

It's mid may and I'm still not totally sure on the last two but I think I've come up with a pretty solid answer for the first so I thought I'd share. In my next career sprint, I want to:

* **Understand how platforms scale.** And by scale I mean _really_ scale. Like how are they dealing with thousands/millions of concurrent users kind of scale.
* **Lead bigger / harder projects.** I've mostly decided I want to stay in the product development side of things for the foreseeable future but that doesn't mean I only want to code what's handed to me! There are other skills involved in bringing a software project from genesis to maturity and I feel like the best way to highlight these skills and subsequently improve on them is to get experience doing it. 

Aside from those, my tenants from [my first job search](https://medium.com/@SIRHAMY/in-review-my-fall-2016-1b4c24d2c6c7) generally hold:

* great (smart, good, awesome, etc.) people
* real && challenging problems
* a focus on self-improvement / learning

I've thus been using a combination of the above to help direct my search. More details to come when I wrap up my search, tentatively scheduled for end of May / early June.

# projects

Aside from studying up for interviews, I've allocated a little bit of effort towards my side projects. Herein lie updates. If you have feedback, [hmu](https://hamy.xyz/connect).

## Will I get cancer?

[Will I get cancer?](https://labs.hamy.xyz/projects/willigetcancer/) is a small project I've been working on for the past few months as part of [Tikkun Olam Tuesdays](https://blog.hamy.xyz/posts/introducing-tikkun-olam-tuesdays/) or my weekly (at least ideally) allocation towards making the world a better place. The problem this project tries to tackle is that while there's a lot of cancer research out there, people still don't have a good grasp of how likely they are to be intimately effected by it. It does this by aggregating data from respected cancer research organizations in a way that's human-centric.

But enough talk, the best way to see what's what is to [see it live](https://willigetcancer.xyz)

## writing

My writing output has been down since I started prioritizing interview study but I think it's still an important part of development for me and is the only way I'm going to get my presence back to anywhere near pre-sirhamy[dot]com levels. So I persist. Some pieces I think are worth highlighting:

* [Imagining personal DOS attacks (or how to ransomware your friends in 17 lines of JavaScript or less)](https://blog.hamy.xyz/posts/imagining-personal-dos-attacks/)
* [Zeit Now: Gitlab CI Integration](https://labs.hamy.xyz/posts/zeit-now-gitlab-ci-integration/)
    * I cross-posted this one to [dev.to](https://dev.to/sirhamy) and while I didn't get a huge amount of reactions, it was easy and seems like a good way to continue building presence

## zima-blue

Sometime in the past few months (idk, time's been flying by), I sat down and binged Netflix's _Love Death and Robots_. It was v good and would highly recommend. Anywho, one of the episodes, _zima blue_, inspired me to make in the usual way I'm inspired to make.

_them: here's my thing._

_me: that looks cool. Bet I could do it better._

So I made:

<blockquote class="instagram-media" data-instgrm-permalink="https://www.instagram.com/p/BwW7QSMDJns/" data-instgrm-version="12" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:540px; min-width:326px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:16px;"> <a href="https://www.instagram.com/p/BwW7QSMDJns/" style=" background:#FFFFFF; line-height:0; padding:0 0; text-align:center; text-decoration:none; width:100%;" target="_blank"> <div style=" display: flex; flex-direction: row; align-items: center;"> <div style="background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 40px; margin-right: 14px; width: 40px;"></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 100px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 60px;"></div></div></div><div style="padding: 19% 0;"></div> <div style="display:block; height:50px; margin:0 auto 12px; width:50px;"><svg width="50px" height="50px" viewBox="0 0 60 60" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-511.000000, -20.000000)" fill="#000000"><g><path d="M556.869,30.41 C554.814,30.41 553.148,32.076 553.148,34.131 C553.148,36.186 554.814,37.852 556.869,37.852 C558.924,37.852 560.59,36.186 560.59,34.131 C560.59,32.076 558.924,30.41 556.869,30.41 M541,60.657 C535.114,60.657 530.342,55.887 530.342,50 C530.342,44.114 535.114,39.342 541,39.342 C546.887,39.342 551.658,44.114 551.658,50 C551.658,55.887 546.887,60.657 541,60.657 M541,33.886 C532.1,33.886 524.886,41.1 524.886,50 C524.886,58.899 532.1,66.113 541,66.113 C549.9,66.113 557.115,58.899 557.115,50 C557.115,41.1 549.9,33.886 541,33.886 M565.378,62.101 C565.244,65.022 564.756,66.606 564.346,67.663 C563.803,69.06 563.154,70.057 562.106,71.106 C561.058,72.155 560.06,72.803 558.662,73.347 C557.607,73.757 556.021,74.244 553.102,74.378 C549.944,74.521 548.997,74.552 541,74.552 C533.003,74.552 532.056,74.521 528.898,74.378 C525.979,74.244 524.393,73.757 523.338,73.347 C521.94,72.803 520.942,72.155 519.894,71.106 C518.846,70.057 518.197,69.06 517.654,67.663 C517.244,66.606 516.755,65.022 516.623,62.101 C516.479,58.943 516.448,57.996 516.448,50 C516.448,42.003 516.479,41.056 516.623,37.899 C516.755,34.978 517.244,33.391 517.654,32.338 C518.197,30.938 518.846,29.942 519.894,28.894 C520.942,27.846 521.94,27.196 523.338,26.654 C524.393,26.244 525.979,25.756 528.898,25.623 C532.057,25.479 533.004,25.448 541,25.448 C548.997,25.448 549.943,25.479 553.102,25.623 C556.021,25.756 557.607,26.244 558.662,26.654 C560.06,27.196 561.058,27.846 562.106,28.894 C563.154,29.942 563.803,30.938 564.346,32.338 C564.756,33.391 565.244,34.978 565.378,37.899 C565.522,41.056 565.552,42.003 565.552,50 C565.552,57.996 565.522,58.943 565.378,62.101 M570.82,37.631 C570.674,34.438 570.167,32.258 569.425,30.349 C568.659,28.377 567.633,26.702 565.965,25.035 C564.297,23.368 562.623,22.342 560.652,21.575 C558.743,20.834 556.562,20.326 553.369,20.18 C550.169,20.033 549.148,20 541,20 C532.853,20 531.831,20.033 528.631,20.18 C525.438,20.326 523.257,20.834 521.349,21.575 C519.376,22.342 517.703,23.368 516.035,25.035 C514.368,26.702 513.342,28.377 512.574,30.349 C511.834,32.258 511.326,34.438 511.181,37.631 C511.035,40.831 511,41.851 511,50 C511,58.147 511.035,59.17 511.181,62.369 C511.326,65.562 511.834,67.743 512.574,69.651 C513.342,71.625 514.368,73.296 516.035,74.965 C517.703,76.634 519.376,77.658 521.349,78.425 C523.257,79.167 525.438,79.673 528.631,79.82 C531.831,79.965 532.853,80.001 541,80.001 C549.148,80.001 550.169,79.965 553.369,79.82 C556.562,79.673 558.743,79.167 560.652,78.425 C562.623,77.658 564.297,76.634 565.965,74.965 C567.633,73.296 568.659,71.625 569.425,69.651 C570.167,67.743 570.674,65.562 570.82,62.369 C570.966,59.17 571,58.147 571,50 C571,41.851 570.966,40.831 570.82,37.631"></path></g></g></g></svg></div><div style="padding-top: 8px;"> <div style=" color:#3897f0; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:550; line-height:18px;"> View this post on Instagram</div></div><div style="padding: 12.5% 0;"></div> <div style="display: flex; flex-direction: row; margin-bottom: 14px; align-items: center;"><div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(0px) translateY(7px);"></div> <div style="background-color: #F4F4F4; height: 12.5px; transform: rotate(-45deg) translateX(3px) translateY(1px); width: 12.5px; flex-grow: 0; margin-right: 14px; margin-left: 2px;"></div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(9px) translateY(-18px);"></div></div><div style="margin-left: 8px;"> <div style=" background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 20px; width: 20px;"></div> <div style=" width: 0; height: 0; border-top: 2px solid transparent; border-left: 6px solid #f4f4f4; border-bottom: 2px solid transparent; transform: translateX(16px) translateY(-4px) rotate(30deg)"></div></div><div style="margin-left: auto;"> <div style=" width: 0px; border-top: 8px solid #F4F4F4; border-right: 8px solid transparent; transform: translateY(16px);"></div> <div style=" background-color: #F4F4F4; flex-grow: 0; height: 12px; width: 16px; transform: translateY(-4px);"></div> <div style=" width: 0; height: 0; border-top: 8px solid #F4F4F4; border-left: 8px solid transparent; transform: translateY(-4px) translateX(8px);"></div></div></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center; margin-bottom: 24px;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 224px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 144px;"></div></div></a><p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="https://www.instagram.com/p/BwW7QSMDJns/" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">A post shared by HAMY.LABS (@hamy.labs)</a> on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2019-04-17T13:24:14+00:00">Apr 17, 2019 at 6:24am PDT</time></p></div></blockquote> <script async src="//www.instagram.com/embed.js"></script>

Right now it's pulling a random photo from the [Unsplash API](https://unsplash.com/developers) and generating a rectangle over it. I have plans to create a web frontend for it to display generations made daily as both a way to complete the project and learn more about cloud hosting options, particularly cdn. But my priorities lie elsewhere so that could be a minute. 

I'll also make a real [project](https://labs.hamy.xyz/projects) page for it eventually but not pressing rn.

_Yes this project is stupid but I promise you'll have more appreciation for it once you watch LDR, kthxbye._

# adventures

Big adventures have mostly been put on hold and even local adventures have taken a back seat to my interviewing priorities. That being said, I did make it out to some cool places. You can [check the Insta](https://www.instagram.com/sir.hamy/) for more.

Some cooler news is that I believe I've found a good hamventures template that both displays the relevant info in an easy-to-parse and attractive way while also being relatively easy / fast for me to put together. This was always a goal of mine as looking up adventures takes so much time already, I wanted to limit the extra cost incurred by sharing them. You can see the newest theme from [my last hamventure post](/posts/hamventures-nyc-2019-04-15).

# self

A lot of the last few months have been focused outward rather than inward. And that's okay.

The important thing is to notice and adjust accordingly.

Some things I've been doing well

* keeping active (physically)
* meditating regularly ([my hot takes here](/posts/one-month-of-meditation/))

Some things I can improve on

* regularly creating and sticking to my goals
* eating cheap and healthy
* keeping stretchy (via yoga mostly)
* regularly healing the world (mostly via [Tikkun Olam Tuesdays](/posts/introducing-tikkun-olam-tuesdays/))

# fin

My hope is that a lot of the (unplanned) irregularity in my life will die down near the end of May and then I can begin focusing more on re-balancing via my habits. Only time will tell if that's true or not, but I'm confident I can handle it either way.

Live long and prosper.

-HAMY.OUT