---
title: "Headspace in computer terms"
date: 2019-05-06T11:20:49-04:00
subtitle: ""
tags: [ 
    "headspace",
    "meditation",
    "computer" ]
comments: true
---

This year, I've made it a habit to meditate each morning (well, most mornings). I've been using [Headspace](https://www.headspace.com/) to assist me in this process and [I've noted several positive changes](/one-month-of-meditation), some that were expected and some that weren't. Naturally, I wanted to share this experience with those around me but I noticed that I kept describing the major benefit of regular meditation as increased "headspace" but was having a hard time describing what exactly that was and why it was beneficial (and, further, why it wasn't just the product of great marketing but a pragmatic descriptor of the underlying phenomenon). 

I work as a software engineer (but [hamgineer](https://labs.hamy.xyz) would be more appropriate) and over time I've come to realize that what SWEs really do is design processes and because that's at the core of what we do, we've developed a rich vocabulary to define these processes. Because life, too, is a set of processes, it's often useful to utilize that rich technology-focused vocabulary to describe similar happenings found elsewhere in life. So in this post I'll try to define headspace in more descriptive terms by leveraging the established vocabulary found in and around technology.

# Your brain as a computer

In order to continue with this analogy we must first establish a baseline of similarity. For these purposes, I'll assert that a brain is like a computer. It takes inputs and produces outputs like sight or hearing and speech or movement. There are background processes like breathing, blinking, and balance. There are resources that are allocated and replenished like energy via food or focus. There are consequences when we over allocate like fatigue, stress, and irritation.

There exist many more similarities from which we could draw parallels but for now I believe these suffice.

# Keeping a computer running smoothly

_For the coming thought experiment we're going to assume that your computer is healthy in that it is without weird corruption or poorly crafted/malicious processes that might cause it to act abnormally._

When diagnosing a computer with performance problems, under normal conditions, there are a few things you look at first

- CPU - how hard is the computer working
- RAM - how much is the computer trying to keep "front of mind" right now
- Memory - how much is the computer trying to keep track of in aggregate

A computer that is displaying some sort of performance / function issues typically has reached its threshold for one of these metrics. Thus a well functioning computer will try to maintain a usage percentage somewhere in the 60-90% range to allow buffer room between actual usage and its maximum capacity. This allows it to run smoothly while also making it robust to load variance that may occur due to an arbitrary process needing some extra resources for some reason. A reason for this may be that its task got harder or the process decided to work harder towards its completion, for instance why your computer might take a perf hit when you open a new Chrome tab on a media-rich website. 

# Keeping a human running smoothly

Similarly to a computer, we have our own processes each with their own tasks. If we were to think of a process for our job, for instance, maybe some tasks are answering emails, going to meetings, and writing reports. For our personal upkeep this might have showering, shaving, and even taxes.

Each of these processes requires resources to schedule and execute their tasks. In just the same way that an arbitrary process may suddenly require extra resources to run so too could a human process require extra resources, ["I'm gonna need you to come in on saturday"](https://knowyourmeme.com/memes/that-would-be-great). In the human world, such variance is often reasoned away with things like Murphy's law, "life isn't fair", or other such maxims.

<img src="https://sayingimages.com/wp-content/uploads/duty-on-saturday-office-space-meme.jpg">

So just as buffer room is required to keep a computer running smoothly, so too do we need buffer room to allow us to progress against our own processes smoothly.

We call this buffer room **headspace**.

It is keeping your resource usage, both mental and physical, at a sustainable level so that when an arbitrary process starts to get resource hungry, you have the ability to satiate it without crashing the rest of them.
