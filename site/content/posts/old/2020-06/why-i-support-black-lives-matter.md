---
title: "I support Black Lives Matter because I believe in basic Human Rights"
date: 2020-06-14T08:37:14-04:00
subtitle: ""
tags: [
    'blacklivesmatter',
    'hamforgood'
]
comments: true
---

Protests have erupted all over the world after several videos of brutal killings by police of unarmed Black people in the United States circulated the internet. Personally, I've struggled reconciling different perspectives on police, race, and their relationship with society and further with recognizing my White, cis, middle class, educated, (etc) privilege and its effect on my life. As an exercise in my journey of understanding my privilege and as another stab at allyship as a verb I wanted to write down some thoughts on why I stand with Black Lives Matter and you should too.

# As an American, I believe all [humans] are created equal

In the Declaration of Independence, it's stated that:

> We hold these truths to be self-evident, that all men are created equal, that they are endowed by their Creator with certain unalienable Rights, that among these are Life, Liberty and the pursuit of Happiness.

In the 14th Amendment to the United States Constitution:

> All persons born or naturalized in the United States, and subject to the jurisdiction thereof, are citizens of the United States and of the State wherein they reside. No State shall make or enforce any law which shall abridge the privileges or immunities of citizens of the United States; nor shall any State deprive any person of life, liberty, or property, without due process of law; nor deny to any person within its jurisdiction the equal protection of the laws.

To me, this means that all humans should be treated equally under the law - and further should have laws and systems put in place to combat inequity due to extra-legal circumstance. One of the things the Black Lives Matter movement is bringing attention to and fighting is that this just isn't the case for People of Color and, in particular, for Black people.

As an American, and of course as a human, I therefore see it as my duty to work to right this inequity in the pursuit of making these values that are so core to our nation be reality vs theory.

* [Minneapolis Police Use Force Against Black People at 7 Times the Rate of Whites](https://www.nytimes.com/interactive/2020/06/03/us/minneapolis-police-use-of-force.html)

# As a Jew, I believe we must stand up for those who are being unduly persecuted

I believe Black Lives Matter is a movement that is representing and fighting for an underrepresented and unduly persecuted group of humans which reminds me of learnings from the Holocaust and should not be dismissed.

The mantra that I took away from those learnings and that is prominently displayed in most works related to the Holocaust is 'Never forget'. Like allyship, I believe that doing so is more a verb rather than an identity - it means not just to retain the knowledge that the Holocaust happened, but to be constantly vigilant for the circumstances and machinations that made such an atrocity possible and to work against them whenever you find them.

For me, this quote from Martin Niemoller encapsulates the idea that complacency puts you on the side of the oppressor and that if you delay action for too long, you may never have another chance to right that wrong:

> First they came for the socialists, and I did not speak out—because I was not a socialist.

> Then they came for the trade unionists, and I did not speak out— because I was not a trade unionist.

> Then they came for the Jews, and I did not speak out—because I was not a Jew.

> Then they came for me—and there was no one left to speak for me.

When I juxtapose these two scenarios - I see striking similarities in the complacency of the majority and the privileged. I don't think it's right to be persecuted in the things you believe in - as Jews have been time and time again - so long as those things aren't unduly infringing on the rights of others, so I think it's ridiculous to be persecuted for things you can't even control - like race, gender, disability, etc, etc, etc.

As a Jew, I therefore see it as my duty to stand up against these injustices, to help those in need, and in general make this world a better place for all of us to live in.

_Sources_

* [MARTIN NIEMÖLLER: "FIRST THEY CAME FOR THE SOCIALISTS..."](https://encyclopedia.ushmm.org/content/en/article/martin-niemoeller-first-they-came-for-the-socialists)

# As a human, I believe all humans should be judged based on their actions, not by things they can't control

We have no evidence to suggest that when we're birthed into this world we have any say in who we come out of, where we come out, or the circumstances in which we come out. In effect, we are randomized with respect to the environment in which we are spawned.

Because of this, I'd argue it's equally likely that your consciousness came into existence in Manhattan as it was to appear in Barcelona, Moscow, or Lagos. We can take this further, saying that your sexual preference, skin color, and number of toes are additional examples of things that you have no control over. If we take the approach that the same consciousness could appear anywhere to any circumstances, it seems odd that we'd have different rules for the same consciousness simply based on the circumstances and form in which it manifested.

As a human, I wouldn't want to be treated differently based solely on a dice roll, so I see it as my duty to work against rules / machinations that do treat humans differently based solely on the circumstances of their birth. In the United States, and seemingly around the world, People of Color and, in particular, Black people are being treated far worse than white people are. You can't choose your skin color, so it's unfair to be treated differently because of it - Black Lives Matter is working to bring attention to and fighting against this, so I support Black Lives Matter.

# What you can do

To sum it up, I believe that humans should be treated equitably regardless of their circumstances. I feel strongly that it is the duty of those with privilege to support those in need. For those reasons, I support the Black Lives Matter movement.

If you don't believe these things, I'd urge you to take a few moments to try and put yourself in the shoes of the persecuted. I think this is a strong technique for building empathy and encouraging action.

If you still disagree, I will respect your opinion. We are all entitled to them. But I want you to know that you are not on the side of human rights, you are willfully infringing upon the freedoms of others, and that I will fight you every step of the way to protect those rights from you.

If you're interested in taking action, I want to signal boost this compilation of [anti-racism resources](http://bit.ly/ANTIRACISMRESOURCES) by people far more informed, experienced, and active than I am / have been. It lists a bunch of different action items you can take today, ranging from learning, to giving, to doing. Pick a few and do them, then do the same next week, and the week after that - repeat.

# What I'm doing

I am still new to this journey and I know that I have a long way to go. Despite my passion for the cause now, I realize that I've been on the side of complacency for basically all my life and I fear that in a few weeks or months that feeling / state of being may come back.

I want to say that this won't happen, but I realize such a thing is easy to promise and hard to do. So I'm taking actions today to help ensure that I continue to practice allyship (verb) and support human rights far beyond the summer of 2020. 

## Measuring allyship through action rather than words

The first thing I'll be doing is measuring my fulfillment of these duties based on the impact of my actions rather than my words. Words are cheap and easy but I think what really makes a difference in most cases is your actions - by measuring with actions I hope to encourage them over words.

## Goaling on making the world a better place

I'm making bettering the world a part of my projects mission statement which informs my goaling each half. By doing this I hope to take a larger emphasis on social impact projects each half which will hopefully yield larger impact over time. On a shorter time scale, I'm taking goals for H2 2020 to push for an anti-racist United States - centered around supporting the Black Lives Matter movement and working for social justice via the presidential election in November.

## Educating myself through books

While action is good, I think educated action is better. As I've said, I've got a lot to learn - and, really, the learning never stops - so I've added several books on white privilege, systemic racism, and allyship to my reading list to help inform and increase the efficacy of my actions moving forward. I'll list those I've added thus far below for posterity and so you might consider adding them to your own list. I believe this needs to be a sustainable practice, so I may not read all of these this month or even this year but will build a habit of continual learning about the state of the world.

A selection of books I've added to my list to learn more about race and society:

* [The Next American Revolution: Sustainable Activism for the Twenty-First Century](https://www.goodreads.com/book/show/9674787-the-next-american-revolution)
* [The New Jim Crow: Mass Incarceration in the Age of Colorblindness](https://www.goodreads.com/book/show/6792458-the-new-jim-crow)
* [The Fire Next Time](https://www.goodreads.com/book/show/464260.The_Fire_Next_Time)
* [So You Want to Talk About Race](https://www.goodreads.com/book/show/35099718-so-you-want-to-talk-about-race)
* [How to Be an Antiracist](https://www.goodreads.com/book/show/40265832-how-to-be-an-antiracist)
* [White Fragility: Why It’s So Hard for White People to Talk About Racism](https://www.goodreads.com/book/show/43708708-white-fragility)
* [Black Feminist Thought: Knowledge, Consciousness, and the Politics of Empowerment](https://www.goodreads.com/book/show/353598.Black_Feminist_Thought)
* [Me and White Supremacy: Combat Racism, Change the World, and Become a Good Ancestor](https://www.goodreads.com/book/show/46002342-me-and-white-supremacy)

## Educating myself through people

Books are good for digging deep into a subject, but as a medium they're rather static. Someone wrote them at some point in time so they'll remain most relevant for that point in time.

I think it's important to also stay up-to-date with events as they unfold and to be flexible to learning about and supporting issues as they come up. I'm a huge Instagram user, so I've taken this opportunity 'reclaim my feed' as Megna put it by unfollowing / muting a lot of the meme influencer accounts I was following and replacing them with activist, political, and arts accounts focused around social justice and community building.

The hope is that by being more informed about issues as they happen, I can make the conscious decision to support them, and help the ones I care about reach critical mass.

## Using my platform to share and support marginalized voices

I don't have a large platform, but I do have _a_ platform so I'll be using it to regularly share and support causes that make the world a better place. Although I think my audience is mostly socially progressive, I do have some parts of my network that are socially conservative. I think it's my duty to use this heterogeneity to bypass the 'social media bubble' to better understand the issues on both sides and work towards a common understanding and maybe even a solution.

Where possible, I want to share the voices of marginalized peoples so that they may speak for themselves and find a greater platform. However, I also think that there's power in using and sharing your own voice so I'll be doing that as well (this post is an example of that). I have a feeling this might be a privileged, white savior view on my part but that's where my head's at right now so I'll log it for posterity.

Beyond sharing and writing on my blog, I've also started taking the discussion to social media. I know that there's a stigma against political talk on Facebook (kinda like the idea that we don't talk about politics or religion at the dinner table) but I think that just encourages the status quo and the status quo is one of oppression, so that furthers oppression. It's time to reclaim these meeting places as forums for critical thought and discussion. I think we're adults and can do this in a respectful manner backed with citations and well thought out arguments. Some of them will get ugly and that's okay - it's a sign of a deeper issue - and we can choose to respond even to ugliness in a calm, respectful manner.

## Making artifacts to explore, explain, and execute

It's no secret that I like building [projects](https://labs.hamy.xyz/projects) so I wanted to find a way to channel this passion and energy into impact. 

I've started by creating a [new art series for social justice](https://shop.hamy.xyz/collections/socialjustice) under [HamForGood](https://labs.hamy.xyz/hamforgood/) giving 80% of proceeds to organizations fighting for social justice and currently supports the ACLU. I've tried several versions of this in the past with little success (like my [covidart](https://shop.hamy.xyz/collections/covidart) series which raised ~$83 to fight coronavirus) so I'll need to keep iterating to figure out better ways to do good.

# Start

Everything written herein is a start. I've got a long way to go and this journey will likely never end, but you must start to journey in the first place so start I will.

Let's make this world a better place for all of us.

-HAMY.OUT