---
title: "Hello, Startup:  Building a business with pragmatism"
date: 2020-06-22T23:03:50-04:00
subtitle: ""
tags: [
    'book',
    'technology',
    'startup',
    'contumption'
]
comments: true
---

I just finished reading [Hello, Startup](https://amzn.to/2V8BKyZ) and wanted to sit down to reflect on my takeaways from it. In general, the book is about building a business - particularly a tech-focused startup - with pragmatism. But I think the takeaways can be generalized further than the startup world - and be written as 'work on the right things at the right time in the right way'.

* validate your ideas by talking to real people about their problems. Do this _before_ you sink time and energy into building something.
* use what you know and iterate as fast as you can. Most startups fail and most ideas are bad, so the best thing you can do for yourself is try a bunch of ideas at minimal cost.
* scaling for humans is almost always more important than machines. This is because humans are almost always the bottleneck - it's easier to buy more machines than hire more humans and when that's not the case, it's a good problem to have.
* be clear on what your mission and core values are, allocate time and effort accordingly. The mission is what you're aiming for and your core values how you'll go about getting there. Having these in sync makes it easier for the rest of the org to hum.
* learn every single day - study, build, and share to solidify this knowledge gain.
* Work on the most important thing - the biggest bang for your buck.

None of these points are particularly groundbreaking, but I think that's the power of them. It's a reminder that doing big things doesn't usually take any measure of greatness - it just requires you to give it your sustained attention. 

I liked a lot of the points in this book and found them to be highly actionable. Here are a few of the ways I'm implementing some of these learnings in my own life:

* creating a monthly habit to validate ideas / problems with real people
* setting learning goals to get more familiar with the tools I have at my disposal to validate problems and bootstrap solutions better, faster, and cheaper
* revised my mission and core values for my life, my projects, and my art
* have restructured my daily schedule and monthly goaling routines to have a strong focus on learning and study

If you're into tech and startups and think a journeyman's guide to creating them would be useful, read the book.