---
title: "Why you should care about climate change"
date: 2019-08-21T05:00:15-04:00
subtitle: ""
tags: []
comments: true
---

Climate change is arguably the most important / severe / urgent global crisis humans face on Earth today. Here's why you should care:

* Climate change is far-reaching
* Climate change will affect you, too
* Climate change will only get worse
* Climate change will get harder to solve as time goes on
* You can solve climate change

# climate change is far-reaching

The climate affects just about every part of life, from what organisms can live there to what each organism has to do to survive there. Although the predicted absolute temperature change is just a few degrees, looked at globally this means every climate will be shifted triggering adapt-or-perish scenarios.

# climate change will affect you, too

It's naive to think that a global change like this won't affect you. It will as you share the globe with everyone else.

Just focusing on the impact to humans, we will likely see:

* food shortages / stress due to changing agriculture zones, animal and plant recovery times
* potable water shortages as glacier -> river cycles get disrupted
* changing life patterns as the hottest areas become too hot in summers to be out during the day
* et al.

Together, this means increased strain on resources which almost always leads to increased conflict between the haves have-nots.

# climate change will only get worse

Based on the latest models, climate change is unlikely to right itself. Thus if we don't do anything, it will only continue to get worse. This means more strain on resources as time goes on.

# climate change will get harder to solve

As climate change gets further and further along, its effects will become increasingly irreperable. Many species and even whole ecosystems will perish or be significantly altered. The harder / better / faster / stronger we start, the more damage we can prevent.

# you can solve climate change

Climate change is a huge, big, really big problem. But its solution starts with you. Changing your habits to be more sustainable and taking an active role in climate change discourse / projects is required to make this happen.

Luckily, it's not that hard to get started. Staying habitual is up to you.

Some things you can do this month to fight climate change:

* reduce shower time to < 5 mins
* purchase carbon offsets (like from [Atmosfair](https://www.atmosfair.de/en/))
* turn off electric things when not using
* consuming less landfill items and re-using items you already own
* eating less meat and more sustainable alternatives

plus tons of other stuff. The key is to just start and get better as you go.

# fin

Climate change is a big deal and I'm unsure anyone really knows how bad things are going to get in the coming decades. What we do know is that climate change will not be a good thing for life as we know it and that there are things we can do today to stop it from happening in a destructive manner.

It all starts with small choices. Together, those small choices add up to large effects and its large effects that we need in order to stop climate change.

So next time you're about to throw something away or choose what to eat or leave the A/C on full-blast all weekend, ask yourself _Am I helping or am I hurting?_ If it's the latter, maybe reconsider.