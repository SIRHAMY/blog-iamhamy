---
title: "In favor of brevity"
date: 2019-08-26T04:54:55-04:00
subtitle: ""
tags: []
comments: true
---

The point of communication is to transfer information. Anything else is fluff.

Fluff is important to identiy and remove as its presence means worse value / time. Time is one of the most important resources we have. Thus we want to maximize its utility.

# brevity pros

* less time spent reading
* less time spent writing
* more clear / focused

# brevity cons

* brash / terse style
* less fleshed out ideas

_When less word do more good, use less word._