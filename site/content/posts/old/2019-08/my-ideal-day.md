---
title: "My ideal day"
date: 2019-08-12T04:26:34-04:00
subtitle: ""
tags: [
    "ideals"
]
comments: true
---

This half I'm theming on sustainability and that's gotten me thinking about how that ties into my idea of an ideal life. This got me thinking about what an ideal life was to me. By answering this question (at least on the scale of a single day), I hope to provide a scaffolding with which to compare my current life cycle to identify areas that could be improved.

I have a growing hunch that there exists an optimal scaffolding for everyone (though these scaffoldings may not be the same for everyone) and that attaining that ideal (via overlap of scaffolding and reality) is much closer to reality than we realize.

# my ideal day

My ideal day is focused around core satisfactions. These are things that, over time, I've come to realize are important factors in my sustained happiness: adventure, projects, and self. By doing so, my scaffolding takes into account that life is work,protecting against the common misconceptions between having a dream life and actually living a dream life (e.g. I want to have a six pack but don't want to go to the gym). 

My ideal day includes:

* projects - something productive
* adventure - something fun / adventurous
* self - all of the above in a healthy / sustainable way

_If you read my [release-notes](/tags/release-notes), you'll note that I organize my efforts around these satisfactions._

## projects

A lot of the things I enjoy doing involve building / creating / making of some kind. An ideal day would thus include something of the sort. This might be picking off a new project from my backlog, writing a post, or making an art.

## adventure

Part of sustainability is keeping life delightful. If you're not consistently enjoying it, you're doing something wrong.

For me, part of that delight is finding new places to explore and experiences to, well, experience. To this end, I make it a habit to find and organize [hamventures](/hamventures) around the city. Not every day has the requisite time and energy to pursue one of these though, so the adventure on an ideal day might be to visit a new place / experience something new or it might just be to catch up with a friend or watch that new show I heard about.

For me, it's less important that I do something specifically or with a specific magnitude, rather that I do something that fits into each of these buckets.

## self

Doing things at the expense of your health / the sustainability of your practices is rarely a good trade-off. This is true for your ideal days as well. If your ideal day lowers the odds that you can continue to live ideal days in the future, then you might not be living an ideal, rather an illusion of an ideal by stealing that satisfaction from your future self. An ideal ideal day then would be one in which you optimize for your satisfaction now while balancing that against the satisfaction of futureSelf to attain an optimization over time.

This may seem like you're selling yourself short now, but, at least anecdotally, I've found that sustainable living can actually increase the satisfaction of the present. By doing so, you then optimize for the now and the future. win-win.

A sustainable day for me includes:

* meditation
* exercise
* healthy food
* a good night's sleep

# fin

Moving forward, I'm going to periodically diff my actual life against my ideal life, using this scaffolding as a guide. It will likely change over time, but I think it's a good first step to creating visibility into where my ideals are slipping, a pre-requisite for doing something about it.

I found this to be a rewarding exercise and think it could be useful for just about anyone. Take a few minutes to consider:

* What does your ideal day look like?
* If you had next Friday off, what would you do?
* Do they differ?