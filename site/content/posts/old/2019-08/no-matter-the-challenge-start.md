---
title: "No matter the challenge, just start"
date: 2019-08-24T11:59:04-04:00
subtitle: ""
tags: [
    "musing"
]
comments: true
---

In the face of titanic challenges and ambiguity, the only thing you can do is start.

Give it 25 minutes. Seriously, start a timer and work on it for 25 minutes.

* Research the problem
* Ideate on approaches
* Make something. At least make progress.

That's all we can do. We can't know til we try. We can't finish til we start.

So just start.