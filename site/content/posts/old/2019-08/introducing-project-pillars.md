---
title: "Introducing Project Pillars"
date: 2019-08-05T22:01:45-04:00
subtitle: "a new way to decide what project to work on next"
tags: []
comments: true
---

_disclaimer: I do work at Facebook. I do not speak for Facebook. My thoughts herein are my own based on my own interpretation of things so don't go saying I'm an official spokesperson for anyone other than HAMY. kthxbye_

This past month, I've been thinking about ways to further streamline my creation process such that I better ensure that I'm working on the "right" things.

_Working hard means nothing if you're working on the wrong thing._

I took a good bit of inspiration from Facebook's idea of valuing impact over all else (this being front of mind cause [I started working there in June](/posts/2019-h1-in-review/)). At first it seems too 2D to be useful, too simple to actually fit in the dynamic world we live in. But on closer inspection (and after lots of personal thought experiments), I find it to be a rather robust and flexible algorithm for solving for x in the real world.

For me, impact is the net change in areas I care about due to my actions.

_Now what does this have to do with my projects?_

Historically, I kinda just created huge lists of ideas and dumped them into a Trello board. I'd go back every now and then to revisit my ideas or add new tidbits here and there but for the most part it was a dumping ground. Few projects were ever dredged up from the depths and most projects I took on were started within a week of them entering a list.

I've used this ad-hoc system since ~April 2018 (the first list on my board) and while it's worked fine, I've found myself increasingly asking myself if my efforts were _really_ in line with my goals. In other words, I had questions about whether the projects I was working on were the right things to be working on.

My first system that tackles this problem is my regular weekly, monthly, quarterly, and yearly reflections and planning sessions. [You can find these under the `release-notes` tag](https://blog.hamy.xyz/tags/release-notes/).

This allows me to habitually set goals and create plans to attain them. However I know that life can come at you fast so I don't have high expectations for attaining these goals, rather I use them more as directional pointers.

This means that when it comes to actually choosing projects to work on, my goals and plans aren't very helpful `comparator`s.

What I've found is that this issue isn't unique to Ham. It's also an issue faced by companies large and small. One of the strategies employed by big cos like Google, Facebook, et al. is to create OKRs (Objectives and Key Results) which help them align their efforts from the top of the organization all the way down through the ICs.

At the IC level, they then implement variants of "pillar evaluations" which are rubrics that can be used to vet the opportunity value of incoming projects. High opportunity projects get slated into the roadmap immediately whereas low opportunity projects get relagated to the backlog.

I already have OKRs but as I mentioned they were too high-level to be useful on the project front. So I've decided to create my own form of pillars to give myself a process for ranking projects in my backlog to better ensure I'm working on what's important, not just what's fresh in my mind.

The Project Pillars are (and likely will continue to be) a WIP so here's a link to the page that will hold them throughout their usefulness:

[See the Project Pillars here](/pages/principles/project-pillars/)

