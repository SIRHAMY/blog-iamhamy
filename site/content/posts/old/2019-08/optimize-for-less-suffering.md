---
title: "In the face of super-human complexity, optimize for less suffering"
date: 2019-08-19T04:30:17-04:00
subtitle: ""
tags: []
comments: true
---

Earth is a complex place. Humanity is a complex experience.

It's hard and possibly impossible to know all of it. Certainly impossible to know all of it and how different stimuli will play out given the system's constant rate of change.

As such it's impossible to optimize the system on a world scale.

What then can we do to ensure we're consistently changing it for the better?

In truth, nothing. This reality grants us few guarantees aside from change.

While we can't ensure progress in a good direction, we can make mindful decisions to increase its likelihood by optimizing on a smaller, easier to comprehend scale. The human scale. Of me and you.

To do so, we can start with a mindfulness of ourselves. What do we believe. What do we feel. Why do we do so. There may not be clear answers to all of these questions, but an awareness of this mindscape is a good place to start.

With a better understanding of ourselves, we can hope to better understand others. To accept. To tolerate. To support.

We can build on this understanding to better predict how our actions may affect another individual and from there work to curate our actions to be positive and, above all, non-hurtful.

Why do we care about the absence of hurt? Because we don't have the answers to all our questions and thus almost any path we choose is going to be shrouded in doubtful prospects.

Suffering breaks this mould. It is undoubtedly an ungood thing. This makes it a reasonable mark to shoot for.

Take a look inward with your actions and your thoughts and ask yourself: are you helping or are you hurting?

_In response to [21 lessons for the 21st century](https://www.goodreads.com/book/show/38820046-21-lessons-for-the-21st-century)_