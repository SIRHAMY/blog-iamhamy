---
title: "What I'm doing to help the Amazon (and the planet)"
date: 2019-08-24T12:02:24-04:00
subtitle: "A practical approach to helping the Amazon today"
tags: [
    "amazon",
    "climate-change",
    "earth",
    "sustainability"
]
comments: true
---

We've all seen the posts about the Amazon being on fire. Now there's a push to go beyond the standard #thoughtsandprayers response (thank the lawd) to actually do something about the crisis.

Through my own research I've found that there are many organizations and causes you can support that will directly combat the current Amazon threat. However I've also found that the whole situation is highly political and further that the current situation may actually be a symptom of an even larger problem.

If I've learned anything from my years debugging, it's that we must treat the cause of a problem if we truly want to fix its symptom. That cause: unsustainable resource usage on this planet.

In this post, I'll describe what I'm personally committing to to help curb my own resource consumption with the goal of lowering my demand on the planet's resources and, by proxy, lowering my footprint on the rainforest, climate, and, in general, Earth. I'll focus on small, easy lifestyle changes because I'm new to this and think that small, mindful change is the most robust process of change implementation.

What's outlined here isn't enough to solve the global sustainability crisis nor even to stop fires in the Amazon. It certainly won't stop the current fires in the Amazon (if you're looking for how to do that, read [the CNET article](https://www.cnet.com/how-to/the-amazon-rainforest-is-on-fire-what-we-know-so-far-and-how-you-can-help/)).  But [it's a start](/posts/no-matter-the-challenge-start).

My changes:

* Starting Meatless Mondays
* Eating less beef and lamb
* Paying a carbon tax
* Voting for sustainability
* Other small changes

# Meatless Mondays

Meat consumption is one of the primary sources of resource wastage and carbon production. A lot of this is due to the way that nutrients travel up the food chain though there are certainly other reasons as well. At each level, only about 10% of the nutrients make it to the next. This means that eating foods with long food chains (like animals) is a lot less efficient than those with short ones (like plants).

_Explaining this phenomenon is beyond the scope of this post, but [Khan Academy has a good intro](https://www.khanacademy.org/science/biology/ecology/intro-to-ecosystems/a/food-chains-food-webs)._

[Meatless Mondays](https://www.meatlessmonday.com/) is a campaign to reduce personal meat consumption by ~15% (1 / 7 = ~14.2%). It's a simple change and can have a pretty drastic impact on my resource consumption.

It's not enough, but [it's a start](/posts/no-matter-the-challenge-start).

**change:** Don't eat meat on Mondays

**expected result:** ~15% reduction in meat consumption which leads to less stress on resources and lessened carbon footprint thus less need for farmland (i.e. in the Amazon) and less need for the carbon to be offset (i.e. lessening the damage to the Earth that damaging the Amazon will do)

# Less beef and lamb

Not all meats are created equal when it comes to resource usage and carbon output. According to the Environmental Working Group, beef and lamb produce 3-6x more carbon / kg of food product than pork or chicken (see [Meat Eater's Guide to climate change and health](http://static.ewg.org/reports/2011/meateaters/pdf/methodology_ewg_meat_eaters_guide_to_health_and_climate_2011.pdf), page 19). According to the World Resources Institute, beef is 2-3x more resource intensive per unit of protein produced than chicken, pork, or fish with respect to both water and land use requirements (see [Animal-based Foods are More Resource-Intensive than Plant-Based Foods](Animal-based Foods are More Resource-Intensive than Plant-Based Foods)).

This means that I can still eat meat many days and still realize a significant reduction in my resource and carbon footprint by just being mindful of which meats I'm eating.

It's not enough, but [it's a start](/posts/no-matter-the-challenge-start).

**change:** Eating less beef and lamb by opting for options containing other sources of protein

**expected result:** Each choice like this results in a potential 50% (or more) decrease in both resource and carbon footprint than I would've had otherwise

# Carbon tax

No matter what I cut out of my life, it's going to be hard to reach a lifestyle that has no direct carbon footprint. But that doesn't mean I can't reach a net carbon footprint.

A carbon tax is a concept where you give resources to projects that reduce carbon (e.g. trees, sustainability improvements, etc.) in an amount that equals or exceeds the amount of carbon you produce. By doing so, you can claim a net carbon neutral (or even negative!) lifestyle with little to no effort (but ofc some resource usage). It's kind of like the effect of working out on your diet - if you work out a little more, you might also be able to afford some extra calories while maintaining a given weight.

First I calculated my estimated carbon footprint with Conservation International's [Carbon footprint calculator](https://www.conservation.org/carbon-footprint-calculator#/). There are many calculators out there, I chose this one because it had a good UI.

My result was 15.09 tons of carbon / year.

I did some research and identified [Atmosfair](https://www.atmosfair.de/en/) as a good org to give to. They focus on bringing renewable energy to areas where they have little energy at all, simultaneously modernizing these areas and allowing them to leapfrog industrial-age energy production techniques which rely heavily on carbon-heavy processes.

Like the footprint calculator, there are many good orgs to give to but this is the one I've chosen. I encourage you to do your own homework.

After doing some conversions from tons to tonnes and inputting my carbon footprint into Atmosfair, I got back an estimate of $349 dollars / year to offset my carbon. At ~$30 per month that's a little more than I want to pay right now, but I decided I could start with $10 / month and work from there.

It's not enough, but [it's a start](/posts/no-matter-the-challenge-start).

**change:** Give $10 / month to a carbon-reducing organization (I chose [Atmosfair](https://www.atmosfair.de/en/))

**expected result:** At $10 / month (about a third of what's required through Atmosfair to approximately offset my carbon footprint), I can reduce my total carbon footprint by ~33%

# Vote

I'm not so naive as to think that everyone is going to make these personal choices for sustainability nor that these personal choices are enough. Governments and organizations are doing far more damage to the environment than people eating the wrong kind of food. That being said, it's naive to think that the people don't have the power to change this. It's a small world and we're all in it.

As such, the final high impact thing I'm committing to is to vote with my time, my money, and my effort for governments, organizations, and initiatives (herein referred to as causes) that support sustainable living and a sustainable earth. This is likely the area with the most potential impact due to the scale of changes wins in these areas can produce, though it's also one of the least clear wrt specific actions to be taken and the expected results of said actions.

But that's life. [So I'll just start somewhere](/posts/no-matter-the-challenge-start).

Some ways to support causes:

* Sign petitions (like those on change.org)
* Donate money
* Sharing causes to network
* Building things

Just as important, not supporting causes doing bad things.

It's not enough, but [it's a start](/posts/no-matter-the-challenge-start).

**change:** Take a more active role in the support of governments, organizations, and initiatives that support sustainable living on this planet.

**expected result:** Unclear, anything from no effect to utopia

# Miscellaneous

I don't expect these to have a huge impact on the environment, but I do think they're easy changes to make and are definite positives. So I'll commit to them here for posterity.

* Bringing and using a re-usable water bottle rather than disposables wherever and whenever possible
* Limiting showers to 5 minutes in length

# Saving the planet (and the forests)

If you've read this far then you'll know that I kind of click-baited you with the title. Nothing I've laid out here will directly stop the Amazon fires. However, you'll also know why I don't believe stopping those fires to be the most important thing to solve right now with respect to the planet or in stopping fires in the Amazon in general. This is because those fires are just a symptom of a larger problem, which is unsustainable resource usage on this planet.

Thus to stop those fires and save the planet, what we really need to do is focus on changing how we go about life on this rock to make it more sustainable.

I don't have all the answers to these problems, but I have started working on them and here's what I've found so far:

* By foregoing meat on Mondays, I can reduce my personal meat demand by ~15% which translates to a similar reduction in my planetary resource usage and carbon footprint (for eating) by about that amount
* By choosing alternatives to beef and lamb, I can realize a 50% (often more) reduction in my planetary resource usage and carbon footprint for each substitution
* By paying $10 / month to a carbon-reducing organization, I can reduce my carbon footprint by 33%
* By voting with my time, money, and effort for governments, organizations, and initiatives that support sustainable practices I can outsize my effects on the planet

As I've said numerous times throughout this post, this is not enough. But [it's a start](/posts/no-matter-the-challenge-start).

For now, that has to be enough cause that's all we've got. I plan to revisit these commitments and continue to modify them over time. 

Hopefully I've been able to inspire you to take some of these commitments on yourself. If not, consider that $10 is the price of a beer in NYC and that chicken entrees are usually less expensive than their beef counterparts. Then reconsider your decision to not make one of these commitments.

Together, we can make the human occupation of planet Earth a little less shitty.

-HAMY.OUT