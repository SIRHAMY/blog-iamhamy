---
title: "August 2018 Release Notes"
date: 2018-08-29T22:36:26-04:00
subtitle: ""
tags: [ "reflections" ]
---

Another month, another post. For those of you reading this via email - rejoice! You're receiving the first ever blast from the Hamniverse.

For those of you reading via my blog, that's cool too but you can also [get these updates via email](http://hamy.xyz/subscribe) if you're more of a subscriber than a poller.

# Projects

This month, my projects have received the most attention and likely the most "gains". I don't have a whole lot to show yet, but a lot of cogs have been put in motion. I'm currently auditing YCombinator's [Startup School](https://www.startupschool.org/) and plan to push my primary project along the same timeline with a tentative v0 release by the end of September and a public release by the end of October. Ambitious, I know, but life may as well be an adventure so I'm trying to live it a little more aggressively.

**Highlights:**

* [Labs](http://hamy.xyz) now has Google Analytics tracking
* I drew out [my website tech stack](http://labs.hamy.xyz/posts/my-site-tech-stack/)
* Created a new Instagram (my 7th? 8th?) [@HAMY.LABS](https://www.instagram.com/hamy.labs/) to showcase more of my visual project outputs
* Attended a Blockchain hackathon in an attempt to have someone convince me why it was worthwhile. Failed. Did hear about some interesting use-cases with respect to auditing, but still feel that centralized computing beats decentralized in most good-faith, real world applications. Read more on [High Scalability](http://highscalability.com/blog/2018/8/22/what-do-you-believe-now-that-you-didnt-five-years-ago-centra.html) (a super dope blog)
* Started [Startup School](https://www.startupschool.org/) - tentative plans to create a company by early 2019
* Been attending more MeetUps around the city - there are a ton. HMU if you ever wanna join.
* Started reading [Zero to One](https://amzn.to/2LCRi6P) - a book that's been recommended to me numerous times but has sat on my GoodReads ToRead shelf for years. Now it's time.

# Self

In the last few months, I've been pondering the ideas of stoicism, frugality, and minimalism. This month, I focused on putting them into action.

**Featured posts:**

* On Frugality - Read [The case for brewing your own cold brew](http://blog.hamy.xyz/posts/case-for-self-brewing-cold-brew/) for how a little change can shave off a whole year from your target retirement date or, if FIRE isn't really your thing (I'm attempting to hit it by 40), how you can put that money towards an additional international vacation each and every year.
* On Minimalism - Read [More of what you love and less of what you don't](http://blog.hamy.xyz/posts/minimalism-more-of-what-you-love-less-of-what-you-dont/) for my thoughts on the core of minimalism and how I'm slowly adapting the philosophy into my lifestyle.

**Relevant reads:**

* [Goodbye, Things: The New Japanese Minimalism](https://amzn.to/2o9xJts) - Not the best book I've ever read but it's easy to digest and currently free on Amazon Prime.

# Adventure

The Hamventures have continued. I visited some good buds up in Chicago a few weeks ago and am currently preparing for a 10 day trip around Hawaii. I'm traveling alot which is cool, but I think it's even cooler that my new focus on frugality and minimalism has actually kept me roughly within my budgets for long-term financial independence - proof that you can be smart about your money and have money for luxuries as well.

# Fin

A few additional callouts:

* If you live in NY, try Citi Bike. It's life-changing to have another mode of transportation at your disposal and only costs ~$60 (with some discounts) for the whole year which blows any other mode of transportation out of the water cost-wise. Plus it's good for the environment and you (exercise++)!
* If you're a reader, give the Libby + library card + Amazon Kindle library combo a try. This combo has been so seamless that I've used it for my last 3 books (all free) and have loved it so much that I've replaced my trusty Kindle Paper White with the standard phone app (which I actually believe has better ergonomics than my older gen Paper White)

That's it from the Hamniverse. HAMY.OUT