---
title: "The case for brewing your own cold brew"
date: 2018-08-05T21:26:16-04:00
subtitle: ""
tags: [
    "hameats",
    "coldbrew",
    'finance'
]
---

I'm an avid coffee drinker. It helps me get shit done and I like getting shit done. As such, I typically drink a serving in the morning right after I wake up to get in gear and another right after lunch to combat potential carbo comas/push through the rest of my day strong.

Unfortunately, depending on how you go about it, this habit can be quite expensive. I currently live in NYC where a large iced coffee (I don't like it hot, seems like a waste of time to let it cool) can push $4 a pop at your local *Bucks/Dunks.

# The true cost of buying coffee

For my first few months in the city, I would grab a cup on my way to work every morning. Luckily, my work place (situated inside a [WeWork](https://refer.wework.com/i/sirhamy)) has free coffee, so I didn't shell out for my post-lunch fix. Weekend consumpiton varied, but it's safe to say I bought at least one cup each Sat/Sun.

Equipped with a good estimate of my coffee consumption habits and associated price, we can calculate its monetary cost over time (I leave the time and environment costs out for simplicity).

cupsOfCoffeePerWeek = 1 cupPerDay * 7 daysPerWeek
    = 7

costOfCoffeePerWeek = 4 dollarsPerCup * 7 cupsOfCoffeePerWeek
    = $28

costOfCoffeePerMonth = 28 costOfCoffeePerWeek * 4 weeksPerMonth
    = $112

So, I was spending ~$112 / month (~$1344 / year) on coffee. That doesn't seem like _that_ much as a standalone number, but I find it helpful to think of it in terms of opportunity cost, especially over time. With that same money, I could get:

* **~11 meals / month** @ $10 dollars a pop (standard takeout place). That's about 4 days of food each month or 48 in the year, which means my coffee money could've bought me ~1.5 months worth of food, assuming I ate out every single time. If you were to make some of those meals yourself, you could stretch that even farther to around 72 meals / year (or almost 2.5 months!), conservatively assuming you took half of that money to cook for yourself @ ~$5 / meal. Of course, you could certainly stretch that even further if you really wanted to.

* **~2 medium sized trips (2 @ $600) or 1 large trip (1 @ $1200) _each year_**. Obviously travel costs can vary widely, but you can certainly find a decently priced trip if you're willing to be flexible via Scott's Cheap Flights and other budget travel resources.

* **An extra $100 / month, $1200 / year to retirement savings.** This calculation will be different for everyone, but given my current personal burn rate and proposed FIRE date at age 40 (in ~15 years) - that $18,000 investment ($1200 / year * 15 years) will be worth ~$26,590 given 5.0% annual interest compounded annually. This would allow me to retire an _entire year_ earlier than I would have had I spent that money on coffee instead.

# Making your own coffee

So we get the benefits of not buying coffee, but I still need to get that coffee fix somehow. After a little research, I was pleased to find that making your own cold brew is actually super easy!

All you need is:

* A container
* Coarse-ground coffee beans
* Something to hold the beans during brewing or to strain the beans out afterwards

That's it.

For my setup, I got:

* [64 oz Coffee Sock ($20*)](https://amzn.to/2OdeCcM) - I liked it because it had everything you needed to get started and read that it was super easy to use. It is. You could certainly get this stuff for a lot cheaper by buying the parts separately/going with a different merchant, but I just wanted to get started as quick as possible. It helped that I had already done some estimates on my costs/savings which made me confident I'd recoup my initial investment within the month.

* [2lb Stone Street coarse ground coffee ($25*)](https://amzn.to/2KuHQ4F) - There's no real reason I bought this brand over another other than that it didn't appear to be from a huge no-face manufacturer (help the little guys!), had good reviews, was already coarsely ground, and was reasonably priced. I'm no coffee snob so YMMV but you can always swap it out for your favorite beans if you wanted something else.

_* prices at time of purchase_

# The process

As I said the process of making cold brew is simple, typically takes less than 20 minutes of active work and only requires 24 hours of waiting time to brew. Your own process may vary slightly, but this is the routine that I've got down.

* Rinse contianer
* Put coffee sock in container
* Fill sock with coarse-ground beans (I do 3/4 cups per 32 oz of liquid but obvs open to experimentation)
* Pop in fridge for 24 hours (brew time open to experimentation)
* Remove and rinse sock
* Enjoy!

_Note: Cold brew coffee can be exceptionally strong, so I typically mix with water depending on how strong the batch was. With my current setup, I do a mix of 50% brew, 50% water but you should experiment to find what best fits your tastes._

# My actual savings

Let's go over my actual savings by brewing my own coffee. I'll leave out the initial cost of the Coffee Sock in these calculations to get a better read on the average savings once in use.

I got the 64 oz Coffee Sock kit which lasts me about a week of normal coffee consumption (at one/two servings per day). I use 3/4 cups of grounds for every 32 oz of liquid, so that means 1.5 cups each brew.

My 2 pound bag of coffee beans is about 10.66 cups of coffee beans by volume

cupsOfCoffeeBeansInBag = 2 lbsOfCoffeeInBag * 5.33 numberOfCupsInLb
    = 10.66 cups of coffee beans / bag

_According to [Black Bear coffee](https://blackbearcoffee.com/resources/83) 1 cup of beans is ~3 oz, so 1 lb of coffee has 16oz / lb / 3 oz/ cup = 5.33 cups_

At 1.5 cups per brew, my coffee grounds (I'm assuming their pretty averagely priced) will last ~7 weeks

numberOfBrewsInBag = 10.66 cupsOfCoffeeInBag / 1.5 cupsOfCoffeeInBrew
    = 7.11 brews / 2lb bag of coffee

I paid ~$25 for my 2lb bag of coffee which means I'm now paying ~$0.51 / day for coffee

costPerCoffeeServing = costOfCoffeeBag / (numberOfBrewsInBag * numberOfServingsPerBrew)
    = $25 / (7 brews in a bag * 7 servings per brew)
    = $0.51 / serving

That's pretty great! This means, I've gone down from paying ~$112/month on coffee to ~$15, **with savings of around $97/month!** As such, all the opportunity cost calculations laid out previously are still valid (with minor tweaks).

_I did overlook the cost of sock replacements in this analysis, but the socks are rumored to last awhile and replacements only cost $13, so even if I had to buy a new one each year, that's only $1/month (~$0.04 / serving) and the ultimate effect is roughly the same._

# In conclusion

As you can see, making even minor changes in your lifestyle can pay off Hamsomely down the road. While I've diverted some of that money towards my long-term financial goals, I still prefer coffee shops for particular occasions like meeting up with a friend, exploring a new part of town, or just feeling the energy of those working around me and am willing to sacrifice some of those long-term gains for short-term enjoyment.

That being said, I am a lot more careful about my coffee and _always_ drink my own on my way to work.

If you've got coffee tips/a favorite coffee ground I should try, [let me know](http://hamy.xyz/connect). Otherwise, until next time.

HAMY.OUT