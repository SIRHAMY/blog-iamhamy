---
title: "On Minimalism: More of what you love and less of what you don't"
date: 2018-08-26T17:48:22-04:00
subtitle: ""
tags: [ "minimalism", 'contumption' ]
---

As part of my continued research into Financial Independence, Frugality, and Stoicism, I stumbled upon the book [_Goodbye, Things: The New Japanese Minimalism_](https://amzn.to/2o9xJts). I'll admit, the decision to read it was driven moreso by it being free via Amazon Prime than anything else, but I found it to be pleasantly complimentary to those aforementioned research areas.

# Minimalism

_We should start with what Minimalism is._

The core idea of Minimalism is to cut out the superfluous. As we all have different values, what is superfluous to one will not necessarily be superfluous to two.

Thus the primary benefit of living life with a Minimalistic perspective/method is that you optimize your time and effort towards those things that you value most.

Remarkably, the Minimalist method is simple: get rid of that that you do not 1) need or 2) love.

By doing this with objects, you remove the overhead incurred by their continued existence within your domain:

* Physical space
* Related thoughts
* * "Why do I have that again? Oh yeah, for whenever I do X project I've been trying to do for the past N years."
* * "Maybe I should just toss this. Eh, maybe next week."
* Related maintenance
* * Moving stuff to get to other stuff
* * Packing up for moves
* * Including in calculations for redecorating/organizing

with tasks:

* Save time
* Save energy

for use in more enjoyable pursuits.

# HaMinimalism

This concept is still new to me and I'm far from being an expert on the subject, but the core concepts struck a chord and mesh nicely with my current thoughts on life habits so I've started experimenting with ways to implement it into my life. To start, I've begun a survey of my possessions and an evaluation of the worth of each:

* Do I need this thing? Is it useful to me in the here and now?
* Do I love it? Does it give me great joy?
* When is the last time I used it? Based on historical data and foreseeable events, when is the next time I'll likely use it?

If I find that (I don't need or love a thing) or that I likely won't use a thing for a long, long while then I consider staging them for deletion which, for me, means placing in a box and donating the next chance I get.

So far, I've only done one iteration. But that iteration resulted in the removal of 9 shirts that I never wear (a reduction of ~16%). This leaves [my (abysmally small) room](http://blog.hamy.xyz/post/2018-q2-in-review/) with that much more space and removes the overhead of thinking about those things I'll never use again.

With that extra free space, mental and physical, I now have more resources to spend on items that better fit me in the here and now - be it a new pair of boots or phylisophical idea. A core part of life is change and I believe adaptation to it is key for a fulfilling playthrough, but sometimes our things tether us unnecessarily to the past.

I'll post more about my thoughts on Minimalism once I have a little more experience with it (I just finished that book today!), but until then feel free to [reach out](http://hamy.xyz/connect) if you've got questions or have your own experiences with the idea.

**TL;DR** I'm trying out minimalism by doing/having more of what I love and less of what I don't