---
title: "2018 Q2 in Review"
date: 2018-07-21T20:22:18-04:00
subtitle: ""
tags: [ "reflections", 'reviews' ]
---

Good morning Hamerica, it is with great honor and, ofc, much humility that I come before you on this 35th day of the 24th year of Ham to resurrect my beloved, sprawling installments of regular Hamflection.

This past year and change, I've grappled with much - from null purpose to INF passions.count to a literal world of responsibility - in a chapter of life I'd like to coin "Welcome to Adulthood" or maybe even "I've a feeling we're not in Georgia anymore". I endeavored through an onslaught of 20 degree weather (w/ windchill), the inevitability that is taxes, and the sticker shock of home furnishings to name but a few of these hurdles.

In the end, the Hamniverse made it to adulthood and it will never be the same.

And that's okay.

Despite all the doubts, missteps, and mundanity, in the past year (and change), I've done a lot. I've experienced, internalized, and produced in directions and magnitudes that I never have before.

And I'm proud of it. For me, that's success.

In fact, that's the point of these posts, to say "Look! I've lived another unitOfTime. I did more than watch Netflix (though I certainly did plenty of that), I also managed to accomplish X, Y, Z, AA, AB, AC, ... (you get the idea)."

So if you stop reading here, and come away with the knowledge that I did stuff, then you got the gist. And will probably save yourself ~20 (actually more like 30) minutes of reading.

I, on the other hand, shall continue to detail my perceived trials and tribulations ad infinitum (or at least ad tedium). I do this in an attempt to categorize and eternalize the meaningful experiences that took place in my reality this quarter (really half year), to log my relative truths in this frame of existence, and to take a measured look at my dreams and corresponding efforts to better understand my current state with respect to my ideal. The goal of such effort is to extract generalizable learnings and immediate action points to better ensure that for every unitOfTime, I am becoming a better Ham.

_Key to long-term success: for( var unitOfTime in self.TimeUnits ) { self.Ham++ }_

In the past 3 years of deliberate Hamflection, I've found that daily reflection is most useful for micro improvements - habits and actions I should implement in the next week or so. Reflecting weekly grants morsels of info that could affect my next month or two. And reflection on a more macro level (say, a quarter) allows me to sift through the findings of each, narrower, scope to pick out patterns and action points that can help direct the year(s) to come.

It should be no surprise, then, that I choose to air these reflections quarterly and why these pieces contain the elements they do. 

_Now sit back, relax, and try to enjoy the next 26 minutes of reading._

# Adventure

First things first,

![Ham in the city](/posts/2018-q2-review/hamy-in-da-streets.jpg)

## I moved to NYC!

In February, my dad (big ups to him) and I packed up a truck and drove it all the way from my pad in Arlington, VA to a shoebox in New York, NY.

The shoebox is in Murray Hill - a hip little neighborhood full of 20-something professionals who like to go and eat out a little too much to be healthy. It's 20 blocks from my office in Union Square (walking distance up here) and a solid jog (~2 miles) from Central Park where I go twice-weekly for [running club](https://www.meetup.com/dashing-whippets/) and sometimes, if it's nice, to bake in the sun.

![Where's Ham?](/posts/2018-q2-review/ny-map.png)

I share my ~500 square foot shoebox with my roommate, Eric, whom I met through a coworker two weeks before the move. The little space we have is actually rather nice - it's in a great location, clean, quiet, and doesn't have any weirdos running/hanging around outside. It even came with its own wine cooler, though that decided to break down once summer hit peak heat - go figure!

The transition from sharing 1100+ square feet to less than half that was difficult, forcing me to aggressively downsize by selling or tossing much of my large furniture. The cut was taxing both physically and mentally - furniture is not cheap, nor light, people!

Luckily, I gave myself plenty of time (a whole 7 days!) between settling on the apartment and our move-in date to figure out exactly how I was going to make this shoebox work. To that end, we took extensive measurements of the rooms and I scoured the interwebs, watching HGTV videos, reading through Tiny House sales materials, and flipping through Ikea lookbooks (surprisingly good stuff in there!) for tips and inspiration on how I could fit all my stuff and still have room for Ham.

This is what I came up with:

![Ham in the city](/posts/2018-q2-review/room-plans.jpg)

As you can see, in my beautifully realistic and to-scale illustrations, the bed is the centerpiece (and really the mostpiece) of my room. The room itself measures approximately 7' 1" x 7' 5", not including the closet, entryway, or oddly-shaped far wall. I opted for a queen-sized bed because I had just spent the past year on a California King and couldn't bare to go smaller. A queen measures ~5' 0" x 6' 8" which means about 33.3 square feet of my total 52.5 square feet - or about 63% of my room - is all mattress. Add in the bed frame, which I'd assert adds another ~5" on each side and ~2" on each end and we end up with a bed footprint of around 37 sq ft or 70% of my available space.

To make up for the large bed footprint, I opted for a storage frame (which is why it's so wide on the sides) that lifts directly up (because there's not enough clearance on the wall-facing sides to effectively utilize a drawer-version), granting me a majority of those 37 sq ft back for storage! I often find myself playing a game of [Rush Hour](https://amzn.to/2A5FmKm) - taking stuff off the bed, inserting/extracting stuff from under the bed, putting stuff back on the bed - but the hassle is 100% worth the extra space.

The shelves on the bed-side which hang my clothes and hold my shoes on top had to be hung high enough so that the bed didn't hit them when opening, low enough that shoes could be placed on top, and had to overlap horizontally because my room was too short to mount three abreast.

On the opposite wall is my suboptimal, though workable workstation complete with hanging office supplies, dual monitors, and a mounted tower. The computer setup is actually pretty sweet, but the workstation as a whole misses the mark because, no matter what I've tried (stadium seat, mounds of pillows, criss-cross applesauce, et al.), sitting on a bed for several hours just cannot compare to the comfort or productivity of sitting in a real chair with real back support in front of a real desk.

![My little slice of Manhattan](/posts/2018-q2-review/my-room.jpg)

_This picture was taken before my mattress arrived - I'm frugal, not a masochist_

Funnily enough, coming into the move I was actually pretty excited about the downsizing. I figured it would force me to take a more critical attitude towards the material items I'd acquired and those I might be tempted to acquire in the future. I thought it would pressure me to focus on what was important and to keep only those things needed to support those values.

To some extent, that's been true. I have found that I'm buying fewer things, that I'm eager to let go of those that are no longer providing value, and have acquired a greater understanding of what I need to support my core values and the life that I want to live.

It's not much. It really doesn't take _that_ much to make me happy and, as a consequence, it doesn't take that many things to support that happiness.

That being said, some of those things include a real chair with real back support in front of a real desk (and likely dual monitors, an ergonomic keyboard, and a trackball mouse but I'll take what I can get).

Yeah, I'm going crazy. I need more space. Stay tuned ~Q1 2019 for HaMansion updates.

![Ham <3 street art](/posts/2018-q2-review/nyc-funsies.jpg)

_Art by [@gondekdraws](https://www.instagram.com/gondekdraws/?hl=en), Ham by Ham_

## NYC sights, NYC lights

One perk of New York is that there's always something interesting going on. This means there is always a reason to be out and about (and otherwise not in your tiny living space).

At first it was overwhelming - _the constant cacophany of car horns, the wails of sirens just as you're about to fall asleep, the speed at which people attempt to circumnavigate each other (with surprising success rates, but sometimes not), the thicc air of a commuter train during rush hour particularly on a hot/rainy/snowy day, the hypothetical germs/diseases you just transferred to your hands as you clutched the train's safety bar for balance, the insane street hierarchy where pedestrian is mightier than motorist and bicyclists aren't even categorized, the smell of a block's worth of people's trash piled on the sidewalk for disposal sometime in the next few hours, the tortoruous decision of waiting 15 minutes and paying $4 for a coffee you're not even going to drink just so you can get the code to the bathroom or attempting to hold it for the next six avenues where the closest accessible bathroom you know of is located, finding out that Times Square is really just for tourists and most New Yorkers avoid it vehemently, having a drink (or two) with every meal, acclimating to $9 drinks being a pretty good deal, coming to terms with the fact that anything you think is new/hip/underground has already been found, done, and tossed by at least a thousand other residents, readjusting your sleep and party routine to accommodate 4AM soft closing times (yes, 130 AM is a reasonable time to leave the pregame - it's like leaving at 1130 PM in a normal city), understanding you need a good ratio (or network) in your group to get into certain clubs after ~1130 if you're not trying to pay cover or buy a table but it can't be too many guys cause clubs like girls but also not too many girls because those same clubs don't think girls will buy anything, 1 ave is like 3 blocks and 20 blocks is walkable but 6 aves aren't because there are weird aves in the middle like Lexington, Park, and Madison which means you're almost always 3 aves (9 blocks) off in your calculations, you can do whatever you want cause there's so many people everywhere that you'll never see again and - you'll notice - people take advantage of that daily..._ - but humans are pretty good at adapting to new conditions and, after a few weeks of blundering through intersections, subways, people, and boroughs, I acquired a nascent sense of direction and righted the ship.

So far, the city has been amazing. There's always something new to see, hear, smell, taste, touch - not always in a good sense - so much so that it's hard not to experience wave after wave of fomo as you trade off one experience for another. This phenomenon is simultaneously daunting and exhilirating. On the one hand, I see something new every single day I step outside my apartment. On the other, I'll never be able to see all of it - _and the vastness of the world comes crashing down about him_.

Of course, that little fact hasn't kept me from trying. For those new to these Hamflections, I typically create a habit of exploration everytime I move to a new city (see [Austin](https://medium.com/@SIRHAMY/in-review-my-summer-2016-88c9c3c74ca7)/[DC](https://medium.com/@SIRHAMY/in-review-my-spring-2017-2574496bbfd1)). Ideally, I'd maintain this habit regardless of time or place, but it's always much easier when the environment is still shiny /shrug.

I did the same thing this move with the added challenge of logging everywhere I went. For various reasons, the completeness of that logging is lacking, but I was still able to gather a fair bit of data:

<iframe width="100%" height="315" src="https://www.youtube.com/embed/X0TuVqQkS2Q" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

[_If the embed's not working, watch the video here._](https://youtu.be/X0TuVqQkS2Q)

_An incomplete log of some places I've been._

Being a new transplant (a New Yorker's way of saying "You're not from 'round here") I often get asked how DC compares to New York, so I figured I'd just lay it out in one place.

### NYC vs DC

Cons:

* Noisier
* Dirtier
* More expensive

Pros:

* More diverse
* More driven
* More art
* More tech
* More clubs
* More experiences

![Stay frosty](/posts/2018-q2-review/stay-frosty.jpg)

_This wasn't even taken in NYC, lul_

## Sustainably-living in a city that never sleeps

As described, my first few months in the city have been an awesome, debaucherous, wild time but they weren't particularly productive with respect to my longer term goals. My backlogs have grown (which is good), but my throughput has slowed to a trickle (not so good).

![Hamventure Backlog](/posts/2018-q2-review/hamventure-backlog.jpg)

_The current state of my Google Maps (the green things are places I want to go)_

With all things, too much can be bad. Too much of a source of happiness typically bleeds into pleasure. Too much of a source of pleasure and your tolerance for that source increases, effectively diminishing your ability to enjoy that source (read: hedonic adaptation).

For me, that has been staying out til sunrise, red-eye traveling, and consuming copious amounts of alcohol week after week after week (or partying like I was in college again). I got tired. I grew restless. I got bored. Meeting up for a pregame, planning weekend adventures, and dancing at clubs all started sounding like chores. And I love dancing at clubs, so, when that became a chore, I knew it was time for a change.

I see life as a set of probabilities with the one assurance being that it will end ("and taxes!" - Probably my mom as she reads this). Just as I would not play poker without trying to implement some sort of strategy (even if that strategy ends up being suboptimal), so, too, would I not float through life without at least trying to push the odds in my favor.

This may seem like a lot of work, but, in practice it's pretty simple. It just means solving problems as you see them.

So as I saw my enjoyment (returns) of common activities shifting, I rebalanced my investments accordingly.

What this looks like is a renewed commitment to sticking to my mid and long-term goals, a focus on habitual, consitent productivity, and a bias towards activities that take place while the sun's still out.

This means I'll be limiting the...

* number of super late nights (4+ AM) to ~0.25 / week.
* number of normal late nights (1 - 3 AM) to ~1 / week
* number of drinks per outing to <= 2 / event (3 - 4 late night)

In their place, I hope to...

* stick more closely to my yearly, quarterly, monthly, and weekly goals by more consistently meeting my 15 hour/week pledged work quota
* Do more daylight things - parks, restaurants, museums, pop-ups, festivals, exercise

_If you're in NY and want to join in on the Hamventures, [hmu!](http://hamy.xyz/connect) I took a cue from Dag's famous "Dag's Supa Fwends!!!" DC group and created a Hamventures group to announce and help coordinate these outings around busy Yuppie schedules. Don't worry if you don't have any events to share yet, we can coast off my backlog for quite some time._

# Projects

_Note: These are all things I've managed to accomplish in the ~5 weeks (at least two of which were spent banging my head against Docker containers) since committing to a more sustainable lifestyle which seems like a pretty good indicator of the direction's efficacy._

![front-iamhamy](/posts/2018-q2-review/front-iamhamy.png)

## Welcome to the Hamniverse

You may have noticed that you're reading this (if reading on the original publishing platform) on [a brand new site!](http://blog.hamy.xyz) For the past x years, I've struggled with the best way to brand and connect all of my different outputs into a cohesive whole. I've started to refer to this seemingly random organization of properties as the Hamniverse, in homage to Marvel's equally distraught realm of infinite universes which allows for valid contradictory story/time lines.

At long last, however, I think I've come to a solution that feels right. _Yes, I know I've said this multiple times before, but this time it's **real**._

A big problem has been an over-reliance on the "sirhamy" moniker. You see, this is my go-to handle across the interwebs, but I think it's often misconstrued as the handle that I've chosen for myself.

On the contrary, my ideal handle is "HAMY" - a construction that references a common nickname, conveys a playful tone (I think), while retaining a light-weight, flexible structure that ably lends itself to my constantly changing ideas of what I, and by proxy it, should be. The problem with the handle is that many others also use the term HAMY, meaning open usernames are hard to find, domain registration is expensive (gets even worse when you get down to 3 letters!), and it would be tough to beat out competitors in search engines.

Thus SIRHAMY was born. _It's literally SIR + HAMY_. Sir because I'm a dude, it still references HAMY as the primary part of the name, and English knights receive that honorific so I thought it was kind of funny to "knight" myself and vaguely imply that I was a better HAMY.

Of course, people that read your username online rarely get all (or really any) of that context. So constructing a brand around that moniker (Instagrams and websites included), I think it sent the message that the least common denominator was "sirhamy" and thus that it was the core of the Hamniverse.

Last year, with the creation of my first pieces of [public art](http://greeneggsandhamy.art) (at least in my adult life), and, more recently, this year with the rebranding of many of my Instagram accounts, I endeavored to recapture that core of my brand. But it still felt like I was straddling two identities and I wasn't sure why.

This past month, as I planned out yet another re-org of the Hamniverse, I came to realize that if, instead of moving around all of my domain-specific properties (greeneggsandhamy.art, hamgin.xyz, hamventures.xyz), I simply changed its root (sirhamy.com), I would at once greatly simplify the problem and enjoy greater consistency across those properties (additional boon to saving on recurring registration fees for all those domains).

It wasn't that the implementation was hard (you can pretty trivially create a *.domain.tld) but that, conceptually, it didn't make sense to have any of those properties be subdomains of one another because there was no one domain that I felt described the Hamniverse, and by proxy me, well enough to deserve that higher-order domain position.

sirhamy.com, by being my primary personal site, was the default choice but I was afraid of giving it too much importance because, as I've mentioned, "SIRHAMY" wasn't exactly what I wanted to convey.

So, once again, I'm trying out a new paradigm for the Hamniverse (PageRank be damned!).

* Main domain: [hamy.xyz](http://hamy.xyz) - conveys "HAMY" as the operative term, easy to see it's stuff all about me, uses .xyz TLD (see explanation below), fits v nicely with my existing properties
* Move all domains to subdomains - art.\*, labs.\*, blog.\* - this is easy to understand organizationally for visitors, sets a precedent organizationally for where other pursuits would go (requisiteMentalCycles- -), and gives me ample (really infinite) room for expansion at basically no additional cost (expanding with my previous paradigm would've required registering another domain which, while cheap, is still money) and in a way that continuously gives credibility to my brand.

_Why .xyz? It was on sale on [NameCheap](http://www.jdoqocy.com/click-8809074-12892698). Also, I know that the top-level domain doesn't really mean much outside of the internet's DNS implementations (meaning websites are the same regardless of the TLD), but still think that the general public has a bias towards top-level domains that are well-known (think .com, .net, .co). So I thought this would be a good time to put my money where my mouth is and do my part to normalize these generic TLDs in an attempt to educate the masses (all ye HaMinions) and work against the stupid system of domain squatters who register popular domains and just wait (squat) until someone comes by willing to pay them oodles of cash to acquire it. My thought process is - if we normalize generic TLDs, then the number of attractive TLDs for website creators increases ten-fold (figure of speech), so the demand for the "normal" TLDs decreases ten-fold, so the pay-off for squatters decreases ten-fold, so the number of squatters and their subsequent squats should decrease proportionally. 2 + 2 = 4 - 1 = 3 that's quick maths._

![Check me out in front of this wall](/posts/2018-q2-review/no-magic.jpg)

## Re-focusing on my magic

For those that choose to hang around me, you've likely heard me espouse my own version (which I likely co-opted from someone else) of "You can do anything you set your mind to". I like the adage because 1) I think it's true and 2) I think it's hopeful (and that hope is one of the best motivators in life), but I think it needs the additional stipulation ", but you can't do everything" to be optimally useful.

_You can do anything in life, but you can't do everything. - stolen by me_

I do this mostly because I think limits produce better results (you can't think outside the box if you have no box) and because humans often overlook the time component of producing change/impact (the time to complete a project fluctuates respective to the time allotted). I think the additional stipulation conveys the finiteness of life, viewed as a resource, which is more representative of the choice (and respective trade-offs) you're making when you pursue a given path.

When I first left college, I chose a path of exploration - exploring the city, making new friends, trying out different art forms - in an attempt to combat burnout (I'm in front of a computer all day), give myself ample headspace to deal with the daunting challenge of moving (for the first time) to a new place long-term, and discover alternative paths I might want to explore in the years ahead. _You can read more about these decisions in my [Fall 2016 reflection](https://medium.com/@SIRHAMY/in-review-my-fall-2016-1b4c24d2c6c7)._

Ultimately, that endeavor was successful. I made DC a home, made some great friends partnering on many adventures, and developed my nascent interests in the visual arts (street/public art, digital design, and, more recently, photography). These are assets I will cherish and nourish for a long, long time.

The issue was that while I was exploring these alternate paths, I wasn't giving much attention to my primary, established one - creating software. But first, perhaps a story about me:

_Begin storyTime;_

I've always liked making stuff. Growing up, I had the typical big box of legos and frankenstein-creations using pieces scavenged from disparate sets. I attempted to start a newspaper in elementary school. I consistently produced poetry and essays starting sometime in high school and trailing off late college (might pick it up again soon).

Moreso than making stuff, though, I really, really liked video games. They were this magical portal to another world where the environment seemed to live on its own (no more stop-motion Lego RPGs!), moving to the rules of some unknown universe, set in motion by some unknown creator. Of the video games, I was especially drawn to those with _magic_.

If there was some form of unseen, powerful force that could only be cultivated through knowledge and training, I was in. I spent many hours playing the likes of Morrowind (still never beat it), Oblivion, KOTOR (let's be real, force users are basically space wizards), et al.

I first discovered programming in high school as part of an effort to make money. I was in high school, didn't have any money, wanted money, but was unwilling to put forth much effort to make that money, so I ended up surfing the internet for easy ways to make money which put me on passive income, which led to online writing, which led to pay-per-view articles, which led to personal websites (cause F the middle-men), which eventually led to me being banned - I think for life - from the Google Adsense network. But that's besides the point.

As part of running that first website, I had to learn some basic HTML/JavaScript to customize my site further than the WYSIWYG editors would allow me to go. Cause an ad in the header, sidebar, and footer just wasn't enough - _more ads meant more views meant more money, duh!_ And as part of that process, I was introduced to the power of code and, though I didn't quite realize it yet, as an extension magic.

You see, as I've stated, I firmly believe that a human can do anything. Now that's not to say that you will accomplish exactly what you set out to do, but I believe that you can accomplish an approximation of that goal that still fulfills a large subset of the desired outcome.

So, for instance, you want to "time travel". You can't currently go back to the 1996 Olympics in person, but you can certainly read about it to get more knowledge than you ever would have acquired by just being there yourself and I'm sure someone's working on some sort of VR application that simulates each of the Olympic stadiums so you could eventually get a feel for what it would've been like. A rough metaphor perhaps, but I think it gets the point across. _This concept always reminds me of making a wish to a genie - "Hey I want to time travel!" \*get teleported to some location in central standard time\* "Not what I meant!"._

And for me, magic is just a deterministic tool that can be used to create a variety of changes at will. _Take a fireball from a wand, an enchantment woven to last til broken, or a spell that works wonders with a levi-o-sa but not a levios-a-._

Code (well, well-written code anyway) is exactly the same. You input a thing, it outputs a thing. Plus, once you get it running, it will continue to run in perpetuity until a) your logic breaks, b) the layer it's running on breaks, or c) your logic dictates it stop. _Your mind is the limit._

To some, my definition may seem off. If this is the case, I'm 98% sure this is because your concept of magic involves the mystery behind how things work and the relative ease at which formidable wielders seem to conjure these constructions. I would, however, assert that both of these are simply misconceptions of what magic really is. If we were closer to the process and people behind these feats, both of these attributes would vanish (because we'd understand the underlying principles and have knowledge of the amount of work required for any accomplishment of sufficient scale) leaving just the core attributes mentioned above. If an attribute can vanish based on a perspective change, then I don't believe that attribute to be an absolute truth - though it can certainly still be relative.

_To take a thought from Thor - Magic is just technology so incredibly advanced that we can't discern it from our classical interpretations of magic._

Amazingly, this power is at the behest of almost every human out there. If you have a computer, you can do magic. It just takes time, effort, and a generous amount of patience.

_And so, my love for code - the closest-to-classical, most practical form of magic I will likely ever acquire - said "hello, world!"._

_End storyTime;_

![Check me out in front of this wall](/posts/2018-q2-review/magic.jpg)

So, back to present day, it's been about a year and a half since I graduated college and I've pretty much left this passion to stagnate. Sure, I've been coding full-time for work, but I haven't been exploring new domains, implementing new-found concepts, or even constructing within my current bounds of knowledge in a requisite fashion nor magnitude to fulfill the wants of that passion. I think, even above all the other realizations discovered in the process of this reflection, that this is the one that has been causing me the most internal strife.

Luckily, the fix is simple - prioritize creation via code more highly so that it receives the requisite mental cycles to be satisfied. Do this by biasing towards projects that benefit most from its incorporation. This seems like a strong choice because 1) it allows me to refocus on a primary passion and rekindle my desire for life-long learning, 2) software is inherently creative and flexible (if you can think it you can build it) which allows for collaboration with other humans (fun and social!) and with other passions so I don't have to skimp on my arts endeavors nor become a boring old Ham, and 3) software is incredibly powerful, allowing me to create real change (hopefully for the better) in the world and also making it extremely valuable which provides attractive outlooks towards my more nebulous long-term goals.

So, my rough plans to balancing my efforts with a larger allocation towards my magic:

* Prioritize projects with software components
* Reach out for collaboration with others to keep the process creative, exciting, and fun

**A call for collaboration:** So, if you're interested in collaborating to make some dope shit that really matters, [HMU!](http://hamy.xyz/connect) I've got a lot of things queued but I can always push new things on. Lmk and we can set up some time to chat.

## Geolog

As shown in **Adventure**, I've started to productionalize [Geolog](http://labs.hamy.xyz/projects/geolog) - some software I created to map arbitrary data over time. I like keeping track of data about myself because I think it can be used to glean useful insights and, perhaps more importantly, create really cool outputs (take, for example, my breakdown of the time I spent on my college job search in my [Fall 2016 reflection](https://medium.com/@SIRHAMY/in-review-my-fall-2016-1b4c24d2c6c7)). But gathering this data and creating these cool outputs is a lot of work! To combat this overhead, I'm trying to habitually create processes to aid in the gathering and refining of this data so that, overtime and with luck, the barriers to this endeavor will become lower and lower.

To use, basically I just need a big list of all the places to show up, the text to show up with it, and the images (if any) to mark the locations. With that, I'll run it through refinement process(es) and then pipe it into the map!

More info on [the project page!](http://labs.hamy.xyz/projects/geolog)

_**Want a Geolog of your own?** [Shoot me an email!](http://hamy.xyz/connect) Note that this process is not automatic so I will be charging for its creation to make up for the time costs, but I do want everyone to get a chance to use it so it won't be expensive._

![Trumpku in Hand](/posts/2018-q2-review/trumpku-in-hand.jpg)

_Seen these guys around?_

# Digital/Street Art

In the past few months, I haven't been creating much art. This is predominantly due to my energy being spent elsewhere but a declined interest in sticker and static digital outputs has also contributed. I still have a lot of ideas on the backlog if interest piques again, but, for now, these jobs aren't being prioritized.

On the bright side, I have been throwing up stickers (and logging their locations!) when I remember, it's not wet outside, and I've got some extra time.


<iframe width="100%" height="315" src="https://www.youtube.com/embed/CuMZjU9RZxY" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

[_You can watch on YouTube here if the embed is broken._](https://youtu.be/CuMZjU9RZxY)

I haven't taken as many street art excursions as I used to either, but there's enough art around the city that I don't have to go very far out of my way to capture good ones. You can find my most recent captures on [@hamy.streetart](http://instagram.com/hamy.streetart)

# Self

This semester, I created the longest-reaching goals I've ever made. I started with some core beliefs on this reality, values I wanted to uphold throughout this journey, and ideal attributes with which to approach it and worked backwards to create definite checkpoints along the way. I won't go deep into these core beliefs here, but will likely talk about them in the near future.

For long term goals, I've found that it's okay to be a little nebulous, primarily because things change with time. As such, I strove to make these simple to boost relevance and consumability while incorporating major points of my ideology.

## Goals: 2034 (Ham at 40)

* Be FIREable - For those that don't know, FIRE stands for Financial Independence / Early Retirement - something I've known about for awhile but have recently taken a deeper interest in. I don't actually anticipate that I'll stop working at this point as I rather like making things. This is more of a commitment to living a sustainable, balanced lifestyle robust to external factors in the years to come.
* Be creating dope shit - I love making things. If I stop making things, there's a problem.
* Be helping the world - I find villains intriguing, that's no secret, but one of my ideals is to leave the world better than I received it. By 40, I hope to be acting on this.

For my other goals, feel free to poke around on my blog. That's where things will live in the long-run and therefore will be the most up-to-date place to find them on the web.

![Cartoon Ham](/posts/2018-q2-review/cartoon-ham.jpg)

_I look like I'm being impersonated by a Ditto - Avatoons is cool though, so you should check 'em out._

# Das it

There's a ton more to talk about - 6 months is a long time (kinda - [The days are long, the decades short](http://blog.samaltman.com/the-days-are-long-but-the-decades-are-short))! - but I'm tired of writing and want to make some real progress on my other projects. Before I let you go, a few calls to action:

_The Hamniverse calls you to..._

* [Reach out](http://hamy.xyz/connect) if you want to collaborate or even just consult on your next project
* Head over to http://labs.hamy.xyz/projects/geolog to build your own Geolog
* [Subscribe to my email list](http://hamy.xyz/subscribe) if you want periodic updates from around the Hamniverse

Okay that's it.

HAMY.out

---

What I've been reading/thinking about

early retirement/FIRE - https://networthify.com/calculator/earlyretirement
Real estate investing
How to focus and leverage my skills most
What happiness is and how to optimize for it
Stoicism via Mr. Money Moustache - https://www.mrmoneymustache.com/2011/10/02/what-is-stoicism-and-how-can-it-turn-your-life-to-solid-gold/
What I've been reading

Articles

Advice
https://patrickcollison.com/advice
http://blog.samaltman.com/the-days-are-long-but-the-decades-are-short
Productivity
http://blog.samaltman.com/productivity
My system has three key pillars: “Make sure to get the important shit done”, “Don’t waste time on stupid shit”, and “make a lot of lists”.
Many people spend too much time thinking about how to perfectly optimize their system, and not nearly enough asking if they’re working on the right problems.  It doesn’t matter what system you use or if you squeeze out every second if you’re working on the wrong thing. The right goal is to allocate your year optimally, not your day.