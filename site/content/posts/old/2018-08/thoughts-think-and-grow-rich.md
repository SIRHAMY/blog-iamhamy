---
title: "Thoughts: Think and Grow Rich"
date: 2018-08-02T23:05:21-04:00
subtitle: ""
tags: [
    'contumption'
]
---

_Think and Grow Rich_ [GoodReads](https://www.goodreads.com/book/show/30186948-think-and-grow-rich) [Amazon](https://amzn.to/2KodeBJ) is a dated book that discusses the power of the mind to affect reality. For those who have not found this to be true - that all deliberate effects on your reality must come from within - this may be a useful read. For you readers, I'd give this book a 4/5.

For those who understand the importance of mindfulness and intent with respect to your power to augment your own reality, then I'd say this book leans too close, too often to conjecture, superstition, and dramaticism to be of much use. For you readers, I'd give it a 2/5.

# Points that resonated

* Willpower over emotion - use emotion as fuel for your calm, deliberate actions

* To achieve, you must have a definiteess of purpose (a definite goal), a definite and reasonable plan to get there, and a willingness to sacrifice for them day in and day out.

* Change is wrought through habit

* You can do great things by yourself, but it is hard and limited to your own ability to output. Surround yourself and collab with worthy individuals to increase the efficacy of your efforts

* Just do it. You're the only one stopping you.