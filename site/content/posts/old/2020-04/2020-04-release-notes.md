---
title: "Release notes: April 2020"
date: 2020-04-26T19:28:23Z
subtitle: ""
tags: [
    "release-notes",
    "reflections"
]
comments: true
---

In April 2020, [I setup then put away my Xbox](#self-adventure-work), [refocused my projects on solving real problems](#projects), [attempted to make art to support COVID-19 relief](#covidart), and [built a platform for audio visualizations](#visualizations).

These are my April 2020 release notes, written in quarantine.

_If you prefer video, I talk through the highlights here:_

<iframe width="560" height="315" src="https://www.youtube.com/embed/-YiqD8gCM28" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# projects

This month, I've been focused a lot on what it is I want to do with my life and how I can get there. Part of this is because it's the start of a new quarter and I regoal every quarter, but I think some of it is also due to the quarantine which brought with it ample downtime and disruption of normal life.

The fruit of these ponderings is a collection of three words that I think describe a happy, successful life. I view them from the perspective that if I were asked to describe my life in three words and these were the words I came up with I'd be doing pretty well.

Those words: happy, calm, impactful

I won't dive too much into happy or calm here as the explanation of the former wouldn't fit into this release notes format and I've already explained the latter a good bit in my [2019 review](https://blog.hamy.xyz/posts/2019-in-review/#meditation-and-friends).

But I will dive a little bit more into impact as I think it's an organic extension of [my project ponderings at the end of March](https://blog.hamy.xyz/posts/2020-q1-review/#projects).

At the end of Q1, I talked about goaling on money and using that as a proxy for measuring the value and impact of my projects. But I also noted that it wasn't a perfect measure and that maybe there were other measures that more closely tracked this value.

I continued to ponder this through April and inadvertently got back into learning about startups through Y Combinator's [2019 Startup School videos](https://www.youtube.com/channel/UCcefcZRL2oaA_uBNeo5UOWg), picking up [hello, startup](https://amzn.to/2xrSMj2), and listening to various business / maker podcasts. Though I'd kind of done these two things separately, I began to realize that startups actually have this problem all the time - they start with no money to prove their value / impact so have to find other ways to prove it out.

This led me back to the start with a problem then validate it idea that I'm sure I've heard and disregarded many, many times in the past few years.

All this to say that I'm regoaling a bit for Q2 - or what's left of it - by focusing on practicing and executing on 1) problem discovery, 2) problem sizing, and 3) problem validation. Not every project I build will fall under this category and inherit these guidelines - take [@hamy.art](https://www.instagram.com/hamy.art/) for instance - so to capture that dichotomy I'm trying out a naming scheme of "builds" for those projects with a primary goal of capturing this value and "hacks" for those projects that don't. I don't know if this bucketing strategy makes sense, but my goal is to focus more of my attention on builds in Q2 as I think I have a lot of room for growth and there's high opportunity in this growth.

I think this approach will lead to more, better project releases by limiting scope and focusing my different kinds of projects - hacks and builds - on their individual purposes. If you want updates on these, [subscribe](https://hamy.xyz/subscribe) to my email list or [connect with me](https://hamy.xyz/connect) on one of my socials.

Now onto my releases this month:

## projects: releases

### covidart

[![covidart series](https://storage.googleapis.com/iamhamy-static/labs/projects/covidart/covidart-screenshot-web.jpg)](https://shop.hamy.xyz/collections/covidart)

During quarantine, I've been constantly reminded of just how lucky I am to be in a good position to weather this pandemic. I have a stable job that I can do remotely and access to all the essentials like food, water, and internet. So I've been thinking about ways I can leverage this position for good.

The first thing I came up with was to create a coronavirus-themed game under [HamForGood](https://labs.hamy.xyz/hamforgood/) with a portion of proceeds benefitting the CDC Foundation. Unfortunately the app stores had some issues with its coronavirus theme so I've had to rework it a bit so it's still under construction.

Then I got inspired by all of the artists and organizations releasing art and efforts to support artists and COVID relief and wondered if I could do anything similar. I am, after all, an #artist. What I came up with was an art series leveraging some of my existing technology that was covid themed and released under [HamForGood](https://labs.hamy.xyz/hamforgood/). I named that series #covidart.

You can find all installments on [@hamy.art](https://www.instagram.com/hamy.art/) and browse available prints on [HAMY.SHOP](https://shop.hamy.xyz/collections/covidart).

Now I'd be lying if I said this was a successful venture. So far it's generated 0 sales and is one of the worst performing blocks of media I've shared on @hamy.art. The low sales wasn't a huge surprise as [I only sold $47 of merchandise in Q1](https://blog.hamy.xyz/posts/2020-q1-review/#goal-make-500-on-side-projects) but it did get me to thinking more about the impact of my efforts - contributing to the train of thought at the beginning of this #projects section.

Despite the null impact, I'm still very interested in this social good profit sharing space. It's something that I think could grow sustainably and do some real good if done correctly - I'm just not there yet.

I'm currently generating additional images in the series which I'll be sharing in the coming weeks so follow me [@hamy.art](https://www.instagram.com/hamy.art/) if you're interested in seeing those.

# visualizations

<iframe width="560" height="315" src="https://www.youtube.com/embed/y5hHeb_KOcY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

_A long-form visualization for Griffin using [monoform](https://labs.hamy.xyz/projects/monoform/)_

Speaking of cool artists releasing things, several of my close friends put out new music - [Steve](https://www.instagram.com/steveis.space/) released his album [The Voyage](https://open.spotify.com/artist/0KJVXXOwGjWD3n8fifvRJY?si=V9im_13EQ0qYKertSTFgtg), [Griffin](https://soundcloud.com/griffinsounds) performed a [live quarantine set](https://www.youtube.com/watch?v=y5hHeb_KOcY) from Munich (visualization above), and [Zach](https://soundcloud.com/trillionsss) [did one too](https://soundcloud.com/trillionsss/live) from ATL.

Visualizations have been a recurring interest for me with [moon-eye](https://labs.hamy.xyz/projects/moon-eye/) being my first interest execution. This month I've been working on some visualizations with Steve for his new album and visualized Griffin's Live from Quarantine mix, embedded above.

But I've also been thinking about how I could scale this interest and what my vision was for visualizations moving forward. How could I streamline their creation? How could I make them more complicated without increasing complexity? How could I share them more broadly?

So I thought about it a little bit and figured some shared libraries and a common hosting location would solve some of those issues. As for sharing, I really love the built-in iTunes visualizer - it's simple and beautiful. Then wondered if I could make something like that for my own visualizers.

So I built [VICIDUAL](https://vicidual.xyz/) to host my visualizations and validate whether people actually want to view these visualizations. I'm planning on adding more visuals as I create them and welcome any feedback you have, so please give it a try!

# self / adventure / work

For #self, my focus has really been to just keep up with good habits.

For #adventure, it's been to keep up with some semblance of social interaction.

For #work, it's been to set clearer boundaries between work and life.

6(?) weeks of quarantine has really done a number on the habits I've built up over the years. I don't have access to a full gym (though thankfully ordered a bunch of weights while they were in stock), I'm not walking around NY as much as I used to, I get grumpy when I'm inside all the time, friends are both always available virtually and never available f2f, etc. etc.

Some things I've done that were positive:

* set up my xbox and, crucially, put it away again
* weekly yoga with Megna
* getting back to exercising most days
* reaching my goal of a 55% savings rate
* reading a little every day
* taking time to reflect / journal
* setting boundaries - physical and temporal - for work and life
* virtual hhs with friends and fam

I still don't feel back up to 100% but maybe that's because things are just different now - it may not be possible to get back to 100% of where I was before coronatine. With any luck, things will start going back to normal but until then I'm just gonna keep trying to make the best of it.

Some things I'm trying:

* short yoga sessions right after work to help define the work / life boundary
* restarting [hamventures](https://hamventures.xyz/) with a focus on digital events
* cooking healthier and cheaper

# fin

That's all I've got for this installment. I hope you're fairing well during this odd time. If you've come up with any hacks / habits to weather the coronatine, please send them my way!

live long and propser,

-HAMY.OUT

