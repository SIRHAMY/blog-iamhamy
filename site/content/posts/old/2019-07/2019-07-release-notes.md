---
title: "Release notes: July 2019"
date: 2019-07-31T18:47:42-04:00
subtitle: ""
tags: [
    "release-notes",
    "reflections"
]
comments: true
draft: false
---

_Read [previous release-notes](/tags/release-notes)._

_Last month, I released [my reflection for the first half of 2019](/posts/2019-h1-in-review/). In it I detailed my new job and the process of finding it, my project progress (2 of which were released), and my hopes / feelings at my 25-year mark._

This month, I've put a lot of effort into the thoughts and systems I need to make this half (and hopefully subsequent halves) more satisfying, productive, and good. In other words, how do I create a system to continually produce impactful halves.

This is no simple task and as such there is no simple way to solve for it. Instead, I've taken a multi-pronged approach in the hopes that in aggregate they'll get me closer to this goal.

To start, I re-structured my planning to be more impact-driven with the hope that this will better direct my effort ouput towards things that matter. I did this in a few ways:

* Created planning sessions every half focused on themes
* Created quarterly OKRs that materialize these themes into measurable, actionable goals
* Created monthly OKRs that breakdown the quarterly ones into building blocks for quarterly and scaffolding for weekly
* Created [Project Pillars](https://blog.hamy.xyz/pages/principles/project-pillars/) with which to vet new project ideas against my current values so that I can be sure I'm at least picking from the right side of the PQ

I don't expect any of these systems to be perfect. Instead, I see these as first drafts that I'll modify to better meet my needs as I discover these deficits / come up with better methods. This process of continual improvement very closely resembles that of (good) software engineering practices and I plan to explore more areas where those practices might be employed. _Related: [build scalable, automated processes to win.](https://blog.hamy.xyz/posts/phoenix-project-takeaways/)_

# projects

Other project releases include:

## introducing [HAMY.ART](https://art.hamy.xyz)

After months of considering it, I finally gathered enough little reasons to warrant putting in the effort to create a new site. As I predicted, due to [the way I built my site](https://labs.hamy.xyz/posts/labs-hosted-on-gke/), creating a new site was relatively simple. I ran into a few snags where I forgot steps or something was missing in my docs but I was able to get it up and running in a few hours. I used those inefficiencies as opportunities to update my docs so hopefully the next site I spin up will be even easier.

As a part of this effort, I finally stood up an image server so there will likely be a bunch more pics in future posts. #yaypics

The site is now live and features the first work in my new, art-focused project _Make America Think Again._ Check it out: https://art.hamy.xyz

# work

For those who read my [20+ min reflection on the first half of this year](/posts/2019-h1-in-review/), you know that I now work at FB. For those that didn't, I now work at FB.

My first 6ish weeks have been spent in a program called "bootcamp" where you get up to speed with FB technology relevant to your role and focus. The first 4ish weeks were spent in classes and the last 2ish weeks have been spent doing something I like to call "team dating". In this process, I find teams I'm interested in, I meet with them to see if we might be a good fit, I work with them for a few days, then repeat until I find a team that I really like and want to spend the next unitOfTime with.

It seems a little weird on the surface, but the ability to actually try out different teams before committing is a great opportunity to find a good match both technically and culturally. So I'm dating my heart out.

Aside from that, I haven't done that much real work. Just a few simple tasks here and there as I learn about the company and pick teams. That being said, in one of those tasks I modified a code path that gets hit over 40 billion (yes, billion with a B) times each week. Was it a trivial change? Yes. Is it still v v v v  cool? Yes.

In the next month or so I hope to settle down with a team (get cuffed, or whatever the cool lingo is nowadays), understand the problem domain, and start making some headway. #impactorgtfo

# self

All this change and travel hasn't been great on my habits. Eating, exercising, building, and connecting have all taken measurable hits. I have plans to right these things, but it'll likely have to wait til after I pick a team and get back from traveling. ='(

I did make one step in the right direction to combat the FB 15 by switching to a smoothie for breakfast instead of bacon, egg, and cheese platters. A hard choice indeed, but one I think is for the greater (smaller) Ham.

# adventure

I'm traveling a lot this month. Stay locked on [@sir.hamy](https://www.instagram.com/sir.hamy/) for a.m.azing pics.

Another thing I've been pondering is how to make adventuring more sustainable (my theme for 2019H2) wrt time / energy / resource output and more productive wrt connection and exploration. I've done a few things in pursuit of this like improving my system for finding [hamventures](https://blog.hamy.xyz/hamventures/) and improving my methods of distribution, both through close connections and publicly (through [email](https://blog.hamy.xyz/hamventures/subscribe/) and, ofc, [Facebook](https://www.facebook.com/hamventuresnyc))

I've a feeling there's a lot more to learn and improve upon. Will report back with answers.

# fin

That's it. Tune in next month.

CALLOUT1: I'm looking for feedback on my new site so if you've got 5 mins, swing on over to https://art.hamy.xyz and [let me know your thoughts](https://hamy.xyz/connect).

CALLOUT2: Also if you're looking for weird shit to do in NYC each week, like my new [Hamventures Facebook page](https://www.facebook.com/hamventuresnyc) so we can drive #engagement.