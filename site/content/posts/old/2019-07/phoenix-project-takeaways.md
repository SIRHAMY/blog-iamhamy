---
title: "Build scalable, automated processes to win"
date: 2019-07-01T23:02:57-04:00
subtitle: ""
tags: [
    "phoenix",
    "book",
    'contumption'
]
comments: true
---

_This brain dump is a reflection on the content of [The Phoenix Project](https://www.goodreads.com/book/show/17255186-the-phoenix-project?ac=1&from_search=true)._

The cycle for progress is:

* experiment
* learn
* repeat

This makes sense. It's essentially the agile principle as well as the underpinnings of the scientific method - both of which have done a pretty good job of standing the test of time and thus have some strength wrt validity.

But you can do this in varying shades of bad.

If you want to do it not so bad, you must put emphasis on building scalable, automated processes for:

* deploys
* quality control
* security
* robustness

You must leverage humans and technology to build these things in order to unlock the ability to build your end-product with high quality and efficiency. If you don't, your opponents will.


# got updates?

Did you like hearing me write? Would you like to read more about what I'm thinking?

[Subscribe](https://hamy.xyz/subscribe) to get periodic email blasts about what I'm doing, when I'm doing them!