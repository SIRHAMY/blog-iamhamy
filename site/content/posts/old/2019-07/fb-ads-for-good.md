---
title: "Fb Ads for Good"
date: 2019-07-06T13:18:15-04:00
subtitle: ""
tags: []
comments: true
draft: true
---

# asking to help

It's insufficient to just go help. Gotta get approval, see what they find is helpful for them in order to create a campaign that can produce the most impact.

```
Hi, my name's Hamilton and I work as a software engineer at Facebook. As an employee, I get ad credit that I can use to better understand how ads work in a real-world setting. With y'all's approval, I'd like to run some ads on behalf of WeForest.

Before I run any ads, I'd like to better understand the goals of WeForest and the current ads strategy. Then we can discuss the types of ads I'll be running (including the goals and content of these ads), the expectations of both sides, and next steps.

Let me know if y'all are interested / have any more questions!

- Hamilton
```

# got updates?

Did you like hearing me write? Would you like to read more about what I'm thinking?

[Subscribe](https://hamy.xyz/subscribe) to get periodic email blasts about what I'm doing, when I'm doing them!