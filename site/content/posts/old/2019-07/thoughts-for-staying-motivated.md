---
title: "Thoughts for staying motivated even when you aren't"
date: 2019-07-29T21:48:57-04:00
subtitle: ""
tags: [
    "motivation",
    "habits"
]
comments: true
---

Motivation is a fickle energy source which is why habits are my preferred form of locomotion. But these energies aren't mutually exclusive which means low motivation oft leads to low habit execution.

This is a collection of thoughts I go over in my head when I notice this phenomenon taking place.

Remember that habits are more important than anything else. Doing something is better than nothing and what you do most of the time is better than what you do sometimes. It's okay to not do it great, or beautifully, or for a long time today. It's not okay to not do it at all.

Think about what you're doing and why. What is the thing you're solving for. How are you planning on accomplishing it. Why is this important and what does it mean for you in the grand scheme of things. Put your effort into perspective to remind yourself of why you were doing x in the first place.

Finally, you've just got to do it. LIfe is hard, there's no getting around that. But it only gets harder if you aren't helping yourself. So when life gets hard, it's okay to take a break. Don't beat yourself up over it. Self-hostility is still hostility. If you wouldn't beat up a friend over it don't beat yourself up over it.

But once you've caught your breath and gotten your feet under you, it's time to keep on trucking. Consider your options, come up with a plan, and do it. [Count down from 5 if you have to](how-to-stop-procrastinating-in-5-seconds-or-less). But you've just got to start.

Do it for 20 minutes. If you're still not in it, consider shelving it for tomorrow and working on something else. If you are, glhf.