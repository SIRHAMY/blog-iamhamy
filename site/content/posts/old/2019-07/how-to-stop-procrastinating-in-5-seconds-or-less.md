---
title: "How to stop procrastinating in 5 seconds or less"
date: 2019-07-23T21:34:19-04:00
subtitle: ""
tags: [
    "life",
    "tutorial",
    "hampions"
]
comments: true
---

1. Think about what you're going to do.
2. Close your eyes.
3. Take a deep breath.
4. Think 5...
5. Think 4...
6. Think 3...
7. Think 2...
8. Think 1...
9. Open your eyes.
10. Do the thing.

Want to speed up the process? Count to 3 instead of 5.
