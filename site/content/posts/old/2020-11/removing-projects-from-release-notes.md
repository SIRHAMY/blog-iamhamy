---
title: "Introducing a shorter Projects section in Release Notes"
date: 2020-11-22T21:47:43Z
subtitle: ""
tags: [
    'projects',
    'release-notes'
]
comments: true
---

# Overview

Starting in December, I'll be modifying the way I share my #projects in [Release Notes](https://blog.hamy.xyz/tags/release-notes/). This section will become a lot shorter - focusing mostly on the high level impact and learnings from my projects in a given unit of time. The pithy project deep dives will be moved to a new quarterly Recap post on [HAMY.LABS](https://labs.hamy.xyz/tags/cases). By separating out the projects section I hope to streamline my Release Notes, draw cleaner distinctions between the content available on HAMY.BLOG and HAMY.LABS, and give my projects dedicated space from which to grow.

# Context

Historically my Release Notes were my most popular pieces of content. It made sense to talk about my [Projects](https://labs.hamy.xyz/projects) as part of them for its cohesiveness and marketing opportunity.

However as I've taken a greater [focus on projects](https://hamy.xyz/about/), my [project-related traffic has skyrocketed](https://blog.hamy.xyz/posts/2020-h1-review/#iamhamy). At the same time, my projects section has grown to take up ~40-50% of my recent Release Notes (See [2020 Q3 Review's Projects section](https://blog.hamy.xyz/posts/2020-q3-review/#projects)) and traffic to my release notes themselves has been on the decline.

This tells me a few things:

* Projects content is popular
* Release Notes are bloated with Projects
* Release Notes aren't as popular anymore

Which makes me think that my Projects are underserved by being included in my Release Notes and that my Release Notes may be worse off by having large Projects sections in them. 

To remedy, I'm giving my Projects sections a dedicated space for deep-dives and recaps. I'll be taking a greater focus on smaller, independent shares for each project, and doing a quarterly recap for just projects. I'll then highlight my projects work and link to these Recaps from within my Release Notes.