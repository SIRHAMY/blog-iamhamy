---
title: "Donating to Ubuntu"
date: 2020-11-22T21:22:26Z
subtitle: ""
tags: [
    'hamforgood',
    'ubuntu'
]
comments: true
---

Today I'm announcing that Ubuntu (and Canonical) will be joining my list of organizations that I support through HamForGood. It will be labeled under the cause `Accessible Technology` with the goal of making technology accessible for everyone, everywhere.

I've become a huge fan of Ubuntu since booting my first version back in 2017. The software is solid, it's fast, it's open source, and it works pretty much everywhere. I use it as my primary operating system on each of my computers and it's my OS of choice for my cloud projects.

Besides being an awesome OS, Ubuntu works on almost every device and is free and open source. As the world becomes increasingly inundated with technology, I think it's paramount to have operating systems that work for everyone, everywhere. Adding Ubuntu to HamForGood is my way of voting for the direction they're going and the efforts they've put in to make that happen.

I'm starting my contributions at $10 / month. 

If you're interested in supporting `Accessible Technology` and Ubuntu along with it, consider donating on the official [Ubuntu donation page](https://ubuntu.com/download/desktop/thank-you)