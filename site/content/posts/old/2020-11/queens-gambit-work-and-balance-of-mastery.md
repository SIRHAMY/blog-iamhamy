---
title: "The Queen's Gambit - The work and balance of mastery"
date: 2020-11-25T14:15:20Z
subtitle: ""
tags: [
    'mastery',
    'balance',
    'netflix',
    'contumption'
]
comments: true
---

I just binged Netflix's The Queen's Gambit in a single ~7 hour couch-sitting session. Two elements leaped out at me throughout the show and I wanted to explore them a bit more here - the work and balance of mastery.

Mastery is your ability to execute your craft at the highest level - a world-class level. In the show, Beth's craft is Chess and by the end she achieves mastery over it. For you it could be cooking or accounting or running - it doesn't necessarily have to be something that you're getting paid for nor do you need to be limited to just one craft. My crafts are [creative technology and entrepreneurship](https://hamy.xyz/about).

What I think the show does really well - besides the cinematography and colors and music etc - is show the work that goes into building mastery and the balance necessary to sustain it. Beth is constantly reading, thinking, and discussing her craft - on a plane, in a bath, in her home, in a hotel, with friends, by herself. Whenever she has the opportunity, she's slowly pushing herself towards her goal of mastery.

I think this work is necessary to master your craft. I'm sure there are a few geniuses out there that may be able to do this in a fraction of the time but for the rest of us it's going to take time and effort in order to increase our abilities. When you factor in the population of the world - ~7.5 billion at time of writing - there's likely going to be a handful of people who treat your craft as their calling and use every available opportunity to better themselves.

To be world-class and to achieve mastery means focusing on your craft just as much as the masters do.

I've attempted to approach this in a systematic way - by implementing the "don't break the chain" methodology of habits (as described in [Atomic Habits](https://amzn.to/3ldTrHX)) where I commit to doing one unit of work towards my goals each day. My current implementation is to 1) build 1 feature in my business or creative technology [projects](https://labs.hamy.xyz/projects) and 2) come up with and opportunity size 1 business idea each and every day. Over time, I'll get better at each and over time I'll become a better creative technologist and entrepreneur.

Now there's a problem with just focusing on the work. Focusing just on the work leads to burnout and robs you of arguably the best knowledge source available - the world around you. This is where the idea of balance comes in.

By balancing your work with the rest of your life you can make your work sustainable, postponing burnout indefinitely and allowing you to continuously increase your mastery over years and years. Moreover, the different parts of our life may help you with your craft more than you might expect. Charlie Munger, the billionaire business partner of Warren Buffet, believes in [Worldly Wisdom](https://fs.blog/2015/09/munger-worldly-wisdom/) which holds that learning from different disciplines allows you to build a 'theory of everything' which helps to better reason about both the specific domain you learned and each other domain you may come across.

In the show, Beth generally does a terrible job of balancing her work and her life. She is so focused on getting better at her craft that when faced with stress and obstacles, her out is to consume alcohol and drugs until she essentially forgets about the stress. A more balanced approach may have led to better, faster outcomes, less wasted time, and far less stress. By the end of the show we see this tranformation happen - building supportive connections, learning from the world around her, and taking care of herself.

I approach this balance by goaling on four different foci of my life - Projects, Self, Adventure, and Work. In each focus I create goals and plans according to my values. For instance I goaled on creating a music video, getting a six pack, and traveling the world in [2020 H1](https://blog.hamy.xyz/posts/2020-h1-review). These goals change along with my values. This ensures that I'm allocating my time and efforts in each focus of my life according to the values I hold and - hopefully - will help me lead a balanced life for many years to come.

In closing, mastery takes a lot of work. It's important to balance that work with the rest of your life - it allows you to improve your mastery over a longer period of time and often increases your effectiveness in the process.

Also, go watch [The Queen's Gambit on Netflix](https://www.netflix.com/us/n/6aea2408-f65b-4ca6-a881-1ee2fe154c4f). It's a superb show. Use that link and help me get a free month of Netflix. pls&ty!

In pursuit of mastery,

-HAMY.OUT