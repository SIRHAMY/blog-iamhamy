---
title: "2020 Week 48 Journal - Systems of Reflection"
date: 2020-11-29T13:34:54Z
subtitle: ""
tags: [
    "journals",
    "systems"
]
comments: true
draft: false
---

# Thinking about Systems

I've been reading (and listening to) [Atomic Habits](https://amzn.to/2JkJ0F4) this past month and it's got me pondering my systems quite a bit. One of the topics I've been pondering most is [identity-based habits](https://jamesclear.com/identity-based-habits) - becoming 'the type of person who can achieve the things you want to achieve'.

It still incorporates the base fundamentals of habits - leveraging consistency and grit - but goes further by further framing a direction and purpose for creating habits and systems. I[ refactored my core systems in October](https://blog.hamy.xyz/posts/2020-10-release-notes/#core-systems) based on this idea and so far it's served me well.

That identity: [I'm a world-class Creative Technologist x Entrepreneur](https://hamy.xyz/about/)

This week I'm refactoring another core system - my system of reflection.

# Systems of Reflection

Over the last several years I've built a [habit of reflection](/tags/reflections). It helps me to compile my thoughts, tally my efforts and results, and extract learnings from my experience.

My previous reflection system worked like this:

* previous reflection system
    * 1 [release notes](/tags/release-notes) / month
    * 1 [review](/tags/reviews) / quarter

This worked well for a time but I've found that these reflections have become increasingly formal and data driven. Not a bad thing by itself but it means that I've lost touch with some of the personal pondering that 1) I love, 2) helps me grow, and 3) others seem to enjoy. Moreover I feel that a monthly cadence is too long to pick up on the more granular highs and lows of life and numerous opportunities to share and reflect.

So I'm implementing a new system moving forward that incorporates a new weekly reflection form - the journal. I plan to make these short and informal. An opportunity and reminder to explore my thoughts once / week, take stock of my progress, and share whatever's top of mind.

* new reflection system
    * 1 [journal](/tags/journals) / week
    * 1 [release notes](/tags/release-notes) / month
    * 1 [review](/tags/reviews) / quarter

All of these will still be filed under [reflections](/tags/reflections) for your perusal.

# Systems to build by

If you've followed me for any amount of time you've likely picked up a hint that I love systems. Code systems, process systems, thought systems, etc. So it should be no surprise that a book on systems prompted me to reconsider more than just a single system. In fact I've reconsidered and refactored so many systems this month I doubt I could list them all but I do have one that I'm particularly excited about - my systems of building.

This week Facebook gave everyone the week of Thanksgiving off (thanks zuck!)! I work at Instagram (by Facebook) so I ofc got it off as well. I decided not to go anywhere because COVID is at its worst levels ever and wanted to be a good #globalcitizen. 

Having a week-long staycation is pretty rare so I wanted to find a way to use it to my advantage as much as possible. What I came up with was to try and live my ideal life to test it out, see where the flaws were, and if I even liked it. 

I'm a big fan of FIRE and [I love building projects](https://labs.hamy.xyz/projects). My current ideal life is to continue building projects forever but to be able to do that with flexible timelines and a diversity of problems / projects. This is actually pretty close to what I already do which is nice.

So this past week I thought about my systems and tried to live my ideal life. I've been working on a new business as part of my effort to [validate 12 ideas this half](https://blog.hamy.xyz/posts/2020-h1-review/#looking-forward) (if I finish this would be the first).

I've really been thinking about the idea of consistency in my projects systems and have come up with two daily habits I want to stick to:

* Build 1 feature in a creative technology / business effort
* Ideate and opportunity size 1 new business idea

I hope to build out a backlog of good business ideas and consistently build them out via these two efforts. [Subscribe](https://hamy.xyz/subscribe) for updates on my businesses!

# Releases

* I've taken a new focus on Twitter as a lot of creatives and solo entrepreneurs are sharing their wares there. [Follow me](https://twitter.com/SIRHAMY)
* Announced that I'll be [shortening my reflection projects sections](https://blog.hamy.xyz/posts/removing-projects-from-release-notes/) to better serve my projects and myself
* Watched The Queen's Gambit and pondered [The work and balance of mastery](https://blog.hamy.xyz/posts/queens-gambit-work-and-balance-of-mastery/)

# Stresses

My new project has taken me a lot longer to build than I thought it would. I was planning on using my week off for it but thought I could finish in a few days. Turns out there was a lot to learn - dotnet core, postgres, docker, Google Cloud - and learning takes time. 

I've been thinking of additional ways to streamline my processes to make this and future projects faster without sacrificing the sustainability of these systems. Some things I'm experimenting with:

* limits on 'screen time' - 1 hour per weekday, 2 hours per weekend day
* continuing to publish [technical posts](https://labs.hamy.xyz/posts) when something takes me longer than 20 minutes to figure out
* disabling social notifications and removing social icons from my home screen

Hoping that these will help me be more focused and healthy in the years to come.

# Learnings

* Fitness makes me happy. I feel like I rediscover this once a month but being fit and active really does improve my mood and productivity.
* Ruthless prioritization is good and natural. I've been coming up with a lot more business ideas with my new business ideation system. By opportunity sizing them at creation time it's easy to see which ideas are more promising than others. Ruthless prioritization then becomes less of a scary / mean thing and more of a necessity. So many ideas and only a finite amount of Ham. On a side note, I've also started goaling my projects on Money, Reach, and Craft. Makes prioritizations much simpler and aligns with pretty much all my values when you factor in [HamForGood](https://labs.hamy.xyz/hamforgood/)

# Gratitude

* I'm grateful to have a solid job that allows me to work with limited exposure to others. As we reach greater and greater heights of the pandemic I think it's important to reflect on this privilege.
* I'm grateful to my family for taking every reasonable precaution against the virus. It's for the greater good and, selfishly / morbidly, means there's a much higher likelihood I can see them again next year.
* I'm grateful for the flexibility I've been able to build in my life. My current existence is near ideal (maybe could do with a 4 day work week instead of 5). I think this balance is becoming increasingly important to me as I live longer.

# Fin

Wishing good health to you and yours.

-HAMY.OUT