---
title: "Release notes: November 2020"
date: 2020-12-01T13:44:43Z
subtitle: ""
tags: [
    "release-notes",
    "reflections"
]
comments: true
draft: false
---

_See all [reflections](/tags/reflections)._

This month I've continued to think a lot about who I want to be and what I want to do. Like [last month](https://blog.hamy.xyz/posts/2020-10-release-notes/#core-systems), that 'identity' is summarized with the intro ['I am a creative technologist x entrepreneur'](https://hamy.xyz/about). At a high level this encapsulates three areas where I want to play - art, technology, and business - and serves as a lens for directing these efforts.

November has largely been a month of shifting gears towards this identity. I've spent a lot of time wrapping up my ongoing efforts, making new plans for this identity, and creating new efforts out of it. 

This month I [released a new audio visualization](#moving-through-time), [began selling audio visualizations](#selling-visualizations), [generated a new vision for my art](#shifting-art-focus), [validated a business](#business), and [refactored my systems of Ham](#self).

My name is Hamilton and these are my November 2020 release notes.

# Projects

This new identity has created widespread changes in my projects focus. I've been thinking about [new directions for my art](#art), taken a renewed focus on [solving real problems for real people](#business), and [created new systems](#pushing-projects-forward) to push these efforts forward.

## Art

### Moving Through Time

The biggest (and only?) major release this month was my visual collab with [Griffin Hanekamp](https://griffinhanekamp.com/) and [Steve Is Space](https://open.spotify.com/artist/0KJVXXOwGjWD3n8fifvRJY?si=V9im_13EQ0qYKertSTFgtg) for their track [Moving Through Time](https://open.spotify.com/track/6ywg7pmtZRpgcaj33YZYm3). I built a custom visualization engine - [C10ud](https://labs.hamy.xyz/projects/c10ud/) - which simulates Gray-Scott Reaction Diffusion in sync with audio.

Watch it on YouTube:

<iframe width="560" height="315" src="https://www.youtube.com/embed/KXqBLySqtUU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

I had originally planned on releasing it in [Q3](https://blog.hamy.xyz/posts/2020-q3-review/#moving-through-time) along with the cover art I created but timelines slipped and here it is almost 3 months later.

This effort showed me a lot of things about my creative process, collaboration style, technical knowledge, and core values - to name a few.

On technology - It took me nearly 2 weeks to code the initial version and it took my machine over 300 hours to encode the video itself. This showed me that I was likely using the wrong tools for the job and that one-off creations like this would only be sustainable if I could devote at least a whole month to them. Moreover, this level of effort would need to provide a corresponding level of value to be worth it - in the form of new tools, skills, time, money, etc.

On creative process and collaboration style - When it comes to my creative process I like starting from a very solid theme / idea but then playing around with it to the point where it often looks very different from the initial idea. This is fine for me but often has problems when done in a collaborative environment. In a collaboration, the expected outcomes are reached as a group so coming back with a final result that is largely different than the expectations will lead to a large variance of receptions. I think this is a good thing to learn about and that it's healthy to not always build in a corner but I've taken on 3 collaborations this half and only one was completed on time and within expectations so something needs to change.

Finally on core values - I've thought a lot about why it took me so long to build this. Besides the technical complexity and poor tool choices, I think a large part of it was that I just didn't value it as much as some of my other projects. I don't think this was due to the nature of the project - I really like [building audio visualizations](https://labs.hamy.xyz/tags/audio-reactive/) - but I think it was due to how I was going about it. I made poor technical decisions which increased friction and the direction I was going in didn't feel like it matched the brand and identity I wanted to build. This lowered my satisfaction and motivation and I think is a major factor in slipping timelines.

All of this, in conjunction with my ongoing systems refactors, has prompted me to reflect on my art and what I want it to be.

### Shifting art focus

My [art projects](https://labs.hamy.xyz/tags/art) this year, and most recently [Moving Through Time](#moving-through-time), showed me that I care deeply about the technologies I'm using, the creative liberty I express, and the feel / aesthetics / lens / brand / identity / quality of my creations. 

Building [a visualizer](https://labs.hamy.xyz/projects/prickly/) isn't enough. It needs to be a good visualizer, a Ham visualizer.

Building [AI-generated art](https://blog.hamy.xyz/posts/2020-q3-review/#ana) isn't enough. It needs to be good art, [@hamy.art](https://www.instagram.com/hamy.art/).

I'm shifting my art mission, goals, and lens to better fit my values and shifting understanding of who I am and what I want to be.

#### Core Values

I'm dealing with my value problems by solidifying my missions, lens, and goaling metrics around my core values. This will help me make better choices about the projects I pick up and take approaches to these projects that align with my values.

Mission: Build monoliths featuring new perspectives on the past, present, and future of the world.

Lens: Monoliths, technology, and society.

Goaling Metrics:

- Craft (The tools and practices of creation)
- Quality
- Beauty + elegance

The mission is a slight revision to the one I released in [October](https://blog.hamy.xyz/posts/2020-10-release-notes/). The main updates are that it now features monoliths and adds goaling metrics for the craft, quality, and beauty / elegance of the artifact - units of measure that I think have been missing in my practice.

Monoliths are something I've been interested in creating for a very long time and was reminded of it in my recent [trip to Storm King](#adventure). I even called this out at the end of [H1](https://blog.hamy.xyz/posts/2020-h1-review/#making-art-with-data) after creating my first monolith [coronation](https://labs.hamy.xyz/projects/coronation/).

My current idea of a monolith is that it's an artifact that lives both as art and outside of it - in a point of time but also independent of time. It's a bit vague as I'm still trying to fully understand this myself and because I want this to serve simulatenously as lens, direction, and destination. I think it'll all become clearer in the coming months as I begin releasing them.

#### Creative Process and Collaboration

As I've mentioned numerous times, I think that collaboration is a great way to learn, challenge yourself, and build connections. That being said, I've had problems successfully completing them. Right now I think failed collaborations are worse than not collaborating at all. As such, I'm going to limit myself to 1 collaboration a half, rely heavily on my values to choose which opportunities are most promising, and on my systems to increase probability of success.

#### Technology

I'm not where I want to be wrt my creative technology. Building things takes too much time, the software I'm writing isn't meeting my standards for myself, it's often not generalizable to other use cases, and my tooling / approach is  pigeonholing me into suboptimal creations. All of this means that I'm taking a lot of time to build things, I'm not increasing my skills or repository of tools over time, and I feel constrained by the technologies I'm using.

To tackle this I'm doing several things.

* Moving to 3D
* Building in Unity and ThreeJS
* Focusing on software craft

Together I think these changes will give me more flexibility, control, and generalizability over time.

## Business

This half I had a goal of validating 12 business ideas and launching 6. As of today, I've validated 1 and launched 0. My progress towards this goal was so bad that I even [considered reducing it to 6 and 3](https://blog.hamy.xyz/posts/2020-q3-review/#building-a-business). But as I've been thinking about my values I've decided that this is still a really good goal to have and system to tend to. Over time, and if executed fully, I think it has a high probability of success.

### Selling visualizations

Over the last few years I've built a lot of [audio reactive visualizations](https://labs.hamy.xyz/tags/audio-reactive/). I've collabed with several artists to compliment their music and now am validating whether more people would like some of these visualizations themselves.

I'm starting on Fiverr to get things up and running quickly. If there's interest I'll consider moving elsewhere. 

If you're interested in a visualization for your audio, [let me know!](https://www.fiverr.com/hamylabs)

### Launching a business

Facebook gave us the week of Thanksgiving off and since I wasn't traveling due to COVID, I decided to build a business. I was hoping to finish it last week but this is the first web app I've really built and hosted in quite awhile and, well, I'm just kind of slow at it.

I'm hoping to do a soft launch this week so if you're interested in learning more about it [connect with me](https://hamy.xyz/connect) on my socials for updates.

## Shares

Shares are what I call any content that I share externally. So this post is one for instance. 

This half I've grappled with what value shares bring to me and how much effort I want to expend on them. I've written [reflections](https://blog.hamy.xyz/tags/reflections) ~monthly for the past several years. These have an implicit value to me as they help me document my life and better understand and think about myself.

But when it comes to sharing about my efforts I've struggled to balance the sharing with the building. Value-wise I hold building > sharing but effort-wise the scales don't often reflect that. 

In the end sharing isn't doing, only doing is doing. So I'm changing how I share to reflect that. Each share will now be complimentary to an effort and only rarely vice versa when the value proposition makes sense.

Some changes:

* More shares will be about ongoing efforts
* [Reviews](https://blog.hamy.xyz/tags/reviews) will now link to independent Project Reviews hosted on [HAMY.LABS](https://labs.hamy.xyz) vs hosting them directly
* I'll be sharing more on [Twitter](https://twitter.com/SIRHAMY) to give more legs to my projects
* I'll be producing less videos for [my YouTube channel](https://www.youtube.com/channel/UCPBY44jxP7gOMkUsP5rlGnw?view_as=subscriber), focusing on the cases where a video share is valuable enough to be worth the effort. I'll only create Release Notes videos for [Reviews](/tags/reviews).

# Self

As I've mentioned throughout this release note, I've been doing a lot of reflecting on my systems. Much of these changes are a direct result of reflections I've done while reading [Atomic Habits](https://amzn.to/2HXBJe7).

At the core of these changes is a belief that systems help me better understand myself and the world around me, make me a more effective human, and that good systems will pay dividends over time. In other words, good systems lead to good outcomes and I should invest further in them.

As a result I've created and refactored many, many systems. Here I'll dive into a few changes.

## Systems of Ham

### Pushing Projects forward

As I mentioned in my [projects](#projects) section, I'm taking a renewed focus on building businesses and regoaling on 12 business validations and 6 business launches / half. This is a lot of work and something I haven't been able to accomplish in the past.

Thus if I want different outcomes this time I need to change my systems to reflect that.

I've added two new daily habits to ensure that I'm always pushing towards this goal.

* Ideate and opportunity size 1 new business idea
* Build 1 feature in a creative technology / business effort

Together this system should ensure I have an ample backlog of projects to take on and that I'm constantly pushing these projects to launch.

### Limiting distractions

Over the past month I've bought a lot of things - new clothes, a new computer, a new tablet. I'm constantly trying to be vigilant of new negative behaviors in case they turn into habits. In this case my worry is hedonic adaptation and is something I've historically solved with rigorous budgeting.

A similar behavior I've noticed crop up over the past month is the amount of content I'm consuming. Over my week off, I binged [The Queen's Gambit](https://blog.hamy.xyz/posts/queens-gambit-work-and-balance-of-mastery/) and played video games for over 15 hours. I have nothing against a good tv show or video game but together they represent over 20 hours that could've been used for something more valuable to me - hanging with friends, exploring the city, building projects.

I've had a hard time curbing this habit of high levels of content consumption so I'm creating a system to try and combat it. That system is to limit my content consumption to:

* 1 hour / weekday
* 2 hours / weekend

This content consumption includes tv, movies, video games, social media, and some types of reading. The specifics will differ from situation to situation - like watching a movie with friends is probably exempt but reading a 30 minute popular science article likely wouldn't be. The idea is to start constructing a system to ensure that I'm spending more of my efforts on things I value and less on things I don't - like [my idea of minimalism](https://blog.hamy.xyz/posts/minimalism-more-of-what-you-love-less-of-what-you-dont/).

I'm relying on myself to enforce this but am using several tools for time tracking:

* Toggl - to track how I'm spending my time from an effort standpoint
* RescueTime - to track what I'm spending my time doing from a program / website standpoint

Will report back with findings.

### More systems

There were too many changes to my systems this month for me to fit into this post. Instead I'm starting a system of reflection and sharing on one system each month. This will be a deep dive into a particular system to allow me to analyze the effectiveness of the system and to share out my systems over time.

The first system I'll share about is my [System of Reflection](https://blog.hamy.xyz/posts/2020-week-48-journal/#systems-of-reflection) which is fittingly written about in my first weekly journal - a new output of this system.

## HamForGood

The last thing I wanted to share about was a new organization I'm supporting through my [personal HamForGood](https://blog.hamy.xyz/pages/systems/hamforgood/) initiative - not to be confused with [HAMY.LABS' HamForGood](https://labs.hamy.xyz/hamforgood/)

I've started [donating to Ubuntu](https://blog.hamy.xyz/posts/donating-to-ubuntu/) (and Canonical) each month. Ubuntu is my primary operating system both for my personal computers and my cloud servers. It's an OS that is performant, powerful, and free for all.

I'm supporting it under the cause of `Accessible Technology`

# Adventure

Not much to share here other than we traveled around the area a bit and went to a few restaurants. 

<blockquote class="instagram-media" data-instgrm-captioned data-instgrm-permalink="https://www.instagram.com/p/CHUHyB9Fbv1/?utm_source=ig_embed&amp;utm_campaign=loading" data-instgrm-version="13" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:540px; min-width:326px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:16px;"> <a href="https://www.instagram.com/p/CHUHyB9Fbv1/?utm_source=ig_embed&amp;utm_campaign=loading" style=" background:#FFFFFF; line-height:0; padding:0 0; text-align:center; text-decoration:none; width:100%;" target="_blank"> <div style=" display: flex; flex-direction: row; align-items: center;"> <div style="background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 40px; margin-right: 14px; width: 40px;"></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 100px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 60px;"></div></div></div><div style="padding: 19% 0;"></div> <div style="display:block; height:50px; margin:0 auto 12px; width:50px;"><svg width="50px" height="50px" viewBox="0 0 60 60" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-511.000000, -20.000000)" fill="#000000"><g><path d="M556.869,30.41 C554.814,30.41 553.148,32.076 553.148,34.131 C553.148,36.186 554.814,37.852 556.869,37.852 C558.924,37.852 560.59,36.186 560.59,34.131 C560.59,32.076 558.924,30.41 556.869,30.41 M541,60.657 C535.114,60.657 530.342,55.887 530.342,50 C530.342,44.114 535.114,39.342 541,39.342 C546.887,39.342 551.658,44.114 551.658,50 C551.658,55.887 546.887,60.657 541,60.657 M541,33.886 C532.1,33.886 524.886,41.1 524.886,50 C524.886,58.899 532.1,66.113 541,66.113 C549.9,66.113 557.115,58.899 557.115,50 C557.115,41.1 549.9,33.886 541,33.886 M565.378,62.101 C565.244,65.022 564.756,66.606 564.346,67.663 C563.803,69.06 563.154,70.057 562.106,71.106 C561.058,72.155 560.06,72.803 558.662,73.347 C557.607,73.757 556.021,74.244 553.102,74.378 C549.944,74.521 548.997,74.552 541,74.552 C533.003,74.552 532.056,74.521 528.898,74.378 C525.979,74.244 524.393,73.757 523.338,73.347 C521.94,72.803 520.942,72.155 519.894,71.106 C518.846,70.057 518.197,69.06 517.654,67.663 C517.244,66.606 516.755,65.022 516.623,62.101 C516.479,58.943 516.448,57.996 516.448,50 C516.448,42.003 516.479,41.056 516.623,37.899 C516.755,34.978 517.244,33.391 517.654,32.338 C518.197,30.938 518.846,29.942 519.894,28.894 C520.942,27.846 521.94,27.196 523.338,26.654 C524.393,26.244 525.979,25.756 528.898,25.623 C532.057,25.479 533.004,25.448 541,25.448 C548.997,25.448 549.943,25.479 553.102,25.623 C556.021,25.756 557.607,26.244 558.662,26.654 C560.06,27.196 561.058,27.846 562.106,28.894 C563.154,29.942 563.803,30.938 564.346,32.338 C564.756,33.391 565.244,34.978 565.378,37.899 C565.522,41.056 565.552,42.003 565.552,50 C565.552,57.996 565.522,58.943 565.378,62.101 M570.82,37.631 C570.674,34.438 570.167,32.258 569.425,30.349 C568.659,28.377 567.633,26.702 565.965,25.035 C564.297,23.368 562.623,22.342 560.652,21.575 C558.743,20.834 556.562,20.326 553.369,20.18 C550.169,20.033 549.148,20 541,20 C532.853,20 531.831,20.033 528.631,20.18 C525.438,20.326 523.257,20.834 521.349,21.575 C519.376,22.342 517.703,23.368 516.035,25.035 C514.368,26.702 513.342,28.377 512.574,30.349 C511.834,32.258 511.326,34.438 511.181,37.631 C511.035,40.831 511,41.851 511,50 C511,58.147 511.035,59.17 511.181,62.369 C511.326,65.562 511.834,67.743 512.574,69.651 C513.342,71.625 514.368,73.296 516.035,74.965 C517.703,76.634 519.376,77.658 521.349,78.425 C523.257,79.167 525.438,79.673 528.631,79.82 C531.831,79.965 532.853,80.001 541,80.001 C549.148,80.001 550.169,79.965 553.369,79.82 C556.562,79.673 558.743,79.167 560.652,78.425 C562.623,77.658 564.297,76.634 565.965,74.965 C567.633,73.296 568.659,71.625 569.425,69.651 C570.167,67.743 570.674,65.562 570.82,62.369 C570.966,59.17 571,58.147 571,50 C571,41.851 570.966,40.831 570.82,37.631"></path></g></g></g></svg></div><div style="padding-top: 8px;"> <div style=" color:#3897f0; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:550; line-height:18px;"> View this post on Instagram</div></div><div style="padding: 12.5% 0;"></div> <div style="display: flex; flex-direction: row; margin-bottom: 14px; align-items: center;"><div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(0px) translateY(7px);"></div> <div style="background-color: #F4F4F4; height: 12.5px; transform: rotate(-45deg) translateX(3px) translateY(1px); width: 12.5px; flex-grow: 0; margin-right: 14px; margin-left: 2px;"></div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(9px) translateY(-18px);"></div></div><div style="margin-left: 8px;"> <div style=" background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 20px; width: 20px;"></div> <div style=" width: 0; height: 0; border-top: 2px solid transparent; border-left: 6px solid #f4f4f4; border-bottom: 2px solid transparent; transform: translateX(16px) translateY(-4px) rotate(30deg)"></div></div><div style="margin-left: auto;"> <div style=" width: 0px; border-top: 8px solid #F4F4F4; border-right: 8px solid transparent; transform: translateY(16px);"></div> <div style=" background-color: #F4F4F4; flex-grow: 0; height: 12px; width: 16px; transform: translateY(-4px);"></div> <div style=" width: 0; height: 0; border-top: 8px solid #F4F4F4; border-left: 8px solid transparent; transform: translateY(-4px) translateX(8px);"></div></div></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center; margin-bottom: 24px;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 224px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 144px;"></div></div></a><p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="https://www.instagram.com/p/CHUHyB9Fbv1/?utm_source=ig_embed&amp;utm_campaign=loading" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">A post shared by Hamilton Greene (@sir.hamy)</a></p></div></blockquote> <script async src="//www.instagram.com/embed.js"></script>

<blockquote class="instagram-media" data-instgrm-captioned data-instgrm-permalink="https://www.instagram.com/p/CH6i7JrF1be/?utm_source=ig_embed&amp;utm_campaign=loading" data-instgrm-version="13" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:540px; min-width:326px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:16px;"> <a href="https://www.instagram.com/p/CH6i7JrF1be/?utm_source=ig_embed&amp;utm_campaign=loading" style=" background:#FFFFFF; line-height:0; padding:0 0; text-align:center; text-decoration:none; width:100%;" target="_blank"> <div style=" display: flex; flex-direction: row; align-items: center;"> <div style="background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 40px; margin-right: 14px; width: 40px;"></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 100px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 60px;"></div></div></div><div style="padding: 19% 0;"></div> <div style="display:block; height:50px; margin:0 auto 12px; width:50px;"><svg width="50px" height="50px" viewBox="0 0 60 60" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-511.000000, -20.000000)" fill="#000000"><g><path d="M556.869,30.41 C554.814,30.41 553.148,32.076 553.148,34.131 C553.148,36.186 554.814,37.852 556.869,37.852 C558.924,37.852 560.59,36.186 560.59,34.131 C560.59,32.076 558.924,30.41 556.869,30.41 M541,60.657 C535.114,60.657 530.342,55.887 530.342,50 C530.342,44.114 535.114,39.342 541,39.342 C546.887,39.342 551.658,44.114 551.658,50 C551.658,55.887 546.887,60.657 541,60.657 M541,33.886 C532.1,33.886 524.886,41.1 524.886,50 C524.886,58.899 532.1,66.113 541,66.113 C549.9,66.113 557.115,58.899 557.115,50 C557.115,41.1 549.9,33.886 541,33.886 M565.378,62.101 C565.244,65.022 564.756,66.606 564.346,67.663 C563.803,69.06 563.154,70.057 562.106,71.106 C561.058,72.155 560.06,72.803 558.662,73.347 C557.607,73.757 556.021,74.244 553.102,74.378 C549.944,74.521 548.997,74.552 541,74.552 C533.003,74.552 532.056,74.521 528.898,74.378 C525.979,74.244 524.393,73.757 523.338,73.347 C521.94,72.803 520.942,72.155 519.894,71.106 C518.846,70.057 518.197,69.06 517.654,67.663 C517.244,66.606 516.755,65.022 516.623,62.101 C516.479,58.943 516.448,57.996 516.448,50 C516.448,42.003 516.479,41.056 516.623,37.899 C516.755,34.978 517.244,33.391 517.654,32.338 C518.197,30.938 518.846,29.942 519.894,28.894 C520.942,27.846 521.94,27.196 523.338,26.654 C524.393,26.244 525.979,25.756 528.898,25.623 C532.057,25.479 533.004,25.448 541,25.448 C548.997,25.448 549.943,25.479 553.102,25.623 C556.021,25.756 557.607,26.244 558.662,26.654 C560.06,27.196 561.058,27.846 562.106,28.894 C563.154,29.942 563.803,30.938 564.346,32.338 C564.756,33.391 565.244,34.978 565.378,37.899 C565.522,41.056 565.552,42.003 565.552,50 C565.552,57.996 565.522,58.943 565.378,62.101 M570.82,37.631 C570.674,34.438 570.167,32.258 569.425,30.349 C568.659,28.377 567.633,26.702 565.965,25.035 C564.297,23.368 562.623,22.342 560.652,21.575 C558.743,20.834 556.562,20.326 553.369,20.18 C550.169,20.033 549.148,20 541,20 C532.853,20 531.831,20.033 528.631,20.18 C525.438,20.326 523.257,20.834 521.349,21.575 C519.376,22.342 517.703,23.368 516.035,25.035 C514.368,26.702 513.342,28.377 512.574,30.349 C511.834,32.258 511.326,34.438 511.181,37.631 C511.035,40.831 511,41.851 511,50 C511,58.147 511.035,59.17 511.181,62.369 C511.326,65.562 511.834,67.743 512.574,69.651 C513.342,71.625 514.368,73.296 516.035,74.965 C517.703,76.634 519.376,77.658 521.349,78.425 C523.257,79.167 525.438,79.673 528.631,79.82 C531.831,79.965 532.853,80.001 541,80.001 C549.148,80.001 550.169,79.965 553.369,79.82 C556.562,79.673 558.743,79.167 560.652,78.425 C562.623,77.658 564.297,76.634 565.965,74.965 C567.633,73.296 568.659,71.625 569.425,69.651 C570.167,67.743 570.674,65.562 570.82,62.369 C570.966,59.17 571,58.147 571,50 C571,41.851 570.966,40.831 570.82,37.631"></path></g></g></g></svg></div><div style="padding-top: 8px;"> <div style=" color:#3897f0; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:550; line-height:18px;"> View this post on Instagram</div></div><div style="padding: 12.5% 0;"></div> <div style="display: flex; flex-direction: row; margin-bottom: 14px; align-items: center;"><div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(0px) translateY(7px);"></div> <div style="background-color: #F4F4F4; height: 12.5px; transform: rotate(-45deg) translateX(3px) translateY(1px); width: 12.5px; flex-grow: 0; margin-right: 14px; margin-left: 2px;"></div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(9px) translateY(-18px);"></div></div><div style="margin-left: 8px;"> <div style=" background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 20px; width: 20px;"></div> <div style=" width: 0; height: 0; border-top: 2px solid transparent; border-left: 6px solid #f4f4f4; border-bottom: 2px solid transparent; transform: translateX(16px) translateY(-4px) rotate(30deg)"></div></div><div style="margin-left: auto;"> <div style=" width: 0px; border-top: 8px solid #F4F4F4; border-right: 8px solid transparent; transform: translateY(16px);"></div> <div style=" background-color: #F4F4F4; flex-grow: 0; height: 12px; width: 16px; transform: translateY(-4px);"></div> <div style=" width: 0; height: 0; border-top: 8px solid #F4F4F4; border-left: 8px solid transparent; transform: translateY(-4px) translateX(8px);"></div></div></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center; margin-bottom: 24px;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 224px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 144px;"></div></div></a><p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="https://www.instagram.com/p/CH6i7JrF1be/?utm_source=ig_embed&amp;utm_campaign=loading" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">A post shared by Megna Saha (@megna.saha)</a></p></div></blockquote> <script async src="//www.instagram.com/embed.js"></script>

# Work

Felt really good to take a week off work though I ended up keeping to a very similar schedule. I think this is because [my work week is almost ideal](https://blog.hamy.xyz/posts/2020-week-48-journal/#gratitude) wrt hours and effort allocation.

I have 1 month left to finish all of my projects so I'm narrowing down what I'm working on to ensure I ship as much stuff as I can. In parallel, I'm working on half goaling for 2021 H1. This process has both influenced and been influenced by my personal half goaling and I'm excited to see what the future holds.

# Fin

Thanks for reading! I'll be spending the majority of December working on my yearly Review and planning for 2021 H1.

If you want updates when it comes out, [subscribe to my email list](https://hamy.xyz/subscribe) or [connect with me on social](https://hamy.xyz/connect).

Spreading systems of gratitude,

-HAMY.OUT