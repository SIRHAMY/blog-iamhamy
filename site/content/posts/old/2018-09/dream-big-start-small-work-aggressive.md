---
title: "Zero to One: Dream Big, Start Small, Work Aggressive"
date: 2018-09-19T23:42:57-04:00
subtitle: ""
tags: [
    'contumption'
]
---

I recently read Peter Thiel's Zero to One. As part of my habit of reflection, here are some of my key takeaways:

**Competition is the bane of capitalism.** If you really want to make it big, you've got to corner a market or else risk nickel and diming the customer, your workers, and your competition to starvation. The given example is that Google has cushy workplaces because it has a monopoly on search (and relevant advertising) and therefore doesn't need to fight in a price war. On the other hand there are rental car companies or general retailers (WalMart) that don't have much product-wise to separate them from the competition. _I'm sure you can poke holes in these arguments, just noting them for posterity._

As such, you should not enter a market with a lot of competition (characterized by numerous providers with near equivalent offerings) unless you believe you have the power to disrupt it (Thiel asserts your offering must be 10x better than the competition to pull this off). Instead, first look for markets you think you can corner, even if they are niche. Example: PayPal going after professional Ebay buyers/sellers first then attempting to expand from there.

**Companies tend to follow the power law wrt value** - only ~20% of companies create ~80% of the value (note that this law tends to occur in many environments including the workplace). As such, many times it's better to join a company you think may make it vs starting yet another avg startup. The key here is to make the future and own a slice of it - whether that means starting the pie yourself or jumping in for it later. A piece of pie is better than no pie.

**Go for the moonshots, find the secrets.** Thiel posits that great companies are simply solving a problem or leveraging an advantage that others have overlooked. Thinking w/ this perspective can hhelp you avoid entering markets with tough competition. _Slightly tangential, but I like to allocate much of my efforts with a hedged bet in moonshots by spending 90% on the strong, safe choices and 10% of it on edgy moonshots - I do this in both investing and project creation._

**Contrary to the common belief that software will eat the world,** Thiel believes that the next big era will consist of computers that help humans better do their duties, leveraging the relative strengths of man and machine.

**To do anything of worth, one must dream big, start small, and work aggressively.** This falls in line with my ideals to live with curiosity, intelligence, and grit.

The read inspired me to construct a vision for myself in the future - that vision being to build something that undeniably pushes the human condition forward. I'm not yet sure what that construction will entail, but it seems a worthy goal to put my effort behind.

If you haven't read it yet, would highly recommend. A lot of interesting thoughts to ponder. Still plugging for Libby as a source of books - easy, free, and legal.