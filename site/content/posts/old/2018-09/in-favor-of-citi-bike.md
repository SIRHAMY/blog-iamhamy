---
title: "In Favor of Citi Bike"
date: 2018-09-20T22:20:12-04:00
subtitle: "Ride more bikes, see more world."
tags: []
---

![Commute times from 35th and 2nd to Union Square](/posts/2018-09/in-favor-of-citi-bike/commute.PNG)

I live in Murray Hill, a neighborhood of the Manhattan borough of NYC, and work near Union Square at [APT - a big data company](https://www.predictivetechnologies.com/en/telecom-and-media). For my usual commute, metroing doesn't make that much sense unless I'm carrying a lot of stuff, am being particularly lazy, or there's some hostile weather conditions. By Google's estimations, the different methods of commute take:

* Metro - 24 mins
* Walk - 26 mins
* Bike - 12 mins

I generally gravitate towards efficiency so here biking takes the cake with walking and metro a wash. I also generally prefer to take the healthy habit when available with biking and walking coming out ahead wrt exercise, nature, and less exposure to germs (at least in an enclosed space). As such, my priorityQueue for transportation types typically looks something like:

1. Biking
2. Walking
3. Metro

Now I'll admit I was deathly afraid of biking in the big apple when I first arrived. For starters, I hadn't biked much since an arm-breaking bike accident in my early teens. Add in the ranks of seemingly-fearless New Yorkers zipping between cars, flicking off honking taxis, and otherwise being reckless in their helmetless work attire and there was quite a lot to make me think thrice. But a quick trip to Chicago and the graciousess (peer pressure?) of a fab host broke down those mental barriers and got me back on my feet (wheels?).

# The numbers

Now I've already said that I'm biased towards biking and walking. Part of this is due to the health-related benefits, but another significant factor (and likely the nail in the coffin) is the price points of these similar options.

1 MonthUnlimitedMetroRidesPrice = ~$110

1 SingleMetroRidePrice = $2.75

TimesToRideEachMonthForUnlimitedToBeWorthwhile = 1 MonthUnlimitedMetroRidesPrice / SingleMetroRidePrice = 110 / 2.75 = 40

Thus, you must ride 40 times each month, or 40/daysInMonth = ~1.33 times each day.

$110 may not seem like a lot at the outset, but I like to put these values in perspective by highlighting their opportunity cost. In [The case for brewing your own cold brew](http://blog.hamy.xyz/posts/case-for-self-brewing-cold-brew/), I detail how brewing my own cold brew saves me ~$100 each month which equates to one large trip every year or a reduction of my time to retirement (assuming 5% interest over a 15 year time horizon) of an entire year. Not bad for a few thoughtful minutes each weekend.

In my case, all I've got to do to realize these metro savings is walk. Sure this may not always be the most pleasant form of transportation but it's usually quite dependable (a good pair of boots and a down coat go a long way in winter).

Let's be real though, there will certainly be times where walking/biking is not practical - w/ luggage, going to Brooklyn/UES/LES - so we can more realistically put these savings somewhere around $30-60 each month. Still sizable - that's a small/medium sized trip each year at $300/$600 respectively.

# What about biking?

I work at a subsidiary of MasterCard, a corporate sponsor of Citi Bike, so we get unlimited 45 minute rides around the city for ~$65 / year.

Yes, you read that right. $65 / year. For essentially unlimited rides. The only time I've ever gone over 45 mins was when I got lost biking from my place in Murray Hill to a hackathon deep in Bushick.

![Commute times from 35th and 2nd to Union Square](/posts/2018-09/in-favor-of-citi-bike/bushwick-long-bike.PNG)

Amortized, the cost of the bike services is about $5.41 each month. I'm not sure how much I'll be biking in winter, but it comes out to only $10.82 each month if I relegate the benefits and associated costs to the warmest 6 months of the year.

So let's recap, for me, an able-bodied individual with no qualms walking/biking the ~1.2 miles to work in Manhattan, rain or shine, I save anywhere between $30-60 each month (assuming 2 - 3 round trips by train each week and including costs of bike) over purchasing the unlimited pass. At worst, that comes out to $360 / year which can net me an additional small trip (within the New york area). At best, that's another $720 which could net me a medium trip (within the continental US) each and every year. Pretty good for a little added exercise each day.

_Ride more bikes, see more world._


