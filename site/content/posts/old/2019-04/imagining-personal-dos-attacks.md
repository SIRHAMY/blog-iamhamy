---
title: "Imagining personal DOS attacks (or how to ransomware your friends in 17 lines of JavaScript or less)"
date: 2019-04-15T11:25:24-04:00
subtitle: ""
tags: [ 
    "security",
    "dos",
    "attack",
    "social-media"
]
comments: true
---

DOS, or [denial of service](https://en.wikipedia.org/wiki/Denial-of-service_attack), attacks are a type of cyber attack that result in, well, denial of service. Traditionally this was accomplishable with a few machines spinning up tons of pseudo-genuine requests against public endpoints. This forces a service to allocate its resources to serving this fake traffic leaving less available for legitimate traffic, resulting in a denial of service to that legitimate traffic. Nowadays modern DOS attacks often involve large botnets of connected machines at different IPs to help circumvent these rules (see [Mirai](https://en.wikipedia.org/wiki/Mirai_(malware))) As these kinds of attacks became easier to pull off/more known and thus more prevalent, platform builders have had to evolve their defences with things like ip-based rate limiting and AI powered behavioral traffic rules to identify and respond to bad actors in real-time.

All this to say that modern DOS attacks and prevention strategies can get quite complicated but that there may be holes in current defensive strategies, abusable with small modifications to the type of attack.

One such example is if we limited the target scope of the attack from the service down to individual users of that service. Think of this as a hyper local DOS attack. The impact of such an attack would obviously be far less reaching but just as, if not more, effective against those users caught within its scope. Optimizations could be made to reasonably increase the surface area wrt requisite human effort/time/machine resources if a single account wasn't the desired area of the attack.

Let's use email as an example of one such an attack. Email seems like a good candidate because it's of high value to the target and it's often used as a second factor for other services so denial of this service could be a good first step in denial of any service if a tiered approach was more to your liking.

Before we go into how we might attack an email, let's talk a little bit about a naive login system to give us some context about how this exploit actually works.

At its core, a login process is just a form that gets user input for an identifier and passcode and verifies that info against its records. It's important to note that any type of knowledge factor is hackable given enough time and chances (the purpose of those password strength indicators is to get you to use enough characters from enough different parts of the character library to make this kind of hacking sufficiently hard (you can [play around with password strengths here](https://howsecureismypassword.net/))). To reduce the odds of a malicious user guessing the correct knowledge token, many systems will implementa a maxInvalidAttempts rule so that hackers can't get in given enough time. This is a pretty good solution as it makes it nigh impossible for a hacker to guess this without insight about the password that could sufficiently narrow the possibility space.

Take for instance a password like `123pass!` that has ~94^8 possibilities (given only ASCII is used and we only use codes [32, 126]). Without a maxInvalidAttempts rule, this would take approximately 19 minutes for a sufficiently resource-allocated computer to crack but is unlikely to be guessed by any computer given only 20 attempts - like a 0.0000000000003% chance and that's assuming they know the correct number of characters in your password string to start with (estimated time from [howsecureismypasword](https://howsecureismypassword.net/))

So we can see how the maxInvalidAttempts bar is useful for keeping bad actors out but what if our goal isn't to get in but just to keep the valid user out? Could we use this mechanism to our advantage?

To answer this question, think back to your highschool days (assuming you were in highschool when smartphones were a thing). When classmates stole your phone, it was unlikely they'd get in. However they could, perhaps even more detrimentally, input a bad password enough times to get your iPhone to lock itself for 5, 10, or even 30 minutes at a time! This wasn't just possible, it was trivial if they could just hold onto your device long enough to wait out each initial lockout before entering bad input and escalating it.

To bring this back to our email attack, let's say we know your email somehow, say `iamfake@fake.com`. This gives us an identifier to use. Now all we need to do is make invalid post requests to the given service's authentication points sufficient times to cause a lockout. Assuming the service has a high lockout bar of say 20 times, this is trivial even for the least performant computers.

Like seriously, this'll run in your browser in < 1 second. [Give it a try.](https://jsfiddle.net/9ktxaodf/)

```
for(var i = 0; i < 20; i++) {
	fetch(
        "https://labs.hamy.xyz/fake-email-login",
        {
            method: "POST",
            mode: "no-cors",
            cache: "no-cache",
            credentials: "same-origin",
            headers: {
            "Content-Type": "application/json"
            },
            body: {
                "email": "iamfake@fake.com",
                "pass": "123pass!"
            }
        })
        .then(response => console.log(response));
}
```

Each successful lockout means that the user 1) has to notice the lockout, 2) has to trigger the reset process, and 3) has to successfully complete it to regain access to their account.

Depending on their account setup and the rules of the service, this could cost them anywhere from 15 minutes to an hour and certainly cause a fair bit of frustration. Take for instance a corporate or bank account that may require you to call a representative and walk through several security questions/personal information to unlock your account. Add onto this the hassle of doing this several times in a row which may cause that entity to flag your account as having problems, increasing requisite information for login and linearly increasing the amount of time to unlock.

If we had taken the tiered approach in which we first attack the emails before attempting to attack other services with corresponding identifiers, it's possible we take not only the time for them to unlock the first account but also the second account (the email) assuming it was used as the primary factor. 

Now if we put these lockouts on a recurring cadence and distribute where the attacks come from / what targets each uses we can come up with a scheme that causes recurring partial DOS for some subset of legitimate users. Not as impactful wrt breadth as a full on DOS but sufficiently disruptive to at least frustrate individual users.

One example of how to do this would be to create a JS payload with either a predefined or dynamic list of accounts / urls combos to attack. At a Max of ~20 attacks required per target, you could easily hit 1000+ targets in the time it took me to read a Hackernoon article, even from my phone browser.

For most of us this likley isn't a problem. Our online accounts are sufficiently obscured and we're small enough targets that putting us in this hitlist, at least manually, doesn't make much sense.

But for some online influencers/big fish in general, this could be a very big deal. Take large Instagram accounts for instance that pull in sizable amounts of money from their presence, making this a primary income stream for these users. To disrupt this, all you need is an internet connection and a weak computer.

Let's do a mind experiment with [The Fat Jewish](https://www.instagram.com/thefatjewish/?hl=en). He's fat. He's Jewish. He has 10.4M followers and some articles claim he makes > $6000 per Instagram ad / mention. Let's assume that he puts one of these out each day, putting him at a rough (365 * 6000 = 2190000) $2.19M in revenue each year.

You could imagine that if we could keep him out of his account / make it hard enough for him to run his business for even a few days, that an extortion fee of a few thousand dollars is well within the realm of possibility considering how much potential revenue he'd be losing without his access. Not a bad pull for a ransomware that can be triggered from almost anywhere by anyone for ~free.

# fin

Probably don't do any of the above at home kids. It's likely illegal and definitely against the target service's ToS.

If you like this / want to read more like this / want to boost my ego, consider [subscribing](https://hamy.xyz).