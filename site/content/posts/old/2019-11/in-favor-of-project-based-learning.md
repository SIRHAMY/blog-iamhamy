---
title: "In Favor of Project Based Learning"
date: 2019-11-03T16:57:46-04:00
subtitle: ""
tags: [
    "musings",
    "learning"
]
comments: true
---

One of my core values is to contantly be progressing / learning / changing. I believe that if one doesn't continue to change, then they'll stagnate, and stagnation is ultimately falling behind.

I believe the best way to grow is arguably to learn, putting your human-given competitive advantage of conscious thought to work. But I've also found that learning and knowledge-gathering for the sake of learning isn't very fun or satisfying. I think that's because it doesn't really _do_ anything. Knowledge might be power, but if it's not put to work then it might as well not exist.

My favorite way to apply knowledge is through building things (if you're interested, you can check out some of my creations on [HAMY.LABS](labs.hamy.xyz)). To me, building is challenging, satisfying, and you often learn a lot along the way.

Of course, if I just build things then I'm mostly re-using the same thoughts and ideas of previous iterations. Eventually I'll start repeating myself which I see as another form of stagnation.

So how can I combat stagnation while maximizing for the satisfaction I inherently find in building? Through project-based learning.

In project-based learning, you learn with a purpose - to find problems and ideas for things to build and then to find ways to go about doing so.

Through this method, we can position building as the reward of learning and fend off stagnation by habitually learning in our building cycles. We maximize the satisfaction gleaned from building by minimizing the learning process to that which is necessary to successfully push through a knew build cycle - this can obviously vary from project to project, but is theoretically always at the minimum time and effort expenditure you're willing and able to make.
