---
title: "This site runs on ads (and Ham)"
date: 2019-11-24T21:14:56-05:00
subtitle: ""
tags: [
    "ads"
]
comments: true
---

This is a notice that this site and related (*.hamy.xyz) sites run on ads.

# why ads?

Because sites aren't free to host or at least I don't host my sites for free. Plus I like money. And this is an easy way to, potentially, make money doing what I'm already doing.

# what kinds of ads?

So far I'm just running banner ads and the occasional affiliate ad in posts about something related. For banner ads, I usually don't curate the content that appears there so there'll likely be times where the ad content clashes a bit with the actual content (for the uninformed this is usually automatically chosen by ad-distributors' magic algorithms). I think most people are ad blind and those that aren't don't think banners detract too much from the content, so I think that edge case is worth the risk. If I find that it's constantly clashing, I'll look to change strategies.

For affiliate links, I'll try to only put links for things that I actually use / like. This is mostly because I want my monetization practices to be very easy for me which means as little work on top of writing a post as possible. Writing about shit I don't like (unless I'm trashing them) is a pain so likely not going to happen.

# why did I write this?

Because I think you're supposed to have a disclaimer on your sites about ads or something like that. Also I wanted some written guidelines for monetization so I could iterate on them in the future.

`Transmission complete.`
