---
title: "Why I write things down"
date: 2019-10-30T15:33:37-07:00
subtitle: ""
tags: [
    "musings"
]
comments: true
---

I write a lot of things down. From regular reflections on my life and sporadic thought experiments to how to build a robust continuous delivery pipeline for your Hugo-statically-generated website. Some might say I'm a bit obsessive about this prose-based logging.

So why do I do it?

# writing is knowing what you think

I think a lot of things us humans do is done on autopilot. I think a little bit of mindfulness can go a long way towards improving your existence - after all if you don't know it exists, you can't fix it.

Writing gets what's in your head out and thus allows you to more objectively view it / understand it and thus allow you to more accurately understand and make decisions on it.

# easy to share

One of the things I dislike most is unnecessary effort. It's just unnecessary.

Needing to repeat my thoughts / arguments is thus obvs not my favorite thing to do. By writing (and subsequently publishing) those thoughts, I now have an easy way to distribute it in a highly-scalable manner.

# best way to hold what you thought for posterity in the future

The human mind is a fickle thing, particularly its ability to remember things accurately. It's fraught with data loss and extremely vulnerable to suggestion / bias. For me, personally, I forget things all. the. time. Thus I think it's extremely useful to hold important thoughts in some manner that is robust to this data corruption.

Writing. It's there, it's in the form it was originally published in, and thus I can return to it whenever I need w / o corruption.

