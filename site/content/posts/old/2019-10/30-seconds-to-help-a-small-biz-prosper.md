---
title: "It only takes 30 seconds to help a small business prosper"
date: 2019-10-23T08:21:04-07:00
subtitle: ""
tags: [
    "ham-for-good",
    "self",
    "projects",
    "small-business"
]
comments: true
---

_Disclaimer: At time of writing, I work at Facebook. Although I mention the company a few times in this post, all views are my own and not that of FB._

A lot of what makes a business prosperous is the ability to find it. If you don't know it exists, you can't be a patron and patronage would seemingly be causal to a business' success.

In the age of the internet (is that the age we're in right now?), much of the discoverability of a given entity is based on its representation within a handful of massive aggregators.

* search engines (Google, Bing, idk if there are other ones but other ones if they exist)
* social media (Facebook, Twitter, Instagram, et al.)
* news papers / other media (NYT Cooking section, Infatuation, Thrillist, even Netflix docs at this point)

A lot of these representations are driven by signals that the aggregators use to try and bubble up the best to the top. Most of these aggregators are largely driven by:

* reviews (you know, those things you put into Yelp)
* images (if a place doesn't have images on Google, are you really going to go?)
* posts / articles / shares (FB wants to bubble up posts with high like counts and shares, Google wants to show places that are written about a lot #pagerank, and Netflix can't make a documentary about a sushi place it hasn't heard of nor will it make a documentary about a sushi place it doesn't know is interesting)

The cool thing is that many ofthese things can be (and certainly are) created by people just like you)

* You can take and upload images to Google, Yelp, Instagram, et al.
* You can review a place on Google and Yelp (we all know that a 4.2 start place with 1200 reviews is better than a 4.2 start place with 56 reviews)
* You can do a video walkthrough on YouTube or post an interview on your personal blog

So the next time you decide to vote with your money for small business, consider also voting with 30 seconds of your time (and a pinch of effort).

PS: This methodology also holds for supporting local artists, so throw them some likes and share their content even if you have no intention of supporting them financially.

Some artists, I'm rooting for:

* [@isspace.us](https://www.instagram.com/isspace.us/)
* [@griffin_hanekamp](https://www.instagram.com/griffin_hanekamp/)
* [@zachplummer](https://www.instagram.com/zachplummer/)
* [@megna.photo](https://www.instagram.com/megna.photo/)

If you're interested in reading more about my philosophy of voting with your time, effort, and money I have a whole section devoted to it in [my last reflection](https://blog.hamy.xyz/posts/2019-q3-review/#self).
