---
title: "Save money (and the world) by making your own cold brew"
date: 2019-10-14T22:56:38-04:00
subtitle: ""
tags: [
    "coldbrew",
    "world-warrior",
    "self",
    "climatechange",
    'finance'
]
comments: true
---

By making your own cold brew, you can:

1. Save money

In [The case for brewing your own cold brew](https://blog.hamy.xyz/posts/case-for-self-brewing-cold-brew/), I calculate the monetary boon for the practice to be ~$1200 / year for me - equivalent to two medium trips or an entire year closer to retirement over a 15 year horizon.

So I won't repeat myself here.

2. Save the planet

Each time you choose to brew your own cold brew, you're likely saving both a plastic cup and a straw.

_How much does that matter?_

In my last post, I estimate I used to buy ~1 cup / day, so that's about 365 cups each year.

According to [earthday.org](https://www.earthday.org/2018/04/18/fact-sheet-how-much-disposable-plastic-we-use/), we use about 500 billion plastic cups each year.

According to Google, there were 7.53 billion people in 2017 (likely higher now).

So if we assume everyone's just average, then that means each person is using about 66 cups each year.

Yikes, I'm way over average and that's just my coffee consumption habits

So by brewing my own cold brew (or I guess you could just bring a reusable cup) most days (let's say 6) of the week, I can get my cup consumption down to 52! Slightly less than the average!

Ofc this doesn't take into account my other plastic cup consumption but it's a start.

Anywho - make your own cold brew to save money and the planet.