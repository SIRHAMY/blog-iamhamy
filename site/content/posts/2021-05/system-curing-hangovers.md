---
title: "System: Curing Hangovers"
date: 2021-05-15T15:09:21Z
subtitle: ""
tags: [
    'systems',
    'hangover',
    'health'
]
comments: true
---

I've had a lot of hangovers in my life. In fact I'd consider myself somewhat of a hangover veteran. You can call me a #hangoverham.

This morning I woke up with a hangover, went through my Morning Ritual, and felt halfway decent just 45 minutes later. I thought that was cool so decided to share my ritual so that #futurehams and #nothams could leverage it for their own benefit.

# System

Obviously you can't cure _every_ hangover. Some are too large or hairy to get over in a reasonable amount of time / effort but I have found this series of steps to be pretty effective most of the time:

1. Wake up and get out of bed as soon as manageable
2. Take a shower
3. Get coffee
4. Drink water
5. Eat an egg sandwich
6. Brush teeth

Typically by the end of this process, I've reduced the hangover's badness by like 80%. That's pretty good for doing normal stuff.

Some other things I do to try and manage the badness:

* Drink water throughout the night and before bed
* Get at least 6 hours of sleep (8+ if possible) but don't let this ruin the whole next day

GL #hungovernothams

-HAMY.OUT