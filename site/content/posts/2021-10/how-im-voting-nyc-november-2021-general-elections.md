---
title: "How I'm voting: November 2021 NYC General Elections"
date: 2021-10-25T23:52:33Z
subtitle: ""
tags: [
    'nyc',
    'elections'
]
comments: true
draft: false
---

In this post I'll share how I'll likely be voting in the November 2021 General (and special?) elections this year. Voting day is November 2 with early voting through October 30thish so make your plan to vote today!

# Picks

* Mayor - [Dem] Adams
* Manhattan Borough President - [Dem] Mark Levine
* Comptroller - [Dem] Brad Lander, says will divest from fossil fuels
* Public Advocate - [Dem] Jumaane Williams, was endorsed by Bernie and NYT in past for Lt. Gov.
    * [Independent] Anthony Herbert, seems very focused on housing equality which I think is a worthy cause. A soft 2nd pick.
* District Attorney - [Dem] Alvin L Bragg
* City Council 2nd Council District - [Dem Soc] Carlina Rivera, the big controversial issue seems to be the development of EV area and whether East River Park will get torn up and raised (as a sea wall) or not. I'm generally in favor of long-term solutions so think we should raise the wall.
* Supreme Court - 2 slots, 2 candidates
* Judge of Civil Court - 2 slots, 2 candidates
* Proposals on Ballot
    1. Updating State's redistricting process - No, while there are some good amendments in here they're coupled with amendments that make it much easier for a majority party to gerrymander districts - we don't want that
    2. Entitlements to Clean Environment - yes, give environmental issues more legal footing
    3. Shortening 10 day voting registration - yes, you can sign up for a bank in a day we should be able to sign up to vote in a day
    4. No-excuse absentee ballots - yes, absentee (and really web-based) ballots should become first-class
    5. expand nyc civil court purview - yes, internet says that this will speed up a lot of civil disputes which seems good


# Resources

Honestly it was _so_ hard to find info on local elections - who the candidates are, what their policies and track records were, who is even on the ballot this week. It feels like it shouldn't be that hard - esp for a high tech city like NYC - but... it was.

Here are some resources I found helpful in today's search:

* Find your pollsite and view a sample ballot (https://findmypollsite.vote.nyc/)
  * This is hosted on vote.nyc which is the NY Board of Elections official website so it's about as good a source of truth as you're gonna get. It doesn't have info about the local candidates but it's a good starting point for research.

Guides

* [Gothamist 2021 Election Guide](https://gothamist.com/news/nyc-2021-election-guide) - Very dem leaning (other party members not well documented) and really only updated at primaries this summer, but still one of the only introductory guides I could find with overviews of the candidates
* [The City General Election Prep](https://www.thecity.nyc/civic-newsroom/2021/10/12/22723235/nyc-general-election-prep-what-to-know-before-voting) - Similarly has a good overview of candidates and some of their positions, including city council which was extremely hard to find info on even for relatively well populated districts