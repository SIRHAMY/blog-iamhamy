---
title: "Release notes: November 2021"
date: 2021-12-05T23:06:10Z
subtitle: ""
tags: [
    "release-notes",
    "reflections"
]
comments: true
draft: false
---

_See all [reflections](/tags/reflections)._


_I regularly reflect on my life - my goals and aspirations, my systems and habits, and my respective progress and diligence. I find these reflections help me to understand myself, the world around me, and how we intersect - providing a foundation from which to ponder, plan, and execute the next steps of my journey. I publish these reflections on [my blog](/tags/reflections) as a way to hold myself accountable, stay in touch with reality, and  provide an autobiographical history of Ham. Enter: The Hamniverse._

The last month of the year is a time of reflection for me. I initiate my thorough process of self evaluation (see: [2020](https://blog.hamy.xyz/posts/2020-review/), [2019](https://blog.hamy.xyz/posts/2019-in-review/)).

This month I've been looking forward to my yearly reflection, thinking about what I would find / learn / change if I did it today. This thought experiment led to a pretty concrete finding:

`skipping systems -> inefficiencies -> preventable negative outcomes`

I've built my life systems over many years of observations, trials, and errors. In the short-term these systems feel like a lot of work with very little gratification - you do the work now, you may see the benefits later. Interestingly though the long-term positive outcomes are rarely measured / rewarded so it's not til you start having bad outcomes that you understand the impact of this long-term work.

2021 has felt like a year of being burned by preventable outcomes. It's a datapoint for what happens when I skip many of my systems. I can kind of sum it up as a year long error phase.

That said nothing terrible has happened, though I'd wager my past efforts in systemization had some effects in weathering the worst outcomes. But overall it's clear that in each of my life domains I've felt worse and underperformed.

As I enter the last month of the year I'm focused on a few things:

* Evaluating what I want to finish
* Wrapping up ongoing projects
* Thinking about what I want to change

In November 2021 I [released my newest Mono](#art), [moved to a new place](#moving), [bought a lot of things](#finance), [regoaled my Finances](#finance), and identified issues in my [Self](#self), [Business](#business), [Career](#career), and [Adventure](#adventure) processes. 

My name is Hamilton and these are my November 2021 release notes.

# Self

_Mission: Build a strong foundation._

Tracking metrics:

* Best self: 6/10

Over the years I've built many systems for myself to help me live my best life.

* Recurring reminders - Trello
* Recurring calendar holds - Google Calendar
* Runbooks - Notion (previously Dropbox)
* Documentation - Notion (previously Dropbox)
* Reflections - here on my blog

Life is uncertain and requires flexibility so I don't expect myself to follow these systems to a T - I'm not a robot, I expect myself to be better than that. But there's typically some 80 / 20 tradeoff where minimal effort can lead to maximal impact. These systems are thus more of a series of suggestions so I can remember the things that worked for me in the past and consider them in my current iteration.

In 2021 I've found that I've rarely been doing the minimal critical mass of suggestions. In fact, many weeks and months I've done like 0. This has led to inefficiencies and suboptimal outcomes across every part of my life.

* [Adventure](#adventure)
    * Few new experiences
    * Not intentionally connecting
    * General boredom, lack of excitement, and couch potato-ness
* [Self](#self)
    * [Mind](#mind) - not solid
    * [Finance](#finance) - Not mindful, overspending
    * [Spirit](#spirit) - languishing
* [Career](#career)
    * Burnt out
* [Projects](#projects)
    * Lots of time, very little output

I'm not yet sure what I'll do to course correct, but it's something that's top of mind as we enter December.

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6046.343045731473!2d-73.99468592326436!3d40.73625116731688!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c259989e14aa8b%3A0xcd00afc9db20caa4!2sUnion%20Square%2C%20New%20York%2C%20NY%2010003!5e0!3m2!1sen!2sus!4v1638752498443!5m2!1sen!2sus" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>

_Map of Union Square_

## Moving

[Megna](https://www.instagram.com/megna.saha/) and I moved into our new place near Union Square in Manhattan. Moving is a pain and I've been trying to cut myself some slack on missing systems to account for the added burden of this operation. The catch 22 is that often during times of higher stress / volatility those systems become more important and impactful.

On the bright side the new place is awesome and is a huge upgrade from our last apartment where we lived throughout the pandemic. Some of my favorite parts:

* Central location - Easy to get around to the neighborhoods we frequent: anywhere in Manhattan, East Williamsburg, and down through DUMBO
* Lots more space - I even have my own office! And I bought a stationary bike and named it! lmfaowrmsb!
* Lots of light - both natural and artificial. People pretend like this doesn't matter but it really does. 

I'll probably share more about the place in my year reflection. Maybe an office tour on [my YouTube](https://www.youtube.com/channel/UCPBY44jxP7gOMkUsP5rlGnw) and I've been trying to convince Megna to start a YouTube and do a whole place tour. Stay tuned.

## Mind

_Mission: Cultivate a peaceful, present, growing mind._

Tracking Metrics:

* Calm: 7/10

I decided that I want my Mind domain to mostly be about the core stability and state of my mind. It's not about learning specific domains or topics - those follow naturally from whatever impact I'm trying to have at any given time. Instead, this domain is about having a good foundation from which to build on top of.

This month I've mostly been focused on being mindful of my mind state and intentional in trying to cultivate it - mostly through meditation.

Two specific things I want to call out:

1) I changed my meditation intention to `Be present. Be patient. Be kind`

Old: `Be present. Be solid. Be a good connection`

I was finding myself getting irritated and anxious pretty often - particularly when working with others. On reflection, it wasn't really what they were doing. Mostly it was just a convenient trigger for me to vent my own internal issues.

I changed my intention to more directly speak to some of the common causes of my volatile mindsets, not being present and not being patient, as well as to one of the primary reasons I strive for a stable mind in the firstplace -> so I'm not a dickhead to those around me.

I haven't been 100% on my meditation practice or to sticking to my intentions but it's certainly a directional improvement over previous months.

_For those curious, I use Headspace to aid in my meditation._

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">A quote I&#39;ve been pondering:<br><br>&quot;Peace is happiness at rest, and happiness is peace in motion. You can convert peace to happiness any time you want.&quot; - <a href="https://twitter.com/naval?ref_src=twsrc%5Etfw">@naval</a></p>&mdash; Hamilton Greene (@SIRHAMY) <a href="https://twitter.com/SIRHAMY/status/1460675037812379649?ref_src=twsrc%5Etfw">November 16, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

2) A quote I've been pondering: 

`Peace is happiness at rest and happiness is peace in motion. You can convert peace to happiness any time you want.`

I found this quote resonated with me a lot. I find it approachably describes the core value proposition behind many of the life philosophies / practices I like to think about:

* Meditation
* Stoicism
* Minimalism

## Body

_Mission: Build a strong physical vessel for a happy, healthy, and impactful life. Be a fit old Ham._

Success Metrics:

* Body feel: 7 / 10
* Abs: 4 / 6

Tracking Metrics:

* Resting heart rate: 56 / 50
* Exercises per day (November 2021): 16 / 30 = 0.53

I've been trying to change my fitness trajectory this month. Generally it's been on the decline since mid 2020. Now that time at lower fitness levels is catching up to me in the form of body changes.

But new place = new me so I've killed off my expensive, rarely-used Equinox membership and reallocated those funds to building myself a decent workout-from-home setup. The idea is that if I have everything right here then even laziness can't be an excuse to not workout. I mean come on - I can literally workout while watching tv.

So far it's had some mild success - I've been exercising more regularly (though still not as intensely as precovid fit-Ham).

Also some headwinds as Megna really doesn't like my stationary bike in the living room and was trying to get rid of it!

## Spirit

_Mission: Build a core of meaningful, positive energy._

Tracking Metrics:

* Excitement: 0
* Languishing: 1

I added Spirit along with the other breakouts of Mind and Body to my Self domain a few months ago. At that time I was largely focused on the core meaning of life, thinking about it from the lens of legacy and core beliefs - like religion and philosophies.

Lately though I've been thinking there's a far simpler, more underserved area of my life that this domain should cover -> my general feelings towards life itself.

Despite my claims of not being a robot, I generally never take goals or direct actions with the express intent of 'happiness'. Historically I think it's because I've thought of happiness as a fleeting and non-useful measure - it's an outcome you can't directly control rather than an action that you can.

This has generally been fine as other parts of my life implicitly led to inputs that led to increased positive energy (read: happiness). However much of these inputs have been stripped away in the past year and half of the pandemic:

* Decreased adventures
* Decreased connection bonding and acquisition
* Decreased self stability
* et al

All this leading to suboptimal feelings and no existing framework to:

1) regularly observe the problem

2) do something about it

This bucket aims to cover both of those and was useful in explicitly calling out the need to course correct when the measures of other domains seemed 'fine'.

## Finance

_Mission: Grow my quality of life, stability, freedom, and ability to make change through wealth by achieving Financial Independence._

Success Metrics:

* Savings Commitment Rate: 35 / 60%
* Financial Independence: 36 / 100%

Just as most of my other systems have slipped, so too has my financial system. Largely I think that's okay.

Over time I've built up a lot of automated systems to deal with my finances so even when I step away from active management for a month or so, things still tend to go alright. Maybe not perfect, but still alright.

In November, I spent a lot of money:

* Moving costs - moving service, new furniture, up-front rent and fees, [my new stationary bike](#body), etc.
* Higher rent - [new place = new me = more expensive](#moving)
* Black Friday / Cyber Monday sales - Best (most honest?) capitalist holidays ever!
* More expensive adventures to expensive nyc things
* More consumptive than usual - food, coffee, etc

But I've also come to the conclusion that not all of this spending is bad. I shouldn't be doing it regularly and need to be very careful of the hedonic treadmill / stationary bike. But if it's within reasonable parameters for your financial situation, there's little sense in adding an unnecessary stress tax on top of it.

To this end, I've dropped my savings commitment rate success metric from 70% to 60%. I think it's a good balance between:

* Pushing myself to remain sustainable (I believe [50% is the sustainability mark](https://blog.hamy.xyz/posts/the-simple-path-to-wealth/))
* Limiting added stress due to pushing for more

# Projects

_Mission: Build a better world._

Tracking Metrics:

* Productivity: 7 / 10

My projects have been in a similar state of languishment as the rest of my domains. This generally makes sense - as above, so below - and below has been pretty spotty. Or in other words, it's hard to build anything on an unstable foundation.

Ideally I'd be shipping a project each month - I tend to alternate between art and business projects. Over time I've found this time frame provides a good balance:

* Consistently pushing out practical projects (~10 a year)
* Enough time to produce a quality artifact without undue stress
* About the time it takes for me to get bored of something
* A ~standard way to break up time

However for the past 3 months I've been aimed at creating 1 art project. It's not a particularly hard or large project. Just never finished it.


<nft-card
contractAddress="0x2953399124f0cbb46d2cbacd8a89cf0599974963"
tokenId="21067349529728974219416647855437048644621326606714483021912751854276298407937">
</nft-card>
<script src="https://unpkg.com/embeddable-nfts/dist/nft-card.min.js"></script>

## Art

_Mission: Explore, build, and share systems of the multiverse._

Success Metrics:

* Domain Explorations: 1

Tracking Metrics:

* Collabs: 0
* Exhibitions + Publications: 0

Over the past few months I realized that my Monoverse vision actually wasn't what I wanted. So I changed it. 

This month I built the closest thing to that vision I've yet made. It's a visual representation of a system - in this case mapping human activities onto a globe. This particular Mono has barely explored its domain with the currently released Instances, but I think it serves as a good proof of concept for my new vision of [The Monoverse](https://labs.hamy.xyz/projects/the-monoverse/).

Separately, I've decided to move away from the Tezos blockchain for my NFTs due to the high volatility of its NFT ecosystem. The service I used to sell NFTs on Tezos (hic et nunc) shut down recently and was quickly replaced by many new instances of the same / similar code but with different maintainers.

In general I think this is a positive thing - it shows the kind of resiliency we can expect from a more decentralized world, one that would never be possible in the existing centralized world. No loss ofdata, no loss of functionality, etc. - even if a service goes offline tomorrow.

That said, this paradigm is still very new and there are a lot of risks involved - namely around trust. If there's no central authority, who is accountable for things running as they should? I think the Tezos ecosystem has a lot of people with a lot of good intentions, but I do have to wager not everyone operates the same.

As a creator of art (and otherwise) I mostly just want to get my creations out there and circulating. To do this efficiently, I like to use whatever the most practical, robust thing available is - even if that's not the shiniest thing on the market.

Currently I think that's Opensea for NFTs. With their adoption of the Polygon blockchain, I can now get all the benefits of their platform without the huge environmental impact caused by using power-hungry blockchains like Ethereum.

Follow me on [Opensea](https://opensea.io/hamy-art) and [IG](https://www.instagram.com/hamy.art) for new releases.

## Business

_Mission: Solve real problems. Make real money._

Success Metrics:

* Monthly Recurring Revenue: $0
* Monthly Recurring Profit: $some_negative_number

Tracking Metrics:

* Businesses Shipped: 0

I've been in an art month for the past 3 months and a general project drought all half which means I've shipped 0 businesses this half.

Separately I've realized that I've been overengineering the shit out of everything I do. As an engineer, this feels like a good path because the artifact is extremely high quality theoretically and on paper. The problem is that we don't live in a world of theory, rather one of reality. Overengineering like this is a poor optimization for this reality.

This is a thought pattern I frequently find myself in, so I'm writing this in the hopes that eventually it'll stick.

![A good xkcd on overengineering](https://imgs.xkcd.com/comics/the_general_problem.png)

_A good [xkcd](https://xkcd.com/974/) on overengineering_

For businesses specifically, the reality is that most customers don't really care how you're providing them value, just that you're providing them value at or above their expectations. Moreover the hardness of the problem you're trying to solve is typically not the hard part of creating a successful business.

The hard part is finding and leveraging a problem and solution fit that actually fits the market - which itself is hard or near impossible without actually shipping anything to the target reality. Any and all efforts that don't get you closer to answering whether it's a good problem x solution x market fit is really just wasted cycles in the scope of a business. Yes, that includes overengineering.

A great thread and replies that explore this idea:

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">If you build an MVP you can skip most parts like design, backend, db etc<br><br>You mostly need a very very basic page that does something rly well and charges $ for it so you can see if the idea is viable<br><br>You don&#39;t have time to focus on other stuff, if you do you spread yourself thin <a href="https://t.co/9q5sfTZEf5">https://t.co/9q5sfTZEf5</a></p>&mdash; @levelsio (@levelsio) <a href="https://twitter.com/levelsio/status/1466674751737188354?ref_src=twsrc%5Etfw">December 3, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

I've been thinking a lot about this in my project drought and I think the path forward for me is this:

* Commit to shipping a business each month (time boxed execution)
    * The shipped business tries to solve on thing well (smaller scope)
    * The shipped business charges money for that one thing (experiment against reality)
        * The only way to know if people actually value your solution and how much i.e. if you have problem x solution x market fit

Based on this acquired data and feedback I can:

* Course correct
* Kill the project
* Broaden the scope
* Find real customers

## Shares

_Mission: Develop, share, and grow ideas for a better world._

Success Metrics:

* Unique Viewers: 8035

Tracking Metrics:

* Shares published: 1

Like my other Projects buckets, Shares has suffered in languishment.

Luckily [my YouTube](https://www.youtube.com/channel/UCPBY44jxP7gOMkUsP5rlGnw) presence has served to buffer my stats due to its unreasonable hotness (at least compared to my other sharing outlets). I'm still figuring out how to produce things people will actually consume and how to do so efficiently (video editing takes me a very long time).

That said, I'm pretty happy with where this bucket is given the amount of work I've put in. Since it's a smaller focus for me than Business or Art I probably won't make too many changes here before end of year.

## HamForGood

_Mission: Make this world a better place._

Success Metrics:

* Actions Taken: 2

I decided I want to track my contributions to the world a little more closely. I think this serves a fw purposes:

* Hold myself accountable to my mission statements
* Create a record of actions to use as references / examples to encourage others to activate in their own way

Still working on the best way to do this.

Actions:

* Voted and [shared my picks](https://blog.hamy.xyz/posts/how-im-voting-nyc-november-2021-general-elections/) for environment-forward candidates in NYC elections
* [7 ways to fight Climate Change as a Software Engineer](https://labs.hamy.xyz/posts/fight-climate-change-software-engineer/)

# Career

_Mission: Create value, change the world, build a legacy._

I've been feeling a bit burnt out at work. It took me awhile to identify this.

Some symptoms:

* Lack of patience and irritability
* Stress
* Tiredness, languishing in my other aspects of life
* Low [Spirit](#spirit)

Some potential causes:

* Doing work I don't enjoy (lots of meetings, lots of ad hoc support)
* Lots of context switching (too many concurrent workstreams)
* Too much work volume for available bandwidth
* Low Spirit (vicious cycle)

I'm focusing my year planning with a few values in mind in order to combat this:

* Under promise (protect bandwidth)
* Find overlaps between work needed to be done and that I enjoy
* Focus on a few big problems (lower context switching)
* More emphasis on Self and Spirit

# Adventure

_Mission: Experience life._

Haven't been doing much of this. Should do more.

# Ham

I'm in need of some recovery. Not passively, but actively. In the things I do, the way I do them, and the value I place in them.

To a restorative December.

-HAMY.OUT