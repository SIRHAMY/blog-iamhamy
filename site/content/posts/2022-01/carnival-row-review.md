---
title: "Carnival Row: A story with a cause"
date: 2022-01-11T15:54:29Z
subtitle: ""
tags: [
    'contumption'
]
comments: true
draft: false
---

[Carnival Row](https://amzn.to/3HTS7Wb) presents a fully-realized mythological world that serves as a backdrop for stories of intolerance that parallel our own. By making the situations familiar enough to recognize but different enough to not feel personally connected, this class of works allows us to reason about taboo situations with objectivity while still allowing the personal connection required to empathize with those different from ourselves.

In doing so, this story - and those like it - enable a degree of boundary-crossing and tolerance ground work that is hard to achieve in areas of high taboo.

Carnival Row tackles numerous issues in its journey. Some standouts:

* Racism
    * Different: Based on fae race rather than skin color
    * Same: The way that areas and classes are only for the 'dominant' race, the way that non-dominants are treated
    * Extra: One storyline juxtaposes the story of forbidden romance between non-dominant male and dominant, noble female with non-dominant being both black (same) and fae (different) thus forcing us to see and face the parallels of these worlds
* Homophobism
    * Same: This world maintains much of the homophobic underpinnings as we have seen in our own. Of course the larger distinctions of races and how different cultures have different views works to further spotlight this intolerance
* Classism
    * Same / Different: Central to the plot is the financial and political elite abusing the lower class for their own benefit. Different actors, same story.

I love stories like this - they are at once beautiful and thought-provoking. A story with a cause.

In today's divided discourse, mass of entertainment options, and equally massive load of misinformation works like this feel like they're onto something. They tap into the attention stream and leverage their entertainment value to keep people coming, imparting just a bit of useful information in the time they're allotted.

Such a method - while not efficient in total information imparted - is still incredibly effective at breaking down barriers and centering subjects, perspectives, and settings that would otherwise be shunned. Even if it's just to ask a simple question - is this right? Does it have to be this way?

I think that there is a large opportunity in this space for education - for subjects and perspectives that have historically been labeled taboo, hard to teach, or boring. By building an immersive world and allowing people to 'live' it, we force them at once to think critically about what they would do, empathize with how the situation feels, and fully understand and face the target scenarios.

I'd be curious to see more works like this. Perhaps this is the way to impart information the old way - through games and stories we experience rather than observe.

* Racism
* Finance
* Pandemics - [The Board Game?](https://amzn.to/3qfWW6f)

_Another similar work I liked was [Don't Look Up](https://www.rottentomatoes.com/m/dont_look_up_2021) and its reflection on misinformation and our chronic inability to work together to prevent even obvious catastrophe. A satire on issues like the ongoing pandemic and climate change._