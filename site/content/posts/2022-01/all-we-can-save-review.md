---
title: "All We Can Save - We need you to solve climate change"
date: 2022-01-25T18:56:53Z
subtitle: ""
tags: [
    'contumption',
    'hamforgood',
    'climatechange'
]
comments: true
draft: false
---

[All We Can Save](https://amzn.to/3AKYdGh) approaches the climate change discussion from a variety of perspectives through diverse voices. It centers the stories and solutions that are oft overlooked by mainstream coverage yet are crucial in fully understanding the problem at hand and ideal ways forward. 

Ultimately, it's a reminder that we are facing an extinction event - the largest scale threat to humanity in millenia. The path forward is unclear, complex, and hard. Yet every person has something to offer, every bit helps, and every bit is required if we want to save all we can.

# Takeaways

* **Climate change is a hard, complex problem.** There are lots of overlapping causes, factors, and impacts. Further, this means that there is no one silver bullet. We have a lot of issues to unravel and remake.
* **There is no one solution, instead multitudes.** There is no silver bullet to solve climate change but there are a lot of different ways we can make an impact. It will take many different solutions working together to produce the change required to avoid disaster.
* **Everyone has a role to play.** There is little time (2030-2050 turning point) to save the world. Yet there is so much to do. You are needed to solve the greatest threat to humanity in our lifetime. Even small actions scaled to many people can make a difference. Give what you can. We're running out of time.

# Start today

The empowering part of this story is that there are so many efforts and volunteers already working to avert disaster. It's easier than ever to plug into these machines and start making an impact on the planet - which is good cause we need the help!

* **Donate $.** - Perhaps the easiest way to get started is to start donating money to orgs fighting climate change. This helps support them in their causes and requires minimal changes in your own lifestyle. I give regularly to [350.org](https://350.org/) via [HamForGood](https://labs.hamy.xyz/hamforgood/) but there are many [well-rated environmental organizations](https://www.charitynavigator.org/index.cfm?bay=content.view&cpid=8636) to give to. Find one you vibe with and start today.
* **Give time and effort** - Join a local organization, make a club at work, advocate in your company and community. I'm part of New York's [local 350](https://350nyc.org/), [Sunrise Movement](https://www.sunrise-nyc.org/aboutus), and [Climate Reality Project](https://climaterealitynyc.org/connect-1) orgs and participate in some sustainability-focused clubs at [work](https://www.linkedin.com/in/hamiltongreene/). There are a lot of ways to get involved, so reach out to a local org (or start your own). Further reading: [7 Ways to fight climate change as a Software Engineer](https://labs.hamy.xyz/posts/fight-climate-change-software-engineer/)
* **Make your voice heard.** Speak up in your circles, community, and through your actions - voting, eating green, sharing your thoughts. It's going to take a lot of us taking little actions, so get started today.

We need you to help fight climate change. Help us save all we can.