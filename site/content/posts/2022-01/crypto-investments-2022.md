---
title: "The cryptocurrencies I'm investing in in 2022"
date: 2022-01-19T15:59:11Z
subtitle: ""
tags: [
    'finance',
    'crypto'
]
comments: true
draft: true
---

# Overview

Crypto is a valuable asset in today's economy. In the past 2 years leading cryptocurrencies like Bitcoin have risen 1200% (and later fallen 50%). The market has taken notice and big financial players from hedge funds, to startups, to traditional banks have started to diversify their portfolio into crypto. Personal investors are following suit.

Crypto is a risky asset:

* New technology
* Lots of players, many not good
* Hard to know what to look for to gauge success

But I think the opportunity is worth exploring. In this post I'll share the cryptocurrencies I'm investing in in 2022 and my investment principles that underlie these decisions.

# Investment Strategy

_Disclaimer: I am not a financial expert and I'm not giving advice. I'm just a random human on the internet ranting about my personal opinions._

My general investment strategy is to try and [grow wealth sustainably](https://blog.hamy.xyz/posts/the-simple-path-to-wealth/) (even if a bit slowly) by focusing on core fundamentals and risk-averse assets. For cryptocurrencies, this is especially true due to its volatile history.

My principles:

* **Only invest money I'm okay with losing** - recent history has shown that many bets in the crypto space end up losing a lot of their value. This ensures you are managing your risk in this risky asset class.
* **Invest regularly and systematically** - I want this to be passive and to not be swayed by market forces. I am big on long-term investments. I don't think I'm a very good individual investor and I certainly can't beat out the big hedge funds / professional investors who spend all their time focused on learning and beating the market. But I can still make smart bets that work for my timeline.
* **Invest in chains and currencies I believe in** - I'm here for the long-term value, not to pump and dump. 
* **Spread allocation among a small assortment** - I don't think I'm good at guessing who is going to win. So I try to increase my odds slightly by investing in a few.
* **Invest in chains with interest rewards** - Interest rewards me for time in investment and makes the long-term view more attractive.

# 1. Ethereum - ETH

Ethereum has grown huge over the past few years. It's also a leading chain when it comes to innovation and real-world usecases. 

While I don't believe Ethereum is the most cutting-edge chain. I do think it has enough interest, backing, and activity to give it legs into the future. This is why Ethereum is currently my main bet.

* Lots of interest and backing
* Lots of activity - building apps and new usecases (NFTs, games)
* Eth 2.0 will bring the chain up to modern standards and give it much needed sustainability improvements (hamytodo - link ethereum vs polygon)
* Eth 2.0 introduces staking at ~4.5%

# 2. Algorand - ALGO

Algorand is a new creation that is still iffy if it'll find its footing or not. I'm optimistic about institutional finance on blockchain and while I am not sure if ALGO is it, I think it's a fair bet with a lot of upside.

Currently ALGO is trading at about $1 per coin and returns about 4% which makes this a risky bet with upside to offset it.

* I'm optimistic about institutional finance on blockchain - even if this isn't it
* Low price means lots of upside if it works out
* 4% interest rewards

# 3. ATOM 

Atom is another risky bet I'm taking because I believe in the space even if I'm not sure this is the vehicle that will win. Atom builds infra that enables standardized cross-chain interaction. I believe that all blockchains will be interoperable - an internet of blockchains if you will.

This is the first one I've seen with rewards so I'm making a bet on it to see where it goes. I think prices are a bit higher than I'd like but I also want to diversiy a little so I'm taking the hit.

* I believe all blockchains will be interoperable - even if this isn't the technology that gets it there
* Price is a bit high but provides diversification
* 4% rewards

# Honorable Mention: Tezos - XTZ

Tezos if by far my favorite blockchain. It is cutting edge, has all the modern bells and whistles, and has a unique structure that enables constant evolution. This is my long-term, big bet.

Now you may be wondering why it's only an honorable mention. Well I mostly invest using Coinbase due to its ease and reliability and currently my region doesn't allow trading of Tezos (mostly cause it's too small for governing bodies to have gotten around to approval). As such I'm not actively trading in it though I am staking my existing trove.

Whenever it gets added back to Coinbase I'll start my monthly investments again.

hamytodo - link coinbase referral code

* Cutting edge technology and revolutionary governance structure
* Unfortunately has flown under the radar a bit which means it lags in official adoption / approvals by government / industry
* 5% rewards

# Conclusion

Cryptocurrencies seem here to stay and the financial value fluctuations signal to me that there's a lot of potential upside - so long as you manage your risk.

Further reading:

* If you want to get started investing in crypto I recommend Coinbase for its ease of use and reliability. Use my Coinbase Referral Link to get $10 free in crypto when you purchase $100 or more. hamytodo - include coinbase referral link
* I lay out my philosophy and strategy for building wealth in my blogpost The Simple Path to Wealth. hamytodo - link post