---
title: "Show Your Work: Your work is the work"
date: 2022-02-13T22:05:46Z
subtitle: ""
tags: [
    'contumption'
]
comments: true
draft: false
---

[Show Your Work](https://amzn.to/34Zc5AU) describes a method of meta creation and its value. It spotlights the fact that the work you do to create your end product is itself an end product worthy of sharing.

We see this more and more today with popular figures showing off their creative process - the "How it Works" for individuals.

This lens of creation is useful to you as a creator for a few reasons:

* Generates closeness with your audience / makes you feel more relatable
* Allows a channel of constant feedback (though you need to be picky about which feedback you allow to get to you)
* Provides a way to grow an audience before your creation is complete

More importantly though, I think this process can be used to better the creation itself. It creates a regular opportunity for feedback and reflection.

* To share something, you must develop a perspective towards it - a lot of times this means facing yourself and your creations and asking yourself "Why am I doing this?" and "Why am I doing it this way?". This regular reflection can help keep you honest and on track.
* It teaches you how to share your work - through constant sharing you can build a muscle for framing your work. This can enhance both the direction of the final product and how well it's received. A poor introduction to something could kill its chances to be big even if the work itself is great.

My creation process includes a Share step for these same reasons. I've found it useful as an iteration tool and to increse the impact of my creations.

* Observe
* Create
* Reflect
* Share

This idea and this process is the core idea behind my many [online presences](https://hamy.xyz/).