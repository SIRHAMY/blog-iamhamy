---
title: "System: My Photo Processing System"
date: 2021-04-01T17:58:17Z
subtitle: ""
tags: [
    'system',
    'photography'
]
comments: true
---

I've recently gotten back into photography. I've taken multiple approaches to the practice over the last several years but each has ended in disinterest. This time I'm trying a more value-based approach - focusing on the things I want to get out of the practice and methods that match the effort I'm willing to put in.

For me, I'm trying to capture the interesting mundanities of life - to me that's reality. I don't like editing. It's boring, so for process I really want to focus on the pictures I take and then limit the amount of effort between taking them to publishing them. That's it.

# Process

## Shoot

Obviously the first step to processing photos is to take photos. I chose the [Fujifilm X-T4](https://amzn.to/3dzLYRB) for its good reviews and versatility. I shoot with a small pancake lens for ease of carrying around with me. I also shoot with Fujifilm's film simulation mode which helps me get the style I want with no post processing.

I won't go into all of my camera settings here but if you're interested let me know and I'll consider sharing my full configuration

* [Fujifilm X-T4](https://amzn.to/3dzLYRB)
* Pancake Lens (easy to carry, versatile lens width)
* Fujifilm simulation

## Select

I don't edit my photos. So my only post processing is really to select the photos I want to share then share them.

I try to keep only the top 1% of my photos to keep quality high and to force myself to constantly think about what makes a photo better / worse.

I use [Darktable](https://www.darktable.org/) for culling but you could use many different softwares for this purpose.

* Filter to just photos that are _not_ rejected
* First pass to remove bad photos in Darktable Culling mode x4
* Iterate to remove potential candidates in Darktable Culling mode x1 / 2 until down to top 1% of photos / feel good about the selection
* Filter to all rejected photos and move them to trash for storage efficiency (and cause I'll likely never go back and look at my bad photos)

## Export

Once I've selected the photos I want I export them for publish. I generally have two different kinds of photos I select, with some overlap between them:

* Photos for my photography Instagram - [@hamy.see](https://www.instagram.com/hamy.see/)
* Photos of friends

I export these to different folders for ease of publishing and finding in the future.

* darktable_exported - this is where I put my personal photos. I continue exporting to jpg with decreasing quality til all photos are < 10MB to fit Buffer's image requirements
* darktable_exported_friends - this is where I put photos of my friends. Quality doesn't really matter here since I usually upload to a shared Google album which does its own compression anyway

## Share

The last thing to do is share! I take a lot of photos and have many IGs so I use Buffer to handle a lot of my sharing logistics.

* Foreach image in darktable_exported, upload to my Buffer Queue with a template description of `PLACE - YEAR`
* Foreach image in darktable_exported_firends, upload to a shared Google photos album for the respective trip and share with the peeps

# Fin

That's my system! It usually takes me < 30 mins to process the photos from a given outing and that's acceptable for me. Buffer takes the logistical overhead out of publishing so all I have to do is wait for a notification and post.

If you wanna check some of my images, give me a follow [@hamy.see](https://www.instagram.com/hamy.see/).

What's your process? See room for improvement? Let me know!

F edits.

-HAMY.OUT