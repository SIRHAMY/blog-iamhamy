---
title: "Project Pillars"
date: 2019-07-22T18:32:18-04:00
subtitle: ""
tags: []
comments: true
---

# goal

Over time I've found myself with the recurring issue of what to do next. This is a problem that is also faced by most teams / companies and one of the more successful ways I've seen them take this challenge on is by comparing the costs / benefits of each effort and using that as a metric in the decision process.

Thus the point of these pillars is to initiate a process that will help me compare my numerous projects so I know what the best prospects are at any one time.

# pillars

* Plug into a Google Sheets calculator
* Calculate
* copy and paste into card

Expected value / hour

* expected value - expected value / month
    * reach * impact
        * reach: # users / month doing the thing / making the money
        * impact: $ / user
* confidence - my confidence that these numbers are right, whether effort or expected value
    * range of (0,1)
* effort - in hours, but choices are limited
    * 1 hour - v small things
    * 10 hours - 1 week
    * 20 hours - 2 weeks
    * 40 hours - 1 month
    * 120 hours - 3 months
    * 240 hours - 6 months
* learning - this is just whether I learn something new and useful or not. If it is, we'll boost by 25%

```
opportunity value / hour = (expected value * confidence * learning) / effort
```