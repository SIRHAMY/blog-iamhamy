---
title: "Post Writing"
date: 2019-02-02T09:23:48-05:00
subtitle: ""
tags: []
comments: true
---

# Goal

The goal of this page is to keep some of the best practices / behaviors I've found/developed over the years for writing/maintaining good posts.

# Post components

## Call to action

Every post should have some sort of call to action. This is like a "okay, you've read this, here are the recommendations for what to do next". This can be in many forms like to donate to a charity for a relevant cause or simply to subscribe to your email list.

If a post doesn't have a specific call-to-action, it should default to using the generic `subscribe` one below;

### Call to action: subscribe

FIN

Want to get updates on my latest Hamventures? [Subscribe to my email list or follow along via RSS!](https://hamy.xyz/subscribe)

