---
title: "SWE Interview Prep"
date: 2019-01-29T22:45:48-05:00
subtitle: ""
tags: []
comments: true
---

Interviewing is hard and uses a lot of muscles that don't get much flexing in every-day work. As such, I felt the need to create a plan to work out these muscles to get back in fighting shape.

# Create a good resume

If you can't sell yourself, you won't get a chance to prove yourself.

So write a good resume. Doesn't need to be fancy, but does need to get the job done.

# Practice for interviews

Sites I like

* LeetCode
* TopCoder
* exercism
* InterviewCake

Languages I want to learn / use

* C#
* Rust

# Goal

Do > 1 non-trivial (medium/hard+) question a day, alternating sites each time, and languages each day.

# What I look for in a company

* Opportunities for professional learning and growth
    * I want to learn how to make things scale
    * Is the team smart?
    * Are we working on cool and interesting things?
    * Do we have the power to make an impact?
* Pragmatic, upbeat culture
    * I want to like where I work.
    * I want to like the people I work with.
* Compensation / returns
    * I want to make bank.
    * Scratch my back and I'll scratch yours.
    * I want to have balance

# timeline

## Prep: 2019.02

Tech: C# | Rust

* Build projects
* BigO
* Implement data structures from scratch
* Practice interview questions
* Create resume
    * send out for review
* Create interview prep grid
* create list of cos

Habit
* 1 medium/hard Q each weekday

## gear up: 2019.03

* make list of areas I'm struggling in, work to solidify those
* Send out resume
* Reach out to cos
* mock interviews
* continue practicing
* take days off

Habit
* 1 medium/hard Q each day

## Interview: 2019.04

* complete projects at work
* take days off

## Switch: 2019.05

* put in notice