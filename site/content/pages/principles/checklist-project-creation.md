---
title: "Checklist: Project Creation"
date: 2019-11-24T11:34:37-05:00
subtitle: ""
tags: []
comments: true
draft: true
---

* Hypothesize - every project starts with an idea and a reason it may be good
* opportunity size - how good is it / might it be? What's the cost?
* experimentation setup - how much are you willing to invest? How will you know if it's "working"? What will make you continue and what will make you quit?