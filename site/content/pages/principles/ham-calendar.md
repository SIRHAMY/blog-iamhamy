---
title: "Ham Calendar"
date: 2019-12-01T13:56:05-05:00
subtitle: ""
tags: []
comments: true
---

Goal: This calendar is to serve as a framework for general timings of my life processes. The hope is that such a framework will allow me to execute with less unease and iterate with a higher probability of changes making a difference through process adherence.

# yearly

* contains 2 halves
    * halves start on Jan 1 and July 1

# halfly

* each half has a vision for each focus -> where do I hope to be at the end of these 6 months
* contains 2 quarters
* the last month of the half is dedicated towards reflection on that half (or year if last half of year), wrapping up endeavors, and preparing for the next half

# quarterly

* each quarter has a vision for each focus -> where do I hope to be at the end of these 3 months
    * these visions are backed up by a few key results to help me understand if I've achieved this or not and that I can track against in my weekly plannings
* contains 3 months
* the last 2 weeks of the last month (unless the 2nd quarter of a half) are dedicated to reflection on the quarter, wrapping up endeavors, and preparing for the next quarter

# monthly

* each month has a big okr for each focus
    * where to I hope to be at the end of the month
    * how will I know I'm there (what key metrics can I use to guide me)
    * what do I plan to do to accomplish this
    * the hope is to accomplish one big thing. For the things that aren't really "big", but more habitual stuff, then the hope is that those become routine and can be done as we move forward by adding it my habits and removing them as "invisible work"
    * projects
        * should pick out and spec 3 projects that I might do
            * pick one to do and do well
* the last week of the month is focused on wrapping up what I did, writing them up in release notes, and preparing for the next month

# weekly

* each week has a general list of things I plan to do and a parallel list for things that I actually did as a lightweight way to keep track of what's going on / falling through
* weeks start on Monday - this gives me a chance on Sunday to finish up the previous week's work and plan for the next week.