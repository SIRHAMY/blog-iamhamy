---
title: "Project Values"
date: 2019-12-02T21:26:17-05:00
subtitle: ""
tags: []
comments: true
draft: true
---

This doc hopes to answer what I value in the projects I create.

1. valuable / high impact

Whatever it is I'm building, I want it to be worth my time => impact / effort

2. I want there to be little ongoing maintenance

The best projects in my eyes would allow me to run them without needing too much active thought. I'd rather that thought going into building new things and solving new problems, both things I really enjoy

3. It needs to be interesting

I ahve a limited life and want to make the most of it. Buidling shit I don't like doesn't really seem like that.

4. I'd like it to make money

I don't really plan on not having a job ever, but I do think it'd be nice to have my projects start making money for me. This could allow me more time to create more things that I want to as well as now there's monetary backing. I want to make sure that this isn't the thing that steers the ship, but I do think it's important and almost more "real" to do so.