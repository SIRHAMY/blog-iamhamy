---
title: "Post Release Schedule"
date: 2019-01-02T08:14:49-05:00
subtitle: ""
tags: []
comments: true
---

# Goal

The goal of this schedule is to maximize readership/robustness through a staggered release/publish of posts across disparate networks.

# Scheduling

Labs
* Weekly on Mondays
* Overflow to Wednesdays if two months ahead in posts

Blog
* Weekly on Wednesdays
* Hamventures weekly on Sundays
* Release notes on first of month, replaces normal posts if on same day
    * email to list
    * share to stories
        * insta
        * fb
* Overflow to Mondays if two months ahead in posts

# Sharing

* Day 0
    * Post
    * Insta story
    * Snap story
    * message to family/close friends
    * send out email blast to list
* Day 1
    * Insta post
        * share post to other profile stories
    * Medium post of intro with link to original post
    * Facebook post
* Day 2
    * Linkedin
    * Twitter
    * Internal work blog (if applicable)