---
title: "My ideals"
date: 2019-01-02T22:17:38-05:00
subtitle: ""
tags: []
comments: true
---

# Self

* Intelligence
* Grit
* Curosity

# Motivation

* Remember what you love: doing, learning, exploring

# Tenets / mantras

* action breeds action, inaction breeds inaction
* Are you helping or are you hurting? Are you happy?
* first things first
* Are you being world class?
* slow is smooth, smooth is fast
* You are valid, You are important. Do You.
* As below, so above and vice versa. If you are outwardly mean/unhappy, it is because you are inwardly unhappy. Fix it and you will radiate outwards.
* The only one stopping you is you