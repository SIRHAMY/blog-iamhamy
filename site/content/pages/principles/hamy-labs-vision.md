---
title: "HAMY Labs Vision"
date: 2019-11-07T20:43:56-05:00
subtitle: ""
tags: []
comments: true
---

# values

* Accuracy - Are we solving the right problem, are we solving it correctly
* Robustness - Does it scale, will it weather the test of time
* Pragmatism - Does it make sense, can we do it

# non-goals

* Impact - Does it make a difference
* Helping, not hurting - Is this good for the world?

# mission

* Explore the possibilities of creation and to make the world a better, more open, and intelligent place