---
title: "Okrs"
date: 2019-07-24T21:42:48-04:00
subtitle: ""
tags: []
comments: true
---

**Every Half:** A theme and sub-themes within each focus to focus on.

**Every quarter:** Focus-divided visions (that are tied to the Half themes). Each focus will have 1-3 visions. Each vision will have 2-3 ways to measure said goal. Can list a few ways to tackle goal if want to. A vision is basically, at the end of this quarter I will be at x.

In the form of `I will STATE`

Example: `I will connect with people through adventure`

**Every month:** Focus-divided goals (that are tied to the quarter goals). Each focus will have 1-3 goals. Each goal will ahve 2-3 ways to measure goal. Also 2-3 ways to tackle said goal.

In the form of `OUTCOME`

Example: `Show initiative and competency at senior engineer level`

* FOCUS
    * PROBLEMSTATEMENTorVISIONSTATEMENT [PROBLEMGUID](RELEVANTHIGHERLEVELPROBLEMGUID, ...)
        * How will it be measured?
            * KEYRESULT: TYPE
            * ...
        * How will it be accomplished?
            * PLAN
            * ...

GUIDs of form {FOCUS}{TIMEID}{TYPE}{NUMBER}

Examples:

WORK2019Q3GOAL2