---
title: "Packing List"
date: 2019-08-08T18:23:32-04:00
subtitle: ""
tags: [
    "checklist",
    "travel"
]
comments: true
---

Goal: Create a system of packing that is fast and accurate

# packing

* basics
    * socks - daysOfTrip + 2 pairs
    * boxers - daysOfTrip + 2
    * shirts - daysOfTrip (can be mix of short and long depending on climate)
    * pants - 2
    * walking shoes
    * rain jacket
    * water bottle
* going out
    * ? going out shoes (depends on how much going out we're doing, how much space we have, how bad reg shoes are for this)
    * going out shirt - 1-2 (depends on how much going out we're doing)
* working out
    * running socks
    * running shoes
    * running shirt
    * running pants
    * running belt
    * workout strap
    * ?swimsuit
* toiletries
    * contacts - 2x days + 2 + 2 * days / 5
    * toothbrush
    * toothpaste
    * floss
    * body soap
    * shampoo
    * vitamins
* electronics
    * phone
    * phone charger
    * watch
    * laptop
    * laptop charger
    * headphones
    * headphone charger
* misc
    * notebook
    * cash - ~$10 / day
    * sunglasses
    * reg glasses
    * keys
    * reading book
    * stickers

## checklists

* is everything charged
    * watch
    * headphones
    * laptop
    * travel battery
* Are you doing anything that requires something specific? Do you have that?
* plan to get to airport in time
* double-check lodging
* double-check travel
* remove unnecessary things from bag

## international

* passport
* ?power converter
