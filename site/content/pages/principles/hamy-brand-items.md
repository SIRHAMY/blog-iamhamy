---
title: "Hamy Brand Items"
date: 2019-09-14T13:01:49-04:00
subtitle: ""
tags: []
comments: true
draft: true
---

# products

## accessories

* start: $40
* max: $50

## apparel

### shirts

* $32 - trying to achieve $10 profit off each sale, even after tax
* brand on outside if possible
* AA 2001 shirt
* title: ID/NAME t-shirt
* width - 10 inches

tags: `apparel`, `SERIESNAME`

description: just the product info

## prints

Pricing, but if one is def bigger than another then jump

* < 12x12 - $40
* < 20x20 - $60
* < 30x30 - $80
* title: ID/NAME Print (white/black background) if applicable

tags: `print`, `SERIESNAME`

Keep dpi > 200

# projects

## blinder

Blinder explores the relationship between eyes and the human perception of art. It uses machine learning and creative frameworks to understand and manipulate images. Read more about the series on [HAMY.ART](https://art.hamy.xyz/projects/blinder/)

tags: `blinder, generative-art, creative-codint`

## webz

