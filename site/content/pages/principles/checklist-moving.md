---
title: "Checklist: Moving"
date: 2019-02-18T11:17:54-05:00
---

**Goal:** Moving to a new place is hard enough as it is. This page hopes to compile best practices for moving so that you'll forget as few steps as possible.

# Change addresses

Your physical address is still tied to a ton of super important things, for better or worse. As such, be sure to change your addresses to prevent confusion and delay in the future.

Generic 

* Credit cards
* USPS mail forwarding
* Banks

Specific

* Amazon - default address