---
title: "Earth Tax"
date: 2020-07-21T13:47:34-04:00
subtitle: ""
tags: [
    'hamforgood',
    'earth-tax',
    'carbon-tax'
]
comments: true
---

I've been thinking more and more about how large, global changes and small, local changes interact with each other. My current understanding and perspective is that we must push for changes at each level in order to make lasting change. 

The Earth Tax is my way of becoming more mindful of my impact on the Earth and intentional in reducing and fixing that impact. By taxing myself, I am able to curb my behavior by providing a consequence to my bad earth decisions and can use the outputs of these consequences to work towards change at the local and global scopes by giving to organizations that are working towards a more sustainable future. Together, this paradigm achieves local, global, proactive, and reactive effects.

I wrote at length about [my thought processes behind the tax](https://blog.hamy.xyz/posts/consider-a-personal-carbon-tax/). Here I will catalog the full, up-to-date version.

# earth taxes

## food

We charge food based on their relative resource consumption and carbon output, focusing on those that have outsized effects on the system.

For each food item bought with the ingredient in it:

* beef - $1
* lamb - $2

## transportation

We charge transportation based on relative resource consumption, carbon output, and the availability of better alternatives.

* personal car ride (non carpool) - $1
* shipping - $1
* flight - $25

## goods

Besides thinking just about the service / good being consumed, we must also consider those things that allow the good to be consumed. Anything that's purchased / used that can't be / isn't recycled and would thus constitute a single-use item must be taxed to help curb its usage and support the finding of effective alternatives.

* non-recycled good - $1