---
title: "Checklist: Finding Adventures"
date: 2018-08-02T23:31:31-04:00
subtitle: ""
tags: []
---

Goal: To create a reproducible process for finding cool things to do

# checklist

`hugo new hamventures/hamventures-nyc-YEAR-MONTH-DAY.md`

* [Facebook Events](https://www.facebook.com/events/)
    * [your upcoming](https://www.facebook.com/events/calendar/)
* [Brightest Young Things](https://brightestyoungthings.com/articles/category/nyc) - they curate weekly articles about "best bets" in select cities
* [Meetup](https://www.meetup.com/)
* [Eventil](https://eventil.com/events/in/US/new-york)
* [AWS loft events](https://aws.amazon.com/start-ups/loft/ny-loft/) 
* [House of Yes](https://houseofyes.org/)
* [Public Arts](https://www.publichotels.com/culture/calendar)
* [Samsung 837 Events](https://www.samsung.com/us/837/events)
* [SFPC NYC](https://www.instagram.com/sfpc_nyc/) - They usually post things on their insta
* [artnet news](https://news.artnet.com/art-world/events) - posts weekly new york art stuff to do
* [Devpost hackathons](https://devpost.com/hackathons)
* [Garys Guide](https://www.garysguide.com/events?region=newyork)
* eventbrite!!!
* Adventure list Trello
* h&mitual trello
* Look at Songkick for upcoming concerts by artists I like

* Garys Guide (email) 
* The Joy List
* Triangle arts association - https://www.triangleartsnyc.org/programs 

## sharing / distribution

* email
* social
    * [fb page](https://www.facebook.com/hamventuresnyc)
        * Link direct to blog post, no intermediate bs
        * But can use same structure as the email
* direct to peeps

## timeline

* 30 mins - finding
* 15 mins - writing
* 15 mins - distribution and planning

# Resources

[] Facebook - search events in the desired city with the desired time frame

[] Google - search for "events in X week of Y"

[] MeetUp - search through groups and events occurring in target place/

[] Personal calendar / bucket list

# Category-specific

## Tech

[] Eventil - aggregates a bunch of tech events all in one

# Place-specific

## Atlanta

[] [Naomi's weekly date night digest](https://medium.com/@naomi_ergun) - Naomi puts together a killer array of events to take a date (even if that date's yourself)

## NYC

[] [Brightest Young Things](https://brightestyoungthings.com/articles/category/nyc) - they curate weekly articles about "best bets" in select cities

[] The Joy List

[] AWS loft events - https://aws.amazon.com/start-ups/loft/ny-loft/ 

[] House of Yes - https://houseofyes.org/

# Sharing

All things in chronological order

Types?
* date
* group
* exploration
* fitness

## Template

```
**TypeOfthing:** stuff, stuff [LINK]
```

tags
```
[
    "hamventures",
    "nyc",
    "things-to-do"
]
```

Sections

```
Hamventures NYC: Week of x

# Monday (DATE)

nothingtoseehere

# Tuesday (DATE)

nothingtoseehere

# Wednesday (DATE)

nothingtoseehere

# Thursday (DATE)

nothingtoseehere

# Friday (DATE)

nothingtoseehere

# Saturday (DATE)

nothingtoseehere

# Sunday (DATE)

nothingtoseehere

# Notable Ongoings

nothingtoseehere

```