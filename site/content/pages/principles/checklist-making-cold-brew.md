---
title: "Checklist: How to make dank cold brew"
date: 2019-02-18T23:25:16-05:00
subtitle: ""
tags: [ "coldbrew" ]
comments: true
---

Size: 3/4 cups of coarsely-ground coffee beans / 32 oz water

Time: ~24 hours (18 - 30 seems to be good)