---
title: "Sharing Content"
date: 2019-10-05T10:23:32-04:00
subtitle: ""
tags: []
comments: true
draft: false
---

* day0 - post lands, send to core group
    * email
    * close friends and fam
* day1 - distribute to the people
    * Facebook post
    * Instagram post
    * Facebook story
    * Instagram story + share to other instas
* day2 - distribute to side channels
    * Work blog / messenger thing
    * Linkedin post
    * Twitter
    * Medium???
    * dev.to???


For release notes

* Monthly
    * intsagram
    * email
    * twitter
* quarterly
    * above
    * facebook
    * linkedin