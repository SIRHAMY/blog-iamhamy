---
title: "Design System"
date: 2019-11-07T20:31:50-05:00
subtitle: ""
tags: []
comments: true
draft: true
---

# colors

## elements

* Accents - red - be403b
* BG - Black
* Primary - White

# fonts

## values

* mono-space - much more like code and easier to reason about
* free for everything - I like free shit, let's show the world how to use it

## candidates

* Headings: Roboto mono - https://fonts.google.com/specimen/Roboto+Mono
* Text: Ubuntu mono - https://fonts.google.com/specimen/Ubuntu+Mono