---
title: "HAMEATS"
date: 2019-05-29T22:05:42-04:00
subtitle: ""
tags: [
    "hameats"
]
comments: true
---

Over the years, I've gotten more than a few odd looks over the things I've eaten / in the manner I've eaten them. HAMEATS is an effort to shed some light on these odd mannerisms and prove once and for all that these make more (or possibly less) sense than they seem.

# what is a HAMEATS?

HAMEATS are a unit of creation that are a) consumable, b) cheap, and c) easy to create. I have high hopes that perhaps these creations are slightly on the healthy side as well but I haven't run the numbers yet and therefore can't promise anything.

* **consumable** - We are creating food so we hope that it is edible
* **cheap** - I can't make anything as good as like a Michelin star restaurant so I might as well make it cheap
* **easy to create** - Cooking is not my calling so I want all this stuff to be easy and quick to make so I can eat and be done with it.

# where can I read about the latest HAMEATS?

All HAMEATS can be found [here](/tags/hameats).