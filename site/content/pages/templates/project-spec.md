---
title: "Project Spec"
date: 2019-12-02T21:12:09-05:00
subtitle: ""
tags: []
comments: true
draft: true
---

This acts as a template for creating project specs for new projects.

# the problem

_What problem are we solving?_

# how it works

_What is it? How does it solve x?_

# what is the opportunity size?

**Reach:** _How many people have this problem?_
**Impact:** _How much does this help?_
**Capability:** _How prepared am I to take on this task?_
**Effort:** _How long will this take?_

# competitors

_What competitors exist? Can we do better? What's the competitive advantage?_