---
title: "HamForGood - Personal"
date: 2020-11-22T21:26:56Z
subtitle: ""
tags: [
    'hamforgood'
]
comments: true
---

# Overview

HamForGood is a system I've created to make the world a better place. In my personal life it consists of monthly donations to organizations supporting causes I care about. In [HAMY.LABS](https://labs.hamy.xyz/hamforgood/) it's an initiative where I give 20% of my profits to support causes that help better the world.

# HamForGood - Personal

Here are the causes and organizations I'm personally supporting each month.

* Art - MoMA - $10
* Climate - 350.org - $10 + $x based on [Earth Tax](https://blog.hamy.xyz/pages/principles/earth-tax/)
* Accessible Technology - Canonical / Ubuntu - $10
* Freedom of Information - New York Times - $15
* Social Justice - ACLU - $10
* Social Justice - ADL - $10
* Social Investing Fund - $125

The Social Investing Fund is a fund I'm creating with an explicit purpose to better the world over a long period of time. The idea is that it'll work very much like a grant - siphoning off money each year to give to worthy causes. Here the strategy is to take 1% from the fund every year and give it to a cause. This 1% should be low enough for the fund to continue to grow into the future but large enough that it could make an impact. Over time I expect its contributions to be sizable.

If you're looking for the causes HAMY.LABS supports, head over to the [HAMY.LABS HamForGood page](https://labs.hamy.xyz/hamforgood/).