---
title: "Affiliate Marketing Notice"
date: 2018-11-10T14:35:14-05:00
subtitle: ""
tags: []
---

This page is to comply with federal guidelines on affiliate marketing.

On occasion, I will provide links to services that include my affiliate marketing id which means that I may be compensated for referring you to that service. I do so in good faith and with limited knowledge. I will not refer you to sites that I know to be bad and will strive to only share sites I believe are useful.

By clicking these links, you help me provide more content and I like providing content, so I thank you for giving me the opportunity!