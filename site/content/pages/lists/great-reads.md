---
title: "Great Reads"
date: 2019-10-01T23:01:14+02:00
subtitle: ""
tags: []
comments: true
draft: true
---

This is an incomplete list of reads I really enjoyed and would highly recommend to others grouped by domain.

# Business

* [Build a side business](https://www.deepsouthventures.com/build-a-side-business/) - a great read on building side businesses online

# Technology

* [Refactoring - not on the backlog!](https://ronjeffries.com/xprog/articles/refactoring-not-on-the-backlog/) - an argument for refactoring as you go
* [On choosing boring technology](https://panelbear.com/blog/boring-tech/) - how boring technology empowers you to do exciting stuff
