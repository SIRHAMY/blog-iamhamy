---
title: "Entrepreneurs I Follow"
date: 2020-12-13T15:54:25Z
subtitle: ""
tags: []
comments: true
---

A short list of entrepreneurs I like keeping up with in alphabetical order of the names I remember them by.

* [Alex West](alexwest.co)
* [Levels.io](https://levels.io/)
* [Paul Graham](http://www.paulgraham.com/)
* [Tiny Projects Dev](https://tinyprojects.dev/)