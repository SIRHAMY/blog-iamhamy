---
title: "Project Creation Brainstorm Prompts"
date: 2019-12-01T20:11:50-05:00
subtitle: ""
tags: []
comments: true
---

Goal: Compile a list of prompts I can use to better ideate for projects in the future.

# usage

One way to use this may be to use a random number generator and generate a few numbers (say 5) then spend a few minutes on each corresponding prompt coming up with ideas.

# prompts

1. Look through work trello board.
2. What's a critical path / what's not working so well? Can we fix it? e.g. `igperf`
3. What's not used much / at all? Can we remove?
4. What processes feel weird / could be better?
5. What urls are open / up for auction? What could we do with that?
6. What search terms are frequent (Google, Amazon, Pinterest, etc.)? Could we do anything related to that?
7. What problem do you have right now?
8. What's not that painful for you but is for others?
    * In other words, what super power do you have that others don't that could uniquely position you to solve something?
9. If you were to lower your pain threshold, what would be painful? What would you need to change?
10. Ask Megna / others what they're trying to do. What's hard about it?
11. What hackathons are currently going on? 
12. What recent things have I come up with solutions to? Is that useful?
13. What do I want to do more of / less of / keep doing? How can I go about that?
14. How do I feel about x? How do you feel about x?
15. Would I rather x or y? Would you rather x or y?
16. What do I want to learn about?
17. Who's doing cool things? Why is it cool? How can I do that / what can I learn from that / how can I participate?
18. Who do I want to work with?
19. What is wrong with the world? 
20. What kind of change do you want to see in the world?
21. What problems are important to you?