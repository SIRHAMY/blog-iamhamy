---
title: "Growth Actions Brainstorm"
date: 2019-09-07T17:17:57-04:00
subtitle: ""
tags: []
comments: true
---

Goal: Compile a list of actions that can be used to grow followings / audience of projects.

# inspo

* Who would be interested in this kind of thing?
* Where can you find those people?

# list

* cross-post posts / sites / services
    * dev.to
    * hacker news
    * reddit - r/creativecoding, r/generative, r/proceduralgeneration
    * news / media outlets and collectives
    * behance
    * Pinterest
    * Quora
* share across own properties
* call out in reflections via calls-to-action and updates
* run ads
* post different content to each for variety for audience and hashtag diversity
* cross-collabs with people (artists / friends / whoever else)
    * megna
    * griffin
    * steve
    * plummer
    * ian
* create art feat. people / places / things
    * emrata
    * billie eilish
* build connections with people
* go to in-person events, host in-person events
* create art tv (w/ instahandle provided)
* submit to local (and maybe non-local) coops, orgs, publications, and shows
    * wallplay network
    * transcender
    * mayday
    * FB
    * NYC Link art #artonlink
    * check org sites:
        * rhizome - https://rhizome.org/community/
    * MTA arts
    * MoMA
    * Pioneer Works
    * Good luck drycleaners
* search online for calls for artists
    * this year
    * this area
* create screensaver with art and handle
* attend / host creative coding / art talks and workshops
    * algoraves
    * sfpc - https://sfpc.io/
    * groups - python, JAMStack
* do make sessions!
