---
title: "Story Questions"
date: 2019-08-11T04:23:10-04:00
subtitle: ""
tags: []
comments: true
---

Story questions (mixed with a good amount of honesty and vulnerability) are a good way to get to know people.

# questions

* What does your ideal day look like?
    * If you could retire tomorrow, what would you do?
* At what point, and surrounding context, were you most satisfied?