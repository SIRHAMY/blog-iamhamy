---
title: "2018 Q3 Goals"
date: 2018-08-02T23:28:08-04:00
subtitle: ""
tags: ["Goals"]
---

## Projects

* Grow 2 monetizable projects
* Release 2 art projects
* Start an academic course

## Adventure

* Explore NYC
* Make friends outside of the Tech bubble

## Self

* Practice stoicism, continue frugality
* Work hard towards long-term goals