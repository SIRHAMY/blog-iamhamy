* projects - I will start making money and leverage my network for opportunities to learn grow
    * make $500 on projects
    * make a collab with someone
* adventure - I will do cool things and grow my network
    * get > 100 hamventures subscribers
    * go on a roadtrip with meg
    * meet / hang out with 2 new people
* self - continue to live best life through habits
    * keep up with habits - meditation, exercise, diet
    * move to only one beef / lamb meal a week and 2 vegetarian days
* work - excell
    * stay on track (and get ahead) with projects
    * start and finish 1 new project
