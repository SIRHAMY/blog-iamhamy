Theme: Thrive - to make progress towards my dreams

I want to establish things that can really grow.

Bucket list
* learn skiing
* roadtrip with Meg
* Get art in a gallery
* make an official music video -> created and released
* Full 401k savings used and leveraging after tax goodness -> 50% overall savings rate
* Make real money off projects > $1000
* be on track for promotion at the end of the year -> EE and above
* make work friends / connections -> have 2 people from work I can be friends with

h1 priorities

* self
* work
* projects
* adventure