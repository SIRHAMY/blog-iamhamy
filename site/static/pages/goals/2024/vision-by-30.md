This is my vision for what I want to be / do by the time I'm 30.

* Be an adventurer - have lots of cool new weird experiences under my belt
* Be a hamgineer - have lots more knowledge and ability, projects, and connections to show for it
* Be a human - have strong, close connections and be a force for good in the world
* Be a jedi - be good to those connections, help where possible, be authentic - you are valid
* Be a powerhouse - be ripped, be thoughtful, be persistent in the pursuit of good, be financially independent

That is all.

updates:

* 2019.06.22 - created