---
title: "2019 Goals"
date: 2019-01-01T20:17:00-05:00
subtitle: ""
tags: []
comments: true
---

# Goals (OKRs)

* Be your best ham
    * expand thy mind
        * read 12 books - 4 fiction, 4 education, 4 spiritual
        * go to a tech conference
    * reduce distractions, focus on goals
        * no jerking off - unless blue balled night before
        * no tv (< 60 minutes each week unless with others (social))
        * Reddit < 1 hr per week
    * purify thy body
        * posture exercises 2x per week
        * get dope abs (nice 5 pack)
        * Shoulders and leg workouts
        * stop scratching
        * run 6 races
        * run 500 miles
        * stop doing drugs, limit to 2x per month
    * be a real-life human
        * do own taxes
        * learn heimlich maneuver and when to use
        * learn cpr and when/how to use
* Be your own ham
    * make ny a home
        * invest in artwork
        * invest in bedroom
        * invest in common areas
    * be you
        * you are valid, do what seems right and makes you happy - focus on real values and satisfaction
        * learn meditation
            * meditate 1000 minutes
        * improve duration and fun
            * 15 mins first time
            * 25 unique sessions w/ either unique agent or experience
            * 2 restraints
            * 2 a
            * 2 new positions
            * 2 toys
    * share you
        * write something every day
        * Reflect after contumption
            * so this should be at least 12 reflections given planned contumption
    * improve you
        * join a yoga gym
* Seize the world
    * write the next chapter
        * write 4 reflections and 12 release notes
        * increase pay by 50%
    * build dope shit
        * make $100 from side projects
        * 1000 views on blogs each month
        * publish 4 experiments
        * create a company
        * increased focus on work, make a hampact
    * heal the world
        * Support causes, people, orgs you believe in - x10
        * $1000 to charities
        * give back with tikkun olam tuesdays
            * publish 4 tikkun olam projects
    * experience the universe
        * go on 12 first dates
        * see 52 new things (1 / week) - don't be a little boi

# Habits

* every day
    * exercise
    * write
    * reflect/meditate
    * read
    * be better than I was the day before
    * no jacking
    * no tv

# Weekly things

* Sunday
    * Budget
    * Find things to do for next week
    * make/get food
    * spades night!
    * yoga
    * clean
* Monday
    * Lift - chest/tri
    * yoga
* Tuesday - tikkun olam
    * Lift - back/bi
    * Running Club
    * no meat eating
    * actively heal the world - seek out new ways to do so in your own way
* Wednesday
    * lift - legs
    * poker night!
* Thursday
    * Lift - shoulders
    * running club
* Friday
    * Hang with people
* Saturday
    * long run
    * do something new

* First saturday of every month - try doing everything "opposite"
* First day of month - release relase notes

# Quarterlies

## Q1

* kill it towards goals
    * release a tikkun olam
    * release a side project
    * run 250 miles
    * write 1 reflection, 3 release notes
    * read 4 books
    * write 15 pieces of content
    * 100 views / month on blogs
* work towards long-term goals
    * meditate every day
    * join a yoga gym/do some yoga
    * have some sex
        * x6
    * go on 4 dates
    * release coingine v1
    * see/do 15 new things

### January

* survey possibilities
    * start practicing
        * interview question every day
        * do 1 programming competition
    * do triplebyte interview
* Build some shit
    * Design and build coingine v0
    * Design and build tikkun olam project willigetcancer v0
    * -write a release note-
    * -write 6 pieces of content-
* Be solid
    * Read 2 books
    * -meditate every day-
    * -do some yoga, try out different places and styles-
        * -class pass-
        * -free trials-
    * run 100 miles - no
    * eat healthy and cheaply
* get out there
    * -go on 2 dates-
    * -have sex- 3
    * -see/do 6 new things-
* Work to a good stopping point
    * Data Setup v1
    * Build dope DISCs
        * 3 prs
        * 5 issues
        * 1 help sesh

#### 2018.01.01 - 2018.01.06

* every day
    * meditate
    * interview question
* look into class pass/yoga places
* -go on a date-
* -do 2 new things-
* -write 1 new piece/article-
* run 20 miles
* -buy/eat from TJs, work-
* -read every day-
* design coingine
* 2 PRs out for DataSetup

#### 2018.01.07 - 2018.01.14

* every day
    * -meditate-
    * interviewq - need to be better about this!
    * -read-
* -design willigetcancer-
* -design coingine-
* -see friends-
* -write 1 new piece/article-
* -run a little-
* do some at-home yoga - nah, but we did do a yoga
* 3 prs out for DISC - almost
* -do 1 new thing-
* run 15 miles - not quite

#### 2018.01.15 - 2018.01.22

* every day
    * -meditate-
    * interviewq (seriously!) - dude, c'mon
    * read
* start implementing willigetcancer - nah need to do
* start implementing coingine - nah need to do
* -go on a date-
* run 15 miles - 3
* -go to game/poker night(s)-
* 3 DISC prs - nah need to pick this up
* try 2 yoga styles/studios - nah but hurt my ankle
* -do 1 new thing-
* eat meals from grocery
* pick out and read a new book

#### 2018.01.21 - 2018.01.28

* every day
    * meditate
    * interviewq
    * read
* Push for creative releases
    * implement willigetcancer base (v0)
    * implement ndt reflection and start reading new book
    * start implementation of coingine
    * draft month release note
* Explore
    * -Do 1 new thing-
    * go to game / poker nights
    * go on a date
*- Learn yoga-
    * -go 3 times-
    * -begin understanding basics-
* Build dope DISCs
    * -3 prs-
    * -5 issues-
    * -1 help sesh-
* Be a solid human
    * -eat meals from grocery-
    * run 10 miles

#### 2018.01.28 - 2018.02.04

* every day
    * meditate
    * interviewq
    * read
* Get back on horse
    * 3 runs
    * 5 interview questions
    * go to game / poker night
    * -start reading a book-
    * -eat meals from grocery-
* Get ready to move in
    * Make plans to move in
    * move money to handle it
* Push for goals
    * 3 yoga sessions, 1 of which is done by self
    * -1 dance class-
    * write more coingine
    * go on a date
    * write february goals
    * -publish January release notes-
* Continue the DISCo
    * 3 prs
    * 5 issues
    * 1 help sesh

### February

* every day
    * meditate
    * interviewq
    * read
* game mode
    * hard practice
    * start interviewing -> pushed timelines back
* be you
    * Make Sessions
        * 2 make sessions with people
        * Release 1 project
        * willigetcancer -> V0 release
    * get some temporary tattoos to try them out
    * Keep dancing
        * 1 class / week
        * enlist others to come
    * Keep yogaing
        * try a new studio
        * 3 yogas / week (2 led, 1 self)
    * Run!
        * Try new running routes
        * Run 50 miles
    * Make NYC home
        * move into place
        * make room nice
        * wrap up all move out procedures
* seize the damn world
    * Build coingine V0 release
    * re-budget for new expenditures
        * entertainment
        * investments
    * -add all old blogposts to new blogs-
    * eat healthy and cheaply
        * grocery store lunches
        * no more $100+ meals
    * Explore
        * See/do 6 new things
        * go on 2 dates
    * Figure out how to do taxes
* DISCo each week
    * 3 prs
    * 5 issues
    * 1 help sesh

#### 2019.02.04 - 2019.02.10

* Habits
    * every day
        * -meditate-
        * interviewq
        * read
    * -DISCo each week-
        * 3 prs
        * 5 issues
        * 1 help sesh
* Build
    * -Settle into Hamcave-
        * Unpack all things
        * Set up furniture
        * Figure out what else is needed, order, and setup
    * Set Coingine foundations
        * create board to track tasks that need doing
        * Write wrapper for hashing algorithm
    * Mind / body
        * 1 dance class
        * 2 led yoga classes
        * 1 self-led yoga class
        * 3 runs

#### 2019.02.11 - 2019.02.18

* Habits
    * every day
        * meditate
        * interviewq
        * read
    * -DISCo each week-
        * 3 prs
        * 5 issues
        * 1 help sesh
* Build
    * Complete move in changes
        * Set up computer - not here yet
            * Load with files
            * Load with programs
        * Lifestyle
            * -Set up phone-
            * Figure out lights
            * Get wake up stuff
            * Set up cleaning
    * Make progress
        * Coingine
            * Create board for tasks
            * Write wrapper for hashing algorithm
        * Tikkun Olam
            * Write csv -> json parser
            * Write simple frontend
            * Begin iterating on the copy
        * Mind/Body
            * -Get back into yoga-
                * -Find a new studio-
                * Do one self-led sesh
            * Groceries!
        * Set up a make session
        * Write 1 piece

#### 2019.02.18 - 2019.02.25

* Habits
    * every day
        * meditate
        * interviewq
        * read
    * -DISCo each week-
        * -3 prs-
        * -5 issues-
        * -1 help sesh-
* Build
    * DISCo backend
        * -Build jq data model-
        * -Build jq polling infra-
        * Build a job vertical
        * -Get plan for DIEngine deploys going forward-
    * Heal the world - cancer site to presentable
        * -Get willigetcancer.xyz domain-
        * Write csv -> json parser
        * Write react frontend served with Rust server
    * Coingine
        * Build hashing algorithm infra
        * Setup planning boards
    * Contumption/Contuction
        * -Write 2 pieces-
        * Read/internalize pieces before participating in discussion
        * cross-post something to dev.to
    * Keep it healthy
        * Do 1 self-led yoga session
        * Run 3 times -> ankle not feeling great, going to rest
    * Get ready for interviews
        * -Build resume-
        * -Send resume around for reviews-
        * Keep reading CTCI
        * Keep practicing
    * Get settled
        * -Set up computer-
        * Continue fixing things as you see them
* Hang
    * -Go see people-
    * -go see a new thing-
    * do a make sesh / presentation thing

#### 2019.02.25 - 2019.03.04

* Habits
    * every day
        * meditate
        * interviewq
        * read
    * DISCo
        * 3 prs
        * 5 issues
        * 1 help sesh
        * Specific
            * Build DI Slave
            * Build DS runplan thing
            * Build job vertical
* Build
    * willigetcancer v0
        * website with options
        * base text
    * Coingine
        * write hashing algorithm
        * set up a cloud miner and mine
        * implement CoinHive on websites to see how it works
    * Health
        * Eat/cook groceries
        * Do yoga x3
        * Come up with training plan for ankle
        * get a library card
    * Interviews
        * 5 med/hard qs
        * put finishing touches on resume
        * send resume out to recruiters
    * Write release notes
* Adventure
    * Hold a make session
    * Do 1 new thing
    * hang with Megna
    * get back on dating apps

### March

* interview - get to boss status
    * create interview prep grid
    * implement data structures from scratch
    * BigO
    * make list of areas I'm struggling in, work to solidify
    * send out resume
    * mock interviews
    * continue practicing
* HamBuild
    * Build dope shit
        * 1 make session
        * phishme/phishy/phisthis v0 out
            * create launch page
            * create 2/3 landing pages for actual phish operations
            * look into email creation with that app thing
        * willigetcancer v0 out
            * launch wigc to friends/fam
            * launch to r/sideprojects
            * launch to Product Hunt
    * seize it I said!
        * 1 cross-post to dev.to
        * 1 cross-post to medium
        * 4 new posts
* solidify
    * get back into yoga, stretch self
        * [] 8 sessions
        * [] 2 self-led
    * less tv and j
        * increase duration to 10 mins
    * do taxes
* Experience
    * Go to 4 new things
        * [x] 2 art museums
        * [x] 1 new opening
        * [x] 1 new experience
* complete
    * disc to v1 release

#### 2019.03.04 - 2019.03.11

* interview
    * 5 interviewQs
    * 5 ctci chapters
    * create interview prep grid
* projects - hambuild
    * willigetcancer v0 out
    * plan a make session
    * publish 1 new post (for use in dev.to cross over)
* self - solidify
    * 1 yoga class
* adventure - experience
    * go to an art opening
* work - continue
    * complete job paradigm
    * get ready for IM week

#### 2019.03.12 - 2019.03.19

* interview
    * 5 interviewQs
    * 5 ctci chapters
    * send out resume to cos
* projects - build
    * create phishme landing page to gather emails / interest
    * do crosspost to dev.to
    * launch willigetcancer v0 to friends and fam
* self - solidify
    * 3 yogas
    * try to run
    * do taxes
* work
    * build out metric set logic
    * get ready for im week

#### 2019.03.18 - 2019.03.25

* self - heal
    * do yogas
    * see what's wrong with foot
    * do taxes
* work - be solid, start looking
    * disco
        * 3 tasks
        * complete slugger
    * interview
        * apply to jobs
        * 5 ctci chapters
        * 5 interviewQs
* projects - plant seeds
    * create phishme landing page
    * create coingine landing page
    * post my post to dev.to
    

## Q2

* work - begin anew, build a good foundation
    * create long-term goals
        * what i want to learn
        * what i want to build
        * how am i going to get there
* projects - build something worthwhile
    * release 1 tikkun olam - willigetcancer?
    * release 1 side project
    * release 1 art project - algorave?
* self - deepen practice, understanding of self, solidify base
    * continue meditation
    * learn advanced yoga moves
    * pick up running again
* adventure
    * travel
    * enjoy
    * see art

### April

* work
    * find jobs
    * wrap up work
    * use days, prepare for transfer
* projects
    * start LLC
    * 10 years, 10 dreams, 1 goal exercies
    * make sesh
* adventure
    * see megna
    * 2 dates
    * lab shul event
* self
    * learn inversions - head stand
    * find way to sustainably eat healthy
    * run 25 miles

#### 2019.04.01 - 2019.04.06

* work
    * interview
    * practice
    * complete 3 issues
* projects
    * plan make session
* adventure
    * look up lab shul events
    * hinge
* self
    * plan to learn / lead up to head stand
    * run 10 miles
    * hang slime sunday

#### 2019.04.15 - 2019.04.22

* self - get back on track
    * workout
    * eat healthy
    * look into headstand prep
* work - check out / close up
    * study hard for interviews - kill em
* projects
    * release zima blue
* adventure
    * house warming
    * go to events

#### 2019.04.29 - 2019.05.05

* work
    * continue to study a bit
    * set up mock interviews for next week
* projects
    * tidy up wigc
    * write reflection outline
* self
    * eat healthy
    * budget well
* adventure
    * come up with things to do while in Atlanta

### May

* work
    * land a new job
    * plan for coming months (work goals!)
* projects
    * build something cool with this time
        * finish zima-blue - web interface like: https://www.producthunt.com/posts/9-gans
        * -finish will i get cancer- - guesses location with ability to change it as well as some style clean up
        * consider working with rhys - art generator yo!!!
    * build something that could make money with this time
        * maybe art generator
        * maybe a real product like the browser miner!!!
        * build site cdn!!!
        * llc!
    * write reflection
* self
    * get back into yoga
        * zen boi
    * get back into a sustainable lifestyle
        * budget wise
        * health wise
    * consider ways to be better to the planet
        * eating, buying, consuming
* adventure
    * See the city
    * -be with meg-
    * see 5 museums

#### 2019.05.12 - 2019.05.19

* work
    * study up boi
* projects
    * create a hamventure email list
    * create barebones interview guide
    * do 1 project progress - plan zima blue frontend / various projects fixes/ideas
* self
    * eat healthy / cheap
    * yoga
    * exercise
* adventure
    * get back into habitual adventure

#### 2019.05.19 - 2019.05.26

* work
    * finish interviews
* projects
    * build mineforgood v0
* self
    * eat cheaply and healthily
    * learn to cook a dank omelette

#### 2019.05.27 - 2019.06.02

* work
    * study for interview
* projects
    * write beer for good
    * plan make session
* self
    * learn headstand
* adventure
    * good luck dry cleaners
    * get a plug
    * industry city
    * sva mfa ca thesis exhibition
    * icp
    * a/d/o

### June

* work
    * decide on job
    * start job
* projects
    * start llc
    * release tikkun olam - beer for good
    * release hamy labs product
    * write H1 reflection
    * do a make session
    * try rust?
* self
    * eat, live cheap
    * be healthy
    * create a 5 year plan
* adventure
    * plan big trips for year - mexico city, europe trip
    * go to meetups for art and tech

#### 2019.06.03 - 2019.06.09

* work
    * decide on a job
* projects
    * build beer for good v0
    * create hamventures email list
    * plan a make sesison
* adventure
    * fucking party
    * new museum
    * goodluck dry cleaners
* self
    * eat, live cheaply
    * work out

#### 2019.06.10 - 2019.06.16

* work
    * do projects
    * explore / enjoy time off
    * make job search post-mortem
* projects
    * soft release beer for good -> get payments working and start paying for climatechange
    * create hamventures email list
    * plan a make session
* adventure
    * megna
    * see some shit
* self
    * eat, live cheaply
    * workout

#### 2019.06.17 - 2019.06.23

* work
    * get ready for orientation!
* projects
    * soft release beer for good!
    * write reflection!
    * share job post post-mortem around
* adventure
    * do some casual shit
    * get swimsuit
    * plan for h&m party
* self
    * eat healthy
    * cancel humming puppy
    * exercise
    * create 5 year plan???

#### 2019.06.24 - 2019.06.30

* work
    * get this party started
* projects
    * release reflection

## Q3

* work - start off on the right foot, gain momentum
    * pick a great team that has the potential to get me to my goals
    * learn everything i can
    * start to make an impact
* projects - keep creating
    * create 3 projects (1 / month)
        * 1 tikkun olam project
        * create 2 art / personal projects
    * start regular make sessions
    * site views to 500 / month
* adventure - more reality
    * 6 (2 / month) plan and execute cool, weird, new adventures with people
    * pursue meg
        * 3 dates
* self - more foundation
    * create more granular, explicit budgets
    * increase meditation sessions to 15 mins
    * run 150 miles (~50 / month, 12.5 / week) - get up to 300 miles this year

In OKR form...
* Objective
    * key results...

* work - start off on the right foot, gain momentum
    * Join a team with:
        * potential for growth
        * exposure to scale
        * good people
        * cool tech
    * Learn as much as I can such that I'm productive and could teach others
        * systems/backend courses
        * web courses
        * tasks
    * make an impact
        * set goals with manager
        * then exceed
* projects - keep creating and learning
    * create 3 projects (1 / month)
        * 1 tikkun olam project
        * 2 art / personal projects
    * start make sessions
    * site views to 500 / month
* adventure - more reality
    * 6 (2 / month) plan and execute cool, weird, new adventures w people
    * pursue meg (3 dates)
* self - more foundation
    * create granular, explicit budgets
        * line items for all recurring expenses
        * folders for all types
        * focus on filling savings / buffer then go to investments
    * increase meditation sessions to 15 mins
    * Run 150 miles (~50 / month, 12.5 / week) -> 300 miles this year

### July

* work
    * pick a good team
    * -create a plan for L5 in a year-
* projects
    * create an art project
    * write some stuff x4
* adventure
    * -date meg-
    * get set up for Italy
    * make work friends
* self - get into a routine with work
    * lifting
    * eating healthy
    * sleeping, etc

#### 2019.07.02 - 2019.07.08

* work
    * ~Get 3 total tasks completed~
    * -Plan for trainings Systems and web-
* projects
    * -create HAMY sticker-
* adventure
    * -chill-
* self
    * lift / exercise

#### 2019.07.08 - 2019.07.15

* work
    * Narrow down teams I want to pursue to 5
    * Start taking systems course
    * Move forward with ad dogfooding
* projects
    * do data art project
* adventure
    * meg
    * do an fb bff event
    * plan for Italy
* self
    * lift / exercise
    * continue meditating @15 mins each day
    * clean apt

#### 2019.07.15 - 2019.07.21

* work
    * continue systems course
    * set up groundwork for dogfooding
    * pick up another task in a new area of code
* projects
    * find more groups that do digital art in NY
    * create a digital art
    * write x1
* adventure
    * meg
    * -plan italy-
    * do a bff event
* self
    * lift / exercise
    * continue meditation 15 mins / day
    * re-engage with friends

#### 2019.07.21 - 2019.07.26

* work
    * try out pub stories
    * close out tasks
    * do dogfooding
* projects
    * do art project
    * write something
* adventure
    * plan italy stuff
    * do bff event
    * hang w peeps this weekend, before going to CHI
* self
    * lift / exercise