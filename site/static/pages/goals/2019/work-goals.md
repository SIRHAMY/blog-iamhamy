# 2019 H2 - 2020 H2

By mid 2020, I want to 

* be ready for promotion
* understand high scale
* have made a sizable impact

For L5

* independent problem-finder
* independent problem-solver
* helper of others

From Blind:

```
From my experience, not prescriptive... E3 GE is a pice or cake, it only gets harder and harder from there:

E3 GE means you took a medium to large size feature and ran with it with little involvement from more senior engineers. If you’re good, it’s pretty easy to get imo, since MA for E3 is basically “do you have any idea how to write code” plus “are you not a huge asshole”.

E4 GE can mean you shipped a large feature from zero by yourself. You did the design doc, sold it to the team, and drove the implementation while maybe dragging an E3 along for the ride as well (for people axis). This is tougher to get, you need to be stepping into a sort of mini-TL type of role or ship a massively complex project, plus appropriate people axis (E3 mentoring/intern). XFN work helps here too, but it’s not as big of a factor.

E5 GE means TLing a large sub team (5+), driving the roadmap and aligning it with XFN stakeholders, actively shaping XFN engagements and delivering wins across a wide scope there, delegating work to subteam members that aligns with their growth paths, and tackling some of the really tricky technical problems yourself. E5 GE means you made your team and your XFN partners more effective/productive, you’ve shown the ability to lead across teams, you are rock-solid technically, and you delivered org wins (hiring, interviewing, etc).
```

All at team+ scale.

* 4 big hacks
* 150 work tasks

## 2019 H2

Goals:

* get ramped up on team
* finish medium projects

## 2019 Q3

* start off on the right foot
    * pick a good team
    * understand business purpose
    * start to make an impact

### July

* pick a good team

### August

* take on a big project

### September

* Ramp up on tools and processes, start doing tasks
* do a bunch of better engineering tasks: `fb codelove`
* video urlgen normalization closeout
* complete img urlgen normalization

### October

* web delivery path
