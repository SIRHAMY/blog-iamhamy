# 2019 Q4 vision

* Work
    * I will be on the path to promotion [2019Q4VISIONWORK1](WORK2019H2THEME2, ...)
        * How will it be measured?
            * Good feedback: bool
            * Significant impact: bool
            * Creation and execution of own workstream: bool
* Projects
    * I will connect with people through projects [2019Q4VISIONPROJECTS1](PROJECTS2019H2THEME1, ADVENTURE2019H2THEME1)
        * How will it be measured?
            * Collab with 3 people: int(>=3)
    * I will make an impact for good [2019Q4VISIONPROJECTS2](PROJECTS2019H2THEME3, PROJECTS2019H2THEME1)
        * How will it be measured?
            * Is the world better for the changes made this quarter?: bool
* Self
    * I will have a sustainable cycle that puts me at peak physical and mental state / performance [2019Q4VISIONSELF1](SELF2019H2THEME1, SELF2019H2THEME2, SELF2019H2THEME3)
        * How will it be measured?
            * Do I have a 6 pack: int(>=6)
            * Am I calm (not stressed): bool
            * Am I / do I feel healthy?: bool
    * I will have processes in place to cultivate and nurture sustainable, healthy relationships [2019Q4VISIONSELF2](ADVENTURE2019H2THEME1)
        * How will it be measured?
            * Are my connection edges healthy: bool
            * Do I have proven ways to mitigate unintended edge death: bool
* Adventure
    * I will connect with people through adventure [2019Q4VISIONADVENTURE1](ADVENTURE2019H2THEME1, ADVENTURE2019H2THEME2)
        * How will it be measured?
            * Adventure at least 6 times: int(>=6)
            * Connect with 3 new people: int(>=3) 