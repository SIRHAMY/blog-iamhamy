# 2019 Q3 OKRs

* work
    * Build a strong foundation from which to drive progress and impact [WORK2019Q3GOAL1](WORK2019H2THEME2)
        * How will it be measured?
            * Ability to be able to explain what team does to non-employee: bool
            * Ability to explain how given improvements / projects might effect the overall team's systems and measure / choose between them for highest opportunity: bool
            * Ability to take on non-trivial tasks (medium features): bool
        * How will it be accomplished?
            * Research product history, how it's used, and roadmap challenges
            * Pick off sizable tasks (ramping up from small to large) and ask a lot of questions
            * Use the product on the client side
    * Work hard, but not too hard - set expectations, establish habits, and gain efficiency [WORK2019Q3GOAL2](WORK2019H2THEME1, SELF2019H2THEME1, SELF2019H2THEME3)
        * How will it be measured?
            * Work < 50 hours per week: int(<50 hrs)
            * Do I want to go to work on Monday: bool
        * How will it be accomplished?
            * Experiment with hours and routines to find what works best (food, caffeine, exercise, etc.)
            * Hang out with team, get to know them, gain empathy for their interests and effort
* projects
    * Implement and iterate on systems to lower friction to project entry and increase motivation [PROJECTS2019Q3GOAL1](PROJECTS2019H2THEME1, PROJECTS2019H2THEME2)
        * How will it be measured?
            * Am I motivated to work on my projects tonight?: bool
            * 1.5 months worth of projects finished (cause starting in middle of quarter): int(6 small opps, 1.5 med opps)
        * How will it be accomplished?
            * Utilize the new okr and project pillars systems I've created
            * Build projects
            * Build project-helpers, like infra that will help me go faster
* adventure
    * Make adventure finding easier wrt both finding all the good shit and distributing that shit [ADVENTURE2019Q3GOAL1](ADVENTURE2019H2THEME2, ADVENTURE2019H2THEME1)
        * How will it be measured?
            * Adventure finding and distribution takes < 1 hour each week: int(< 1 hour)
            * Adventures are always available on 2 platforms in addition to blog: int(>= 2 additional platforms)
        * How will it be accomplished?
            * Build out a good email template
            * Build a good adventure-finding checklist
            * look into automation of distribution to platforms
* self
    * Find ways to eat and really live sustainably both wrt my personal health / image goals and planetary goals [SELF2019Q3GOAL1](SELF2019H2THEME3, SELF2019H2THEME1)
        * How will it be measured?
            * Reduce projected carbon footprint by at least 10%: int(>10% reduction in estimated carbon footprint)
            * Gain 0 lbs: int(0 lbs change)
            * Get a cut six pack: bool
        * How will it be accomplished?
            * Research ways to reduce carbon footprint (things like using re-usable containers, paying carbon tax, and meatless mondays could help)
            * Exercise by default (do full workouts when I can, do some sort of exercise each day, consider biking)
            * Eat regular portions of food despite fb being so damn enticing