# 2019 H2 Half THEMEs

Theme: Sustainability

In H2, I will be focusing on sustainability. To me, sustainability means systems that optimize for your values but that are robust to the factors of life. In other words, it's the attribute of getting you what you want indefinitely.

* work
    * WORK2019H2THEME1 - sustainable effort
    * WORK2019H2THEME2 - sustainable results
* projects
    * PROJECTS2019H2THEME1 - sustainable effort output
    * PROJECTS2019H2THEME2 - sustainable growth / results in projects
    * PROJECTS2019H2THEME3 - positive sustainable effects on the world
* adventure
    * ADVENTURE2019H2THEME1 - sustainable community of connections
    * ADVENTURE2019H2THEME2 - sustainable adventures
* self
    * SELF2019H2THEME1 - sustainable eating
    * SELF2019H2THEME2 - sustainable health
    * SELF2019H2THEME3 - sustainable fitness