# what

FB is having a zine fest on August 15. That's in like 1 month. I want to enter.

# what am I making

I want to make a zine. It's gonna be an exploration of technology's reflection of ourselves

# how am I doing this

I'm going to give prompts to people and they are going to use phone's autocorrect to send me the full text that they created with the prompt.

So an example I made was

Prompt: I want somebody that loves me like kanye {autoinsert}

Output: I want somebody that loves me like kanye loves you.
- Hamilton, Android 9 on Pixel 3

# prompts

What makes a good prompt? On Google, it's something like 'does x...' as we can mine people's searches for what they  care about. Here it's different, cause we actually want to mine what a phone knows about a person, so it should maybe be more personal.

## I want somebody that loves me like kanye {}

* I want somebody that loves me like kanye loves you.
    * Hamilton, Android 9 on Pixel 3

## Make America {}

## Kanye loves {}

# potential prompts

## Who's that pokemon? {}

## Shake it like a polaroid {}