WIP: Notes will go here

# Development

## Commands

Running site to get to hugo commands:
```
docker-compose run --service-ports hugo_dev_env -rm
cd site
hugo serve --bind 0.0.0.0
```
* Run with drafts: `hugo serve --bind 0.0.0.0 -D`

# Content

## Tags

* journals - For weekly journals
* reflections - For monthly+ reflections
* systems - for systems